Rails.application.routes.draw do

  get '/' => 'home#index' # this is only used for devise > after change password > redirect

  # get 'logout/index'
  resources :logout, only: [:index]

  devise_for :users, skip: [:registrations]
  as :user do
    get 'users/edit' => 'devise/registrations#edit', as: 'edit_user_registration' # change password without registration
    patch 'users' => 'devise/registrations#update', as: 'user_registration' # change password without registration
  end

  resources :home, only: [:index, :show]

  namespace :hq do
    resources :myc_programs, only: [:index, :index_data] do
      collection do
        post 'index_data'
      end
    end

    resources :users do
      collection do
        post 'index_data'
      end
      member do
        get 'invite_confirm'
        patch 'invite'
        get 'edit_status'
        get 'set_profile' # after login
      end

      resources :teachers, only: [:new, :create, :edit, :update]
      resources :studio_users, only: [:index, :new, :create] do
        collection do
          post 'index_data'
        end
      end
    end

    resources :teachers, only: [:edit, :update]

    resources :studio_users, only: [:destroy] do
      member do
        get 'delete'
      end
    end

    resources :studios do
      collection do
        post 'data'
      end
    end

    resources :product_codes, except: [:delete, :show] do
      collection do
        post 'index_data'
      end
      member do
        post 'pack_data_by_code'
        get 'select_pack'
        post 'add_pack'
        get 'remove_pack_confirm/:product_pack_id', action: 'remove_pack_confirm'
        delete 'remove_pack/:product_pack_id', action: 'remove_pack'

        post 'item_data_by_code'
        get 'select_item'
        post 'add_item'
        get 'remove_item_confirm/:product_item_id', action: 'remove_item_confirm'
        delete 'remove_item/:product_item_id', action: 'remove_item'
        get 'show_pack_items/:product_pack_id', action: 'show_pack_items'
      end
    end

    resources :product_packs, except: [:delete, :show] do
      collection do
        post 'index_data'
      end
      member do
        post 'item_data_by_pack'
        get 'select_item'
        post 'add_item'
        get 'remove_item_confirm/:product_item_id', action: 'remove_item_confirm'
        delete 'remove_item/:product_item_id', action: 'remove_item'
      end

      resources :product_items, only: [:index]
    end

    resources :product_items, only: [:index, :new, :create, :edit, :update] do
      collection do
        post 'index_data'
      end
    end

    resources :student_reports, only: [:index] do
      collection do
        post 'index_data'
        post 'index_total_data'
        post 'generate'
      end
    end
  end

  namespace :myc do
    resources :calendar, only: [:index]

    resources :teachers, only: [:index] do
      collection do
        post 'index_data'
      end
    end

    resources :students, except: [:delete, :show] do
      collection do
        post 'index_data'
      end
    end

    resources :myc_classes, only: [:index, :new, :create, :show] do
      collection do
        post 'index_data'
        post 'new_students_data'
        post 'old_students_data'
        post 'in_class_students_data'
        post 'student_attendance_data'
      end

      member do
        get 'choose_students_to_add'
        post 'add_students'

        get 'choose_student_to_remove'
        post 'check_student_attendance'
        post 'remove_student'

        get 'dissolve_confirm'
        patch 'dissolve'

        get 'undo_dissolve_confirm'
        patch 'undo_dissolve'
      end
    end

    resources :lessons do
      member do
        get 'choose_teacher_to_change'
        post 'change_teacher'

        get 'set_data_for_lesson_make_up/:make_up_type', action: 'set_data_for_lesson_make_up'
        post 'lesson_make_up'

        get 'choose_students_to_mark_attendance'
        post 'mark_attendance'

        get 'unmark_attendance_confirm'
        patch 'unmark_attendance'
      end
    end

    resources :families, except: [:delete] do
      collection do
        post 'index_data'
      end
    end

    resources :holidays, except: [:delete]  do
      collection do
        post 'index_data'
      end
    end

    resources :invoices, except: [:delete]  do
      resources :invoice_items, only: [:destroy] do
        collection do
          post 'index_data'
        end
        member do
          get 'delete', action: 'delete'
        end
      end

      collection do
        post 'index_data'
        post 'invoice_new_student_data'
        post 'invoice_existing_student_data'

        get 'choose_student', action: 'choose_student'
        post 'add_new_student', action: 'add_new_student'
        post 'add_existing_student', action: 'add_existing_student'

        get 'fee_item/program', action: 'program'
        get 'fee_item/discount', action: 'discount'
        get 'fee_item/lesson', action: 'lesson'
        post 'material_data', action: 'material_data'

      end
      member do
        get 'fee_item/program', action: 'program'
        get 'fee_item/discount', action: 'discount'
        get 'fee_item/lesson', action: 'lesson'
        get 'fee_item/material', action: 'material'
        get 'fee_item/other', action: 'other'

        post 'program_fee_item_data', action: 'program_fee_item_data'
        post 'discount_fee_item_data', action: 'discount_fee_item_data'
        post 'lesson_fee_item_data', action: 'lesson_fee_item_data'
        post 'add_program_fee_item', action: 'add_program_fee_item'
        post 'add_discount_fee_item', action: 'add_discount_fee_item'
        post 'add_lesson_fee_item', action: 'add_lesson_fee_item'
        post 'add_material_fee_item', action: 'add_material_fee_item'
        post 'add_other_fee_item', action: 'add_other_fee_item'
      end
    end

    resources :teacher_commissions, only: [:index, :show, :edit, :update] do
      collection do
        post 'index_data'
        post 'show_data'
        get 'generate_confirm'
        post 'generate'
      end
      resources :teacher_commission_items do
        collection do
          post 'index_data'
        end
        member do
          patch 'update_percentage'
          get 'edit_percentage'
          get 'edit_remarks'
        end
      end

      resources :teacher_commission_item_additionals, only: [:new, :create] do
        collection do
          post 'index_data'
        end
      end

      resources :teacher_commission_others, only: [:new, :create] do
        collection do
          post 'index_data'
        end
      end
    end

    resources :teacher_commission_item_additionals, only: [:edit, :update, :destroy] do
      member do
        get 'delete'
      end
    end

    resources :teacher_commission_others, only: [:edit, :update, :destroy] do
      member do
        get 'delete'
      end
    end
  end


end

# Date
Date::DATE_FORMATS[:default] = "%d/%m/%y"

# Time
Time::DATE_FORMATS[:default] = "%d/%m/%y %H:%M:%S"
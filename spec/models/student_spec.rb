require 'rails_helper'

describe Student do
  it 'has a valid factory' do
    expect(build(:student)).to be_valid
  end

  it 'is valid with a program_id, family_id, studio_id, nric_name, first_name, sur_name, gender, date_of_birth, referred_by, enquiry_date, status' do
    student = Student.new(
         myc_program_id: '1',
         family_id:  '1',
         studio_id:  '1',
         nric_name: 'Jonson',
         first_name: 'jon',
         sur_name: 'son',
         gender: 'M',
         date_of_birth: '2010-05-11',
         referred_by: 'Current_Parent',
         enquiry_date: '2016-05-10',
         status: 'new_class_registered'
    )
    expect(student).to be_valid
  end

  it 'is invalid without an program_id' do
    student = build(:student, myc_program_id: nil)
    student.valid?
    expect(student.errors[:myc_program_id].size).to eq 1
  end

  it 'is invalid without an family_id' do
    student = build(:student, family_id: nil)
    student.valid?
    expect(student.errors[:family_id].size).to eq 1
  end

  it 'is invalid without an studio_id' do
    student = build(:student, studio_id: nil)
    student.valid?
    expect(student.errors[:studio_id].size).to eq 1
  end

  it 'is invalid without an nric_name' do
    student = build(:student, nric_name: nil)
    student.valid?
    expect(student.errors[:nric_name].size).to eq 1
  end

  it 'is invalid without an first_name' do
    student = build(:student, first_name: nil)
    student.valid?
    expect(student.errors[:first_name].size).to eq 1
  end

  it 'is invalid without an sur_name' do
    student = build(:student, sur_name: nil)
    student.valid?
    expect(student.errors[:sur_name].size).to eq 1
  end

  it 'is invalid without an gender' do
    student = build(:student, gender: nil)
    student.valid?
    expect(student.errors[:gender].size).to eq 1
  end

  it 'is invalid without an date_of_birth' do
    student = build(:student, date_of_birth: nil)
    student.valid?
    expect(student.errors[:date_of_birth].size).to eq 1
  end

  it 'is invalid without an referred_by' do
    student = build(:student, referred_by: nil)
    student.valid?
    expect(student.errors[:referred_by].size).to eq 1
  end

  it 'is invalid without an enquiry_date' do
    student = build(:student, enquiry_date: nil)
    student.valid?
    expect(student.errors[:enquiry_date].size).to eq 1
  end

  it 'is invalid without an status' do
    student = build(:student, status: nil)
    student.valid?
    expect(student.errors[:status].size).to eq 1
  end

  it 'is invalid with duplicate both nric_name and date_of_birth' do
    create(:student, nric_name: 'Jame', date_of_birth: '2010-05-10')
    student = build(:student, nric_name: 'Jame', date_of_birth: '2010-05-10')
    student.valid?
    expect(student.errors.size).to eq 1
  end

  it 'is valid with duplicate nric_name but not duplicate date_of_birth' do
    create(:student, nric_name: 'Jame', date_of_birth: '2010-05-10')
    student = build(:student, nric_name: 'Jame', date_of_birth: '2010-05-11')
    expect(student).to be_valid
  end

  it 'is valid with duplicate date_of_birth but not duplicate nric_name' do
    create(:student, nric_name: 'Jame', date_of_birth: '2010-05-10')
    student = build(:student, nric_name: 'Cindy', date_of_birth: '2010-05-10')
    expect(student).to be_valid
  end

  it 'return humanize status of student' do
    humanize_status = Student.humanize_statuses
    expect(humanize_status[0]).to eq ['New class registered', 'new_class_registered']
    expect(humanize_status[1]).to eq ['Current', 'current']
    expect(humanize_status[2]).to eq ['Suspended', 'suspended']
    expect(humanize_status[3]).to eq ['Terminated', 'terminated']
    expect(humanize_status[4]).to eq ['Deleted', 'deleted']

  end

end



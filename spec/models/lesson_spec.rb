require 'rails_helper'

describe Lesson do
  it 'has a valid factory' do
    expect(build(:lesson)).to be_valid
  end

  it 'is valid with lesson_no, start_at, reason_for_cancel, status, myc_class_id, teacher_id' do
    lesson = Lesson.new(
      lesson_no: 1,
      start_at: Time.now,
      reason_for_cancel: 'test',
      status: 'not_marked',
      myc_class_id: 1,
      teacher_id: 1
    )
    expect(lesson).to be_valid
  end

  it 'is invalid without lesson_no' do
    lesson = build(:lesson, lesson_no: nil)
    lesson.valid?
    expect(lesson.errors[:lesson_no].size).to be > 0
  end

  it 'is invalid without start_at' do
    lesson = build(:lesson, start_at: nil)
    lesson.valid?
    expect(lesson.errors[:start_at].size).to be > 0
  end

  it 'is invalid without status' do
    lesson = build(:lesson, status:nil)
    lesson.valid?
    expect(lesson.errors[:status].size).to be > 0
  end

  it 'is invalid without myc_class_id' do
    lesson = build(:lesson, myc_class_id:nil)
    lesson.valid?
    expect(lesson.errors[:myc_class_id].size).to be > 0
  end

  it 'is invalid without teacher_id' do
    lesson = build(:lesson, teacher_id:nil)
    lesson.valid?
    expect(lesson.errors[:teacher_id].size).to be > 0
  end
end
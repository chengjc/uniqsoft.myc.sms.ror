require 'rails_helper'

RSpec.describe StudentReport, type: :model do
  it 'has a valid factory' do
    expect(build(:student_report)).to be_valid
  end

  it 'is valid with all correct attributes' do
    report = StudentReport.new(
        studio_id: 1,
        teacher_id: 1,
        report_date: Date.today,
        student_count_sr1: 1,
        student_count_sr2: 1,
        student_count_sr3: 1,
        student_count_ss1: 1,
        student_count_ss2: 1,
        student_count_sb1: 1,
        student_count_sb2: 1,
        student_count_sb3: 1,
        student_count_mb1: 1,
        student_count_mb2: 1,
        student_count_mb3: 1,
        total: 1,
    )
    expect(report).to be_valid
  end

  it 'is invalid without a studio_id' do
    report = build(:student_report, studio_id: nil)
    report.valid?
    expect(report.errors[:studio_id].size).to eq(1)
  end

  it 'is invalid without a teacher_id' do
    report = build(:student_report, teacher_id: nil)
    report.valid?
    expect(report.errors[:teacher_id].size).to eq(1)
  end

  it 'is invalid without a report_date' do
    report = build(:student_report, report_date: nil)
    report.valid?
    expect(report.errors[:report_date].size).to eq(1)
  end

  it 'is invalid without a student_count_sr1' do
    report = build(:student_report, student_count_sr1: nil)
    report.valid?
    expect(report.errors[:student_count_sr1].size).to eq(1)
  end

  it 'is invalid without a student_count_sr2' do
    report = build(:student_report, student_count_sr2: nil)
    report.valid?
    expect(report.errors[:student_count_sr2].size).to eq(1)
  end

  it 'is invalid without a student_count_sr3' do
    report = build(:student_report, student_count_sr3: nil)
    report.valid?
    expect(report.errors[:student_count_sr3].size).to eq(1)
  end

  it 'is invalid without a student_count_ss1' do
    report = build(:student_report, student_count_ss1: nil)
    report.valid?
    expect(report.errors[:student_count_ss1].size).to eq(1)
  end

  it 'is invalid without a student_count_ss2' do
    report = build(:student_report, student_count_ss2: nil)
    report.valid?
    expect(report.errors[:student_count_ss2].size).to eq(1)
  end

  it 'is invalid without a student_count_sb1' do
    report = build(:student_report, student_count_sb1: nil)
    report.valid?
    expect(report.errors[:student_count_sb1].size).to eq(1)
  end

  it 'is invalid without a student_count_sb2' do
    report = build(:student_report, student_count_sb2: nil)
    report.valid?
    expect(report.errors[:student_count_sb2].size).to eq(1)
  end

  it 'is invalid without a student_count_sb3' do
    report = build(:student_report, student_count_sb3: nil)
    report.valid?
    expect(report.errors[:student_count_sb3].size).to eq(1)
  end

  it 'is invalid without a student_count_mb1' do
    report = build(:student_report, student_count_mb1: nil)
    report.valid?
    expect(report.errors[:student_count_mb1].size).to eq(1)
  end

  it 'is invalid without a student_count_mb2' do
    report = build(:student_report, student_count_mb2: nil)
    report.valid?
    expect(report.errors[:student_count_mb2].size).to eq(1)
  end

  it 'is invalid without a student_count_mb3' do
    report = build(:student_report, student_count_mb3: nil)
    report.valid?
    expect(report.errors[:student_count_mb3].size).to eq(1)
  end

  it 'is invalid with a duplicate studio_id, teacher_id and report_date' do
    studio_id = 1
    teacher_id = 1
    report_date = Date.today
    create(:student_report, studio_id: studio_id, teacher_id: teacher_id, report_date: report_date)
    report = build(:student_report, studio_id: studio_id, teacher_id: teacher_id, report_date: report_date)
    report.valid?
    expect(report.errors[:report_date].size).to eq(1)
  end

  # it 'generates report' do
  #   program = create(:myc_program)
  #   teacher = create(:teacher)
  #   student = create(:student)
  #   myc_class = create(:myc_class)
  #   myc_class_student = create(:myc_class_student)
  #   lesson = create(:lesson)
  #
  #
  #   expect(StudentReport.where(
  #       studio_id: 1,
  #       teacher_id: 2,
  #       report_date: 2,
  #       student_count_sr1: 2,
  #       student_count_sr2: 2,
  #       student_count_sr3: 2,
  #       student_count_ss1: 2,
  #       student_count_ss2: 2,
  #       student_count_ss3: 2,
  #   )).to exist
  # end
end

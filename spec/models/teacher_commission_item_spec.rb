require 'rails_helper'

describe TeacherCommissionItem do
  it 'has a valid factory' do
    expect(build(:teacher_commission_item)).to be_valid
  end

  it 'is valid with program, day ,time, student_nric_name, lesson_date, term_fee, lesson_fee,no_of_lesson ,total_fee, percentage, commission ,remarks ' do
    item = TeacherCommissionItem.new(
        program: 'sunshine 1',
        day: 'Mon',
        time: '00:00',
        student_nric_name: 'john',
        lesson_date: '01/01/16',
        term_fee: '10.00',
        lesson_fee: '11.00',
        no_of_lesson: '1',
        total_fee: '12.00',
        percentage: '1',
        commission: '1',
        remarks: 'remarks',
    )
    expect(item).to be_valid
  end

  it 'is invalid without a program' do
    item = build(:teacher_commission_item, program: nil)
    item.valid?
    expect(item.errors[:program].size).to eq(1)
  end

  it 'is invalid without a day' do
    item = build(:teacher_commission_item, day: nil)
    item.valid?
    expect(item.errors[:day].size).to eq(1)
  end

  it 'is invalid without a time' do
    item = build(:teacher_commission_item, time: nil)
    item.valid?
    expect(item.errors[:time].size).to eq(1)
  end

  it 'is invalid without a student_nric_name' do
    item = build(:teacher_commission_item, student_nric_name: nil)
    item.valid?
    expect(item.errors[:student_nric_name].size).to eq(1)
  end

  it 'is invalid without a lesson_date' do
    item = build(:teacher_commission_item, lesson_date: nil)
    item.valid?
    expect(item.errors[:lesson_date].size).to eq(1)
  end

  it 'is invalid without a term_fee' do
    item = build(:teacher_commission_item, term_fee: nil)
    item.valid?
    expect(item.errors[:term_fee].size).to be > 0
  end

  it 'is invalid without a no_of_lesson' do
    item = build(:teacher_commission_item, no_of_lesson: nil)
    item.valid?
    expect(item.errors[:no_of_lesson].size).to be > 0
  end

  it 'is invalid without a total_fee' do
    item = build(:teacher_commission_item, total_fee: nil)
    item.valid?
    expect(item.errors[:total_fee].size).to be > 0
  end

  it 'is invalid without a percentage' do
    item = build(:teacher_commission_item, percentage: nil)
    item.valid?
    expect(item.errors[:percentage].size).to be > 0
  end

  it 'is invalid without a commission' do
    item = build(:teacher_commission_item, commission: nil)
    item.valid?
    expect(item.errors[:commission].size).to be > 0
  end

  it 'is invalid if percentage is not a number' do
    params = { percentage: 'a'}
    instance = TeacherCommissionItem.validate_percentage(params)
    expect(instance.errors[:percentage].size).to eq(1)
  end
end
require 'rails_helper'

describe ProductCode do
  it 'has a valid factory' do
    expect(build(:product_code)).to be_valid
  end

  it 'is valid with a code, price, remarks, status' do
    code = ProductCode.new(
        code: 'code name',
        price: 0.01,
        remarks: 'remarks',
        status: 'active'
    )
    expect(code).to be_valid
  end

  it 'is invalid without a code' do
    code = build(:product_code, code: nil)
    code.valid?
    expect(code.errors[:code].size).to eq(1)
  end

  it 'is invalid without a status' do
    code = build(:product_code, status: nil)
    code.valid?
    expect(code.errors[:status].size).to eq(1)
  end

  it 'is invalid with a duplicate code and status active' do
    name = 'code'
    status = 'active'
    create(:product_code, code: name, status: status)
    code = build(:product_code, code: name, status: status)
    code.valid?
    expect(code.errors[:code].size).to eq(1)
  end
end
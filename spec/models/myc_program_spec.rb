require 'rails_helper'

describe MycProgram do
  it 'has a valid factory' do
    expect(build(:myc_program)).to be_valid
  end

  it 'is valid with a code, price, remarks, status' do
    program = MycProgram.new(
        program_name: 'name',
        program_code: 'code',
        program_order: 1,
        status: 'active'
    )
    expect(program).to be_valid
  end

  it 'is invalid without a program_name' do
    program = build(:myc_program, program_name: nil)
    program.valid?
    expect(program.errors[:program_name].size).to eq(1)
  end

  it 'is invalid without a program_code' do
    program = build(:myc_program, program_code: nil)
    program.valid?
    expect(program.errors[:program_code].size).to eq(1)
  end

  it 'is invalid without a program_order' do
    program = build(:myc_program, program_order: nil)
    program.valid?
    expect(program.errors[:program_order].size).to be > 0
  end

  it 'is invalid without a status' do
    program = build(:myc_program, status: nil)
    program.valid?
    expect(program.errors[:status].size).to eq(1)
  end

  it 'is invalid with a duplicate program_code and status active' do
    name = 'program'
    status = 'active'
    create(:myc_program, program_code: name, status: status)
    program = build(:myc_program, program_code: name, status: status)
    program.valid?
    expect(program.errors[:program_code].size).to eq(1)
  end
end
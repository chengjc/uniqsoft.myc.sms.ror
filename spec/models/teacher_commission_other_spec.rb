require 'rails_helper'

describe TeacherCommissionOther do
  it 'has a valid factory' do
    expect(build(:teacher_commission_other)).to be_valid
  end

  it 'is valid with a item, amount, remarks' do
    item = TeacherCommissionOther.new(
        item: 'other fee',
        amount: '1.11',
        remarks: 'remarks',
    )
    expect(item).to be_valid
  end

  it 'is invalid without a item' do
    item = build(:teacher_commission_other, item: nil)
    item.valid?
    expect(item.errors[:item].size).to eq(1)
  end

  it 'is invalid without a amount' do
    item = build(:teacher_commission_other, amount: nil)
    item.valid?
    expect(item.errors[:amount].size).to be > 0
  end
end
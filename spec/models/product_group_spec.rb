require 'rails_helper'

describe ProductGroup do
  it 'has a valid factory' do
    expect(build(:product_group)).to be_valid
  end

  it 'is valid with a description, display_order, status' do
    group = ProductGroup.new(
        description: 'description',
        display_order: 1,
        status: 'active'
    )
    expect(group).to be_valid
  end

  it 'is invalid without a description' do
    group = build(:product_group, description: nil)
    group.valid?
    expect(group.errors[:description].size).to eq(1)
  end

  it 'is invalid without a status' do
    group = build(:product_group, status: nil)
    group.valid?
    expect(group.errors[:status].size).to eq(1)
  end

  it 'is invalid with a duplicate description and status active' do
    description = 'description'
    status = 'active'
    create(:product_group, description: description, status: status)
    group = build(:product_group, description: description, status: status)
    group.valid?
    expect(group.errors[:description].size).to eq(1)
  end
end
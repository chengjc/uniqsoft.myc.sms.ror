require 'rails_helper'

describe InvoiceItem do
  it 'has a valid factory' do
    expect(build(:invoice_item)).to be_valid
  end

  it 'is valid with a invoice_id, item_name, quantity, price' do
    invoice_item = InvoiceItem.new(
        invoice_id: '1',
        item_name: 'Lesson Fee',
        quantity: 1,
        price: 100.00,
    )
    expect(invoice_item).to be_valid
  end

  it 'is invalid without an invoice_id' do
    invoice_item = build(:invoice_item, invoice_id: nil)
    invoice_item.valid?
    expect(invoice_item.errors[:invoice_id].size).to eq 1
  end

  it 'is invalid without an item_name' do
    invoice_item = build(:invoice_item, item_name: nil)
    invoice_item.valid?
    expect(invoice_item.errors[:item_name].size).to eq 1
  end

  it 'is invalid without an quantity' do
    invoice_item = build(:invoice_item, quantity: nil)
    invoice_item.valid?
    expect(invoice_item.errors[:quantity].size).to be >0
  end

  it 'is invalid without an price' do
    invoice_item = build(:invoice_item, price: nil)
    invoice_item.valid?
    expect(invoice_item.errors[:price].size).to be >0
  end


end
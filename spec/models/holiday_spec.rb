require 'rails_helper'

describe Holiday do
  it 'has a valid factory' do
    expect(build(:holiday)).to be_valid
  end

  it 'is valid with a studio_id, holiday_date, holiday_name' do
    holiday = Holiday.new(
      studio_id: '1',
      holiday_date: Date.today-2,
      holiday_name: 'New Year'
    )
    expect(holiday).to be_valid
  end

  it 'is invalid without an studio_id' do
    holiday = build(:holiday, studio_id: nil)
    holiday.valid?
    expect(holiday.errors[:studio_id].size).to eq 1
  end

  it 'is invalid without an holiday_date' do
    holiday = build(:holiday, holiday_date: nil)
    holiday.valid?
    expect(holiday.errors[:holiday_date].size).to eq 1
  end

  it 'is invalid without an holiday_name' do
    holiday = build(:holiday, holiday_name: nil)
    holiday.valid?
    expect(holiday.errors[:holiday_name].size).to eq 1
  end

  it 'is invalid with duplicate holiday_date and studio_id' do
    create(:holiday, studio_id: '1', holiday_date: Date.today-1)
    new_holiday = build(:new_holiday, studio_id: '1', holiday_date: Date.today-1)
    new_holiday.valid?
    expect(new_holiday.errors[:holiday_date].size).to eq 1
  end

  it 'return list of maximum and minimum year' do
    create(:holiday, holiday_date: Date.today-365)
    create(:holiday, holiday_date: Date.today)
    year1 = Date.today.year-1.to_i
    year2 = Date.today.year.to_i

    year_for_filter = Holiday.year_for_filter
    expect(year_for_filter[0]).to eq year1
    expect(year_for_filter[1]).to eq year2
  end

end

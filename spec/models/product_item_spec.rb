require 'rails_helper'

describe ProductItem do
  it 'has a valid factory' do
    expect(build(:product_item)).to be_valid
  end

  it 'is valid with a description, quantity_on_hand ,quantity_trigger_alert, cost, status' do
    item = ProductItem.new(
        description: 'item name',
        quantity_on_hand: 1,
        quantity_trigger_alert: 1,
        cost: 1.11,
        status: 'active'
    )
    expect(item).to be_valid
  end

  it 'is invalid without an description' do
    item = build(:product_item, description: nil)
    item.valid?
    expect(item.errors[:description].size).to eq(1)
  end

  it 'is invalid without an quantity_on_hand' do
    item = build(:product_item, quantity_on_hand: nil)
    item.valid?
    expect(item.errors[:quantity_on_hand].size).to be > 0
  end

  it 'is invalid without an quantity_trigger_alert' do
    item = build(:product_item, quantity_trigger_alert: nil)
    item.valid?
    expect(item.errors[:quantity_trigger_alert].size).to be > 0
  end

  it 'is invalid without an cost' do
    item = build(:product_item, cost: nil)
    item.valid?
    expect(item.errors[:cost].size).to be > 0
  end

  it 'is invalid without an status' do
    item = build(:product_item, status: nil)
    item.valid?
    expect(item.errors[:status].size).to eq(1)
  end

  it 'is invalid with a duplicate description and status active' do
    name = 'item'
    status = 'active'
    create(:product_item, description: name, status: status)
    item = build(:product_item, description: name, status: status)
    item.valid?
    expect(item.errors[:description].size).to eq(1)
  end
end
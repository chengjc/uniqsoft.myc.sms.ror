require 'rails_helper'

describe MycClass do
  it 'has a valid factory' do
    expect(build(:myc_class)).to be_valid
  end

  it 'is valid with status_id, myc_level_id, start_at, status' do
    myc_class = MycClass.new(
      studio_id: 2,
      myc_level_id: 1,
      start_at: Time.now,
      status_updated_at: Time.now,
      status: 'on_going'
    )
    expect(myc_class).to be_valid
  end

  it 'is invalid without studio_id' do
    myc_class = build(:myc_class, studio_id: nil)
    myc_class.valid?
    expect(myc_class.errors[:studio_id].size).to be > 0
  end

  it 'is invalid without myc_level_id' do
    myc_class = build(:myc_class, myc_level_id: nil)
    myc_class.valid?
    expect(myc_class.errors[:myc_level_id].size).to be > 0
  end

  it 'is invalid without start_at' do
    myc_class = build(:myc_class, start_at: nil)
    myc_class.valid?
    expect(myc_class.errors[:start_at].size).to be > 0
  end

  it 'is invalid without status' do
    myc_class = build(:myc_class, status: nil)
    myc_class.valid?
    expect(myc_class.errors[:status].size).to be > 0
  end
end
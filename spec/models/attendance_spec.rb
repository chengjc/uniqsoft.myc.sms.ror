require 'rails_helper'

describe Attendance do
  it 'has a valid factory' do
    expect(build(:attendance)).to be_valid
  end

  it 'has a valid lesson_id, student_id and status' do
    attendance = Attendance.new(
      lesson_id: 1,
      student_id: 1,
      status: 'null'
    )
    expect(attendance).to be_valid
  end

  it 'is invalid without lesson_id' do
    attendance = build(:attendance, lesson_id: nil)
    attendance.valid?
    expect(attendance.errors[:lesson_id].size).to be > 0
  end

  it 'is invalid without student_id' do
    attendance = build(:attendance, student_id: nil)
    attendance.valid?
    expect(attendance.errors[:student_id].size).to be > 0
  end

  it 'is invalid without status' do
    attendance = build(:attendance, status: nil)
    attendance.valid?
    expect(attendance.errors[:status].size).to be > 0
  end
end
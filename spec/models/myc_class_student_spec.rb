require 'rails_helper'

describe MycClassStudent do
  it 'has a valid factory' do
    expect(build(:myc_class_student)).to be_valid
  end

  it 'has valid myc_class_id, student_id and status' do
    myc_class_student = MycClassStudent.new(
      myc_class_id: 1,
      student_id: 1,
      status: 'in_class'
    )
    expect(myc_class_student).to be_valid
  end

  it 'is invalid without myc_class_id' do
    myc_class_student = build(:myc_class_student, myc_class_id: nil)
    myc_class_student.valid?
    expect(myc_class_student.errors['myc_class_id'].size).to be > 0
  end

  it 'is invalid without student_id' do
    myc_class_student = build(:myc_class_student, student_id: nil)
    myc_class_student.valid?
    expect(myc_class_student.errors['student_id'].size).to be > 0
  end

  it 'is invalid without status' do
    myc_class_student = build(:myc_class_student, status: nil)
    myc_class_student.valid?
    expect(myc_class_student.errors['status'].size).to be > 0
  end

end
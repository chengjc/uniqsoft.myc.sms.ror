require 'rails_helper'

describe Teacher do
  it 'has a valid factory' do
    expect(build(:teacher)).to be_valid
    expect(build(:teacher_new)).to be_valid
  end

  it 'is valid with a user_name, password ,email, status' do
    user = User.new(
        nric_name: 'tester',
        password: '12345678',
        email: 'tester@test.com',
        status: 'active'
    )
    expect(user).to be_valid
  end

  it 'is invalid without an nric_name' do
    user = build(:user, nric_name: nil)
    user.valid?
    expect(user.errors[:nric_name].size).to be > 0
  end

  it 'is invalid without an password' do
    user = build(:user, password: nil)
    user.valid?
    expect(user.errors[:password].size).to be > 0
  end

  it 'is invalid without an email' do
    user = build(:user, email: nil)
    user.valid?
    expect(user.errors[:email].size).to be > 0
  end

  it 'is invalid without an status' do
    user = build(:user, status: nil)
    user.valid?
    expect(user.errors[:status].size).to be > 0
  end

  it 'is invalid with a duplicate email' do
    create(:user, email: 'test@test.com')
    user = build(:user, email: 'test@test.com')
    user.valid?
    expect(user.errors[:email].size).to be > 0
  end
end
require 'rails_helper'

describe Invoice do
  it 'has a valid factory' do
    expect(build(:invoice)).to be_valid
  end

  it 'is valid with a studio_id, student_id, invoice_number, invoice_on, due_on, teacher_name, program_name, day, time, total_price, bal_due, status' do
    invoice = Invoice.new(
        studio_id: '1',
        student_id: '1',
        invoice_number: 'ABC-001',
        invoice_on: '09/06/16',
        due_on: '09/06/16',
        teacher_name: 'Jame',
        program_name: 'Sunrise 1',
        day: 'Fri',
        time: '12:00 AM',
        total_price: 100.00,
        bal_due: 100.00,
        status: 'open'
    )
    expect(invoice).to be_valid
  end

  it 'is invalid without an studio_id' do
    invoice = build(:invoice, studio_id: nil)
    invoice.valid?
    expect(invoice.errors[:studio_id].size).to eq 1
  end

  it 'is invalid without an student_id' do
    invoice = build(:invoice, student_id: nil)
    invoice.valid?
    expect(invoice.errors[:student_id].size).to eq 1
  end

  it 'is invalid without an invoice_number' do
    invoice = build(:invoice, invoice_number: nil)
    invoice.valid?
    expect(invoice.errors[:invoice_number].size).to eq 1
  end

  it 'is invalid without an invoice_on' do
    invoice = build(:invoice, invoice_on: nil)
    invoice.valid?
    expect(invoice.errors[:invoice_on].size).to eq 1
  end

  it 'is invalid without an due_on' do
    invoice = build(:invoice, due_on: nil)
    invoice.valid?
    expect(invoice.errors[:due_on].size).to eq 1
  end

  it 'is invalid without an program_name' do
    invoice = build(:invoice, program_name: nil)
    invoice.valid?
    expect(invoice.errors[:program_name].size).to eq 1
  end

  it 'is invalid without an total_price' do
    invoice = build(:invoice, total_price: nil)
    invoice.valid?
    expect(invoice.errors[:total_price].size).to be > 1
  end

  it 'is invalid without an bal_due' do
    invoice = build(:invoice, bal_due: nil)
    invoice.valid?
    expect(invoice.errors[:bal_due].size).to be > 1
  end

  it 'is invalid without an status' do
    invoice = build(:invoice, status: nil)
    invoice.valid?
    expect(invoice.errors[:status].size).to eq 1
  end

  it 'is invalid with total_price is not numeric value' do
    invoice = build(:invoice, total_price: 'abc')
    invoice.valid?
    expect(invoice.errors[:total_price].size).to eq 1
  end

  it 'is invalid with bal_due is not numeric value' do
    invoice = build(:invoice, bal_due: 'abc')
    invoice.valid?
    expect(invoice.errors[:bal_due].size).to eq 1
  end

  it 'is invalid with duplicate invoice_number and studio_id' do
    create(:invoice, invoice_number: 'ABC001')
    invoice = build(:invoice, invoice_number: 'ABC001')
    invoice.valid?
    expect(invoice.errors[:invoice_number].size).to eq 1
  end


end
require 'rails_helper'

describe ProductPack do
  it 'has a valid factory' do
    expect(build(:product_pack)).to be_valid
  end

  it 'is valid with a description, quantity_on_hand ,quantity_trigger_alert, cost, status' do
    item = ProductPack.new(
        description: 'pack name',
        status: 'active'
    )
    expect(item).to be_valid
  end

  it 'is invalid without a description' do
    item = build(:product_pack, description: nil)
    item.valid?
    expect(item.errors[:description].size).to eq(1)
  end

  it 'is invalid without a status' do
    item = build(:product_pack, status: nil)
    item.valid?
    expect(item.errors[:status].size).to eq(1)
  end

  it 'is invalid with a duplicate description and status active' do
    name = 'item'
    status = 'active'
    create(:product_pack, description: name, status: status)
    item = build(:product_pack, description: name, status: status)
    item.valid?
    expect(item.errors[:description].size).to eq(1)
  end
end
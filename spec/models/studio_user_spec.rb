require 'rails_helper'

describe StudioUser do
  it 'is valid with a studio_id, user_id ,role_id' do
    studio_user = StudioUser.new(
        studio_id: 1,
        user_id: 1,
        role_id: 1
    )
    expect(studio_user).to be_valid
  end

  it 'is invalid without an studio_id' do
    studio_user = build(:studio_user, studio_id: nil)
    studio_user.valid?
    expect(studio_user.errors[:studio_id].size).to eq(1)
  end

  it 'is invalid without an user_id' do
    studio_user = build(:studio_user, user_id: nil)
    studio_user.valid?
    expect(studio_user.errors[:user_id].size).to eq(1)
  end

  it 'is invalid without an role_id' do
    studio_user = build(:studio_user, role_id: nil)
    studio_user.valid?
    expect(studio_user.errors[:role_id].size).to eq(1)
  end

  it 'is valid with a duplicate studio_id' do
    StudioUser.create(
        studio_id: 1,
        user_id: 1,
        role_id: 0
    )

    studio_user = StudioUser.new(
        studio_id: 1,
        user_id: 0,
        role_id: 0
    )

    studio_user.valid?
    expect(studio_user.errors[:studio_id].size).to eq(0)
  end

  it 'is invalid with a duplicate studio_id and user_id' do
    StudioUser.create(
        studio_id: 1,
        user_id: 1,
        role_id: 0
    )

    studio_user = StudioUser.new(
        studio_id: 1,
        user_id: 1,
        role_id: 1
    )

    studio_user.valid?
    expect(studio_user.errors[:studio_id].size).to eq(1)
  end

  it 'returns role name' do
    expect(StudioUser.role_name(StudioUser.roles[:super_user])).to eq('Super user')
    expect(StudioUser.role_name(StudioUser.roles[:studio_admin])).to eq('Studio admin')
    expect(StudioUser.role_name(StudioUser.roles[:teacher])).to eq('Teacher')
  end
end
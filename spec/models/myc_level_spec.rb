require 'rails_helper'

describe MycLevel do
  it 'has a valid factory' do
    expect(build(:myc_level)).to be_valid
  end

  it 'is valid with a name, age, total_lesson, lesson_per_term, lesson_fee_per_term, lesson_duration_minute, level_order, status, myc_program_id' do
    level = MycLevel.new(
        level_name: 'name',
        age: 1,
        total_lesson: 1,
        lesson_per_term: 1,
        lesson_fee_per_term: 1.0000,
        lesson_duration_minute: 1,
        level_order: 1,
        status: 'active',
        myc_program_id: 1
    )
    expect(level).to be_valid
  end

  it 'is invalid without a level_name' do
    level = build(:myc_level, level_name: nil)
    level.valid?
    expect(level.errors[:level_name].size).to eq(1)
  end

  it 'is invalid without a age' do
    level = build(:myc_level, age: nil)
    level.valid?
    expect(level.errors[:age].size).to eq(1)
  end

  it 'is invalid without a total_lesson' do
    level = build(:myc_level, total_lesson: nil)
    level.valid?
    expect(level.errors[:total_lesson].size).to be > 0
  end


  it 'is invalid without a lesson_per_term' do
    level = build(:myc_level, lesson_per_term: nil)
    level.valid?
    expect(level.errors[:lesson_per_term].size).to be > 0
  end

  it 'is invalid without a lesson_fee_per_term' do
    level = build(:myc_level, lesson_fee_per_term: nil)
    level.valid?
    expect(level.errors[:lesson_fee_per_term].size).to be > 0
  end

  it 'is invalid without a lesson_duration_minute' do
    level = build(:myc_level, lesson_duration_minute: nil)
    level.valid?
    expect(level.errors[:lesson_duration_minute].size).to be > 0
  end

  it 'is invalid without a level_order' do
    level = build(:myc_level, level_order: nil)
    level.valid?
    expect(level.errors[:level_order].size).to be > 0
  end

  it 'is invalid without a status' do
    level = build(:myc_level, status: nil)
    level.valid?
    expect(level.errors[:status].size).to eq(1)
  end

  it 'is invalid without a myc_program_id' do
    level = build(:myc_level, myc_program_id: nil)
    level.valid?
    expect(level.errors[:myc_program_id].size).to be > 0
  end
end

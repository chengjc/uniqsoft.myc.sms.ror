require 'rails_helper'

describe TeacherCommission do
  it 'has a valid factory' do
    expect(build(:teacher_commission)).to be_valid
  end

  it 'is valid with a studio_id, techer_id ,teacher_nric_name, teacher_statement_date, net_pay' do
    item = TeacherCommission.new(
        studio_id: 1,
        teacher_id: 1,
        teacher_nric_name: 'john',
        statement_date: Date.today,
        net_pay: 0.0000,
    )
    expect(item).to be_valid
  end

  it 'is invalid without a studio_id' do
    item = build(:teacher_commission, studio_id: nil)
    item.valid?
    expect(item.errors[:studio_id].size).to eq(1)
  end

  it 'is invalid without a teacher_nric_name' do
    item = build(:teacher_commission, teacher_nric_name: nil)
    item.valid?
    expect(item.errors[:teacher_nric_name].size).to eq(1)
  end

  it 'is invalid without a teacher_statement_date' do
    item = build(:teacher_commission, statement_date: nil)
    item.valid?
    expect(item.errors[:statement_date].size).to eq(1)
  end

  it 'is invalid without a net_pay' do
    item = build(:teacher_commission, net_pay: nil)
    item.valid?
    expect(item.errors[:net_pay].size).to be > 0
  end

  it 'returns current month teachers' do
    studio_id = 1
    teacher_user = create(:user, nric_name: 'john')
    teacher_user_2 = create(:user, nric_name: 'kent')
    create(:studio_user, user_id: teacher_user.id, studio_id: studio_id, role_id: StudioUser.roles[:teacher])
    create(:studio_user, user_id: teacher_user_2.id, studio_id: studio_id, role_id: StudioUser.roles[:teacher])
    teacher = create(:teacher, user_id: teacher_user.id)
    teacher_2 = create(:teacher, user_id: teacher_user_2.id)
    # myc_class = create(:myc_class, teacher_id: teacher.id)
    myc_class = create(:myc_class,
        studio_id: studio_id,
        myc_level_id: 1,
        start_date: Date.today,
        status: 0
    )
    # create(:lesson, myc_class_id: myc_class.id, teacher_id: teacher.id)
    create(:lesson,
        myc_class_id: myc_class.id,
        teacher_id: teacher.id,
        lesson_no: 1,
        start_at: Date.today,
        status: Lesson.statuses[:marked]
    )

    teachers = TeacherCommission.current_month_teachers(studio_id)
    expect(teachers.size).to eq(1)
    teacher_current_month = teachers[0]
    expect(teacher_current_month.teacher_nric_name).to eq('john')
    expect(teacher_current_month.teacher_id).to eq(teacher.id)

    # add teacher 2 in another month
    create(:lesson,
        myc_class_id: myc_class.id,
        teacher_id: teacher_2.id,
        lesson_no: 2,
        start_at: (Date.today - 1.months).at_end_of_month,
        status: Lesson.statuses[:marked]
    )

    teachers = TeacherCommission.current_month_teachers(studio_id)
    expect(teachers.size).to eq(1)
    teacher_current_month = teachers[0]
    expect(teacher_current_month.teacher_nric_name).to eq('john')
    expect(teacher_current_month.teacher_id).to eq(teacher.id)

    # add teacher 2 in same month
    create(:lesson,
        myc_class_id: myc_class.id,
        teacher_id: teacher_2.id,
        lesson_no: 3,
        start_at: Date.today,
        status: Lesson.statuses[:marked]
    )

    teachers = TeacherCommission.current_month_teachers(studio_id)
    expect(teachers.size).to eq(2)
    is_teacher_1 = false
    is_teacher_2 = false
    teachers.each do |teacher_current_month|
      if teacher_current_month.teacher_nric_name == teacher_user.nric_name && teacher_current_month.teacher_id == teacher.id
        is_teacher_1 = true
      elsif teacher_current_month.teacher_nric_name == teacher_user_2.nric_name && teacher_current_month.teacher_id == teacher_2.id
        is_teacher_2 = true
      end
    end
    expect(is_teacher_1).to eq(true)
    expect(is_teacher_2).to eq(true)
  end

  it 'returns net pay of a teacher' do
    teacher_commission = create(:teacher_commission)
    item = create(:teacher_commission_item, teacher_commission_id: teacher_commission.id)
    item_additional = create(:teacher_commission_item_additional, teacher_commission_id: teacher_commission.id)
    item_other = create(:teacher_commission_other, teacher_commission_id: teacher_commission.id)
    net_pay = TeacherCommission.net_pay(teacher_commission.id)
    total_pay = item.commission + item_additional.commission + item_other.amount
    expect(net_pay).to eq(total_pay)

    # add more items
    item_2 = create(:teacher_commission_item, teacher_commission_id: teacher_commission.id)
    item_additional_2 = create(:teacher_commission_item_additional, teacher_commission_id: teacher_commission.id)
    item_other_2 = create(:teacher_commission_other, teacher_commission_id: teacher_commission.id)
    net_pay_2 = TeacherCommission.net_pay(teacher_commission.id)
    total_pay_2 = total_pay + item_2.commission + item_additional_2.commission + item_other_2.amount
    expect(net_pay_2).to eq(total_pay_2)
  end

end
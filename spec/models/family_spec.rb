require 'rails_helper'

describe Family do
  it 'has a valid factory' do
    expect(build(:family)).to be_valid
  end

  it 'is valid with a address1, city_or_state, country, primary_contact_name, primary_contact_relationship, primary_contact_phone_no, primary_contact_email, status' do
    family = Family.new(
        studio_id: '1',
        address1: 'test',
        city_or_state: 'test',
        country: 'test',
        primary_contact_name: 'test',
        primary_contact_relationship: 'test',
        primary_contact_phone_no: 'test',
        primary_contact_email: 'email@test.com',
        primary_contact_accompany_for_student: 'true',
        status: 'active'
    )
    expect(family).to be_valid
  end

  it 'is invalid without an address1' do
    family = build(:family, address1: nil)
    family.valid?
    expect(family.errors[:address1].size).to eq 1
  end

  it 'is invalid without an city_or_state' do
    family = build(:family, city_or_state: nil)
    family.valid?
    expect(family.errors[:city_or_state].size).to eq 1
  end

  it 'is invalid without an country' do
    family = build(:family, country: nil)
    family.valid?
    expect(family.errors[:country].size).to eq 1
  end

  it 'is invalid without an primary_contact_name' do
    family = build(:family, primary_contact_name: nil)
    family.valid?
    expect(family.errors[:primary_contact_name].size).to eq 1
  end

  it 'is invalid without an primary_contact_relationship' do
    family = build(:family, primary_contact_relationship: nil)
    family.valid?
    expect(family.errors[:primary_contact_relationship].size).to eq 1
  end

  it 'is invalid without an primary_contact_phone_no' do
    family = build(:family, primary_contact_phone_no: nil)
    family.valid?
    expect(family.errors[:primary_contact_phone_no].size).to eq 1
  end

  it 'is invalid without an primary_contact_email' do
    family = build(:family, primary_contact_email: nil)
    family.valid?
    expect(family.errors[:primary_contact_email].size).to be > 0
  end

  it 'is invalid without an status' do
    family = build(:family, status: nil)
    family.valid?
    expect(family.errors[:status].size).to eq 1
  end

  it 'is invalid without checked primary_contact_accompany_for_student or other_contact_accompany_for_student' do
    family = build(:family, primary_contact_accompany_for_student: 'false', other_contact_accompany_for_student: 'false')
    family.valid?
    expect(family.errors.size).to eq 1
  end
end
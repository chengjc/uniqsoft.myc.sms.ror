require 'rails_helper'

describe Studio do
  it 'has a valid factory' do
    expect(build(:studio)).to be_valid
  end

  it 'is valid with a studio_code, studio_name, address, company_name, company_registration_no, phone_no, email, facebook_address, website_address, status' do
    studio = Studio.new(
        studio_code: 'HQ',
        studio_name: 'Head Quater',
        address: 'Yangon',
        company_name: 'MS',
        company_registration_no: '123',
        phone_no: '+95 092232322',
        email: 'headquater@ms.com',
        facebook_address: 'http//www.facebook.com/ms',
        website_address: 'http//www.ms.com.mm',
        status: 'active'
    )
    expect(studio).to be_valid
  end

  it 'is invalid without an studio_code' do
    studio = build(:studio, studio_code: nil)
    studio.valid?
    expect(studio.errors[:studio_code].size).to eq 1
  end

  it 'is invalid without an studio_name' do
    studio = build(:studio, studio_name: nil)
    studio.valid?
    expect(studio.errors[:studio_name].size).to eq 1
  end

  it 'is invalid without an status' do
    studio = build(:studio, status: nil)
    studio.valid?
    expect(studio.errors[:status].size).to eq 1
  end

  it 'is invalid with an invalid email format' do
    studio = build(:studio, email: 'test')
    studio.valid?
    expect(studio.errors[:email].size).to eq 1
  end

  it 'is invalid with duplicate studio_code, studio_name, status' do
    create(:studio, studio_code: 'HQ', studio_name: 'Head Quater', status: 'active')
    studio = build(:studio, studio_code: 'HQ', studio_name: 'Head Quater', status: 'active')
    studio.valid?
    expect(studio.errors[:studio_code].size).to eq 1
  end
end


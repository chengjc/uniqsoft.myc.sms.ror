FactoryGirl.define do
  factory :product_group do
    description {Faker::Name.name}
    display_order { rand(0..20) }
    status 'active'
  end
end

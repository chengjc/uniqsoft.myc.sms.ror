FactoryGirl.define do
  factory :student_report do
    studio_id { rand(0..10) }
    teacher_id { rand(0..10) }
    report_date Date.today
    student_count_sr1 { rand(0..100) }
    student_count_sr2 { rand(0..100) }
    student_count_sr3 { rand(0..100) }
    student_count_ss1 { rand(0..100) }
    student_count_ss2 { rand(0..100) }
    student_count_sb1 { rand(0..100) }
    student_count_sb2 { rand(0..100) }
    student_count_sb3 { rand(0..100) }
    student_count_mb1 { rand(0..100) }
    student_count_mb2 { rand(0..100) }
    student_count_mb3 { rand(0..100) }
    total { rand(0..100) }
  end
end

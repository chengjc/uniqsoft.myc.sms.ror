require 'faker'

FactoryGirl.define do

  factory :teacher do
    user_id 1
    gender 'M'
    date_of_birth Date.today
    number_of_children {rand(1..10)}
    full_time_occupation { Faker::Company.profession }
    highest_piano_practical_standard_passed { rand(1..10) }
    highest_theory_standard_passed { rand(1..10) }
    has_college_abrsm true
    has_college_lcm true
    has_college_trinity true
    college_others { Faker::University.name }
    other_music_qualifications { Faker::University.name }
    before_myc_teaching_individual_lesson_started_year { rand(2000..2010) }
    before_myc_teaching_individual_lesson_total_year { rand(0..10) }
    before_myc_teaching_group_lesson_started_year { rand(2000..2010) }
    before_myc_teaching_group_lesson_total_year { rand(0..10) }
    date_of_myc_teacher_agreement Date.today-1
    date_of_myc_studio_agreement Date.today-2
    date_of_myc_l1 Date.today-3
    date_of_myc_l2mb2 Date.today-4
    date_of_myc_l2ss2 Date.today-5
    date_of_myc_l3sb3 Date.today-6
    date_of_myc_l2sb2 Date.today-7
    date_of_myc_l3mb3 Date.today-8
    date_of_1st_teacher_visit Date.today-9
    date_of_2nd_teacher_visit Date.today-10
    date_of_3rd_teacher_visit Date.today-11
  end

  # different from :teacher for testing update
  factory :teacher_new, class: Teacher do
    user_id 1
    gender 'F'
    date_of_birth (Date.today - 1)
    number_of_children {rand(11..20)}
    full_time_occupation { Faker::Company.profession }
    highest_piano_practical_standard_passed { rand(11..20) }
    highest_theory_standard_passed { rand(11..20) }
    has_college_abrsm false
    has_college_lcm false
    has_college_trinity false
    college_others { Faker::University.name }
    other_music_qualifications { Faker::University.name }
    before_myc_teaching_individual_lesson_started_year { rand(3000..3010) }
    before_myc_teaching_individual_lesson_total_year { rand(11..20) }
    before_myc_teaching_group_lesson_started_year { rand(3000..3010) }
    before_myc_teaching_group_lesson_total_year { rand(11..20) }
    date_of_myc_teacher_agreement Date.today-20
    date_of_myc_studio_agreement Date.today-21
    date_of_myc_l1 Date.today-22
    date_of_myc_l2mb2 Date.today-23
    date_of_myc_l2ss2 Date.today-24
    date_of_myc_l3sb3 Date.today-25
    date_of_myc_l2sb2 Date.today-26
    date_of_myc_l3mb3 Date.today-27
    date_of_1st_teacher_visit Date.today-28
    date_of_2nd_teacher_visit Date.today-29
    date_of_3rd_teacher_visit Date.today-30
  end
end
FactoryGirl.define do
  factory :myc_level do
    sequence(:level_name) { |n| "#{n}"}
    age { rand(0..50) }
    total_lesson { rand(1..10) }
    lesson_per_term { rand(1..10) }
    lesson_fee_per_term { Faker::Number.decimal(4) }
    lesson_duration_minute { rand(0..100) }
    level_order { rand(0..10) }
    status 'active'
    myc_program_id 1
  end

  factory :new_myc_level do
    sequence(:level_name) { |n| "#{n}"}
    age { rand(50..100) }
    total_lesson { rand(10..20) }
    lesson_per_term { rand(10..20) }
    lesson_fee_per_term { Faker::Number.decimal(4) }
    lesson_duration_minute { rand(200..300) }
    level_order { rand(10..20) }
    status 'active'
    myc_program_id 2
  end
end

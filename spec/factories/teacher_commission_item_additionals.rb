FactoryGirl.define do
  factory :teacher_commission_item_additional do
    teacher_commission_id {rand(1..100)}
    program { Faker::Name.first_name }
    day 'Mon'
    time '11:11'
    student_nric_name { Faker::Name.first_name }
    lesson_date Date.today
    term_fee { Faker::Number.decimal(4) }
    lesson_fee { Faker::Number.decimal(4) }
    no_of_lesson {rand(1..100)}
    total_fee { Faker::Number.decimal(4) }
    percentage {rand(1..50)}
    commission { Faker::Number.decimal(4) }
    remarks 'this is remakrs'
  end

  # different from :teacher_commission_item_additional for testing update
  factory :teacher_commission_item_additional_new, class: TeacherCommissionItemAdditional do
    teacher_commission_id {rand(2..200)}
    program { Faker::Name.last_name }
    day 'Tue'
    time '22:22'
    student_nric_name { Faker::Name.first_name }
    lesson_date Date.today-1
    term_fee { Faker::Number.decimal(4) }
    lesson_fee { Faker::Number.decimal(4) }
    no_of_lesson {rand(2..200)}
    total_fee { Faker::Number.decimal(4) }
    percentage {rand(51..100)}
    commission { Faker::Number.decimal(4) }
    remarks 'this is remakrs 2'
  end
end

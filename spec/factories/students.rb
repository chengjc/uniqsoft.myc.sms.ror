FactoryGirl.define do
  factory :student do
    myc_level_id '1'
    family_id '1'
    studio_id '1'
    enrolment_piano { rand(0..10) }
    enrolment_theory { rand(0..10) }
    enrolment_other { rand(0..10) }
    preferred_day_one 'Mon'
    preferred_day_two 'Wed'
    preferred_day_three 'Fri'
    preferred_time_one '9-12 am'
    preferred_time_two '12-6 pm'
    preferred_time_three '6-9 pm'
    nric_name { Faker::Name.first_name }
    first_name { Faker::Name.first_name }
    sur_name { Faker::Name.first_name }
    gender 'M'
    date_of_birth Date.today-9
    piano_standard 'Piano Standard'
    piano_college 'ABRSM'
    piano_school_attended { Faker::University.name }
    theory_standard { Faker::University.name }
    theory_college 'ABRSM'
    theory_school_attended { Faker::University.name }
    violin_standard { Faker::University.name }
    violin_college 'ABRSM'
    violin_school_attended { Faker::University.name }
    other_standard { Faker::University.name }
    other_college 'ABRSM'
    other_school_attended { Faker::University.name }
    referred_by 'Current Parent'
    enquiry_date Date.today-5
    registration_date Date.today-1
    status 'current'
    instrument_owned_other {Faker::Name.first_name}
  end

  factory :new_student, class: Student do
    myc_level_id '1'
    family_id '1'
    studio_id '1'
    enrolment_piano { rand(11..20) }
    enrolment_theory { rand(11..20) }
    enrolment_other { rand(11..20) }
    preferred_day_one 'Tue'
    preferred_day_two 'Sat'
    preferred_day_three 'Sun'
    preferred_time_one '6-9 pm'
    preferred_time_two '9-12 am'
    preferred_time_three '12-6 pm'
    nric_name { Faker::Name.first_name }
    first_name { Faker::Name.first_name }
    sur_name { Faker::Name.first_name }
    gender 'F'
    date_of_birth Date.today-10
    piano_standard { Faker::University.name }
    piano_college 'Guildhall'
    piano_school_attended { Faker::University.name }
    theory_standard { Faker::University.name }
    theory_college 'Guildhall'
    theory_school_attended { Faker::University.name }
    violin_standard { Faker::University.name }
    violin_college 'Guildhall'
    violin_school_attended { Faker::University.name }
    other_standard { Faker::University.name }
    other_college 'Guildhall'
    other_school_attended { Faker::University.name }
    referred_by 'Ex Parent'
    enquiry_date Date.today-3
    registration_date Date.today-4
    status 'terminated'
    instrument_owned_other {Faker::Name.first_name}
  end
end


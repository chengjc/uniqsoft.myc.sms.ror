require 'faker'

FactoryGirl.define do
  factory :lesson do
    lesson_no '1'
    start_at Time.now
    reason_for_cancel 'test'
    status 'not_marked'
    myc_class_id '1'
    teacher_id '1'
  end
end
require 'faker'

FactoryGirl.define do
  factory :myc_class do
    studio_id '2'
    myc_level_id '1'
    start_at Time.now
    status_updated_at Time.now
    status 'on_going'
  end
end
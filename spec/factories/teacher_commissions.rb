FactoryGirl.define do
  factory :teacher_commission do
    studio_id 1
    teacher_id 1
    teacher_nric_name { Faker::Name.name }
    statement_date Date.today
    net_pay { Faker::Number.decimal(4) }
  end
end

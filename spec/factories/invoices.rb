FactoryGirl.define do
  factory :invoice do
    studio_id '1'
    student_id '1'
    invoice_number {Faker::Number.number(4)}
    invoice_on Date.today-9
    due_on Date.today-15
    teacher_name {Faker::Name.first_name}
    program_name {Faker::Name.first_name}
    day 'Fri'
    time '10:00 PM'
    total_price { Faker::Number.decimal(4) }
    bal_due { Faker::Number.decimal(4) }
    status 'open'
  end

  factory :new_invoice, class: Invoice do
    studio_id '1'
    student_id '1'
    invoice_number {Faker::Number.number(4)}
    invoice_on Date.today-5
    due_on Date.today-10
    teacher_name {Faker::Name.first_name}
    program_name {Faker::Name.first_name}
    day 'Sat'
    time '09:00 AM'
    total_price { Faker::Number.decimal(4) }
    bal_due { Faker::Number.decimal(4) }
    status 'paid'
  end
end





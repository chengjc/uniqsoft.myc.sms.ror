FactoryGirl.define do
  factory :myc_program do
    program_name {Faker::Name.first_name}
    sequence(:program_code) { |n| "code #{n}"}
    program_order { rand(0..10) }
    status 'active'
  end

  factory :new_myc_program, class: MycProgram do
    program_name {Faker::Name.first_name}
    program_code {Faker::Name.first_name}
    program_order { rand(10..20) }
    status 'inactive'
  end
end

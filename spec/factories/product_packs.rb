FactoryGirl.define do
  factory :product_pack do
    description { Faker::Name.first_name }
    status 'active'
  end

  # different from :product_item for testing update
  factory :product_pack_new, class: ProductPack do
    description { Faker::Name.first_name }
    status 'inactive'
  end
end

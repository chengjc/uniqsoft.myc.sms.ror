FactoryGirl.define do
  factory :holiday do
    studio_id '1'
    holiday_date Date.today-9
    holiday_name {Faker::Name.first_name}
  end

  factory :new_holiday, class: Holiday do
    studio_id '1'
    holiday_date Date.today-4
    holiday_name {Faker::Name.first_name}
  end
end

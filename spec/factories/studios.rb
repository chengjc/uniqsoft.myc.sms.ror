FactoryGirl.define do
  factory :studio do
    sequence(:studio_code) { |n| "code #{n}"}
    studio_name { Faker::Name.first_name }
    address {Faker::Address.city}
    company_name {Faker::Name.first_name}
    company_registration_no { rand(0..10) }
    phone_no {Faker::PhoneNumber.cell_phone}
    email {Faker::Internet.email}
    facebook_address {Faker::Internet.domain_name}
    website_address {Faker::Internet.domain_name}
    status 'active'
  end

  factory :new_studio, class: Studio do
    sequence(:studio_code) { |n| "code #{n}"}
    studio_name { Faker::Name.first_name }
    address {Faker::Address.city}
    company_name {Faker::Name.first_name}
    company_registration_no { rand(11..20) }
    phone_no {Faker::PhoneNumber.cell_phone}
    email {Faker::Internet.email}
    facebook_address {Faker::Internet.domain_name}
    website_address {Faker::Internet.domain_name}
    status 'active'
  end
end


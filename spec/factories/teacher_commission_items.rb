FactoryGirl.define do
  factory :teacher_commission_item do
    program { Faker::Name.first_name }
    day 'Mon'
    time '11:11'
    student_nric_name { Faker::Name.first_name }
    lesson_date Date.today
    term_fee { Faker::Number.decimal(4) }
    lesson_fee { Faker::Number.decimal(4) }
    no_of_lesson {rand(1..100)}
    total_fee { Faker::Number.decimal(4) }
    percentage {rand(1..50)}
    commission { Faker::Number.decimal(4) }
    remarks 'this is remakrs'
  end

end

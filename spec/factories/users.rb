require 'faker'

FactoryGirl.define do
  factory :user do
    nric_name { Faker::Name.first_name }
    phone_number { Faker::PhoneNumber.cell_phone }
    status 'active'
    password { Faker::Internet.password(8) }
    email { Faker::Internet.email }

    # factory :super_user do
    #
    #   role 'super_user'
    # end
    #
    # factory :studio_admin do
    #   role 'studio_admin'
    # end
    #
    # factory :teacher do
    #   role 'teacher'
    # end
  end
end
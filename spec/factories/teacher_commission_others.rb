FactoryGirl.define do
  factory :teacher_commission_other do
    item { Faker::Name.name }
    amount { Faker::Number.decimal(4) }
    remarks { Faker::Address.street_name }
  end

  # different from :teacher_commission_other for testing update
  factory :teacher_commission_other_new, class: TeacherCommissionOther do
    item { Faker::Name.title }
    amount { Faker::Number.decimal(4) }
    remarks { Faker::Address.street_address }
  end
end

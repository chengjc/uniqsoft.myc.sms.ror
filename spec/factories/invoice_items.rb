FactoryGirl.define do
  factory :invoice_item do
    invoice_id '1'
    item_name {Faker::Name.first_name}
    quantity {rand(1..5)}
    price { Faker::Number.decimal(4) }
  end

  factory :new_invoice_item, class: InvoiceItem do
    invoice_id '1'
    item_name {Faker::Name.first_name}
    quantity {rand(5..9)}
    price { Faker::Number.decimal(4) }
  end
end





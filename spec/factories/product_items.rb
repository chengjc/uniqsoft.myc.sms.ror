FactoryGirl.define do
  factory :product_item do
    description { Faker::Name.first_name }
    quantity_on_hand {rand(1..100)}
    quantity_trigger_alert {rand(1..100)}
    cost { Faker::Number.decimal(4) }
    remarks { Faker::Name.name }
    status 'active'
  end

  # different from :product_item for testing update
  factory :product_item_new, class: ProductItem do
    description { Faker::Name.first_name }
    quantity_on_hand {rand(2..200)}
    quantity_trigger_alert {rand(2..200)}
    cost { Faker::Number.decimal(4) }
    remarks { Faker::Name.name }
    status 'inactive'
  end
end

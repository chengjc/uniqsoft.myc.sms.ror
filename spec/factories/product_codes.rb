FactoryGirl.define do
  factory :product_code do
    myc_level_id 1
    code { Faker::Name.name }
    price { Faker::Number.decimal(4) }
    remarks { Faker::Name.name }
    status 'active'
  end

  # different from :product_code for testing update
  factory :product_code_new, class: ProductCode do
    myc_level_id 1
    code { Faker::Name.name }
    price { Faker::Number.decimal(4) }
    remarks { Faker::Name.name }
    status 'inactive'
  end
end

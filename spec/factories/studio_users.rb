FactoryGirl.define do
  factory :studio_user do
    studio_id {rand(1..100)}
    user_id {rand(1..100)}
    role_id {rand(1..2)}
  end
end

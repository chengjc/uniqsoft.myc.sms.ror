FactoryGirl.define do
  factory :family do
      studio_id '1'
      address1 { Faker::Address.street_name}
      address2 { Faker::Address.street_name}
      postal_code 'ABC'
      city_or_state { Faker::Address.city }
      country { Faker::Address.country }
      primary_contact_name { Faker::Name.first_name }
      primary_contact_relationship 'Father'
      primary_contact_phone_no { Faker::PhoneNumber.cell_phone }
      primary_contact_email { Faker::Internet.email }
      primary_contact_occupation 'ABC'
      primary_contact_accompany_for_student 'true'
      other_contact_name { Faker::Name.first_name }
      other_contact_relationship 'Father'
      other_contact_phone_no { Faker::PhoneNumber.cell_phone }
      other_contact_email { Faker::Internet.email }
      other_contact_occupation 'ABC'
      other_contact_accompany_for_student 'true'
      status 'active'
  end

  # different from :teacher for testing update
  factory :family_new, class: Family do
      studio_id '1'
      address1 { Faker::Address.street_name}
      address2 { Faker::Address.street_name}
      postal_code 'DEF'
      city_or_state { Faker::Address.city }
      country { Faker::Address.country }
      primary_contact_name { Faker::Name.first_name }
      primary_contact_relationship 'Mother'
      primary_contact_phone_no { Faker::PhoneNumber.cell_phone }
      primary_contact_email { Faker::Internet.email }
      primary_contact_occupation 'EER'
      primary_contact_accompany_for_student 'true'
      other_contact_name { Faker::Name.first_name }
      other_contact_relationship 'Anty'
      other_contact_phone_no { Faker::PhoneNumber.cell_phone }
      other_contact_email { Faker::Internet.email }
      other_contact_occupation 'ggh'
      other_contact_accompany_for_student 'false'
      status 'active'
  end

end


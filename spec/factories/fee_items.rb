FactoryGirl.define do
  factory :fee_item do
    fee_item_name {Faker::Name.first_name}
    price { Faker::Number.decimal(4) }
    fee_type 'program'
    
  end

  factory :new_fee_item, class: FeeItem do
    fee_item_name {Faker::Name.first_name}
    price { Faker::Number.decimal(4) }
    fee_type 'discount'

  end
end

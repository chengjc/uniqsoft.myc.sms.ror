module LoginMacros
  # example:
  # log_in(role_id: 1)
  # log_in(role_id: 1, user: user)
  # log_in(role_id: 1, user: user, studio_id: 1)
  def log_in(options)
    if options[:user]
      user = options[:user]
    else
      user = create(:user)
    end

    if (options[:studio_id])
      studio_id = options[:studio_id]
    else
      if options[:role_id] == StudioUser.roles[:super_user]
        studio = create(:studio, studio_name: Rails.configuration.hq_studio_name)
        studio_id = studio.id
      else
        studio = create(:studio)
        studio_id = studio.id
      end
    end

    StudioUser.create(studio_id: studio_id, user_id: user.id, role_id: options[:role_id])

    visit '/users/sign_in'
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end
end
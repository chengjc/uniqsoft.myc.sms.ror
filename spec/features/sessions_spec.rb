require 'rails_helper'

feature 'Session management' do

  context 'when logged in as any user ' do
    scenario 'logs out and in again' do
      user = create(:user)
      studio = create(:studio)
      role_id = StudioUser.roles[:teacher]
      StudioUser.create(studio_id: studio.id, user_id: user.id, role_id: role_id)
      visit '/users/sign_in'
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'
      click_link 'Logout'
      expect(page).to have_content('Logout Successfully')
      expect(current_path).to eq '/logout/'
      click_link 'Login'
      expect(current_path).to eq '/users/sign_in'
    end

    scenario "shows user's profile and studios order by studio name" do
      user = create(:user)
      studio = create(:studio, studio_name: 'a')
      role_id = StudioUser.roles[:studio_admin]
      StudioUser.create(studio_id: studio.id, user_id: user.id, role_id: role_id)

      studio_2 = create(:studio, studio_name: 'b')
      role_id_2 = StudioUser.roles[:teacher]
      StudioUser.create(studio_id: studio_2.id, user_id: user.id, role_id: role_id_2)

      visit '/users/sign_in'
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'
      expect(page).to have_content(user.email)
      expect(page).to have_content(StudioUser.role_name(role_id))
      expect(page).to have_content(user.status.humanize)
      expect(page).to have_content(studio.studio_name)

      click_button studio.studio_name
      click_link studio_2.studio_name

      expect(page).to have_content(user.email)
      expect(page).to have_content(StudioUser.role_name(role_id_2))
      expect(page).to have_content(user.status.humanize)
      expect(page).to have_content(studio_2.studio_name)
    end

    scenario 'changes password' do
      user = create(:user)
      log_in(user: user, role_id: StudioUser.roles[:super_user])
      new_password = user.password + 'new'
      click_button user.email
      click_link 'Change your password'
      within 'h2' do
        expect(page).to have_content 'Change Password'
      end
      fill_in 'user_email', with: user.email
      fill_in 'user[password]', with: new_password
      fill_in 'user[password_confirmation]', with: new_password
      fill_in 'user[current_password]', with: user.password
      click_button 'Update'
      expect(current_path).to eq '/'

      click_link 'Logout'

      visit '/users/sign_in'
      fill_in 'Email', with: user.email
      fill_in 'Password', with: new_password
      click_button 'Log in'
      expect(page).to have_content(user.email) # display login user name on topnavbar layout
    end
  end

  context 'when logged in as HQ user' do
    scenario 'views only Users, Studios, MYC Programs, Material, Student Reports menus' do
      log_in(role_id: StudioUser.roles[:super_user])
      expect(page).to have_content 'Users'
      expect(page).to have_content 'Studios'
      expect(page).to have_content 'MYC Programs'
      expect(page).to have_content 'Material'
      expect(page).to have_content 'Items'
      expect(page).to have_content 'Packs'
      expect(page).to have_content 'Codes'
      expect(page).to have_content 'Student Reports'
      expect(page).to_not have_content 'Calendar'
      expect(page).to_not have_content 'Students'
      expect(page).to_not have_content 'Families'
      expect(page).to_not have_content 'Classes'
      expect(page).to_not have_content 'Money'
      expect(page).to_not have_content 'Teacher Commissions'
      expect(page).to_not have_content 'Invoices'
      expect(page).to_not have_content 'Holidays'
    end
  end

  context 'when logged in as studio admin' do
    scenario 'views studio admin menus' do
      log_in(role_id: StudioUser.roles[:studio_admin])
      expect(page).to_not have_content 'Users'
      expect(page).to_not have_content 'Studios'
      expect(page).to_not have_content 'MYC Programs'
      expect(page).to_not have_content 'Material'
      expect(page).to_not have_content 'Student Reports'
      expect(page).to have_content 'Calendar'
      expect(page).to have_content 'Students'
      expect(page).to have_content 'Families'
      expect(page).to have_content 'Classes'
      expect(page).to have_content 'Money'
      expect(page).to have_content 'Teacher Commissions'
      expect(page).to have_content 'Holidays'
    end
  end

  context 'when logged in as teacher' do
    scenario 'views only classes and calendar menus' do
      log_in(role_id: StudioUser.roles[:teacher])
      expect(page).to_not have_content 'Users'

      expect(page).to_not have_content 'Studios'
      expect(page).to_not have_content 'MYC Programs'
      expect(page).to_not have_content 'Material'
      expect(page).to_not have_content 'Student Reports'
      expect(page).to_not have_content 'Students'
      expect(page).to_not have_content 'Families'
      expect(page).to_not have_content 'Holidays'
      expect(page).to have_content 'Calendar'
      expect(page).to have_content 'Classes'
      expect(page).to have_content 'Money'
      expect(page).to have_content 'Teacher Commissions'
    end
  end
end

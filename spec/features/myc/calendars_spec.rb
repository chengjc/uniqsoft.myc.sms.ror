require 'rails_helper'

feature 'Calendar management' do
  scenario 'shows lesson events', js: true do
    # prepare admin
    studio = create(:studio)
    studio_admin = create(:user)

    # prepare teacher
    teacher_user = create(:user)
    teacher = create(:teacher, user_id: teacher_user.id)
    StudioUser.create(studio_id: studio.id, user_id: teacher_user.id, role_id: StudioUser.roles[:teacher])

    # prepare class and lesson
    program = create(:myc_program)

    myc_class = create(:myc_class, studio_id: studio.id, myc_program_id: program.id, start_date: Time.now, status: MycClass.statuses[:on_going])

    lesson = create(
        :lesson,
        myc_class_id: myc_class.id,
        teacher_id: teacher.id,
        lesson_no: 1,
        start_at: Time.now,
        status: Lesson.statuses[:marked]
    )
    log_in(role_id: StudioUser.roles[:studio_admin], user: studio_admin, studio_id: studio.id)
    find_link('Calendar').click
    expect(current_path).to eq '/myc/calendar'

    expect(page).to have_content 'Calendar'
    expect(page).to have_content "#{lesson.start_at.strftime('%l:%M %P')} #{program.program_code},#{teacher_user.nric_name}"
  end

  scenario 'shows holiday events', js: true do
    # prepare admin
    studio = create(:studio)
    studio_admin = create(:user)

    holiday = create(:holiday, studio_id: studio.id, holiday_date: Date.current)

    log_in(role_id: StudioUser.roles[:studio_admin], user: studio_admin, studio_id: studio.id)
    find_link('Calendar').click
    expect(current_path).to eq '/myc/calendar'

    expect(page).to have_content 'Calendar'
    expect(page).to have_content "#{holiday.holiday_name}"
  end
end

require 'rails_helper'

feature 'Student management' do
  context 'Student create and update', js:true do
    scenario 'shows highlight for required fields when creating student' do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      family = create(:family)
      find_link('People').click
      find_link('Students').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Students / New'
      end
      new_student_url = '/myc/students/new'
      expect(current_path).to eq new_student_url

      expect(page).to have_xpath("//select[@id='student_family_id' and @class='form-control error']")

      select family.primary_contact_name, from: 'student_family_id'

      find_link('Student Profile').click

      expect(page).to have_xpath("//select[@id='student_myc_program_id' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='student_nric_name' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='student_first_name' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='student_sur_name' and @class='form-control error']")
      expect(page).to have_xpath("//select[@id='student_date_of_birth_1i' and @class='error']")
      expect(page).to have_xpath("//select[@id='student_date_of_birth_2i' and @class='error']")
      expect(page).to have_xpath("//select[@id='student_date_of_birth_3i' and @class='error']")
      expect(page).to have_xpath("//select[@id='student_gender' and @class='form-control error']")
      expect(page).to have_xpath("//select[@id='student_status' and @class='form-control error']")
      expect(page).to have_xpath("//select[@id='student_referred_by' and @class='form-control error']")
    end

    scenario 'jump to family new page when click New Family Link and back to student new page when saving new family' do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      find_link('People').click
      find_link('Students').click
      find_link('New').click
      find_link('New Family').click

      within 'h2' do
        expect(page).to have_content 'Families / New'
      end
      new_families_url = '/myc/families/new'
      expect(current_path).to eq new_families_url

      expect{
        fill_in 'family_address1', with: 'No2'
        fill_in 'family_address2', with: '7 street'
        fill_in 'family_postal_code', with: 'AAD'
        fill_in 'family_city_or_state', with: 'MaYanGone'
        fill_in 'family_country', with: 'Myanmar'
        fill_in 'family_primary_contact_name', with: 'Jame'
        fill_in 'family_primary_contact_relationship', with: 'Father'
        fill_in 'family_primary_contact_phone_no', with: '123'
        fill_in 'family_primary_contact_email', with: 'jame@test.com'
        fill_in 'family_primary_contact_occupation', with: 'SSSSS'
        check 'family_primary_contact_accompany_for_student'
        fill_in 'family_other_contact_name', with: 'Michel'
        fill_in 'family_other_contact_relationship', with: 'Mother'
        fill_in 'family_other_contact_phone_no', with: '456'
        fill_in 'family_other_contact_email', with: 'michel@test.com'
        fill_in 'family_other_contact_occupation', with: 'GGGG'
        check 'family_other_contact_accompany_for_student'

        find_button('Save').click
        sleep 1
      }.to change { Family.count }.by(1)

      expect(current_path).to eq '/myc/students/new'
      within 'h2' do
        expect(page).to have_content 'Students / New'
      end
      expect(page).to have_content(I18n.t('save_success'))
    end

    scenario 'jump to family edit page when click Edit Family Link and back to student new page when saving edited family' do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      family = create(:family)
      find_link('People').click
      find_link('Students').click
      find_link('New').click
      select family.primary_contact_name, from: 'student_family_id'

      find_link('Edit Family').trigger('click')
      expect(current_path).to eq "/myc/families/#{family.id}/edit"
      within 'h2' do
        expect(page).to have_content 'Families / Edit'
      end

      fill_in 'family_primary_contact_name', with: 'Nicole'

      find_button('Save').click

      expect(current_path).to eq '/myc/students/new'
      within 'h2' do
        expect(page).to have_content 'Students / New'
      end
      expect(page).to have_content(I18n.t('save_success'))

      select 'Nicole', from: 'student_family_id'
    end

    scenario 'don\'t go to server when click save button after select family without filling student profile' do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      family = create(:family)
      find_link('People').click
      find_link('Students').click
      find_link('New').click
      select family.primary_contact_name, from: 'student_family_id'

      find_button('Save').click

      expect(page).to have_xpath("//select[@id='student_myc_program_id' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='student_nric_name' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='student_first_name' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='student_sur_name' and @class='form-control error']")
      expect(page).to have_xpath("//select[@id='student_date_of_birth_1i' and @class='error']")
      expect(page).to have_xpath("//select[@id='student_date_of_birth_2i' and @class='error']")
      expect(page).to have_xpath("//select[@id='student_date_of_birth_3i' and @class='error']")
      expect(page).to have_xpath("//select[@id='student_gender' and @class='form-control error']")
      expect(page).to have_xpath("//select[@id='student_status' and @class='form-control error']")
      expect(page).to have_xpath("//select[@id='student_referred_by' and @class='form-control error']")
    end

    scenario 'create student' do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      family = create(:family)
      myc_program = create(:myc_program)
      student = build(:student)
      student_status = 'Current'
      find_link('People').click
      find_link('Students').click
      find_link('New').click

      within 'h2' do
        expect(page).to have_content 'Students / New'
      end
      new_students_url = '/myc/students/new'
      expect(current_path).to eq new_students_url


      select family.primary_contact_name, from: 'student_family_id'

      find_link('Student Profile').click

      expect{
        select myc_program.program_name, from: 'student_myc_program_id'
        fill_in 'student_enrolment_piano', with: student.enrolment_piano
        fill_in 'student_enrolment_theory', with: student.enrolment_theory
        fill_in 'student_enrolment_other', with: student.enrolment_other
        select student.preferred_day_one, from: 'student_preferred_day_one'
        select student.preferred_day_two, from: 'student_preferred_day_two'
        select student.preferred_day_three, from: 'student_preferred_day_three'
        select student.preferred_time_one, from: 'student_preferred_time_one'
        select student.preferred_time_two, from: 'student_preferred_time_two'
        select student.preferred_time_three, from: 'student_preferred_time_three'
        fill_in 'student_nric_name', with: student.nric_name
        fill_in 'student_first_name', with: student.first_name
        fill_in 'student_sur_name', with: student.sur_name
        select student.gender, from: 'student_gender'
        select student.date_of_birth.year, from: 'student_date_of_birth_1i' #field id
        select Date::MONTHNAMES[student.date_of_birth.mon], from: 'student_date_of_birth_2i' #field id
        select student.date_of_birth.mday, from: 'student_date_of_birth_3i' #field id
        select student.referred_by, from: 'student_referred_by'
        page.execute_script("$('#student_enquiry_date').val('#{student.enquiry_date}')")
        page.execute_script("$('#student_registration_date').val('#{student.registration_date}')")
        select student_status, from: 'student_status'

        find_link('Music Background').click

        fill_in 'student_piano_standard', with: student.piano_standard
        select student.piano_college, from: 'student_piano_college'
        fill_in 'student_piano_school_attended', with: student.piano_school_attended
        fill_in 'student_theory_standard', with: student.theory_standard
        select student.theory_college, from: 'student_theory_college'
        fill_in 'student_theory_school_attended', with: student.theory_school_attended
        fill_in 'student_violin_standard', with: student.violin_standard
        select student.violin_college, from: 'student_violin_college'
        fill_in 'student_violin_school_attended', with: student.violin_school_attended
        fill_in 'student_other_standard', with: student.other_standard
        select student.other_college, from: 'student_other_college'
        fill_in 'student_other_school_attended', with: student.other_school_attended

        find_link('Instrument Owned').click

        check 'student_instrument_owned_has_piano'
        check 'student_instrument_owned_has_organ'
        check 'student_instrument_owned_has_keyboard'
        check 'student_instrument_owned_has_violin'
        fill_in 'student_instrument_owned_other', with: student.instrument_owned_other
        find_button('Save').click
        sleep 1
      }.to change { Student.count }.by(1)

      expect(current_path).to eq '/myc/students'
      within 'h2' do
        expect(page).to have_content 'Students'
      end
      expect(page).to have_content(I18n.t('save_success'))

      within('tr', :text => student.nric_name) do
        expect(page).to have_content(student.nric_name)
        expect(page).to have_content(student_status)
      end

      new_student_id = Student.where(nric_name: student.nric_name).pluck(:id).first

      within('tr', :text => student.nric_name) do
        find_link(student.nric_name).click
      end

      expect(current_path).to eq "/myc/students/#{new_student_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Students / Edit'
      end

      expect(page).to have_select('student_family_id', selected: family.primary_contact_name)
      find_link('Student Profile').click

      expect(page).to have_select('student_myc_program_id', selected: myc_program.program_name)
      expect(page).to have_field('student_enrolment_piano', with: student.enrolment_piano)
      expect(page).to have_field('student_enrolment_theory', with: student.enrolment_theory)
      expect(page).to have_field('student_enrolment_other', with: student.enrolment_other)
      expect(page).to have_select('student_preferred_day_one', selected: student.preferred_day_one)
      expect(page).to have_select('student_preferred_day_two', selected: student.preferred_day_two)
      expect(page).to have_select('student_preferred_day_three', selected: student.preferred_day_three)
      expect(page).to have_select('student_preferred_time_one', selected: student.preferred_time_one)
      expect(page).to have_select('student_preferred_time_two', selected: student.preferred_time_two)
      expect(page).to have_select('student_preferred_time_three', selected: student.preferred_time_three)
      expect(page).to have_field('student_nric_name', with: student.nric_name)
      expect(page).to have_field('student_first_name', with: student.first_name)
      expect(page).to have_field('student_sur_name', with: student.sur_name)
      expect(page).to have_select('student_gender', selected: student.gender)
      expect(page).to have_select('student[date_of_birth(1i)]', selected: student.date_of_birth.year.to_s)
      expect(page).to have_select('student[date_of_birth(2i)]', selected: Date::MONTHNAMES[student.date_of_birth.mon])
      expect(page).to have_select('student[date_of_birth(3i)]', selected: student.date_of_birth.mday.to_s)
      expect(page).to have_select('student_referred_by', selected: student.referred_by)
      expect(page).to have_field('student_enquiry_date', with: student.enquiry_date)
      expect(page).to have_field('student_registration_date', with: student.registration_date)
      expect(page).to have_select('student_status', selected: student_status)

      find_link('Music Background').click

      expect(page).to have_field('student_piano_standard', with: student.piano_standard)
      expect(page).to have_select('student_piano_college', selected: student.piano_college)
      expect(page).to have_field('student_piano_school_attended', with: student.piano_school_attended)
      expect(page).to have_field('student_theory_standard', with: student.theory_standard)
      expect(page).to have_select('student_theory_college', selected: student.theory_college)
      expect(page).to have_field('student_theory_school_attended', with: student.theory_school_attended)
      expect(page).to have_field('student_violin_standard', with: student.violin_standard)
      expect(page).to have_select('student_violin_college', selected: student.violin_college)
      expect(page).to have_field('student_violin_school_attended', with: student.violin_school_attended)
      expect(page).to have_field('student_other_standard', with: student.other_standard)
      expect(page).to have_select('student_other_college', selected: student.other_college)
      expect(page).to have_field('student_other_school_attended', with: student.other_school_attended)

      find_link('Instrument Owned').click

      expect(page).to have_checked_field('student_instrument_owned_has_piano') if student.instrument_owned_has_piano
      expect(page).to have_checked_field('student_instrument_owned_has_organ') if student.instrument_owned_has_organ
      expect(page).to have_checked_field('student_instrument_owned_has_keyboard') if student.instrument_owned_has_keyboard
      expect(page).to have_checked_field('student_instrument_owned_has_violin') if student.instrument_owned_has_violin
      expect(page).to have_field('student_instrument_owned_other', with: student.instrument_owned_other)

    end

    scenario 'create student with the existing nric_name and date_of_birth' do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      family = create(:family)
      myc_program = create(:myc_program)
      student = create(:student)
      new_student = build(:new_student)
      student_status = 'Current'
      find_link('People').click
      find_link('Students').click
      find_link('New').click

      within 'h2' do
        expect(page).to have_content 'Students / New'
      end
      new_students_url = '/myc/students/new'
      expect(current_path).to eq new_students_url

      select family.primary_contact_name, from: 'student_family_id'

      find_link('Student Profile').click

      expect{
        select myc_program.program_name, from: 'student_myc_program_id'
        fill_in 'student_enrolment_piano', with: new_student.enrolment_piano
        fill_in 'student_enrolment_theory', with: new_student.enrolment_theory
        fill_in 'student_enrolment_other', with: new_student.enrolment_other
        select new_student.preferred_day_one, from: 'student_preferred_day_one'
        select new_student.preferred_day_two, from: 'student_preferred_day_two'
        select new_student.preferred_day_three, from: 'student_preferred_day_three'
        select new_student.preferred_time_one, from: 'student_preferred_time_one'
        select new_student.preferred_time_two, from: 'student_preferred_time_two'
        select new_student.preferred_time_three, from: 'student_preferred_time_three'
        fill_in 'student_nric_name', with: student.nric_name
        fill_in 'student_first_name', with: new_student.first_name
        fill_in 'student_sur_name', with: new_student.sur_name
        select new_student.gender, from: 'student_gender'
        select student.date_of_birth.year, from: 'student_date_of_birth_1i' #field id
        select Date::MONTHNAMES[student.date_of_birth.mon], from: 'student_date_of_birth_2i' #field id
        select student.date_of_birth.mday, from: 'student_date_of_birth_3i' #field id
        select new_student.referred_by, from: 'student_referred_by'
        page.execute_script("$('#student_enquiry_date').val('#{new_student.enquiry_date}')")
        page.execute_script("$('#student_registration_date').val('#{new_student.registration_date}')")
        select student_status, from: 'student_status'

        find_link('Music Background').click

        fill_in 'student_piano_standard', with: new_student.piano_standard
        select new_student.piano_college, from: 'student_piano_college'
        fill_in 'student_piano_school_attended', with: new_student.piano_school_attended
        fill_in 'student_theory_standard', with: new_student.theory_standard
        select new_student.theory_college, from: 'student_theory_college'
        fill_in 'student_theory_school_attended', with: new_student.theory_school_attended
        fill_in 'student_violin_standard', with: new_student.violin_standard
        select new_student.violin_college, from: 'student_violin_college'
        fill_in 'student_violin_school_attended', with: new_student.violin_school_attended
        fill_in 'student_other_standard', with: new_student.other_standard
        select new_student.other_college, from: 'student_other_college'
        fill_in 'student_other_school_attended', with: new_student.other_school_attended

        find_link('Instrument Owned').click

        check 'student_instrument_owned_has_piano'
        check 'student_instrument_owned_has_organ'
        check 'student_instrument_owned_has_keyboard'
        check 'student_instrument_owned_has_violin'
        fill_in 'student_instrument_owned_other', with: new_student.instrument_owned_other
        find_button('Save').click
        sleep 1
      }.to change { Student.count }.by(0)

      expect(current_path).to eq '/myc/students'
      within 'h2' do
        expect(page).to have_content 'Students / New'
      end
      expect(page).to have_content(I18n.t('save_fail'))
      expect(page).to have_content('Nric name has already been taken')
    end

    scenario 'update student' do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      family = create(:family)
      new_family = create(:family_new)
      myc_program = create(:myc_program)
      new_myc_program = create(:new_myc_program, status: MycProgram.statuses[:active])
      student = create(:student)
      new_student = build(:new_student)
      student_status = 'Current'
      new_student_status = 'Terminated'
      find_link('People').click
      find_link('Students').click

      within('tr', :text => student.nric_name) do
        expect(page).to have_content(student.nric_name)
        expect(page).to have_content(student_status)
      end

      new_student_id = Student.where(nric_name: student.nric_name).pluck(:id).first

      within('tr', :text => student.nric_name) do
        find_link(student.nric_name).click
      end

      expect(current_path).to eq "/myc/students/#{new_student_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Students / Edit'
      end

      select new_family.primary_contact_name, from: 'student_family_id'

      find_link('Student Profile').click

      expect{
        select new_myc_program.program_name, from: 'student_myc_program_id'
        fill_in 'student_enrolment_piano', with: new_student.enrolment_piano
        fill_in 'student_enrolment_theory', with: new_student.enrolment_theory
        fill_in 'student_enrolment_other', with: new_student.enrolment_other
        select new_student.preferred_day_one, from: 'student_preferred_day_one'
        select new_student.preferred_day_two, from: 'student_preferred_day_two'
        select new_student.preferred_day_three, from: 'student_preferred_day_three'
        select new_student.preferred_time_one, from: 'student_preferred_time_one'
        select new_student.preferred_time_two, from: 'student_preferred_time_two'
        select new_student.preferred_time_three, from: 'student_preferred_time_three'
        fill_in 'student_nric_name', with: new_student.nric_name
        fill_in 'student_first_name', with: new_student.first_name
        fill_in 'student_sur_name', with: new_student.sur_name
        select new_student.gender, from: 'student_gender'
        select new_student.date_of_birth.year, from: 'student_date_of_birth_1i' #field id
        select Date::MONTHNAMES[new_student.date_of_birth.mon], from: 'student_date_of_birth_2i' #field id
        select new_student.date_of_birth.mday, from: 'student_date_of_birth_3i' #field id
        select new_student.referred_by, from: 'student_referred_by'
        page.execute_script("$('#student_enquiry_date').val('#{new_student.enquiry_date}')")
        page.execute_script("$('#student_registration_date').val('#{new_student.registration_date}')")
        select new_student_status, from: 'student_status'

        find_link('Music Background').click

        fill_in 'student_piano_standard', with: new_student.piano_standard
        select new_student.piano_college, from: 'student_piano_college'
        fill_in 'student_piano_school_attended', with: new_student.piano_school_attended
        fill_in 'student_theory_standard', with: new_student.theory_standard
        select new_student.theory_college, from: 'student_theory_college'
        fill_in 'student_theory_school_attended', with: new_student.theory_school_attended
        fill_in 'student_violin_standard', with: new_student.violin_standard
        select new_student.violin_college, from: 'student_violin_college'
        fill_in 'student_violin_school_attended', with: new_student.violin_school_attended
        fill_in 'student_other_standard', with: new_student.other_standard
        select new_student.other_college, from: 'student_other_college'
        fill_in 'student_other_school_attended', with: new_student.other_school_attended

        find_link('Instrument Owned').click

        uncheck 'student_instrument_owned_has_piano'
        uncheck 'student_instrument_owned_has_organ'
        uncheck 'student_instrument_owned_has_keyboard'
        uncheck 'student_instrument_owned_has_violin'
        fill_in 'student_instrument_owned_other', with: new_student.instrument_owned_other
        find_button('Save').click
        sleep 1
      }.to change { Student.count }.by(0)

      expect(current_path).to eq '/myc/students'
      within 'h2' do
        expect(page).to have_content 'Students'
      end
      expect(page).to have_content(I18n.t('save_success'))

      within('tr', :text => new_student.nric_name) do
        expect(page).to have_content(new_student.nric_name)
        expect(page).to have_content(new_student_status)
      end

      # check updated data with the original data
      new_student_id = Student.where(nric_name: new_student.nric_name).pluck(:id).first

      within('tr', :text => new_student.nric_name) do
        find_link(new_student.nric_name).click
      end

      expect(current_path).to eq "/myc/students/#{new_student_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Students / Edit'
      end

      expect(page).to have_select('student_family_id', selected: new_family.primary_contact_name)
      find_link('Student Profile').click

      expect(page).to have_select('student_myc_program_id', selected: new_myc_program.program_name)
      expect(page).to have_field('student_enrolment_piano', with: new_student.enrolment_piano)
      expect(page).to have_field('student_enrolment_theory', with: new_student.enrolment_theory)
      expect(page).to have_field('student_enrolment_other', with: new_student.enrolment_other)
      expect(page).to have_select('student_preferred_day_one', selected: new_student.preferred_day_one)
      expect(page).to have_select('student_preferred_day_two', selected: new_student.preferred_day_two)
      expect(page).to have_select('student_preferred_day_three', selected: new_student.preferred_day_three)
      expect(page).to have_select('student_preferred_time_one', selected: new_student.preferred_time_one)
      expect(page).to have_select('student_preferred_time_two', selected: new_student.preferred_time_two)
      expect(page).to have_select('student_preferred_time_three', selected: new_student.preferred_time_three)
      expect(page).to have_field('student_nric_name', with: new_student.nric_name)
      expect(page).to have_field('student_first_name', with: new_student.first_name)
      expect(page).to have_field('student_sur_name', with: new_student.sur_name)
      expect(page).to have_select('student_gender', selected: new_student.gender)
      expect(page).to have_select('student[date_of_birth(1i)]', selected: new_student.date_of_birth.year.to_s)
      expect(page).to have_select('student[date_of_birth(2i)]', selected: Date::MONTHNAMES[new_student.date_of_birth.mon])
      expect(page).to have_select('student[date_of_birth(3i)]', selected: new_student.date_of_birth.mday.to_s)
      expect(page).to have_select('student_referred_by', selected: new_student.referred_by)
      expect(page).to have_field('student_enquiry_date', with: new_student.enquiry_date)
      expect(page).to have_field('student_registration_date', with: new_student.registration_date)
      expect(page).to have_select('student_status', selected: new_student_status)

      find_link('Music Background').click

      expect(page).to have_field('student_piano_standard', with: new_student.piano_standard)
      expect(page).to have_select('student_piano_college', selected: new_student.piano_college)
      expect(page).to have_field('student_piano_school_attended', with: new_student.piano_school_attended)
      expect(page).to have_field('student_theory_standard', with: new_student.theory_standard)
      expect(page).to have_select('student_theory_college', selected: new_student.theory_college)
      expect(page).to have_field('student_theory_school_attended', with: new_student.theory_school_attended)
      expect(page).to have_field('student_violin_standard', with: new_student.violin_standard)
      expect(page).to have_select('student_violin_college', selected: new_student.violin_college)
      expect(page).to have_field('student_violin_school_attended', with: new_student.violin_school_attended)
      expect(page).to have_field('student_other_standard', with: new_student.other_standard)
      expect(page).to have_select('student_other_college', selected: new_student.other_college)
      expect(page).to have_field('student_other_school_attended', with: new_student.other_school_attended)

      find_link('Instrument Owned').click

      expect(page).to have_checked_field('student_instrument_owned_has_piano') if new_student.instrument_owned_has_piano
      expect(page).to have_checked_field('student_instrument_owned_has_organ') if new_student.instrument_owned_has_organ
      expect(page).to have_checked_field('student_instrument_owned_has_keyboard') if new_student.instrument_owned_has_keyboard
      expect(page).to have_checked_field('student_instrument_owned_has_violin') if new_student.instrument_owned_has_violin
      expect(page).to have_field('student_instrument_owned_other', with: new_student.instrument_owned_other)

    end
  end
end
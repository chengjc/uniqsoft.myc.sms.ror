require 'rails_helper'

feature 'Invoice management' do
  context 'Invoice create and update' do

    scenario 'create invoice with in class student', js: true  do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)

      # create class with student
      family = create(:family)
      student = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      myc_program = create(:myc_program, program_name: 'Sunrise')
      myc_level = create(:myc_level, level_name: '1')
      teacher = create(:teacher)
      myc_class = create_class_with_add_student(myc_program, myc_level, teacher, student)

      # create invoice
      within 'ul#side-menu' do
        find_link('Money').click
      end
      find_link('Invoices').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Invoices / New'
      end
      new_invoices_url = '/myc/invoices/new'
      latest_invoice_number = '1001'
      expect(current_path).to eq new_invoices_url

        expect(page).to have_field('invoice_invoice_number', with: latest_invoice_number)
        expect(page).to have_field('invoice_invoice_on', with: Date.today)
        expect(page).to have_field('invoice_due_on', with: Date.today)
        expect(page).to have_field('invoice_status', with: Invoice.statuses.keys[Invoice.statuses[:open]])

      find_link('Add Student').click
      within ('div#modal') do
        find_link('Existing Student').click
        within('h4') do
          expect(page).to have_content('Student')
        end

        within('table#invoice-existing-students-table tr', :text => student.nric_name) do
          expect(page).to have_content(myc_program.program_name+' '+myc_level.level_name)
          find(:css, "input[value='#{student.id}']").set(true)
        end

        find_button('Save').click
      end

        expect(page).to have_field('student_name', with: student.nric_name)
        expect(page).to have_field('invoice_program_name', with: myc_program.program_name+' '+myc_level.level_name)
        expect(page).to have_field('invoice_teacher_name', with: teacher.user.nric_name)
        expect(page).to have_field('invoice_day', with: myc_class.start_at.strftime("%I:%M %p"))
        expect(page).to have_field('invoice_time', with: myc_class.start_at.strftime("%a"))

        find_button('Save').click

        expect(page).to have_content 'Invoices / Edit'

      # edit to confirm if data was saved
      expect(page).to have_field('student_name', with: student.nric_name)
      expect(page).to have_field('invoice_program_name', with: myc_program.program_name+' '+myc_level.level_name)
      expect(page).to have_field('invoice_teacher_name', with: teacher.user.nric_name)
      expect(page).to have_field('invoice_day', with: myc_class.start_at.strftime("%I:%M %p"))
      expect(page).to have_field('invoice_time', with: myc_class.start_at.strftime("%a"))
      expect(page).to have_field('invoice_invoice_number', with: latest_invoice_number)
      expect(page).to have_field('invoice_invoice_on', with: Date.today)
      expect(page).to have_field('invoice_due_on', with: Date.today)
      expect(page).to have_field('invoice_status', with: Invoice.statuses.keys[Invoice.statuses[:open]])

    end

    scenario 'create invoice with in new student', js: true  do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)

      # create class with student
      family = create(:family)
      student = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      myc_program = create(:myc_program, program_name: 'Sunrise')
      myc_level = create(:myc_level, level_name: '1')

      # create invoice
      within 'ul#side-menu' do
        find_link('Money').click
      end
      find_link('Invoices').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Invoices / New'
      end
      new_invoices_url = '/myc/invoices/new'
      latest_invoice_number = '1001'
      expect(current_path).to eq new_invoices_url

      expect(page).to have_field('invoice_invoice_number', with: latest_invoice_number)
      expect(page).to have_field('invoice_invoice_on', with: Date.today)
      expect(page).to have_field('invoice_due_on', with: Date.today)
      expect(page).to have_field('invoice_status', with: Invoice.statuses.keys[Invoice.statuses[:open]])

      find_link('Add Student').click
      within ('div#modal') do
        within('h4') do
          expect(page).to have_content('Student')
        end

        within('table#invoice-new-students-table tr', :text => student.nric_name) do
          expect(page).to have_content(myc_program.program_name+' '+myc_level.level_name)
          find(:css, "input[value='#{student.id}']").set(true)
        end

        find_button('Save').click
      end

      expect(page).to have_field('student_name', with: student.nric_name)
      expect(page).to have_field('invoice_program_name', with: myc_program.program_name+' '+myc_level.level_name)

      find_button('Save').click

      expect(page).to have_content 'Invoices / Edit'

      # edit to confirm if data was saved
      expect(page).to have_field('student_name', with: student.nric_name)
      expect(page).to have_field('invoice_program_name', with: myc_program.program_name+' '+myc_level.level_name)
      expect(page).to have_field('invoice_invoice_number', with: latest_invoice_number)
      expect(page).to have_field('invoice_invoice_on', with: Date.today)
      expect(page).to have_field('invoice_due_on', with: Date.today)
      expect(page).to have_field('invoice_status', with: Invoice.statuses.keys[Invoice.statuses[:open]])

    end

    scenario 'edit invoice and add invoice item', js: true  do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)

      invoice = create(:invoice)
      within 'ul#side-menu' do
        find_link('Money').click
      end
      find_link('Invoices').click

      within('h2') do
        expect(page).to have_content('Invoices')
      end

      within('tr', :text => invoice.invoice_number) do
        find_link(invoice.invoice_number).click
      end
      find_link('Add').click
      find_link('Program').click

      within ('div#modal') do
        within('h4') do
          expect(page).to have_content('Program')
        end

        within('table#invoice-new-students-table tr', :text => student.nric_name) do
          expect(page).to have_content(myc_program.program_name+' '+myc_level.level_name)
          find(:css, "input[value='#{student.id}']").set(true)
        end

        find_button('Save').click
      end

    end

  end

  private
  def create_class_with_add_student(myc_program, myc_level, teacher, student)
    within 'ul#side-menu' do
      find_link('Classes').click
    end

    find_link('New').click

    start_at = Time.now

    fill_in 'myc_class_start_date', with: start_at.strftime("%d/%m/%y")
    fill_in 'myc_class_start_time', with: start_at.strftime("%I:%M %p")
    select myc_program.program_name+' '+myc_level.level_name, from: 'myc_class_myc_level_id'
    select teacher.user.nric_name, from: 'myc_class_teacher_id'

    find_button('Save').click
    sleep 1

    find_link('Add Student').click

    within ('div#modal') do
      find(:css, "input[value='#{student.id}']").set(true)

      find_button('Save').click
      sleep 1
    end

    myc_class = MycClass.find_by(myc_level_id: myc_level.id)
    return myc_class
  end

end
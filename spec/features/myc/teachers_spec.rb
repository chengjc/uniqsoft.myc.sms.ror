require 'rails_helper'

feature 'Teacher management' do
  scenario 'views profile with filter search', js: true do
    # create a studio for teacher
    studio = create(:studio)

    # create a studio admin
    studio_admin_user = create(:user)

    # create a new teacher user
    teacher_user = create(:user)
    teacher_profile = create(:teacher, user_id: teacher_user.id)
    log_in(role_id: StudioUser.roles[:super_user])

    # assign to studio with role teacher to teacher-user
    find_link('Users').click
    within('tr', :text => teacher_user.nric_name) do
      find_by_id('btn-action').click
      find_link('Assign studios').click
    end

    find_link('Add studio').click
    select studio.studio_name, from: 'studio_user_studio_id'
    select StudioUser.role_name(StudioUser.roles[:teacher]), from: 'studio_user_role_id'
    find_button('Add').click

    # assign to studio with role admin to admin user
    find_link('Cancel').click
    # sleep 1
    find_link('Users').click
    # sleep 1

    within('tr', :text => studio_admin_user.nric_name) do
      find_by_id('btn-action').click
      find_link('Assign studios').click
    end

    find_link('Add studio').click
    select studio.studio_name, from: 'studio_user_studio_id'
    select StudioUser.role_name(StudioUser.roles[:studio_admin]), from: 'studio_user_role_id'
    find_button('Add').click
    # sleep 1
    find_link('Logout').click

    # login as studio admin
    visit '/users/sign_in'
    fill_in 'Email', with: studio_admin_user.email
    fill_in 'Password', with: studio_admin_user.password
    click_button 'Log in'

    # visit myc teacher
    find_link('People').click
    find_link('Teachers').click
    # sleep 1

    within 'h2' do
      expect(page).to have_content 'Teachers'
    end
    expect(current_path).to eq "/myc/teachers/"

    within('tr', text: teacher_user.email) do
      expect(page).to have_content teacher_user.nric_name
      expect(page).to have_content teacher_user.phone_number
      expect(page).to have_content teacher_user.email
      expect(page).to have_content teacher_user.status.humanize
      expect(page).to have_content teacher_profile.date_of_myc_l1
      expect(page).to have_content teacher_profile.date_of_myc_l2mb2
      expect(page).to have_content teacher_profile.date_of_myc_l2ss2
      expect(page).to have_content teacher_profile.date_of_myc_l3sb3
      expect(page).to have_content teacher_profile.date_of_myc_l2sb2
      expect(page).to have_content teacher_profile.date_of_myc_l3mb3
      expect(page).to have_content teacher_profile.date_of_1st_teacher_visit
      expect(page).to have_content teacher_profile.date_of_2nd_teacher_visit
      expect(page).to have_content teacher_profile.date_of_3rd_teacher_visit
    end

    # Filter Inactive status
    find_link('Filter').click
    select 'Inactive', from: 'status-select'
    find_button('Search').click
    # sleep 1
    expect(page).to_not have_content teacher_user.nric_name

    # Filter active status
    find_link('Filter').click
    select 'Active', from: 'status-select'
    find_button('Search').click
    # sleep 1
    expect(page).to have_content teacher_user.nric_name
  end
end
require 'rails_helper'

feature 'Family management' do
  context 'Family create and update' do
    scenario 'shows highlight for required fields when creating family', js: true do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      find_link('People').click
      find_link('Families').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Families / New'
      end
      new_family_url = '/myc/families/new'
      expect(current_path).to eq new_family_url

      expect(page).to have_xpath("//input[@id='family_address1' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='family_city_or_state' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='family_country' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='family_primary_contact_name' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='family_primary_contact_relationship' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='family_primary_contact_phone_no' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='family_primary_contact_email' and @class='form-control error']")
    end

    scenario 'create family with valid input', js: true  do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      find_link('People').click
      find_link('Families').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Families / New'
      end
      new_families_url = '/myc/families/new'
      family = build(:family)
      family_status = 'Active'
      expect(current_path).to eq new_families_url

      expect{
        fill_in 'family_address1', with: family.address1
        fill_in 'family_address2', with: family.address2
        fill_in 'family_postal_code', with: family.postal_code
        fill_in 'family_city_or_state', with: family.city_or_state
        fill_in 'family_country', with: family.country
        fill_in 'family_primary_contact_name', with: family.primary_contact_name
        fill_in 'family_primary_contact_relationship', with: family.primary_contact_relationship
        fill_in 'family_primary_contact_phone_no', with: family.primary_contact_phone_no
        fill_in 'family_primary_contact_email', with: family.primary_contact_email
        fill_in 'family_primary_contact_occupation', with: family.primary_contact_occupation
        check 'family_primary_contact_accompany_for_student'
        fill_in 'family_other_contact_name', with: family.other_contact_name
        fill_in 'family_other_contact_relationship', with: family.other_contact_relationship
        fill_in 'family_other_contact_phone_no', with: family.other_contact_phone_no
        fill_in 'family_other_contact_email', with: family.other_contact_email
        fill_in 'family_other_contact_occupation', with: family.other_contact_occupation
        check 'family_other_contact_accompany_for_student'
        select family_status,  from: 'family_status'

        find_button('Save').click
        sleep 1
      }.to change { Family.count }.by(1)

      expect(current_path).to eq '/myc/families/'
      within 'h2' do
        expect(page).to have_content 'Families'
      end
      expect(page).to have_content(I18n.t('save_success'))

      within('tr', :text => family.primary_contact_name) do
        expect(page).to have_content(family.primary_contact_name)
        expect(page).to have_content(family.primary_contact_relationship)
        expect(page).to have_content(family.primary_contact_email)
        expect(page).to have_content(family.primary_contact_phone_no)
        expect(page).to have_content(family_status)
      end

      # edit to confirm if data was saved
      new_family_id = Family.where(primary_contact_name: family.primary_contact_name).pluck(:id).first

      within('tr', :text => family.primary_contact_name) do
        find_link(family.primary_contact_name).click
      end

      expect(current_path).to eq "/myc/families/#{new_family_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Families / Edit'
      end

      expect(page).to have_field('family_address1', with: family.address1)
      expect(page).to have_field('family_address2', with: family.address2)
      expect(page).to have_field('family_postal_code', with: family.postal_code)
      expect(page).to have_field('family_city_or_state', with: family.city_or_state)
      expect(page).to have_field('family_country', with: family.country)
      expect(page).to have_field('family_primary_contact_name', with: family.primary_contact_name)
      expect(page).to have_field('family_primary_contact_relationship', with: family.primary_contact_relationship)
      expect(page).to have_field('family_primary_contact_phone_no', with: family.primary_contact_phone_no)
      expect(page).to have_field('family_primary_contact_email', with: family.primary_contact_email)
      expect(page).to have_field('family_primary_contact_occupation', with: family.primary_contact_occupation)
      expect(page).to have_checked_field('family_primary_contact_accompany_for_student')if family.primary_contact_accompany_for_student
      expect(page).to have_field('family_other_contact_name', with: family.other_contact_name)
      expect(page).to have_field('family_other_contact_relationship', with: family.other_contact_relationship)
      expect(page).to have_field('family_other_contact_phone_no', with: family.other_contact_phone_no)
      expect(page).to have_field('family_other_contact_email', with: family.other_contact_email)
      expect(page).to have_field('family_other_contact_occupation', with: family.other_contact_occupation)
      expect(page).to have_checked_field('family_other_contact_accompany_for_student')if family.other_contact_accompany_for_student
      expect(page).to have_select('family_status', selected: family_status)
    end

    scenario 'create family without checking Accompany for student ', js: true  do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      find_link('People').click
      find_link('Families').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Families / New'
      end
      family = build(:family)
      new_families_url = '/myc/families/new'
      expect(current_path).to eq new_families_url

      expect{
        fill_in 'family_address1', with: family.address1
        fill_in 'family_address2', with: family.address2
        fill_in 'family_postal_code', with: family.postal_code
        fill_in 'family_city_or_state', with: family.city_or_state
        fill_in 'family_country', with: family.country
        fill_in 'family_primary_contact_name', with: family.primary_contact_name
        fill_in 'family_primary_contact_relationship', with: family.primary_contact_relationship
        fill_in 'family_primary_contact_phone_no', with: family.primary_contact_phone_no
        fill_in 'family_primary_contact_email', with: family.primary_contact_email
        fill_in 'family_primary_contact_occupation', with: family.primary_contact_occupation
        fill_in 'family_other_contact_name', with: family.other_contact_name
        fill_in 'family_other_contact_relationship', with: family.other_contact_relationship
        fill_in 'family_other_contact_phone_no', with: family.other_contact_phone_no
        fill_in 'family_other_contact_email', with: family.other_contact_email
        fill_in 'family_other_contact_occupation', with: family.other_contact_occupation

        find_button('Save').click
        sleep 1
      }.to change { Family.count }.by(0)

      expect(current_path).to eq '/myc/families'
      within 'h2' do
        expect(page).to have_content 'Families / New'
      end
      expect(page).to have_content(I18n.t('save_fail'))
      expect(page).to have_content(I18n.t('check_accompany_for_student'))

    end

    scenario 'update family ', js: true  do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      family = create(:family)
      new_family = build(:family_new)
      new_family_status = 'Deleted'
      find_link('People').click
      find_link('Families').click

      within_table 'families-table' do
        find_link(family.primary_contact_name).click
      end

      within 'h2' do
        expect(page).to have_content 'Families / Edit'
      end

      expect{
        fill_in 'family_address1', with: new_family.address1
        fill_in 'family_address2', with: new_family.address2
        fill_in 'family_postal_code', with: new_family.postal_code
        fill_in 'family_city_or_state', with: new_family.city_or_state
        fill_in 'family_country', with: new_family.country
        fill_in 'family_primary_contact_name', with: new_family.primary_contact_name
        fill_in 'family_primary_contact_relationship', with: new_family.primary_contact_relationship
        fill_in 'family_primary_contact_phone_no', with: new_family.primary_contact_phone_no
        fill_in 'family_primary_contact_email', with: new_family.primary_contact_email
        fill_in 'family_primary_contact_occupation', with: new_family.primary_contact_occupation
        check 'family_primary_contact_accompany_for_student'
        fill_in 'family_other_contact_name', with: new_family.other_contact_name
        fill_in 'family_other_contact_relationship', with: new_family.other_contact_relationship
        fill_in 'family_other_contact_phone_no', with: new_family.other_contact_phone_no
        fill_in 'family_other_contact_email', with: new_family.other_contact_email
        fill_in 'family_other_contact_occupation', with: new_family.other_contact_occupation
        check 'family_other_contact_accompany_for_student'
        select new_family_status, from: 'family_status'

        find_button('Save').click
        sleep 1
      }.to change { Family.count }.by(0)

      expect(current_path).to eq '/myc/families/'
      within 'h2' do
        expect(page).to have_content 'Families'
      end
      expect(page).to have_content(I18n.t('save_success'))

      # edit to confirm if data was saved
      new_family_id = Family.where(primary_contact_name: new_family.primary_contact_name).pluck(:id).first

      find_link('Filter').click
      select new_family_status, from: 'status-select'
      find_button('Search').click

      within_table 'families-table' do
        find_link(new_family.primary_contact_name).click
      end

      expect(current_path).to eq "/myc/families/#{new_family_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Families / Edit'
      end

      expect(page).to have_field('family_address1', with: new_family.address1)
      expect(page).to have_field('family_address2', with: new_family.address2)
      expect(page).to have_field('family_postal_code', with: new_family.postal_code)
      expect(page).to have_field('family_city_or_state', with: new_family.city_or_state)
      expect(page).to have_field('family_country', with: new_family.country)
      expect(page).to have_field('family_primary_contact_name', with: new_family.primary_contact_name)
      expect(page).to have_field('family_primary_contact_relationship', with: new_family.primary_contact_relationship)
      expect(page).to have_field('family_primary_contact_phone_no', with: new_family.primary_contact_phone_no)
      expect(page).to have_field('family_primary_contact_email', with: new_family.primary_contact_email)
      expect(page).to have_field('family_primary_contact_occupation', with: new_family.primary_contact_occupation)
      expect(page).to have_checked_field('family_primary_contact_accompany_for_student')if new_family.primary_contact_accompany_for_student
      expect(page).to have_field('family_other_contact_name', with: new_family.other_contact_name)
      expect(page).to have_field('family_other_contact_relationship', with: new_family.other_contact_relationship)
      expect(page).to have_field('family_other_contact_phone_no', with: new_family.other_contact_phone_no)
      expect(page).to have_field('family_other_contact_email', with: new_family.other_contact_email)
      expect(page).to have_field('family_other_contact_occupation', with: new_family.other_contact_occupation)
      expect(page).to have_checked_field('family_other_contact_accompany_for_student')if new_family.other_contact_accompany_for_student
      expect(page).to have_select('family_status', selected: new_family_status)

      find_link('Cancel').click

        find_link('Filter').click
        select new_family_status, from: 'status-select'
        find_button('Search').click

      within('tr', :text => new_family.primary_contact_name) do
        expect(page).to have_content(new_family.primary_contact_name)
        expect(page).to have_content(new_family.primary_contact_relationship)
        expect(page).to have_content(new_family.primary_contact_email)
        expect(page).to have_content(new_family.primary_contact_phone_no)
        expect(page).to have_content(new_family_status)
      end

    end
  end
end


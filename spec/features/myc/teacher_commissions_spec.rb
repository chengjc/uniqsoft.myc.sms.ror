require 'rails_helper'

feature 'Teacher commissions management' do

  scenario 'generates commission', js: true do
    # prepare admin
    studio = create(:studio)
    studio_admin = create(:user)

    # prepare teacher
    teacher_user = create(:user)
    teacher = create(:teacher, user_id: teacher_user.id)
    StudioUser.create(studio_id: studio.id, user_id: teacher_user.id, role_id: StudioUser.roles[:teacher])

    # prepare teacher 2
    teacher_user_2 = create(:user)
    teacher_2 = create(:teacher, user_id: teacher_user_2.id)
    StudioUser.create(studio_id: studio.id, user_id: teacher_user_2.id, role_id: StudioUser.roles[:studio_admin])

    # prepare student, class and lessons
    student = create(:student)

    program = create(:myc_program)

    myc_class = create(:myc_class, studio_id: studio.id, myc_program_id: program.id, start_date: Time.now, status: MycClass.statuses[:on_going])
    myc_class_next_month = create(:myc_class, studio_id: studio.id, myc_program_id: program.id, start_date: Time.now.next_month, status: MycClass.statuses[:on_going])
    myc_class_last_month = create(:myc_class, studio_id: studio.id, myc_program_id: program.id, start_date: Time.now.last_month, status: MycClass.statuses[:on_going])

    lesson_current_month_teacher_1 = create(
        :lesson,
        myc_class_id: myc_class.id,
        teacher_id: teacher.id,
        lesson_no: 1,
        start_at: Time.now,
        status: Lesson.statuses[:marked]
    )

    # test if it will include other teacher
    lesson_current_month_teacher_2 = create(
        :lesson,
        myc_class_id: myc_class.id,
        teacher_id: teacher_2.id,
        lesson_no: 1,
        start_at: Time.now,
        status: Lesson.statuses[:marked]
    )

    # test it should not include next month
    create(
        :lesson,
        myc_class_id: myc_class_next_month.id,
        teacher_id: teacher.id,
        lesson_no: 1,
        start_at: Time.now.next_month,
        status: Lesson.statuses[:marked]
    )

    # test it should not include last month
    create(
        :lesson,
        myc_class_id: myc_class_last_month.id,
        teacher_id: teacher.id,
        lesson_no: 1,
        start_at: Time.now.last_month,
        status: Lesson.statuses[:marked]
    )

    create(
        :attendance,
        lesson_id: lesson_current_month_teacher_1.id,
        student_id: student.id,
        status: Attendance.statuses[:null]
    )

    # test if it should include other teacher
    create(
        :attendance,
        lesson_id: lesson_current_month_teacher_2.id,
        student_id: student.id,
        status: Attendance.statuses[:null]
    )

    # test case
    # commission for current month / teacher / student = lesson fee x no of lessons x percentage / 100
    lesson_fee = program.lesson_fee_per_term / program.lesson_per_term
    commission = lesson_fee * 1 * Rails.configuration.teacher_commission_percentage / 100

    # test case
    # new percentage
    percentage_new = 99
    commission_new_percentage = lesson_fee * 1 * percentage_new / 100

    # test case
    # new remarks
    remarks_new = 'this is a new remarks'

    # test case
    # add additional item
    additional_item = build(:teacher_commission_item_additional)
    commission_additional_item = (additional_item.lesson_fee * additional_item.no_of_lesson * additional_item.percentage / 100)

    # test case
    # edit additional item
    additional_item_new = build(:teacher_commission_item_additional_new)
    commission_additional_item_new = (additional_item_new.lesson_fee * additional_item_new.no_of_lesson * additional_item_new.percentage / 100)

    # test 5
    # add others
    other_item = build(:teacher_commission_other)

    # test 5
    # edit others
    other_item_new = build(:teacher_commission_other_new)


    # login
    log_in(role_id: StudioUser.roles[:studio_admin], user: studio_admin, studio_id: studio.id)
    find_link('Money').click
    find_link('Teacher Commissions').click
    find_link('Generate current month').click

    within('#modal-content') do
      find_link('Generate').click
    end
    sleep 1
    expect(page).to have_content(I18n.t('generate_success'))
    within 'h2' do
      expect(page).to have_content 'Teacher Commissions'
    end
    within('tr', text: teacher_user.nric_name) do
      expect(page).to have_content Date.today.strftime('%m/%y')
      expect(page).to have_content number_to_currency(commission)
      find_link(teacher_user.nric_name).click
    end
    teacher_commisssion_id = TeacherCommission.where(teacher_id: teacher.id).pluck(:id).first
    expect(current_path).to eq "/myc/teacher_commissions/#{teacher_commisssion_id}"

    expect(page).to have_field('teacher_nric_name', with: teacher_user.nric_name, disabled: true)
    expect(page).to have_field('statement_date', with: Date.today.to_s, disabled: true)
    expect(page).to have_field('net_pay', with: number_to_currency(commission), disabled: true)

    within('#table-item') do
      expect(page).to have_content program.program_name
      expect(page).to have_content myc_class.start_at.strftime('%a')
      expect(page).to have_content I18n.l(myc_class.start_at, format: :short)
      expect(page).to have_content student.nric_name
      expect(page).to have_content lesson_current_month_teacher_1.start_at.strftime('%d/%m/%y') + '(L1)'
      expect(page).to have_content number_to_currency(lesson_fee)
      expect(page).to have_content 1
      expect(page).to have_content number_to_currency(lesson_fee)
      expect(page).to have_content Rails.configuration.teacher_commission_percentage
      expect(page).to have_content number_to_currency(commission)
    end

    #edit percentage
    within('tr', text: student.nric_name) do
      find_button('btn-action').click
      find_link('Edit percentage').click
    end

    # test invalid input
    fill_in 'teacher_commission_item_percentage', with: '2.2'
    find_button('Save').click
    sleep 1
    expect(page).to have_content(I18n.t('regex_positive_number_0_to_999_message'))

    fill_in 'teacher_commission_item_percentage', with: percentage_new
    find_button('Save').click
    sleep 1
    expect(page).to have_content(I18n.t('save_success'))

    expect(page).to have_field('net_pay', with: number_to_currency(commission_new_percentage), disabled: true)
    within('#table-item') do
      expect(page).to have_content program.program_name
      expect(page).to have_content myc_class.start_at.strftime('%a')
      expect(page).to have_content I18n.l(myc_class.start_at, format: :short)
      expect(page).to have_content student.nric_name
      expect(page).to have_content lesson_current_month_teacher_1.start_at.strftime('%d/%m/%y') + '(L1)'
      expect(page).to have_content number_to_currency(lesson_fee)
      expect(page).to have_content 1
      expect(page).to have_content number_to_currency(lesson_fee)
      expect(page).to have_content percentage_new
      expect(page).to have_content number_to_currency(commission_new_percentage)
    end

    #edit remarks
    within('tr', text: student.nric_name) do
      find_button('btn-action').click
      find_link('Edit remarks').click
    end
    fill_in 'teacher_commission_item_remarks', with: remarks_new
    find_button('Save').click

    expect(page).to have_content(I18n.t('save_success'))

    expect(page).to have_field('net_pay', with: number_to_currency(commission_new_percentage), disabled: true)
    within('#table-item') do
      expect(page).to have_content program.program_name
      expect(page).to have_content myc_class.start_at.strftime('%a')
      expect(page).to have_content I18n.l(myc_class.start_at, format: :short)
      expect(page).to have_content student.nric_name
      expect(page).to have_content lesson_current_month_teacher_1.start_at.strftime('%d/%m/%y') + '(L1)'
      expect(page).to have_content number_to_currency(lesson_fee)
      expect(page).to have_content 1
      expect(page).to have_content number_to_currency(lesson_fee)
      expect(page).to have_content percentage_new
      expect(page).to have_content number_to_currency(commission_new_percentage)
      expect(page).to have_content remarks_new
    end

    # add other commissions
    find_link('Add other commissions').click
    # find_link('Add other commissions').trigger('click')
    sleep 1
    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_program' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_day' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_time' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_student_nric_name' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_lesson_date' and @class='form-control datePicker error']")

    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_term_fee' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_lesson_fee' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_no_of_lesson' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='teacher_commission_item_additional_percentage' and @class='form-control error']")

    fill_in 'teacher_commission_item_additional_program', with: additional_item.program
    fill_in 'teacher_commission_item_additional_day', with: additional_item.day
    fill_in 'teacher_commission_item_additional_time', with: additional_item.time
    fill_in 'teacher_commission_item_additional_student_nric_name', with: additional_item.student_nric_name
    fill_in 'teacher_commission_item_additional_lesson_date', with: additional_item.lesson_date
    fill_in 'teacher_commission_item_additional_term_fee', with: additional_item.term_fee
    fill_in 'teacher_commission_item_additional_remarks', with: additional_item.remarks

    # test invalid input
    fill_in 'teacher_commission_item_additional_no_of_lesson', with: '1.1'
    fill_in 'teacher_commission_item_additional_lesson_fee', with: 'a'
    fill_in 'teacher_commission_item_additional_percentage', with: '2.2'
    find_button('Save').click
    sleep 1
    expect(page).to have_content(I18n.t('regex_currency_message'))
    expect(page).to have_content(I18n.t('regex_positive_number_0_to_999_message'))

    fill_in 'teacher_commission_item_additional_no_of_lesson', with: additional_item.no_of_lesson
    fill_in 'teacher_commission_item_additional_lesson_fee', with: additional_item.lesson_fee
    fill_in 'teacher_commission_item_additional_percentage', with: additional_item.percentage

    find_button('Save').click
    sleep 1
    expect(page).to have_content(I18n.t('save_success'))

    expect(page).to have_field('net_pay', with: number_to_currency(commission_new_percentage + commission_additional_item), disabled: true)

    within('#table-item-additional') do
      expect(page).to have_content additional_item.program
      expect(page).to have_content additional_item.day
      expect(page).to have_content additional_item.time
      expect(page).to have_content additional_item.student_nric_name
      expect(page).to have_content additional_item.lesson_date
      expect(page).to have_content number_to_currency(additional_item.term_fee)
      expect(page).to have_content additional_item.no_of_lesson
      expect(page).to have_content number_to_currency(additional_item.lesson_fee)
      expect(page).to have_content additional_item.percentage
      expect(page).to have_content number_to_currency(commission_additional_item)
      expect(page).to have_content additional_item.remarks
    end

    #edit additional item
    within('#table-item-additional') do
      within('tr', text: additional_item.student_nric_name) do
        find_by_id('btn-action').click
        find_link('Edit').click
      end
    end

    fill_in 'teacher_commission_item_additional_program', with: additional_item_new.program
    fill_in 'teacher_commission_item_additional_day', with: additional_item_new.day
    fill_in 'teacher_commission_item_additional_time', with: additional_item_new.time
    fill_in 'teacher_commission_item_additional_student_nric_name', with: additional_item_new.student_nric_name
    fill_in 'teacher_commission_item_additional_lesson_date', with: additional_item_new.lesson_date
    fill_in 'teacher_commission_item_additional_term_fee', with: additional_item_new.term_fee
    fill_in 'teacher_commission_item_additional_lesson_fee', with: additional_item_new.lesson_fee
    fill_in 'teacher_commission_item_additional_no_of_lesson', with: additional_item_new.no_of_lesson
    fill_in 'teacher_commission_item_additional_percentage', with: additional_item_new.percentage
    fill_in 'teacher_commission_item_additional_remarks', with: additional_item_new.remarks

    find_button('Save').click
    sleep 1
    expect(page).to have_content(I18n.t('save_success'))

    expect(page).to have_field('net_pay', with: number_to_currency(commission_new_percentage + commission_additional_item_new), disabled: true)

    within('#table-item-additional') do
      expect(page).to have_content additional_item_new.program
      expect(page).to have_content additional_item_new.day
      expect(page).to have_content additional_item_new.time
      expect(page).to have_content additional_item_new.student_nric_name
      expect(page).to have_content additional_item_new.lesson_date
      expect(page).to have_content number_to_currency(additional_item_new.term_fee)
      expect(page).to have_content additional_item_new.no_of_lesson
      expect(page).to have_content number_to_currency(additional_item_new.lesson_fee)
      expect(page).to have_content additional_item_new.percentage
      expect(page).to have_content number_to_currency(commission_additional_item_new)
      expect(page).to have_content additional_item_new.remarks
    end

    # add others
    find_link('Add others').click
    expect(page).to have_xpath("//input[@id='teacher_commission_other_item' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='teacher_commission_other_amount' and @class='form-control error']")

    fill_in 'teacher_commission_other_item', with: other_item.item
    fill_in 'teacher_commission_other_amount', with: other_item.amount
    fill_in 'teacher_commission_other_remarks', with: other_item.remarks

    find_button('Save').click
    sleep 1
    expect(page).to have_content(I18n.t('save_success'))

    expect(page).to have_field('net_pay', with: number_to_currency(commission_new_percentage + commission_additional_item_new + other_item.amount.to_f), disabled: true)

    within('#table-other') do
      expect(page).to have_content other_item.item
      expect(page).to have_content number_to_currency(other_item.amount)
      expect(page).to have_content other_item.remarks
    end

    # edit others
    within('#table-other') do
      within('tr', text: other_item.item) do
        find_by_id('btn-action').click
        find_link('Edit').click
      end
    end

    fill_in 'teacher_commission_other_item', with: other_item_new.item
    fill_in 'teacher_commission_other_amount', with: other_item_new.amount
    fill_in 'teacher_commission_other_remarks', with: other_item_new.remarks

    find_button('Save').click
    sleep 1
    expect(page).to have_content(I18n.t('save_success'))

    expect(page).to have_field('net_pay', with: number_to_currency(commission_new_percentage + commission_additional_item_new + other_item_new.amount.to_f), disabled: true)

    within('#table-other') do
      expect(page).to have_content other_item_new.item
      expect(page).to have_content number_to_currency(other_item_new.amount)
      expect(page).to have_content other_item_new.remarks
    end

    # remove others
    within('#table-other') do
      within('tr', text: other_item_new.item) do
        find_by_id('btn-action').click
        find_link('Delete').click
      end
    end

    within('#modal-content') do
      find_link('Delete').click
    end
    sleep 1
    expect(page).to have_content(I18n.t('delete_success'))

    expect(page).to have_field('net_pay', with: number_to_currency(commission_new_percentage + commission_additional_item_new), disabled: true)

    within('#table-other') do
      expect(page).to_not have_content other_item_new.item
      expect(page).to_not have_content number_to_currency(other_item_new.amount)
      expect(page).to_not have_content other_item_new.remarks
    end

    # remove additional item
    within('#table-item-additional') do
      within('tr', text: additional_item_new.student_nric_name) do
        find_by_id('btn-action').click
        find_link('Delete').click
      end
    end

    within('#modal-content') do
      find_link('Delete').click
    end
    sleep 1
    expect(page).to have_content(I18n.t('delete_success'))

    expect(page).to have_field('net_pay', with: number_to_currency(commission_new_percentage), disabled: true)

    within('#table-item-additional') do
      expect(page).to_not have_content additional_item_new.program
      expect(page).to_not have_content additional_item_new.day
      expect(page).to_not have_content additional_item_new.time
      expect(page).to_not have_content additional_item_new.student_nric_name
      expect(page).to_not have_content additional_item_new.lesson_date
      expect(page).to_not have_content number_to_currency(additional_item_new.term_fee)
      expect(page).to_not have_content additional_item_new.no_of_lesson
      expect(page).to_not have_content number_to_currency(additional_item_new.lesson_fee)
      expect(page).to_not have_content additional_item_new.percentage
      expect(page).to_not have_content number_to_currency(commission_additional_item_new)
      expect(page).to_not have_content additional_item_new.remarks
    end

    # back to index
    find_link('Cancel').click
    within('tr', text: teacher_user.nric_name) do
      expect(page).to have_content Date.today.strftime('%m/%y')
      expect(page).to have_content number_to_currency(commission_new_percentage)
    end

    # test filter
    find_link('Filter').click
    select teacher_user.nric_name, from: 'teacher-select'
    find_button('Search').click

    within('#table') do
      within('tr', text: teacher_user.nric_name) do
        expect(page).to have_content Date.today.strftime('%m/%y')
        expect(page).to have_content number_to_currency(commission_new_percentage)
      end
      expect(page).to_not have_content teacher_user_2.nric_name
      expect(page).to_not have_content number_to_currency(commission)
    end

    select teacher_user_2.nric_name, from: 'teacher-select'
    find_button('Search').click

    within('#table') do
      within('tr', text: teacher_user_2.nric_name) do
        expect(page).to have_content Date.today.strftime('%m/%y')
        expect(page).to have_content number_to_currency(commission)
      end
      expect(page).to_not have_content teacher_user.nric_name
      expect(page).to_not have_content number_to_currency(commission_new_percentage)
    end

    # test teacher's permission: cannot generate, can only view his own data
    find_link('Logout').click
    log_in(role_id: StudioUser.roles[:studio_admin], user: teacher_user, studio_id: studio.id)

    find_link('Money').click
    find_link('Teacher Commissions').click
    expect(page).to_not have_xpath("//a[.='Generate']")
    find_link('Filter').click
    expect(find('#teacher-select').value).to eq(teacher_user.nric_name)
    within('table') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    within('tr', text: teacher_user.nric_name) do
      expect(page).to have_content Date.today.strftime('%m/%y')
      expect(page).to have_content number_to_currency(commission_new_percentage)
    end

  end
end

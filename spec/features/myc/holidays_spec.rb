require 'rails_helper'

feature 'Holiday management' do
  context 'Holiday create and update' do
    scenario 'shows highlight for required fields when creating Holiday', js: true do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      find_link('Holidays').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Holidays / New'
      end
      new_holiday_url = '/myc/holidays/new'
      expect(current_path).to eq new_holiday_url

      expect(page).to have_xpath("//input[@id='holiday_holiday_date' and @class='form-control datePicker error']")
      expect(page).to have_xpath("//input[@id='holiday_holiday_name' and @class='form-control error']")
    end

    scenario 'create holiday with valid input', js: true  do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      find_link('Holidays').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Holidays / New'
      end
      new_holidays_url = '/myc/holidays/new'
      holiday = build(:holiday)
      expect(current_path).to eq new_holidays_url

      expect{
        page.execute_script("$('#holiday_holiday_date').val('#{holiday.holiday_date}')")
        fill_in 'holiday_holiday_name', with: holiday.holiday_name

        find_button('Save').click
        sleep 1
      }.to change { Holiday.count }.by(1)

      expect(current_path).to eq '/myc/holidays'
      within 'h2' do
        expect(page).to have_content 'Holidays'
      end
      expect(page).to have_content(I18n.t('save_success'))

      within('tr', :text => holiday.holiday_name) do
        expect(page).to have_content(holiday.holiday_name)
        expect(page).to have_content(holiday.holiday_date)
      end

      # edit to confirm if data was saved
      new_holiday_id = Holiday.where(holiday_date: holiday.holiday_date, studio_id: studio.id).pluck(:id).first

      within('tr', :text => holiday.holiday_name) do
        find_link(holiday.holiday_name).click
      end

      expect(current_path).to eq "/myc/holidays/#{new_holiday_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Holidays / Edit'
      end

      expect(page).to have_field('holiday_holiday_date', with: holiday.holiday_date)
      expect(page).to have_field('holiday_holiday_name', with: holiday.holiday_name)
    end

    scenario 'create holiday with invalid input', js: true  do
      studio = create(:studio)
      holiday = create(:holiday)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      find_link('Holidays').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Holidays / New'
      end
      new_holidays_url = '/myc/holidays/new'
      expect(current_path).to eq new_holidays_url

      expect{
        page.execute_script("$('#holiday_holiday_date').val('#{holiday.holiday_date}')")
        fill_in 'holiday_holiday_name', with: holiday.holiday_name

        find_button('Save').click
        sleep 1
      }.to change { Holiday.count }.by(0)

      expect(current_path).to eq '/myc/holidays'
      within 'h2' do
        expect(page).to have_content 'Holidays / New'
      end
      expect(page).to have_content(I18n.t('save_fail'))
      expect(page).to have_content('Holiday date has already been taken')
    end

    scenario 'update holiday', js: true  do
      studio = create(:studio)
      log_in(role_id: StudioUser.roles[:studio_admin], studio_id: studio.id)
      find_link('Holidays').click
      holiday = create(:holiday)
      new_holiday = build(:new_holiday)

      within('tr', :text => holiday.holiday_name) do
        find_link(holiday.holiday_name).click
      end

      within 'h2' do
        expect(page).to have_content 'Holidays / Edit'
      end

      expect(current_path).to eq "/myc/holidays/#{holiday.id}/edit"

      expect{
        page.execute_script("$('#holiday_holiday_date').val('#{new_holiday.holiday_date}')")
        fill_in 'holiday_holiday_name', with: new_holiday.holiday_name

        find_button('Save').click
        sleep 1
      }.to change { Holiday.count }.by(0)

      expect(current_path).to eq '/myc/holidays'
      within 'h2' do
        expect(page).to have_content 'Holidays'
      end
      expect(page).to have_content(I18n.t('save_success'))

      within('tr', :text => new_holiday.holiday_name) do
        expect(page).to have_content(new_holiday.holiday_name)
        expect(page).to have_content(new_holiday.holiday_date)
      end

      # edit to confirm if data was saved
      new_holiday_id = Holiday.where(holiday_date: new_holiday.holiday_date, studio_id: studio.id).pluck(:id).first

      within('tr', :text => new_holiday.holiday_name) do
        find_link(new_holiday.holiday_name).click
      end

      expect(current_path).to eq "/myc/holidays/#{new_holiday_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Holidays / Edit'
      end

      expect(page).to have_field('holiday_holiday_date', with: new_holiday.holiday_date)
      expect(page).to have_field('holiday_holiday_name', with: new_holiday.holiday_name)
    end
  end
end  
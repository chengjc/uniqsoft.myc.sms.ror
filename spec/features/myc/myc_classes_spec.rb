require 'rails_helper'

feature 'Myc Class Management' do
  background do
    @studio = create(:studio)
    log_in(role_id: StudioUser.roles[:studio_admin], studio_id: @studio.id)
  end

  context 'Myc Class Create', js: true do
    scenario 'shows highlight for required fields when creating myc class' do
      find_link('Classes').click
      find_link('New').click

      within 'h2' do
        expect(page).to have_content 'Classes / New'
      end

      new_myc_class_url = '/myc/myc_classes/new'
      expect(current_path).to eq new_myc_class_url

      expect(page).to have_xpath("//input[@id='myc_class_start_date' and @class='form-control datePicker error']")
      expect(page).to have_xpath("//input[@id='myc_class_start_time' and @class='form-control timePicker error']")
      expect(page).to have_xpath("//select[@id='myc_class_myc_level_id' and @class='form-control error']")
      expect(page).to have_xpath("//select[@id='myc_class_teacher_id' and @class='form-control error']")
    end

    scenario 'create myc class' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      holiday = create(:holiday, { studio_id: @studio.id, holiday_date: Date.today + 7 })

      find_link('Classes').click
      find_link('New').click

      within 'h2' do
        expect(page).to have_content 'Classes / New'
      end

      new_myc_class_url = '/myc/myc_classes/new'
      expect(current_path).to eq new_myc_class_url

      myc_class = build(:myc_class)

      fill_in 'myc_class_start_date', with: myc_class.start_at.strftime("%d/%m/%y")
      fill_in 'myc_class_start_time', with: myc_class.start_at.strftime("%I:%M %p")
      select myc_program.program_name + ' ' + myc_level.level_name, from: 'myc_class_myc_level_id'
      select teacher.user.nric_name, from: 'myc_class_teacher_id'

      find_button('Save').click
      sleep 1

      new_myc_class = MycClass.first
      expect(current_path).to eq "/myc/myc_classes/#{new_myc_class.id}"
      within 'h2' do
        expect(page).to have_content 'Classes / Lessons'
      end
      expect(page).to have_content I18n.t('create_class_success')

      # check the class data is correct in show page
      expect(page).to have_field('start_date', with: myc_class.start_at.strftime("%d/%m/%y"))
      expect(page).to have_field('myc_program', with: myc_program.program_name + ' ' + myc_level.level_name)
      expect(page).to have_field('start_time', with: myc_class.start_at.strftime("%I:%M %p"))

      expect(page).to have_field('status', with: MycClass.statuses.keys[ MycClass.statuses[:on_going] ].humanize)

      # test the lessons datatable correct in show page
      expect(new_myc_class.lessons.length).to eq myc_level.total_lesson

      x = 0
      lesson_no = 1
      lesson_date_and_time = myc_class.start_at.to_datetime
      loop do
        if (holiday.holiday_date.to_s != lesson_date_and_time.to_date.to_s)
          x += 1
          break if x > myc_level.total_lesson
          expect(find(:xpath, "//*[@id='lessons-table']//tr[#{x}]//td[2]").text).to eq(lesson_no.to_s)    # check lesson_no
          expect(find(:xpath, "//*[@id='lessons-table']//tr[#{x}]//td[3]").text).to eq(lesson_date_and_time.strftime("%d/%m/%y"))    # check lesson_date
          expect(find(:xpath, "//*[@id='lessons-table']//tr[#{x}]//td[4]").text).to eq(teacher.user.nric_name)    # check teacher name
          lesson_no += 1
        end
        lesson_date_and_time = lesson_date_and_time + 7.days
      end

      #   go to Class index page and test that page
      within 'h2' do
        find_link('Classes').click
      end

      expect(current_path).to eq "/myc/myc_classes"
      within 'h2' do
        expect(page).to have_content 'Classes'
      end

      find_link('Filter').click

      expect(page).to have_xpath("//select[@id='teacher-select' and @class='form-control']")
      expect(page).to have_xpath("//select[@id='level-select' and @class='form-control']")
      expect(page).to have_xpath("//select[@id='student-select' and @class='form-control']")
      expect(page).to have_xpath("//select[@id='status-select' and @class='form-control']")

      within('tr', :text => new_myc_class.myc_program.program_name + ' ' +new_myc_class.myc_level.level_name) do
        expect(page).to have_content(new_myc_class.start_at.strftime("%I:%M"))
        expect(page).to have_content(new_myc_class.start_at.strftime("%a"))
        expect(page).to have_content(new_myc_class.status.humanize)
      end
    end
  end

  context 'Dissolve and Undo Dissolve Class', js: true do
    scenario 'Delete the class if the attendance is nor marked when dissolve' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)
      program_and_level_name = myc_program.program_name + ' ' + myc_level.level_name
      find_link('Dissolve').click
      within('div#modal') do
        expect(page).to have_content(I18n.t('confirm_dissolve'))
        find_link('Yes').click
        sleep 1
      end

      expect(current_path).to eq "/myc/myc_classes"
      expect(page).to have_content I18n.t('delete_success')
      within('table#classes-table') do
        expect(page).to have_no_content(program_and_level_name)
      end
    end

    scenario 'Dissolve and undo dissolve class success' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)

      myc_class = create_class(myc_level, teacher)

      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      student_cherry = create(:student, { nric_name: 'Cherry Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      students = [student_mike, student_cherry]
      add_students_to_class_and_mark_attendance(myc_class, students)
      find_link('Dissolve').click
      sleep 1

      within('div#modal') do
        expect(page).to have_content(I18n.t('confirm_dissolve'))
        find_link('Yes').click
        sleep 1
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('dissolve_success')
      expect(page).to have_field('status', with: MycClass.statuses.keys[MycClass.statuses[:dissolved]].humanize)
      expect(page).to have_xpath("//a[@id='dissolve' and @disabled='disabled']")
      expect(page).to have_xpath("//a[@id='go-to-next-level' and @disabled='disabled']")
      expect(page).to have_xpath("//a[@id='add-student' and @disabled='disabled']")
      expect(page).to have_xpath("//a[@id='remove-student' and @disabled='disabled']")
      expect(page).to have_xpath("//button[@id='btn-action' and @disabled='disabled']")

      find_link('Undo Dissolve').click

      within('div#modal') do
        expect(page).to have_content(I18n.t('confirm_undo_dissolve'))
        find_link('Yes').click
        sleep 1
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('undo_dissolve_success')
      expect(page).to have_field('status', with: MycClass.statuses.keys[MycClass.statuses[:on_going]].humanize)
      expect(page).to have_xpath("//a[@id='undo-dissolve' and @disabled='disabled']")
    end
  end

  context 'Add Student', js: true do
    scenario 'add newly registered student and old student to the class' do
      family = create(:family)
      student = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })

      program_sunrise = create(:myc_program, program_name: 'Sunrise')
      level_sunrise1 = create(:myc_level, {myc_program_id: program_sunrise.id, level_name: 1})
      teacher = create(:teacher)
      myc_class_1 = create_class(level_sunrise1, teacher)

      find_link('Add Student').click

      # test within Add Student dialog
      within ('div#modal') do
        within('h4') do
          expect(page).to have_content('Add Student')
        end

        within('table#new-students-table tr', :text => student.nric_name) do
          expect(page).to have_content(student.myc_program.program_name)
          expect(page).to have_content(student.status.humanize)

          find(:css, "input[value='#{student.id}']").set(true)
        end

        find_button('Save').click
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class_1.id}"
      expect(page).to have_content I18n.t('add_success')
      within('table#student-table thead') do
        expect(page).to have_content(student.nric_name)
      end
      expect(find(:xpath, "//*[@id='student-table']//tr[1]//td[2]").text).to eq(student.date_of_birth.to_s)
      expect(find(:xpath, "//*[@id='student-table']//tr[2]//td[2]").text).to eq(student.family.primary_contact_name)
      expect(find(:xpath, "//*[@id='student-table']//tr[3]//td[2]").text).to eq(student.family.primary_contact_phone_no)
      myc_class_student = MycClassStudent.myc_class_student_by_class_and_student(myc_class_1.id, student.id)
      expect(find(:xpath, "//*[@id='student-table']//tr[4]//td[2]").text).to eq(myc_class_student.status.humanize)

    #   test adding the existing student to class
      student = Student.find(student.id)
      level_sunrise2 = create(:myc_level, {myc_program_id: program_sunrise.id, level_name: 2})
      myc_class_2 = create_class(level_sunrise2, teacher)

      find_link('Add Student').click

      # test within Add Student dialog
      within ('div#modal') do
        find_link('Existing Students').click
        within('table#old-students-table tr', :text => student.nric_name) do
          expect(page).to have_content(myc_class_1.myc_program.program_name)
          expect(page).to have_content(student.status.humanize)
          find(:css, "input[value='#{student.id}']").set(true)
        end

        find_button('Save').click
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class_2.id}"
      expect(page).to have_content I18n.t('add_success')
      within('table#student-table thead') do
        expect(page).to have_content(student.nric_name)
      end
      expect(find(:xpath, "//*[@id='student-table']//tr[1]//td[2]").text).to eq(student.date_of_birth.to_s)
      expect(find(:xpath, "//*[@id='student-table']//tr[2]//td[2]").text).to eq(student.family.primary_contact_name)
      expect(find(:xpath, "//*[@id='student-table']//tr[3]//td[2]").text).to eq(student.family.primary_contact_phone_no)
      myc_class_student = MycClassStudent.myc_class_student_by_class_and_student(myc_class_2.id, student.id)
      expect(find(:xpath, "//*[@id='student-table']//tr[4]//td[2]").text).to eq(myc_class_student.status.humanize)
    end
  end

  context 'Remove Student', js: true do
    scenario 'remove student from class' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)

      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      student_cherry = create(:student, { nric_name: 'Cherry Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      students = [ student_mike, student_cherry]
      add_students_to_class(students)
      student = Student.first

      find_link('Remove Student').click

      within ('div#modal') do
        within('table#in-class-students-table tr', :text => student.nric_name) do
          expect(page).to have_content(student.status.humanize)
          find(:css, "input[value='#{student.id}']").set(true)
        end

        find_button('Save').click

        within 'h4' do
          expect(page).to have_content('Confirmation')
        end

        expect(page).to have_content(I18n.t('confirm_remove_student'))
        find_button('Yes').click
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('remove_success')
      within('table#student-table thead') do
        expect(page).to have_no_content(student.nric_name)
      end
    end

    scenario 'remove student who has attended one lesson from class' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)

      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      add_students_to_class_and_mark_attendance(myc_class, [student_mike])

      student_mike = Student.find(student_mike.id)
      find_link('Remove Student').click
      sleep 1

      within ('div#modal') do
        within('table#in-class-students-table tr', :text => student_mike.nric_name) do
          expect(page).to have_content(student_mike.status.humanize)
          find(:css, "input[value='#{student_mike.id}']").set(true)
        end

        find_button('Save').click

        within 'h4' do
          expect(page).to have_content('Remove Student')
        end

        expect(page).to have_xpath("//select[@id='student_status' and @class='form-control']")
        select MycClassStudent.statuses.keys[MycClassStudent.statuses[:dropped_out]].humanize, from: 'student_status'
        find_button('Remove').click

        within 'h4' do
          expect(page).to have_content('Confirmation')
        end

        expect(page).to have_content(I18n.t('confirm_remove_student'))
        find_button('Yes').click
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('remove_success')
      within('table#student-table thead') do
        expect(page).to have_content(student_mike.nric_name)
      end

      expect(find(:xpath, "//*[@id='student-table']//tr[1]//td[2]").text).to eq(student_mike.date_of_birth.to_s)
      expect(find(:xpath, "//*[@id='student-table']//tr[2]//td[2]").text).to eq(student_mike.family.primary_contact_name)
      expect(find(:xpath, "//*[@id='student-table']//tr[3]//td[2]").text).to eq(student_mike.family.primary_contact_phone_no)
      expect(find(:xpath, "//*[@id='student-table']//tr[4]//td[2]").text).to eq(MycClassStudent.statuses.keys[MycClassStudent.statuses[:dropped_out]].humanize)
    end
  end

  context 'Change Teacher', js: true do
    scenario 'change teacher for one lesson' do
      user = create(:user)
      new_teacher = create(:teacher, user_id: user.id)
      StudioUser.create(studio_id: @studio.id, user_id: user.id, role_id: StudioUser.roles[:teacher])

      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"

      first_lesson = myc_class.lessons.find_by(lesson_no: 1)

      # click change_teacher link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          expect(page).to have_link("Change Teacher", href: "/myc/lessons/#{first_lesson.id}/choose_teacher_to_change")
          find_link("Change Teacher").click
        end
      end

      # test within Change teacher dialog
      within ('div#modal') do
        within('h4') do
          expect(page).to have_content('Change Teacher')
        end

        expect(page).to have_xpath("//select[@id='teacher_id' and @class='form-control']")
        expect(page).to have_xpath("//input[@id='one_lesson' and @value='OneLesson' and @type='radio']")
        expect(page).to have_xpath("//input[@id='many_lessons' and @value='ManyLesson' and @type='radio']")
        select new_teacher.user.nric_name, from: 'teacher_id'

        find_button('Save').click
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('change_success')
      within('table#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
        expect(page).to have_content(new_teacher.user.nric_name)
      end
    end

    scenario 'change teacher for many lessons' do
      user = create(:user)
      new_teacher = create(:teacher, user_id: user.id)
      StudioUser.create(studio_id: @studio.id, user_id: user.id, role_id: StudioUser.roles[:teacher])

      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"

      first_lesson = myc_class.lessons.find_by("lesson_no = 1")

      # click change_teacher link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          expect(page).to have_link("Change Teacher", href: "/myc/lessons/#{first_lesson.id}/choose_teacher_to_change")
          find_link("Change Teacher").click
        end
      end

      within ('div#modal') do
        within('h4') do
          expect(page).to have_content('Change Teacher')
        end

        select new_teacher.user.nric_name, from: 'teacher_id'
        choose 'many_lessons'

        find_button('Save').click
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('change_success')

      lesson_no = 1
      loop do
        break if lesson_no > myc_level.total_lesson
        expect(find(:xpath, "//*[@id='lessons-table']//tr[#{lesson_no}]//td[4]").text).to eq(new_teacher.user.nric_name)

        lesson_no += 1
      end

    end

    scenario 'Cannot change teacher because the attendance is marked' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)
      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      student_cherry = create(:student, { nric_name: 'Cherry Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      students = [student_mike, student_cherry]
      add_students_to_class_and_mark_attendance(myc_class, students)

      marked_lesson = myc_class.lessons.find_by(status: Lesson.statuses[:marked])

      # click change_teacher link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => marked_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          find_link("Change Teacher").click
        end
      end

      expect(page).to have_content(I18n.t('cannot_change_teacher'))
    end
  end

  context 'Lesson makeup', js: true do
    scenario 'Lesson make up to specific date' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)
      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"

      current_lesson = myc_class.lessons.first

      # click make up to specific date link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => current_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          expect(page).to have_link("Make up to specific date", href: "/myc/lessons/#{current_lesson.id}/set_data_for_lesson_make_up/makeUpToSpecificDate")
          find_link("Make up to specific date").click
          sleep 1
        end
      end

      within ('div#modal') do
        within('h4') do
          expect(page).to have_content('Lesson Make Up')
        end

        expect(page).to have_xpath("//input[@id='make_up_date' and @class='form-control datePicker error']")
        expect(page).to have_xpath("//input[@id='make_up_time' and @class='form-control timePicker error']")
        expect(page).to have_xpath("//input[@id='reason' and @class='form-control error']")

        fill_in 'make_up_date', with: (Date.today + 10).to_s
        fill_in 'make_up_time', with: (Time.now + 3*60*60).strftime("%I:%M %p")
        fill_in 'reason', with: 'Test lesson makeup'
        find_button('Save').click
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('make_up_success')
      within('table#lessons-table tr', :text => current_lesson.start_at.strftime("%d/%m/%y")) do
        expect(page).to have_content('Test lesson makeup')
      end

      updated_lesson = Lesson.find(current_lesson.id)
      within('table#lessons-table tr', :text => updated_lesson.start_at.strftime("%d/%m/%y")) do
        expect(page).to have_content(updated_lesson.lesson_no)
        expect(page).to have_content(updated_lesson.teacher.user.nric_name)
      end

    end   # end of Lesson make up to specific date scenario

    scenario 'No lesson for this week' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)
      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"

      current_lesson = myc_class.lessons.first

      # click no lesson for this week link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => current_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          expect(page).to have_link("No lesson for this week", href: "/myc/lessons/#{current_lesson.id}/set_data_for_lesson_make_up/afterLastLesson")
          find_link("No lesson for this week").click
          sleep 1
        end
      end

      within ('div#modal') do
        within('h4') do
          expect(page).to have_content('Lesson Make Up')
        end

        expect(page).to have_xpath("//input[@id='reason' and @class='form-control error']")

        fill_in 'reason', with: 'Test lesson makeup'
        find_button('Save').click
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('make_up_success')
      within('table#lessons-table tr', :text => current_lesson.start_at.strftime("%d/%m/%y")) do
        expect(page).to have_content('Test lesson makeup')
      end

      updated_lesson = Lesson.find(current_lesson.id)
      within('table#lessons-table tr', :text => updated_lesson.start_at.strftime("%d/%m/%y")) do
        expect(page).to have_content(updated_lesson.lesson_no)
        expect(page).to have_content(updated_lesson.teacher.user.nric_name)
      end

    end     # end of No lesson for this week scenario

    scenario 'Cannot make up lesson because the attendance is marked' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)
      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      student_cherry = create(:student, { nric_name: 'Cherry Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      students = [student_mike, student_cherry]
      add_students_to_class_and_mark_attendance(myc_class, students)

      marked_lesson = myc_class.lessons.find_by(status: Lesson.statuses[:marked])

      # click make up to specific date link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => marked_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          find_link("Make up to specific date").click
        end
      end
      expect(page).to have_content(I18n.t('cant_make_up_lesson'))

      find_link("No lesson for this week").click
      expect(page).to have_content(I18n.t('cant_make_up_lesson'))
    end

    scenario 'Cannot make up lesson to holiday date' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)
      holiday = create(:holiday, { studio_id: @studio.id, holiday_date: Date.today + 7 })

      first_lesson = myc_class.lessons.find_by(lesson_no: 1)

      # click make up to specific date link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          expect(page).to have_link("Make up to specific date", href: "/myc/lessons/#{first_lesson.id}/set_data_for_lesson_make_up/makeUpToSpecificDate")
          find_link("Make up to specific date").click
          sleep 1
        end
      end

      within ('div#modal') do
        fill_in 'make_up_date', with: holiday.holiday_date.to_s
        fill_in 'make_up_time', with: (Time.now + 3*60*60).strftime("%I:%M %p")
        fill_in 'reason', with: 'Test lesson makeup'
        find_button('Save').click
      end

      expect(page).to have_content I18n.t('cant_make_up_lesson_because_of_holiday')
    end
  end

  context 'Mark attendance', js: true do
    scenario 'Mark attendance for lesson' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)

      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      student_cherry = create(:student, { nric_name: 'Cherry Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      students = [ student_mike, student_cherry]
      add_students_to_class(students)
      first_lesson = myc_class.lessons.first
      student_mike.reload
      student_cherry.reload

      # click mark attendance link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          expect(page).to have_link("Mark Attendance", href: "/myc/lessons/#{first_lesson.id}/choose_students_to_mark_attendance")
          find_link("Mark Attendance").click
        end
      end

      within ('div#modal') do
        within('h4') do
          expect(page).to have_content('Attendance')
        end

        expect(page).to have_link("Present")
        expect(page).to have_link("Absence")
        expect(page).to have_link("Sick")

        find_link('Present').click
        expect(page).to have_content(I18n.t('choose_students'))

        within('table#students-attendance-table tr', :text => student_mike.nric_name) do
          attendance = Attendance.find_by(lesson_id: first_lesson.id, student_id: student_mike.id)
          expect(page).to have_content(attendance.status.humanize)
          find(:css, "input[value='#{attendance.id}']").set(true)
        end

        find_link('Present').click
        find_button('Save').click
        expect(page).to have_content(I18n.t('mark_attendance_for_all_students'))

        within('table#students-attendance-table tr', :text => student_cherry.nric_name) do
          attendance = Attendance.find_by(lesson_id: first_lesson.id, student_id: student_cherry.id)
          expect(page).to have_content(attendance.status.humanize)
          find(:css, "input[value='#{attendance.id}']").set(true)
        end

        find_link('Present').click

        within('table#students-attendance-table tr', :text => student_mike.nric_name) do
          expect(page).to have_content(Attendance.statuses.keys[Attendance.statuses[:present]].humanize)
        end

        within('table#students-attendance-table tr', :text => student_cherry.nric_name) do
          expect(page).to have_content(Attendance.statuses.keys[Attendance.statuses[:present]].humanize)
        end

        find_button('Save').click
        sleep 1
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('mark_success')
      within('#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
        expect(page).to have_content(Attendance.statuses.keys[Attendance.statuses[:present]].humanize)
      end
    end

    scenario 'Mark attendance for last lesson and update class status to finish' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)

      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      student_cherry = create(:student, { nric_name: 'Cherry Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      students = [ student_mike, student_cherry]
      add_students_to_class(students)
      last_lesson = myc_class.lessons.order(lesson_no: :desc).first
      student_mike.reload
      student_cherry.reload
      within('table#lessons-table tr', :text => last_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click
        within('div#operation-menu') do
          find_link("Mark Attendance").click
        end
      end

      within ('div#modal') do
        attendance = Attendance.find_by(lesson_id: last_lesson.id, student_id: student_mike.id)
        find(:css, "input[value='#{attendance.id}']").set(true)
        attendance = Attendance.find_by(lesson_id: last_lesson.id, student_id: student_cherry.id)
        find(:css, "input[value='#{attendance.id}']").set(true)
        find_link('Present').click
        find_button('Save').click
        sleep 1
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('mark_success')
      expect(page).to have_field('status', with: MycClass.statuses.keys[MycClass.statuses[:finished]].humanize)

      # check whether the in class student status updated to graduated or not
      expect(find(:xpath, "//*[@id='student-table']//tr[4]//td[2]").text).to eq(MycClassStudent.statuses.keys[MycClassStudent.statuses[:graduated]].humanize)
      expect(find(:xpath, "//*[@id='student-table']//tr[4]//td[3]").text).to eq(MycClassStudent.statuses.keys[MycClassStudent.statuses[:graduated]].humanize)

      # check for mark attendance
      expect(find(:xpath, "//*[@id='lessons-table']//tr[#{myc_class.lessons.length}]//td[4]").text).to eq(Attendance.statuses.keys[Attendance.statuses[:present]].humanize)
      expect(find(:xpath, "//*[@id='lessons-table']//tr[#{myc_class.lessons.length}]//td[5]").text).to eq(Attendance.statuses.keys[Attendance.statuses[:present]].humanize)

    end
  end

  context 'Unmark Attendance', js: true do
    scenario 'Unmark attendance for lesson' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)

      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      student_cherry = create(:student, { nric_name: 'Cherry Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      students = [student_mike, student_cherry]
      add_students_to_class_and_mark_attendance(myc_class, students)
      first_lesson = myc_class.lessons.first

      # click unmark attendance link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click

        within('div#operation-menu') do
          expect(page).to have_link("Unmark Attendance", href: "/myc/lessons/#{first_lesson.id}/unmark_attendance_confirm")
          find_link("Unmark Attendance").click
        end
      end

      within ('div#modal') do
        expect(page).to have_content(I18n.t('confirm_unmark_attendance'))
        find_link('Yes').click
        sleep 1
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('unmark_success')
      within('table#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
        expect(page).to have_content('-')
      end
    end

    scenario 'Unmark attendance for last lesson and update class status to on going' do
      myc_program = create(:myc_program)
      myc_level = create(:myc_level, { myc_program_id: myc_program.id })
      teacher = create(:teacher)
      myc_class = create_class(myc_level, teacher)

      family = create(:family)
      student_mike = create(:student, { nric_name: 'Mike Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      student_cherry = create(:student, { nric_name: 'Cherry Jones', status: Student.statuses[:new_class_registered], family_id: family.id })
      students = [ student_mike, student_cherry]
      add_students_to_class(students)

      last_lesson = myc_class.lessons.order(lesson_no: :desc).first
      student_mike = myc_class.students.first
      student_cherry = myc_class.students.order(id: :desc).first
      within('table#lessons-table tr', :text => last_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click
        within('div#operation-menu') do
          find_link("Mark Attendance").click
        end
      end

      within ('div#modal') do
        attendance = Attendance.find_by(lesson_id: last_lesson.id, student_id: student_mike.id)
        find(:css, "input[value='#{attendance.id}']").set(true)
        attendance = Attendance.find_by(lesson_id: last_lesson.id, student_id: student_cherry.id)
        find(:css, "input[value='#{attendance.id}']").set(true)
        find_link('Present').click
        find_button('Save').click
        sleep 1
      end

      # click unmark attendance link from the operation menu of the lesson table
      within('table#lessons-table tr', :text => last_lesson.start_at.strftime("%d/%m/%y")) do
        find_by_id("btn-action").click
        find_link("Unmark Attendance").click
      end

      within ('div#modal') do
        expect(page).to have_content(I18n.t('confirm_unmark_attendance'))
        find_link('Yes').click
        sleep 1
      end

      expect(current_path).to eq "/myc/myc_classes/#{myc_class.id}"
      expect(page).to have_content I18n.t('unmark_success')
      expect(page).to have_field('status', with: MycClass.statuses.keys[MycClass.statuses[:on_going]].humanize)

      # check whether the in class student status updated to graduated or not
      expect(find(:xpath, "//*[@id='student-table']//tr[4]//td[2]").text).to eq(MycClassStudent.statuses.keys[MycClassStudent.statuses[:in_class]].humanize)
      expect(find(:xpath, "//*[@id='student-table']//tr[4]//td[3]").text).to eq(MycClassStudent.statuses.keys[MycClassStudent.statuses[:in_class]].humanize)

      # check for mark attendance
      expect(find(:xpath, "//*[@id='lessons-table']//tr[#{myc_class.lessons.length}]//td[4]").text).to eq('-')
      expect(find(:xpath, "//*[@id='lessons-table']//tr[#{myc_class.lessons.length}]//td[5]").text).to eq('-')
    end
  end

  private
  def create_class(myc_level, teacher)
    within 'ul#side-menu' do
      find_link('Classes').click
    end

    find_link('New').click

    start_at = Time.now
    program_and_level_name = myc_level.myc_program.program_name + ' ' + myc_level.level_name

    fill_in 'myc_class_start_date', with: start_at.strftime("%d/%m/%y")
    fill_in 'myc_class_start_time', with: start_at.strftime("%I:%M %p")
    select program_and_level_name, from: 'myc_class_myc_level_id'
    select teacher.user.nric_name, from: 'myc_class_teacher_id'

    find_button('Save').click
    sleep 1

    myc_class = MycClass.find_by(myc_level_id: myc_level.id)
    return myc_class
  end

  def add_students_to_class(students)
    find_link('Add Student').click

    within ('div#modal') do
      students.each do |student|
        find(:css, "input[value='#{student.id}']").set(true)
      end

      find_button('Save').click
      sleep 1
    end
  end

  def add_students_to_class_and_mark_attendance(myc_class, students)
    add_students_to_class(students)
    first_lesson = myc_class.lessons.first
    within('table#lessons-table tr', :text => first_lesson.start_at.strftime("%d/%m/%y")) do
      find_by_id("btn-action").click
      within('div#operation-menu') do
        find_link("Mark Attendance").click
      end
    end

    within ('div#modal') do
      students.each do |student|
        attendance = Attendance.find_by(lesson_id: first_lesson.id, student_id: student.id)
        find(:css, "input[value='#{attendance.id}']").set(true)
      end

      find_link('Present').click
      find_button('Save').click
      sleep 1
    end
  end

end
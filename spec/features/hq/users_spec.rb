require 'rails_helper'

feature 'User managemet' do
  context 'User create and update' do
    scenario 'shows highlight for required fields when creating user', js: true do
      log_in(role_id: StudioUser.roles[:super_user])
      find_link('Users').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Users / New'
      end
      new_user_url = '/hq/users/new'
      expect(current_path).to eq new_user_url

      expect(page).to have_xpath("//input[@id='user_nric_name' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='user_email' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='user_phone_number' and @class='form-control error']")
    end

    scenario 'creates user', js: true do
      log_in(role_id: StudioUser.roles[:super_user])
      find_link('Users').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Users / New'
      end
      expect(current_path).to eq '/hq/users/new'

      user_name = 'john'
      user_email = 'john@test.com'
      user_phone_number = '123'
      user_status = 'Active'

      fill_in 'user_nric_name', with: user_name
      fill_in 'user_email', with: user_email
      fill_in 'user_phone_number', with: user_phone_number
      select user_status, from: 'Status'
      find_button('Save').click
      expect(current_path).to eq '/hq/users/'
      within 'h2' do
        expect(page).to have_content 'Users'
      end
      expect(page).to have_content(I18n.t('save_success_and_click_teacher_profile_or_assign_studios'))

      within('tr', :text => user_email) do
        expect(page).to have_content(user_name)
        expect(page).to have_content(user_phone_number)
        expect(page).to have_content(user_status)
      end
    end

    scenario 'creates user with invalid data: duplicate email', js: true do
      email = 'john@test.com'
      create(:user, email: email)
      log_in(role_id: StudioUser.roles[:super_user])
      find_link('Users').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Users / New'
      end
      expect(current_path).to eq '/hq/users/new'

      fill_in 'user_nric_name', with: 'john'
      fill_in 'user_email', with: email
      fill_in 'user_phone_number', with: '123'
      select 'Active', from: 'Status'
      find_button('Save').click
      expect(current_path).to eq '/hq/users'
      within 'h2' do
        expect(page).to have_content 'Users / New'
      end
      expect(page).to have_content(I18n.t('save_fail'))
      expect(page).to have_content('Email has already been taken')
    end

    scenario 'updates user', js: true do
      user = create(:user)
      log_in(role_id: StudioUser.roles[:super_user])
      find_link('Users').click
      within_table 'table' do
        find_link(user.nric_name).click
      end

      within 'h2' do
        expect(page).to have_content 'Users / Edit'
      end

      new_nric_name = user.nric_name + 'new'
      new_email = user.email + 'new'
      new_phone_number = user.phone_number + 'new'
      new_status = 'Inactive'
      fill_in 'user_nric_name', with: new_nric_name
      fill_in 'user_email', with: new_email
      fill_in 'user_phone_number', with: new_phone_number
      select new_status, from: 'Status'

      find_button('Save').click
      # sleep 1

      expect(current_path).to eq hq_users_path

      within 'h2' do
        expect(page).to have_content 'Users'
      end

      expect(page).to have_content(I18n.t('save_success'))

      find_link('Filter').click
      select '', from: 'status-select'
      find_button('Search').click

      within('tr', :text => user.email) do
        expect(page).to have_content(new_nric_name)
        expect(page).to have_content(new_phone_number)
        expect(page).to have_content(new_status)
      end
    end

    scenario 'shows highlight for required fields when creating teacher', js: true do
      user = create(:user)
      log_in(ruser: user, role_id: StudioUser.roles[:super_user])
      find_link('Users').click
      within('tr', :text => user.nric_name) do
        find_by_id('btn-action').click
        find_link('New teacher profile').click
      end
      expect(current_path).to eq "/hq/users/#{user.id}/teachers/new"
      within 'h2' do
        expect(page).to have_content 'Users / Teacher / New'
      end

      expect(page).to have_xpath("//select[@id='teacher_gender' and @class='form-control error']")
      expect(page).to have_xpath("//select[@id='teacher_date_of_birth_1i' and @class='error']")
      expect(page).to have_xpath("//select[@id='teacher_date_of_birth_2i' and @class='error']")
      expect(page).to have_xpath("//select[@id='teacher_date_of_birth_3i' and @class='error']")
    end
  end

  context 'Teacher management' do
    scenario 'creates and edits teacher', js: true do
      user = create(:user)
      log_in(user: user, role_id: StudioUser.roles[:super_user])
      find_link('Users').click
      within('tr', :text => user.nric_name) do
        find_by_id('btn-action').click
        find_link('New teacher profile').click
      end
      expect(current_path).to eq "/hq/users/#{user.id}/teachers/new"
      within 'h2' do
        expect(page).to have_content 'Users / Teacher / New'
      end

      expect(page).to have_field('user_nric_name', with: user.nric_name, disabled: true)
      expect(page).to have_field('user_phone_number', with: user.phone_number, disabled: true)
      expect(page).to have_field('user_email', with: user.email, disabled: true)

      teacher_new = build(:teacher)

      select teacher_new.gender, from: 'Gender'
      select teacher_new.date_of_birth.year, from: 'teacher_date_of_birth_1i' #field id
      select Date::MONTHNAMES[teacher_new.date_of_birth.mon], from: 'teacher_date_of_birth_2i' #field id
      select teacher_new.date_of_birth.mday, from: 'teacher_date_of_birth_3i' #field id
      fill_in 'teacher_number_of_children', with: teacher_new.number_of_children
      fill_in 'teacher_full_time_occupation', with: teacher_new.full_time_occupation
      fill_in 'teacher_highest_piano_practical_standard_passed', with: teacher_new.highest_piano_practical_standard_passed
      fill_in 'teacher_highest_theory_standard_passed', with: teacher_new.highest_theory_standard_passed
      check 'teacher_has_college_abrsm' if teacher_new.has_college_abrsm
      check 'teacher_has_college_lcm' if teacher_new.has_college_lcm
      check 'teacher_has_college_trinity' if teacher_new.has_college_trinity
      fill_in 'teacher_college_others', with: teacher_new.college_others
      fill_in 'teacher_other_music_qualifications', with: teacher_new.other_music_qualifications
      fill_in 'teacher_before_myc_teaching_individual_lesson_started_year', with: teacher_new.before_myc_teaching_individual_lesson_started_year
      fill_in 'teacher_before_myc_teaching_individual_lesson_total_year', with: teacher_new.before_myc_teaching_individual_lesson_total_year
      fill_in 'teacher_before_myc_teaching_group_lesson_started_year', with: teacher_new.before_myc_teaching_group_lesson_started_year
      fill_in 'teacher_before_myc_teaching_group_lesson_total_year', with: teacher_new.before_myc_teaching_group_lesson_total_year
      page.execute_script("$('#teacher_date_of_myc_teacher_agreement').val('#{teacher_new.date_of_myc_teacher_agreement}')")
      page.execute_script("$('#teacher_date_of_myc_studio_agreement').val('#{teacher_new.date_of_myc_studio_agreement}')")
      page.execute_script("$('#teacher_date_of_myc_l1').val('#{teacher_new.date_of_myc_l1}')")
      page.execute_script("$('#teacher_date_of_myc_l2mb2').val('#{teacher_new.date_of_myc_l2mb2}')")
      page.execute_script("$('#teacher_date_of_myc_l2ss2').val('#{teacher_new.date_of_myc_l2ss2}')")
      page.execute_script("$('#teacher_date_of_myc_l3sb3').val('#{teacher_new.date_of_myc_l3sb3}')")
      page.execute_script("$('#teacher_date_of_myc_l2sb2').val('#{teacher_new.date_of_myc_l2sb2}')")
      page.execute_script("$('#teacher_date_of_myc_l3mb3').val('#{teacher_new.date_of_myc_l3mb3}')")
      page.execute_script("$('#teacher_date_of_1st_teacher_visit').val('#{teacher_new.date_of_1st_teacher_visit}')")
      page.execute_script("$('#teacher_date_of_2nd_teacher_visit').val('#{teacher_new.date_of_2nd_teacher_visit}')")
      page.execute_script("$('#teacher_date_of_3rd_teacher_visit').val('#{teacher_new.date_of_3rd_teacher_visit}')")

      click_button 'Save'
      expect(page).to have_content(I18n.t('save_success'))

      expect(current_path).to eq '/hq/users/'
      within 'h2' do
        expect(page).to have_content 'Users'
      end
      expect(page).to have_content(I18n.t('save_success'))

      # edit to confirm if data was saved
      within('tr', :text => user.email) do
        find_by_id('btn-action').click
        find_link('Edit teacher profile').click
      end
      teacher_new_id = Teacher.where(user_id: user.id).pluck(:id).first
      expect(current_path).to eq "/hq/teachers/#{teacher_new_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Users / Teacher / Edit'
      end
      expect(page).to have_field('user_nric_name', with: user.nric_name, disabled: true)
      expect(page).to have_field('user_phone_number', with: user.phone_number, disabled: true)
      expect(page).to have_field('user_email', with: user.email, disabled: true)
      expect(page).to have_select('teacher_gender', selected: teacher_new.gender)
      expect(page).to have_select('teacher[date_of_birth(1i)]', selected: teacher_new.date_of_birth.year.to_s) #field name
      expect(page).to have_select('teacher[date_of_birth(2i)]', selected: Date::MONTHNAMES[teacher_new.date_of_birth.mon]) #field name
      expect(page).to have_select('teacher[date_of_birth(3i)]', selected: teacher_new.date_of_birth.mday.to_s) #field name
      expect(page).to have_field('teacher_number_of_children', with: teacher_new.number_of_children)
      expect(page).to have_field('teacher_full_time_occupation', with: teacher_new.full_time_occupation)
      expect(page).to have_field('teacher_highest_piano_practical_standard_passed', with: teacher_new.highest_piano_practical_standard_passed)
      expect(page).to have_field('teacher_highest_theory_standard_passed', with: teacher_new.highest_theory_standard_passed)
      expect(page).to have_checked_field('teacher_has_college_abrsm') if teacher_new.has_college_abrsm
      expect(page).to have_checked_field('teacher_has_college_lcm') if teacher_new.has_college_lcm
      expect(page).to have_checked_field('teacher_has_college_trinity') if teacher_new.has_college_trinity
      expect(page).to have_field('teacher_college_others', with: teacher_new.college_others)
      expect(page).to have_field('teacher_other_music_qualifications', with: teacher_new.other_music_qualifications)
      expect(page).to have_field('teacher_before_myc_teaching_individual_lesson_started_year', with: teacher_new.before_myc_teaching_individual_lesson_started_year)
      expect(page).to have_field('teacher_before_myc_teaching_individual_lesson_total_year', with: teacher_new.before_myc_teaching_individual_lesson_total_year)
      expect(page).to have_field('teacher_before_myc_teaching_group_lesson_started_year', with: teacher_new.before_myc_teaching_group_lesson_started_year)
      expect(page).to have_field('teacher_before_myc_teaching_group_lesson_total_year', with: teacher_new.before_myc_teaching_group_lesson_total_year)
      expect(page).to have_field('teacher_date_of_myc_teacher_agreement', with: teacher_new.date_of_myc_teacher_agreement)
      expect(page).to have_field('teacher_date_of_myc_studio_agreement', with: teacher_new.date_of_myc_studio_agreement)
      expect(page).to have_field('teacher_date_of_myc_l1', with: teacher_new.date_of_myc_l1)
      expect(page).to have_field('teacher_date_of_myc_l2mb2', with: teacher_new.date_of_myc_l2mb2)
      expect(page).to have_field('teacher_date_of_myc_l2ss2', with: teacher_new.date_of_myc_l2ss2)
      expect(page).to have_field('teacher_date_of_myc_l3sb3', with: teacher_new.date_of_myc_l3sb3)
      expect(page).to have_field('teacher_date_of_myc_l2sb2', with: teacher_new.date_of_myc_l2sb2)
      expect(page).to have_field('teacher_date_of_myc_l3mb3', with: teacher_new.date_of_myc_l3mb3)
      expect(page).to have_field('teacher_date_of_1st_teacher_visit', with: teacher_new.date_of_1st_teacher_visit)
      expect(page).to have_field('teacher_date_of_2nd_teacher_visit', with: teacher_new.date_of_2nd_teacher_visit)
      expect(page).to have_field('teacher_date_of_3rd_teacher_visit', with: teacher_new.date_of_3rd_teacher_visit)
    end

    scenario 'updates teacher', js: true do
      user = create(:user)
      teacher = create(:teacher, user_id: user.id)
      log_in(user: user, role_id: StudioUser.roles[:super_user])
      find_link('Users').click
      within('tr', text: user.email) do
        find_by_id('btn-action').click
        find_link('Edit teacher profile').click
      end
      expect(current_path).to eq "/hq/teachers/#{teacher.id}/edit"
      within 'h2' do
        expect(page).to have_content 'Users / Teacher / Edit'
      end

      teacher_new = build(:teacher_new)

      select teacher_new.gender, from: 'Gender'
      select teacher_new.date_of_birth.year, from: 'teacher_date_of_birth_1i' #field id
      select Date::MONTHNAMES[teacher_new.date_of_birth.mon], from: 'teacher_date_of_birth_2i' #field id
      select teacher_new.date_of_birth.mday, from: 'teacher_date_of_birth_3i' #field id
      fill_in 'teacher_number_of_children', with: teacher_new.number_of_children
      fill_in 'teacher_full_time_occupation', with: teacher_new.full_time_occupation
      fill_in 'teacher_highest_piano_practical_standard_passed', with: teacher_new.highest_piano_practical_standard_passed
      fill_in 'teacher_highest_theory_standard_passed', with: teacher_new.highest_theory_standard_passed
      check 'teacher_has_college_abrsm' if teacher_new.has_college_abrsm
      check 'teacher_has_college_lcm' if teacher_new.has_college_lcm
      check 'teacher_has_college_trinity' if teacher_new.has_college_trinity
      fill_in 'teacher_college_others', with: teacher_new.college_others
      fill_in 'teacher_other_music_qualifications', with: teacher_new.other_music_qualifications
      fill_in 'teacher_before_myc_teaching_individual_lesson_started_year', with: teacher_new.before_myc_teaching_individual_lesson_started_year
      fill_in 'teacher_before_myc_teaching_individual_lesson_total_year', with: teacher_new.before_myc_teaching_individual_lesson_total_year
      fill_in 'teacher_before_myc_teaching_group_lesson_started_year', with: teacher_new.before_myc_teaching_group_lesson_started_year
      fill_in 'teacher_before_myc_teaching_group_lesson_total_year', with: teacher_new.before_myc_teaching_group_lesson_total_year
      page.execute_script("$('#teacher_date_of_myc_teacher_agreement').val('#{teacher_new.date_of_myc_teacher_agreement}')")
      page.execute_script("$('#teacher_date_of_myc_studio_agreement').val('#{teacher_new.date_of_myc_studio_agreement}')")
      page.execute_script("$('#teacher_date_of_myc_l1').val('#{teacher_new.date_of_myc_l1}')")
      page.execute_script("$('#teacher_date_of_myc_l2mb2').val('#{teacher_new.date_of_myc_l2mb2}')")
      page.execute_script("$('#teacher_date_of_myc_l2ss2').val('#{teacher_new.date_of_myc_l2ss2}')")
      page.execute_script("$('#teacher_date_of_myc_l3sb3').val('#{teacher_new.date_of_myc_l3sb3}')")
      page.execute_script("$('#teacher_date_of_myc_l2sb2').val('#{teacher_new.date_of_myc_l2sb2}')")
      page.execute_script("$('#teacher_date_of_myc_l3mb3').val('#{teacher_new.date_of_myc_l3mb3}')")
      page.execute_script("$('#teacher_date_of_1st_teacher_visit').val('#{teacher_new.date_of_1st_teacher_visit}')")
      page.execute_script("$('#teacher_date_of_2nd_teacher_visit').val('#{teacher_new.date_of_2nd_teacher_visit}')")
      page.execute_script("$('#teacher_date_of_3rd_teacher_visit').val('#{teacher_new.date_of_3rd_teacher_visit}')")

      find_button('Save').click
      expect(page).to have_content(I18n.t('save_success'))

      # edit to confirm if new changes were saved
      within('tr', text: user.email) do
        # click_button 'btn-action'
        # click_link 'Edit teacher profile'
        find_by_id('btn-action').click
        find_link('Edit teacher profile').click
      end
      expect(current_path).to eq "/hq/teachers/#{teacher.id}/edit"
      within 'h2' do
        expect(page).to have_content 'Users / Teacher / Edit'
      end
      expect(page).to have_field('user_nric_name', with: user.nric_name, disabled: true)
      expect(page).to have_field('user_phone_number', with: user.phone_number, disabled: true)
      expect(page).to have_field('user_email', with: user.email, disabled: true)
      expect(page).to have_select('teacher_gender', selected: teacher_new.gender)
      expect(page).to have_select('teacher[date_of_birth(1i)]', selected: teacher_new.date_of_birth.year.to_s) #field name
      expect(page).to have_select('teacher[date_of_birth(2i)]', selected: Date::MONTHNAMES[teacher_new.date_of_birth.mon]) #field name
      expect(page).to have_select('teacher[date_of_birth(3i)]', selected: teacher_new.date_of_birth.mday.to_s) #field name
      expect(page).to have_field('teacher_number_of_children', with: teacher_new.number_of_children)
      expect(page).to have_field('teacher_full_time_occupation', with: teacher_new.full_time_occupation)
      expect(page).to have_field('teacher_highest_piano_practical_standard_passed', with: teacher_new.highest_piano_practical_standard_passed)
      expect(page).to have_field('teacher_highest_theory_standard_passed', with: teacher_new.highest_theory_standard_passed)
      expect(page).to have_checked_field('teacher_has_college_abrsm') if teacher_new.has_college_abrsm
      expect(page).to have_checked_field('teacher_has_college_lcm') if teacher_new.has_college_lcm
      expect(page).to have_checked_field('teacher_has_college_trinity') if teacher_new.has_college_trinity
      expect(page).to have_field('teacher_college_others', with: teacher_new.college_others)
      expect(page).to have_field('teacher_other_music_qualifications', with: teacher_new.other_music_qualifications)
      expect(page).to have_field('teacher_before_myc_teaching_individual_lesson_started_year', with: teacher_new.before_myc_teaching_individual_lesson_started_year)
      expect(page).to have_field('teacher_before_myc_teaching_individual_lesson_total_year', with: teacher_new.before_myc_teaching_individual_lesson_total_year)
      expect(page).to have_field('teacher_before_myc_teaching_group_lesson_started_year', with: teacher_new.before_myc_teaching_group_lesson_started_year)
      expect(page).to have_field('teacher_before_myc_teaching_group_lesson_total_year', with: teacher_new.before_myc_teaching_group_lesson_total_year)
      expect(page).to have_field('teacher_date_of_myc_teacher_agreement', with: teacher_new.date_of_myc_teacher_agreement)
      expect(page).to have_field('teacher_date_of_myc_studio_agreement', with: teacher_new.date_of_myc_studio_agreement)
      expect(page).to have_field('teacher_date_of_myc_l1', with: teacher_new.date_of_myc_l1)
      expect(page).to have_field('teacher_date_of_myc_l2mb2', with: teacher_new.date_of_myc_l2mb2)
      expect(page).to have_field('teacher_date_of_myc_l2ss2', with: teacher_new.date_of_myc_l2ss2)
      expect(page).to have_field('teacher_date_of_myc_l3sb3', with: teacher_new.date_of_myc_l3sb3)
      expect(page).to have_field('teacher_date_of_myc_l2sb2', with: teacher_new.date_of_myc_l2sb2)
      expect(page).to have_field('teacher_date_of_myc_l3mb3', with: teacher_new.date_of_myc_l3mb3)
      expect(page).to have_field('teacher_date_of_1st_teacher_visit', with: teacher_new.date_of_1st_teacher_visit)
      expect(page).to have_field('teacher_date_of_2nd_teacher_visit', with: teacher_new.date_of_2nd_teacher_visit)
      expect(page).to have_field('teacher_date_of_3rd_teacher_visit', with: teacher_new.date_of_3rd_teacher_visit)
    end
  end

  context 'Studio management' do
    scenario 'assigns studios', js: true do
      user = create(:user)
      studio_new = create(:studio)
      log_in(user: user, role_id: StudioUser.roles[:super_user])
      find_link('Users').click
      within 'h2' do
        expect(page).to have_content 'Users'
      end
      expect(current_path).to eq '/hq/users'

      within('tr', text: user.email) do
        # click_button 'btn-action'
        # click_link 'Assign studios'
        find_by_id('btn-action').click
        find_link('Assign studios').click
      end
      expect(current_path).to eq "/hq/users/#{user.id}/studio_users"
      within 'h2' do
        expect(page).to have_content 'Users / Studios'
      end

      expect(page).to have_field('user_nric_name', with: user.nric_name, disabled: true)
      within_table 'table' do
        expect(page).to have_content(Rails.configuration.hq_studio_name)
        expect(page).to have_content(StudioUser.role_name(StudioUser.roles[:super_user]))
      end

      # click_button 'Add studio'
      find_link('Add studio').click
      role_id_new = StudioUser.roles[:studio_admin]
      select studio_new.studio_name, from: 'studio_user_studio_id'
      select StudioUser.role_name(role_id_new), from: 'studio_user_role_id'
      # click_button 'Save'
      find_button('Add').click

      expect(page).to have_content(I18n.t('save_success'))

      within_table 'table' do
        expect(page).to have_content(Rails.configuration.hq_studio_name)
        expect(page).to have_content(StudioUser.role_name(StudioUser.roles[:super_user]))
        expect(page).to have_content(studio_new.studio_name)
        expect(page).to have_content(StudioUser.role_name(role_id_new))
      end

      # delete
      within('tr', text: studio_new.studio_name) do
        find_link('Delete').click
      end
      within('#modal') do
        find_link('Delete').click
      end

      expect(page).to have_content(I18n.t('delete_success'))

      within_table 'table' do
        expect(page).to have_content(Rails.configuration.hq_studio_name)
        expect(page).to have_content(StudioUser.role_name(StudioUser.roles[:super_user]))
      end

      # cancel
      find_link('Cancel').click
      within 'h2' do
        expect(page).to have_content 'Users'
      end
      expect(current_path).to eq '/hq/users/'
    end
  end

  context 'Status management' do
    scenario 'changes status from active to inactive', js: true do
      change_status('Active', 'Inactive')
    end

    scenario 'changes status from inactive to active', js: true do
      change_status('Inactive', 'Active')
    end

    scenario 'changes status from active to deleted', js: true do
      change_status('Active', 'Deleted')
    end

    scenario 'changes status from deleted to active', js: true do
      change_status('Deleted', 'Active')
    end

    scenario 'changes status from active to on leave', js: true do
      change_status('Active', 'On leave')
    end

    scenario 'changes status from on leave to active', js: true do
      change_status('On leave', 'Active')
    end

    scenario 'changes status from active to terminated', js: true do
      change_status('Active', 'Terminated')
    end

    scenario 'changes status from Terminated to active', js: true do
      change_status('Terminated', 'Active')
    end
  end

  scenario 'invite', js: true do
    user = create(:user)
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Users').click
    within('tr', text: user.email) do
      find_button('btn-action').click
      find_link('Invite').click
    end

    Timecop.freeze()

    find_link('Yes').click
    sleep 1 #email may take some time
    expect(page).to have_content(I18n.t('invite_success'))
    invitation_sent_at = User.where(email: user.email).pluck(:invitation_sent_at).first
    expect(invitation_sent_at).to be_within(1.second).of Time.now
    within('tr', :text => user.email) do
      expect(page).to have_content(invitation_sent_at)
    end

    Timecop.return
  end

  private

  def change_status(from_status, to_status)
    user = create(:user, status: from_status.parameterize.underscore) #downcase and underscore
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Users').click
    find_link('Filter').click
    select '', from: 'status-select'
    find_button('Search').click
    # sleep 1

    within('tr', text: user.email) do
      find_button('btn-action').click
      find_link('Edit status').click
    end

    within('#modal-content') do
      select to_status, from: 'New Status'
      find_button('Save').click
    end

    expect(page).to have_content(I18n.t('save_success'))
    within('tr', text: user.email) do
      expect(page).to have_content(to_status)
    end
  end
end

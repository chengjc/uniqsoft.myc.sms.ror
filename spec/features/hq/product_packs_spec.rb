require 'rails_helper'

feature 'Product pack management' do
  scenario 'shows highlight for required fields when creating pack', js: true do
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Packs').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Packs / New'
    end
    expect(current_path).to eq '/hq/product_packs/new'

    expect(page).to have_xpath("//input[@id='product_pack_description' and @class='form-control error']")
    expect(find('#product_pack_status').value).to eq('active')
  end

  scenario 'creates, edits product pack and adds, removes items', js: true do
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Packs').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Packs / New'
    end
    expect(current_path).to eq '/hq/product_packs/new'

    product_pack = build(:product_pack)

    fill_in 'product_pack_description', with: product_pack.description
    select product_pack.status.humanize, from: 'Status'
    find_button('Save').click
    sleep 1
    product_pack_id = ProductPack.where(description: product_pack.description).pluck(:id).first
    # after saved will go to edit page to add new items
    expect(current_path).to eq "/hq/product_packs/#{product_pack_id}/edit"
    within 'h2' do
      expect(page).to have_content 'Product Packs / Edit'
    end
    expect(page).to have_content(I18n.t('save_success'))

    expect(page).to have_field('product_pack_description', with: product_pack.description)
    expect(find('#product_pack_status').value).to eq(product_pack.status)

    # add new item
    item = create(:product_item)
    find_link('Add item').click
    within('#modal-content') do
      select item.description, from: 'product_item_id'
      find_button('Save').click
    end
    expect(page).to have_content(I18n.t('save_success'))

    within('tr', text: item.description) do
      expect(page).to have_content number_to_currency(item.cost)
    end

    # remove item
    within('tr', text: item.description) do
      find_button('btn-action').click
      find_link('Remove').click
    end

    within('#modal-content') do
      find_link('Remove').click
    end
    expect(page).to have_content(I18n.t('save_success'))

    # cancel to go to index page to check if pack data is shown
    # find_link('Cancel').click
    find_link('Cancel').trigger("click")
    within('tr', text: product_pack.description) do
      expect(page).to have_content product_pack.description
      expect(page).to have_content product_pack.status.humanize
    end
  end

  scenario 'creates pack with invalid data: duplicate description', js: true do
    description = 'pack 1'
    create(:product_pack, description: description)
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Packs').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Packs / New'
    end
    expect(current_path).to eq '/hq/product_packs/new'

    fill_in 'product_pack_description', with: description
    select 'Active', from: 'Status'
    find_button('Save').click
    expect(current_path).to eq '/hq/product_packs'
    within 'h2' do
      expect(page).to have_content 'Product Packs / New'
    end
    expect(page).to have_content(I18n.t('save_fail'))
    expect(page).to have_content('Description has already been taken')
  end

  scenario 'updates pack', js: true do
    product_pack = create(:product_pack)

    log_in(role_id: StudioUser.roles[:super_user])

    find_link('Material').click
    find_link('Packs').click
    within('tr', text: product_pack.description) do
      find_link(product_pack.description).click
    end

    product_pack_new = build(:product_pack_new)

    fill_in 'product_pack_description', with: product_pack_new.description
    select product_pack_new.status.humanize, from: 'Status'

    find_button('Save').click
    expect(page).to have_content(I18n.t('save_success'))

    # edit to confirm if new changes were saved
    # new status is not active, remove filter first
    find_link('Filter').click
    select product_pack_new.status.humanize, from: 'status-select'
    find_button('Search').click
    within('tr', text: product_pack_new.description) do
      find_link(product_pack_new.description).click
    end

    expect(page).to have_field('product_pack_description', with: product_pack_new.description)
    expect(find('#product_pack_status').value).to eq(product_pack_new.status)
  end
end

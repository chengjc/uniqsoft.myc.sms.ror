require 'rails_helper'

feature 'MYC program management' do

  scenario 'creates and edits item', js: true do
    myc_program = create(:myc_program)
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('MYC Programs').click
    within 'h2' do
      expect(page).to have_content 'MYC Programs'
    end
    expect(current_path).to eq '/hq/myc_programs'

    within('tr', text: myc_program.program_code) do
      expect(page).to have_content myc_program.program_name
      expect(page).to have_content myc_program.status.humanize
    end
  end
end

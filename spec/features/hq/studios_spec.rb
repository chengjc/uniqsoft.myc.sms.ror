require 'rails_helper'

feature 'Studio managemet' do
  context 'Studio create and update' do
    scenario 'shows highlight for required fields when creating studio', js: true do
      log_in(role_id: StudioUser.roles[:super_user])
      find_link('Studios').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Studios / New'
      end
      new_studio_url = '/hq/studios/new'
      expect(current_path).to eq new_studio_url

      expect(page).to have_xpath("//input[@id='studio_studio_code' and @class='form-control error']")
      expect(page).to have_xpath("//input[@id='studio_studio_name' and @class='form-control error']")
    end

    scenario 'creates studio', js: true do
      log_in(role_id: StudioUser.roles[:super_user])
      find_link('Studios').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Studios / New'
      end
      expect(current_path).to eq '/hq/studios/new'

      studio = build(:studio)
      studio_status = 'Active'

      fill_in 'studio_studio_code', with: studio.studio_code
      fill_in 'studio_studio_name', with: studio.studio_name
      fill_in 'studio_address', with: studio.address
      fill_in 'studio_company_name', with: studio.company_name
      fill_in 'studio_company_registration_no', with: studio.company_registration_no
      fill_in 'studio_phone_no', with: studio.phone_no
      fill_in 'studio_email', with: studio.email
      fill_in 'studio_facebook_address', with: studio.facebook_address
      fill_in 'studio_website_address', with: studio.website_address
      select studio_status, from: 'studio_status'

      find_button('Save').click
      expect(current_path).to eq '/hq/studios'
      within 'h2' do
        expect(page).to have_content 'Studios'
      end
      expect(page).to have_content(I18n.t('save_success'))

      within('tr', :text => studio.studio_name) do
        expect(page).to have_content(studio.studio_name)
        expect(page).to have_content(studio.studio_code)
        expect(page).to have_content(studio.address)
        expect(page).to have_content(studio.company_name)
        expect(page).to have_content(studio.company_registration_no)
        expect(page).to have_content(studio.phone_no)
        expect(page).to have_content(studio.email)
        expect(page).to have_content(studio.facebook_address)
        expect(page).to have_content(studio.website_address)
        expect(page).to have_content(studio_status)
      end

      new_studio_id = Studio.where(studio_name: studio.studio_name).pluck(:id).first

      within('tr', :text => studio.studio_name) do
        find_link(studio.studio_name).click
      end

      expect(current_path).to eq "/hq/studios/#{new_studio_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Studios / Edit'
      end

      expect(page).to have_field('studio_studio_name', with: studio.studio_name)
      expect(page).to have_field('studio_studio_code', with: studio.studio_code)
      expect(page).to have_field('studio_address', with: studio.address)
      expect(page).to have_field('studio_company_name', with: studio.company_name)
      expect(page).to have_field('studio_company_registration_no', with: studio.company_registration_no)
      expect(page).to have_field('studio_phone_no', with: studio.phone_no)
      expect(page).to have_field('studio_email', with: studio.email)
      expect(page).to have_field('studio_facebook_address', with: studio.facebook_address)
      expect(page).to have_field('studio_website_address', with: studio.website_address)
      expect(page).to have_select('studio_status', selected: studio_status)
    end

    scenario 'creates studio with invalid input', js: true do
      log_in(role_id: StudioUser.roles[:super_user])
      find_link('Studios').click
      find_link('New').click
      within 'h2' do
        expect(page).to have_content 'Studios / New'
      end
      expect(current_path).to eq '/hq/studios/new'

      create(:studio, studio_code: 'HQ', studio_name: 'Head Quater', status: 'active')

      studio = build(:studio, studio_code: 'HQ', studio_name: 'Head Quater')
      studio_status = 'Active'

      fill_in 'studio_studio_code', with: studio.studio_code
      fill_in 'studio_studio_name', with: studio.studio_name
      fill_in 'studio_address', with: studio.address
      fill_in 'studio_company_name', with: studio.company_name
      fill_in 'studio_company_registration_no', with: studio.company_registration_no
      fill_in 'studio_phone_no', with: studio.phone_no
      fill_in 'studio_email', with: studio.email
      fill_in 'studio_facebook_address', with: studio.facebook_address
      fill_in 'studio_website_address', with: studio.website_address
      select studio_status, from: 'studio_status'

      find_button('Save').click

      expect(current_path).to eq '/hq/studios'
      within 'h2' do
        expect(page).to have_content 'Studios / New'
      end
      expect(page).to have_content(I18n.t('save_fail'))
      expect(page).to have_content('Studio code has already been taken')
    end

    scenario 'update studio', js: true  do
      log_in(role_id: StudioUser.roles[:super_user])
      find_link('Studios').click
      studio = create(:studio)
      new_studio = build(:new_studio)
      new_studio_status = 'Inactive'

      within('tr', :text => studio.studio_name) do
        find_link(studio.studio_name).click
      end

      within 'h2' do
        expect(page).to have_content 'Studios / Edit'
      end

      expect(current_path).to eq "/hq/studios/#{studio.id}/edit"

      fill_in 'studio_studio_code', with: new_studio.studio_code
      fill_in 'studio_studio_name', with: new_studio.studio_name
      fill_in 'studio_address', with: new_studio.address
      fill_in 'studio_company_name', with: new_studio.company_name
      fill_in 'studio_company_registration_no', with: new_studio.company_registration_no
      fill_in 'studio_phone_no', with: new_studio.phone_no
      fill_in 'studio_email', with: new_studio.email
      fill_in 'studio_facebook_address', with: new_studio.facebook_address
      fill_in 'studio_website_address', with: new_studio.website_address
      select new_studio_status, from: 'studio_status'

      find_button('Save').click

      expect(current_path).to eq '/hq/studios'
      within 'h2' do
        expect(page).to have_content 'Studios'
      end
      expect(page).to have_content(I18n.t('save_success'))

      find_link('Filter').click
      select new_studio_status, from: 'status-select'
      find_button('Search').click

      within('tr', :text => new_studio.studio_name) do
        expect(page).to have_content(new_studio.studio_name)
        expect(page).to have_content(new_studio.studio_code)
        expect(page).to have_content(new_studio.address)
        expect(page).to have_content(new_studio.company_name)
        expect(page).to have_content(new_studio.company_registration_no)
        expect(page).to have_content(new_studio.phone_no)
        expect(page).to have_content(new_studio.email)
        expect(page).to have_content(new_studio.facebook_address)
        expect(page).to have_content(new_studio.website_address)
        expect(page).to have_content(new_studio_status)

      end

      # edit to confirm if data was saved
      new_studio_id = Studio.where(studio_name: new_studio.studio_name).pluck(:id).first

      within('tr', :text => new_studio.studio_name) do
        find_link(new_studio.studio_name).click
      end

      expect(current_path).to eq "/hq/studios/#{new_studio_id}/edit"
      within 'h2' do
        expect(page).to have_content 'Studios / Edit'
      end

      expect(page).to have_field('studio_studio_name', with: new_studio.studio_name)
      expect(page).to have_field('studio_studio_code', with: new_studio.studio_code)
      expect(page).to have_field('studio_address', with: new_studio.address)
      expect(page).to have_field('studio_company_name', with: new_studio.company_name)
      expect(page).to have_field('studio_company_registration_no', with: new_studio.company_registration_no)
      expect(page).to have_field('studio_phone_no', with: new_studio.phone_no)
      expect(page).to have_field('studio_email', with: new_studio.email)
      expect(page).to have_field('studio_facebook_address', with: new_studio.facebook_address)
      expect(page).to have_field('studio_website_address', with: new_studio.website_address)
      expect(page).to have_select('studio_status', selected: new_studio_status)
    end
  end
end
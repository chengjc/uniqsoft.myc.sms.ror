require 'rails_helper'

feature 'Product item management' do
  scenario 'shows highlight for required fields when creating item', js: true do
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Items').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Items / New'
    end
    expect(current_path).to eq '/hq/product_items/new'

    expect(page).to have_xpath("//input[@id='product_item_description' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='product_item_cost' and @class='form-control error']")
    expect(page).to have_xpath("//input[@id='product_item_quantity_on_hand' and @class='form-control error']")
    expect(page).to have_field('Quantity to Trigger Alert (0 for no trigger)', with: '0')
    expect(find('#product_item_status').value).to eq('active')
  end

  scenario 'creates and edits item', js: true do
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Items').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Items / New'
    end
    expect(current_path).to eq '/hq/product_items/new'

    product_item = build(:product_item)

    fill_in 'product_item_description', with: product_item.description
    fill_in 'product_item_cost', with: product_item.cost
    fill_in 'product_item_quantity_on_hand', with: product_item.quantity_on_hand
    fill_in 'product_item_quantity_trigger_alert', with: product_item.quantity_trigger_alert
    fill_in 'product_item_remarks', with: product_item.remarks
    select product_item.status.humanize, from: 'Status'
    find_button('Save').click
    expect(current_path).to eq '/hq/product_items'
    within 'h2' do
      expect(page).to have_content 'Product Items'
    end
    expect(page).to have_content(I18n.t('save_success'))

    within('tr', text: product_item.description) do
      expect(page).to have_content number_to_currency(product_item.cost)
      expect(page).to have_content product_item.quantity_on_hand
      expect(page).to have_content product_item.quantity_trigger_alert
      expect(page).to have_content product_item.remarks
      expect(page).to have_content product_item.status.humanize
    end

    # edit to confirm if data was saved
    within('tr', text: product_item.description) do
      find_link(product_item.description).click
    end
    product_item_id = ProductItem.where(description: product_item.description).pluck(:id).first
    expect(current_path).to eq "/hq/product_items/#{product_item_id}/edit"
    within 'h2' do
      expect(page).to have_content 'Product Items / Edit'
    end
    expect(page).to have_field('product_item_description', with: product_item.description)
    expect(page).to have_field('product_item_cost', with: product_item.cost)
    expect(page).to have_field('product_item_quantity_on_hand', with: product_item.quantity_on_hand)
    expect(page).to have_field('product_item_quantity_trigger_alert', with: product_item.quantity_trigger_alert)
    expect(page).to have_field('product_item_remarks', with: product_item.remarks)
    expect(find('#product_item_status').value).to eq(product_item.status)
  end

  scenario 'creates product item with invalid data: duplicate description', js: true do
    description = 'item 1'
    create(:product_item, description: description)
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Items').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Items / New'
    end
    expect(current_path).to eq '/hq/product_items/new'

    fill_in 'product_item_description', with: description
    fill_in 'product_item_cost', with: 1
    fill_in 'product_item_quantity_on_hand', with: 1
    fill_in 'product_item_quantity_trigger_alert', with: 1
    fill_in 'product_item_remarks', with: 1
    select 'Active', from: 'Status'
    find_button('Save').click
    expect(current_path).to eq '/hq/product_items'
    within 'h2' do
      expect(page).to have_content 'Product Items / New'
    end
    expect(page).to have_content(I18n.t('save_fail'))
    expect(page).to have_content('Description has already been taken')
  end

  scenario 'updates product item', js: true do
    product_item = create(:product_item)

    log_in(role_id: StudioUser.roles[:super_user])

    find_link('Material').click
    find_link('Items').click
    within('tr', text: product_item.description) do
      find_link(product_item.description).click
    end

    product_item_new = build(:product_item_new)

    fill_in 'product_item_description', with: product_item_new.description
    fill_in 'product_item_cost', with: product_item_new.cost
    fill_in 'product_item_quantity_on_hand', with: product_item_new.quantity_on_hand
    fill_in 'product_item_quantity_trigger_alert', with: product_item_new.quantity_trigger_alert
    fill_in 'product_item_remarks', with: product_item_new.remarks
    select product_item_new.status.humanize, from: 'Status'

    find_button('Save').click
    expect(page).to have_content(I18n.t('save_success'))

    # edit to confirm if new changes were saved
    # new status is not active, remove filter first
    find_link('Filter').click
    select product_item_new.status.humanize, from: 'status-select'
    find_button('Search').click
    within('tr', text: product_item_new.description) do
      find_link(product_item_new.description).click
    end

    expect(page).to have_field('product_item_description', with: product_item_new.description)
    expect(page).to have_field('product_item_cost', with: product_item_new.cost)
    expect(page).to have_field('product_item_quantity_on_hand', with: product_item_new.quantity_on_hand)
    expect(page).to have_field('product_item_quantity_trigger_alert', with: product_item_new.quantity_trigger_alert)
    expect(page).to have_field('product_item_remarks', with: product_item_new.remarks)
    expect(find('#product_item_status').value).to eq(product_item_new.status)

  end
end

require 'rails_helper'

feature 'Student report management' do

  scenario 'generates report', js: true do
    # prepare admin
    studio = create(:studio)
    studio_2 = create(:studio)
    # studio_admin = create(:user)

    # prepare teacher
    teacher_user = create(:user)
    teacher = create(:teacher, user_id: teacher_user.id)
    StudioUser.create(studio_id: studio.id, user_id: teacher_user.id, role_id: StudioUser.roles[:teacher])

    # prepare teacher 2
    teacher_user_2 = create(:user)
    teacher_2 = create(:teacher, user_id: teacher_user_2.id)
    StudioUser.create(studio_id: studio.id, user_id: teacher_user_2.id, role_id: StudioUser.roles[:studio_admin])

    # prepare student, class and lessons
    student = create(:student)
    student_2 = create(:student)
    student_3 = create(:student)
    student_4 = create(:student)

    program_sr1 = create(:myc_program, program_code: 'SR1')
    program_sr2 = create(:myc_program, program_code: 'SR2')
    program_sr3 = create(:myc_program, program_code: 'SR3')

    myc_class = create(:myc_class, studio_id: studio.id, myc_program_id: program_sr1.id, start_date: Time.now, status: MycClass.statuses[:on_going])
    myc_class_2 = create(:myc_class, studio_id: studio.id, myc_program_id: program_sr2.id, start_date: Time.now, status: MycClass.statuses[:on_going])
    myc_class_3 = create(:myc_class, studio_id: studio.id, myc_program_id: program_sr3.id, start_date: Time.now, status: MycClass.statuses[:on_going])

    lesson_date = Date.today
    create(:lesson, myc_class_id: myc_class.id, teacher_id: teacher.id, lesson_no: 1, start_at: lesson_date, status: Lesson.statuses[:marked])

    create(:myc_class_student, myc_class_id: myc_class.id, student_id: student.id, status: MycClassStudent.statuses[:in_class])

    # login
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Student Reports').click
    within 'h2' do
      expect(page).to have_content 'Student Reports'
    end
    find_link('Generate').click

    sleep 1
    expect(page).to have_content(I18n.t('generate_success'))

    within('#table') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    expect_student_report_table('table', 1, studio.studio_name, teacher_user.nric_name, lesson_date.strftime('%m/%y'), '1', '0', '0', '1')
    expect_student_report_total_table('table-total', 1, '1', '0', '0', '1')

    # add 2nd lesson: should have no effect
    create(:lesson, myc_class_id: myc_class.id, teacher_id: teacher.id, lesson_no: 2, start_at: lesson_date, status: Lesson.statuses[:marked])

    find_link('Generate').click
    sleep 1
    expect(page).to have_content(I18n.t('generate_success'))

    within('#table') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    expect_student_report_table('table', 1, studio.studio_name, teacher_user.nric_name, lesson_date.strftime('%m/%y'), '1', '0', '0', '1')

    expect_student_report_total_table('table-total', 1, '1', '0', '0', '1')

    # add 2nd student:
    create(:myc_class_student, myc_class_id: myc_class.id, student_id: student_2.id, status: MycClassStudent.statuses[:in_class])

    find_link('Generate').click
    sleep 1
    expect(page).to have_content(I18n.t('generate_success'))

    within('#table') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    expect_student_report_table('table', 1, studio.studio_name, teacher_user.nric_name, lesson_date.strftime('%m/%y'), '2', '0', '0', '2')
    expect_student_report_total_table('table-total', 1, '2', '0', '0', '2')

    # add 3rd student with sr2:
    create(:myc_class_student, myc_class_id: myc_class_2.id, student_id: student_3.id, status: MycClassStudent.statuses[:in_class])
    create(:lesson, myc_class_id: myc_class_2.id, teacher_id: teacher.id, lesson_no: 1, start_at: lesson_date, status: Lesson.statuses[:marked])

    find_link('Generate').click
    sleep 1
    expect(page).to have_content(I18n.t('generate_success'))

    within('#table') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    expect_student_report_table('table', 1, studio.studio_name, teacher_user.nric_name, Date.today.strftime('%m/%y'), '2', '1', '0', '3')
    expect_student_report_total_table('table-total', 1, '2', '1', '0', '3')

    # add 2nd teacher and 4th student with sr3:
    lesson_date_2 = Date.today.next_month
    create(:myc_class_student, myc_class_id: myc_class_3.id, student_id: student_4.id, status: MycClassStudent.statuses[:in_class])
    create(:lesson, myc_class_id: myc_class_3.id, teacher_id: teacher_2.id, lesson_no: 1, start_at: lesson_date_2, status: Lesson.statuses[:marked])

    find_link('Generate').click
    sleep 1
    expect(page).to have_content(I18n.t('generate_success'))

    within('#table') do
      expect(page).to have_xpath(".//tr", count: 3) #table row count include header
    end

    if teacher_user.nric_name < teacher_user_2.nric_name # check the sorting order by name
      expect_student_report_table('table', 1, studio.studio_name, teacher_user.nric_name, lesson_date.strftime('%m/%y'), '2', '1', '0', '3')
      expect_student_report_table('table', 2, studio.studio_name, teacher_user_2.nric_name, lesson_date_2.strftime('%m/%y'), '0', '0', '1', '1')
    else
      expect_student_report_table('table', 1, studio.studio_name, teacher_user_2.nric_name, lesson_date_2.strftime('%m/%y'), '0', '0', '1', '1')
      expect_student_report_table('table', 2, studio.studio_name, teacher_user.nric_name, lesson_date.strftime('%m/%y'), '2', '1', '0', '3')
    end
    expect_student_report_total_table('table-total', 1, '2', '1', '1', '4')

    # test filter by teacher
    select teacher_user.nric_name, from: 'teacher-select'
    find_button('Search').click

    within('#table') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    expect_student_report_table('table', 1, studio.studio_name, teacher_user.nric_name, lesson_date.strftime('%m/%y'), '2', '1', '0', '3')
    expect_student_report_total_table('table-total', 1, '2', '1', '0', '3')

    # test filter by studio
    select studio_2.studio_name, from: 'studio-select'
    find_button('Search').click
    sleep 1

    within('#table') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    expect(find(:xpath, "//*[@id='table']//tr[1]//td[1]").text).to eq('No matching records found')

    within('#table-total') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    expect(find(:xpath, "//*[@id='table-total']//tr[1]//td[1]").text).to eq('')
    expect(find(:xpath, "//*[@id='table-total']//tr[1]//td[2]").text).to eq('')
    expect(find(:xpath, "//*[@id='table-total']//tr[1]//td[3]").text).to eq('')

    # test filter by studio
    select studio.studio_name, from: 'studio-select'
    find_button('Search').click
    sleep 1
    within('#table') do
      expect(page).to have_xpath(".//tr", count: 2) #table row count include header
    end
    expect_student_report_table('table', 1, studio.studio_name, teacher_user.nric_name, lesson_date.strftime('%m/%y'), '2', '1', '0', '3')
    expect_student_report_total_table('table-total', 1, '2', '1', '0', '3')

    # drop out 4th student:
    # MycClassStudent.where(student_id: student_4.id).update(myc_class_student_4.id, status: MycClassStudent.statuses[:dropped_out])
    #
    # find_link('Generate').click
    # sleep 1
    # expect(page).to have_content(I18n.t('generate_success'))
    #
    # within('table') do
    #   expect(page).to have_xpath(".//tr", count: 3) #table row count include header
    # end
    #
    # row = 1
    # column = 0
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(studio.studio_name)
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(teacher_user.nric_name)
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(Date.today.strftime('%m/%y'))
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('2') # SR1
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('1') # SR2
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0') # SR3
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    # expect(find(:xpath, "//*[@id='table']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('3') # total


  end

  private

  def expect_student_report_table(table_name, row, studio_name, teacher_name, month_year, sr1, sr2, sr3, total)
    column = 0
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(studio_name)
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(teacher_name)
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(month_year)
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(sr1) # SR1
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(sr2) # SR2
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(sr3) # SR3
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(total) # total
  end

  def expect_student_report_total_table(table_name, row, sr1, sr2, sr3, total)
    column = 0
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(sr1) # SR1
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(sr2) # SR2
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(sr3) # SR3
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq('0')
    expect(find(:xpath, "//*[@id='#{table_name}']//tr[#{row}]//td[ #{column += 1} ]").text).to eq(total) # total
  end

end

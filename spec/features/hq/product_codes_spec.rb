require 'rails_helper'

feature 'Product code management' do
  scenario 'shows highlight for required fields when creating code', js: true do
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Codes').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Codes / New'
    end
    expect(current_path).to eq '/hq/product_codes/new'

    expect(page).to have_xpath("//input[@id='product_code_code' and @class='form-control error']")
    expect(page).to have_field 'Price', with: '0.0'
    expect(page).to have_field 'cost', with: '', disabled: true
    expect(find('#product_code_status').value).to eq('active')
  end

  scenario 'creates, edits product code and adds, removes items', js: true do
    myc_program = create(:myc_program)
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Codes').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Codes / New'
    end
    expect(current_path).to eq '/hq/product_codes/new'

    product_code = build(:product_code)

    fill_in 'product_code_code', with: product_code.code
    fill_in 'product_code_price', with: product_code.price
    fill_in 'product_code_remarks', with: product_code.remarks
    select myc_program.program_name, from: 'MYC Program'
    select product_code.status.humanize, from: 'Status'
    find_button('Save').click
    sleep 1

    product_code_id = ProductCode.where(code: product_code.code).pluck(:id).first
    # after saved will go to edit page to add new packs / items
    expect(current_path).to eq "/hq/product_codes/#{product_code_id}/edit"
    within 'h2' do
      expect(page).to have_content 'Product Codes / Edit'
    end
    expect(page).to have_content(I18n.t('save_success'))

    expect(page).to have_field 'product_code_code', with: product_code.code
    expect(page).to have_field 'product_code_price', with: product_code.price
    expect(page).to have_field 'product_code_remarks', with: product_code.remarks
    expect(find('#product_code_status').value).to eq(product_code.status)

    # prepare db
    pack = create(:product_pack)
    item_1 = create(:product_item)
    item_2 = create(:product_item)
    item_3 = create(:product_item)
    pack.product_items << item_1
    pack.product_items << item_2

    # add new pack
    find_link('Add pack').click
    within('#modal-content') do
      select pack.description, from: 'product_pack_id'
      find_button('Save').click
    end
    expect(page).to have_content(I18n.t('save_success'))

    pack_cost = item_1.cost + item_2.cost
    within('tr', text: pack.description) do
      expect(page).to have_content number_to_currency(pack_cost)
    end

    # view pack
    within('tr', text: pack.description) do
      find_button('btn-action').click
      find_link('View').click
    end

    within('#modal-content') do
      within('tr', text: item_1.description) do
        expect(page).to have_content number_to_currency(item_1.cost)
      end
      within('tr', text: item_2.description) do
        expect(page).to have_content number_to_currency(item_2.cost)
      end
      find_link('Close').click
    end

    # update new total cost
    expect(page).to have_field 'cost', with: number_to_currency(pack_cost), disabled: true

    # add new item
    find_link('Add item').click
    within('#modal-content') do
      select item_3.description, from: 'product_item_id'
      find_button('Save').click
    end
    expect(page).to have_content(I18n.t('save_success'))

    within('tr', text: item_3.description) do
      expect(page).to have_content number_to_currency(item_3.cost)
    end

    # new total cost
    total_cost = item_1.cost + item_2.cost + item_3.cost
    expect(page).to have_field 'cost', with: number_to_currency(total_cost), disabled: true

    # edit pack
    within('tr', text: pack.description) do
      find_link(pack.description).click
    end

    expect(current_path).to eq "/hq/product_packs/#{pack.id}/edit"
    within 'h2' do
      expect(page).to have_content 'Product Packs / Edit'
    end

    #back to product code index
    find_link('Codes').click

    within('tr', text: product_code.code) do
      expect(page).to have_content number_to_currency(total_cost)
      expect(page).to have_content number_to_currency(product_code.price)
      expect(page).to have_content product_code.status.humanize
      expect(page).to have_content myc_program.program_name
      expect(page).to have_content product_code.remarks
      find_link(product_code.code).click
    end

    # remove pack
    within('tr', text: pack.description) do
      find_button('btn-action').click
      find_link('Remove').click
    end

    within('#modal-content') do
      find_link('Remove').click
    end
    expect(page).to have_content I18n.t('save_success')

    expect(page).to_not have_content pack.description

    # update new total cost
    expect(page).to have_field('cost', with: number_to_currency(item_3.cost), disabled: true)

    # remove item
    within('tr', text: item_3.description) do
      find_button('btn-action').click
      find_link('Remove').click
    end

    within('#modal-content') do
      find_link('Remove').click
    end
    expect(page).to have_content(I18n.t('save_success'))

    # update new total cost
    expect(page).to have_field('cost', with: number_to_currency(0.0), disabled: true)

    # cancel to go to index page to check if code data is shown
    find_link('Cancel').trigger('click')
    within('tr', text: product_code.code) do
      expect(page).to have_content product_code.code
      expect(page).to have_content 0.0
      expect(page).to have_content number_to_currency(product_code.price)
      expect(page).to have_content myc_program.program_name
      expect(page).to have_content product_code.remarks
      expect(page).to have_content product_code.status.humanize
    end
  end

  scenario 'creates code with invalid data: duplicate code', js: true do
    myc_program = create(:myc_program)
    code = 'code 1'
    create(:product_code, code: code)
    log_in(role_id: StudioUser.roles[:super_user])
    find_link('Material').click
    find_link('Codes').click
    find_link('New').click
    within 'h2' do
      expect(page).to have_content 'Product Codes / New'
    end
    expect(current_path).to eq '/hq/product_codes/new'

    fill_in 'product_code_code', with: code
    select myc_program.program_name, from: 'MYC Program'
    select 'Active', from: 'Status'
    find_button('Save').click
    expect(current_path).to eq '/hq/product_codes'
    within 'h2' do
      expect(page).to have_content 'Product Codes / New'
    end
    expect(page).to have_content(I18n.t('save_fail'))
    expect(page).to have_content('Code has already been taken')
  end

  scenario 'updates code', js: true do
    myc_program = create(:myc_program)

    product_code = create(:product_code)

    log_in(role_id: StudioUser.roles[:super_user])

    find_link('Material').click
    find_link('Codes').click
    within('tr', text: product_code.code) do
      find_link(product_code.code).click
    end

    product_code_new = build(:product_code_new)

    fill_in 'product_code_code', with: product_code_new.code
    fill_in 'product_code_price', with: product_code_new.price
    fill_in 'product_code_remarks', with: product_code_new.remarks
    select product_code_new.status.humanize, from: 'Status'

    find_button('Save').click
    expect(page).to have_content(I18n.t('save_success'))

    # edit to confirm if new changes were saved
    # new status is not active, remove filter first1
    find_link('Filter').click
    select product_code_new.status.humanize, from: 'status-select'
    find_button('Search').click
    within('tr', text: product_code_new.code) do
      expect(page).to have_content number_to_currency(product_code_new.price)
      expect(page).to have_content product_code_new.remarks
      expect(page).to have_content product_code_new.status.humanize
      expect(page).to have_content myc_program.program_name

      find_link(product_code_new.code).click
    end

    expect(page).to have_field 'product_code_code', with: product_code_new.code
    expect(page).to have_field 'product_code_price', with: product_code_new.price
    expect(page).to have_field 'product_code_remarks', with: product_code_new.remarks
    expect(find('#product_code_myc_program_id').value).to eq(myc_program.id.to_s)
    expect(find('#product_code_status').value).to eq(product_code_new.status)
  end
end

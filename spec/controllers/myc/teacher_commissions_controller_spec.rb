require 'rails_helper'

describe Myc::TeacherCommissionsController do

  describe 'teacher access' do

    describe 'GET #generate_confirm' do
      it 'denies access' do
        session[:current_user_role_id] = StudioUser.roles[:teacher]
        sign_in build(:user)
        get 'generate_confirm'
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'POST #generate' do
      it 'denies access' do
        session[:current_user_role_id] = StudioUser.roles[:teacher]
        sign_in build(:user)
        post 'generate'
        expect(response).to redirect_to '/users/sign_in'
      end
    end


    describe 'GET #show' do
      it 'denies access if not his record' do
        create(:teacher_commission, teacher_id: 1)
        session[:current_user_role_id] = StudioUser.roles[:teacher]
        sign_in build(:user)
        get 'show', id: 1
        expect(response).to redirect_to '/users/sign_in'
      end

      it 'allows access if is his record' do
        user = create(:user)
        teacher = create(:teacher, user_id: user.id)
        teacher_commission = create(:teacher_commission, teacher_id: teacher.id)
        session[:current_user_role_id] = StudioUser.roles[:teacher]
        sign_in user
        get 'show', id: teacher_commission.id
        expect(response).to render_template :show
      end
    end

    describe 'GET #show_data' do
      it 'denies access if not his record' do
        teacher_commission = create(:teacher_commission, teacher_id: 1)
        session[:current_user_role_id] = StudioUser.roles[:teacher]
        sign_in build(:user)
        post 'show_data', id: teacher_commission.id
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

end

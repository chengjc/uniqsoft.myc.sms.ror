require 'rails_helper'

describe Hq::UsersController do
  describe 'access other than super_user' do
    before :each do
      # create user role other than super user
      lower = StudioUser.roles[:super_user] + 1
      upper = StudioUser.roles[:super_user] + 10
      session[:current_user_role_id] = rand(lower..upper)
      sign_in build(:user)
    end

    describe 'GET #index' do
      it 'denies access' do
        get 'index'
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'GET #index_data' do
      it 'denies access' do
        get 'index_data'
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'GET #new' do
      it 'denies access' do
        get 'new'
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'POST #create' do
      it 'denies access' do
        post 'create'
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'GET #edit' do
      it 'denies access' do
        get 'edit', id: 1
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'GET #edit_status' do
      it 'denies access' do
        get 'edit_status', id: 1
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'PATCH #update' do
      it 'denies access' do
        patch 'update', id: 1
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'GET #invite_confirm' do
      it 'denies access' do
        get 'invite_confirm', id: 1
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'PATCH #invite' do
      it 'denies access' do
        patch 'invite', id: 1
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end

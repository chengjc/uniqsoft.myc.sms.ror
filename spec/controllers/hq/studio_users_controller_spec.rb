require 'rails_helper'

describe Hq::StudioUsersController do
  describe 'access other than super_user' do
    before :each do
      # create user role other than super user
      lower = StudioUser.roles[:super_user] + 1
      upper = StudioUser.roles[:super_user] + 10
      session[:current_user_role_id] = rand(lower..upper)
      sign_in build(:user)
    end

    describe 'GET #index' do
      it 'denies access' do
        get 'index', {user_id: 1}
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'GET #index_data' do
      it 'denies access' do
        get 'index_data', {user_id: 1}
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'GET #new' do
      it 'denies access' do
        get 'new', {user_id: 1}
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'POST #create' do
      it 'denies access' do
        post 'create', {user_id: 1}
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'GET #delete' do
      it 'denies access' do
        get 'delete', id: 1
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    describe 'DELETE #destroy' do
      it 'denies access' do
        delete 'destroy', id: 1
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end

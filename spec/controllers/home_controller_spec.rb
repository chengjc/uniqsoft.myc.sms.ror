# require 'rails_helper'
#
# describe HomeController do
#   before :each do
#     sign_in build(:user)
#   end
#
#   describe 'GET #index' do
#     it 'renders the :index template' do
#       get :index
#       expect(response).to render_template :index
#     end
#   end
#
#   describe 'GET #show' do
#     context 'with valid attributes' do
#       it 'assigns current studio id, name, and role id, name to sessions' do
#         user = create(:user)
#         studio = create(:studio)
#         role_id = StudioUser.roles[:teacher]
#         StudioUser.create(studio_id: studio.id, user_id: user.id, role_id: role_id)
#
#         sign_in user
#
#         # get 'index'
#         expect(session[:current_studio_id]).to eq studio.id.to_s
#         expect(session[:current_studio_name]).to eq studio.studio_name
#         expect(session[:current_user_role_id]).to eq role_id
#         expect(session[:current_user_role_name]).to eq StudioUser.role_name(role_id)
#       end
#
#       it 'renders the :index template' do
#         user = create(:user)
#         studio = create(:studio)
#         StudioUser.create(studio_id: studio.id, user_id: user.id, role_id: StudioUser.roles[:teacher])
#
#         sign_in user
#
#         get :index, id: studio.id
#         expect(response).to render_template :index
#       end
#
#       context 'with invalid attributes' do
#         it 'renders the :index template' do
#           user = create(:user)
#           studio = create(:studio)
#           StudioUser.create(studio_id: studio.id, user_id: user.id, role_id: StudioUser.roles[:teacher])
#
#           sign_in user
#
#           get :show, id: 0 # studio_id = 0
#           expect(response).to redirect_to '/logout/index'
#         end
#       end
#     end
#   end
# end

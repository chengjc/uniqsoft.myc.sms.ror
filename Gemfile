source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# Bootstrap is HTML, CSS, and JS framework for developing responsive, mobile first projects on the web
gem 'bootstrap-sass', '~> 3.3.6'
# Font Awesome gives you scalable vector icons that can instantly be customized — size, color, drop shadow, and anything that can be done with the power of CSS
gem 'font-awesome-rails', '4.3.0.0'
# Simple Form aims to be as flexible as possible while helping you with powerful components to create your forms
gem 'simple_form'

# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'



# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Devise is a flexible authentication solution for Rails based on Warden
gem 'devise'
# An invitation strategy for devise, send email
gem 'devise_invitable', '~> 1.5.2'
# Simple, efficient background processing
gem 'sidekiq'

gem 'devise-async'
# For rails console, a Ruby library that pretty prints Ruby objects in full color exposing their internal structure with proper indentation
gem 'awesome_print', require:'ap'
# Client Side Validations, used to highlight required fields
gem 'jquery-validation-rails'
# This gem packages the jQuery DataTables plugin for easy use with the Rails 3.1+ asset pipleine. It provides all the basic DataTables files, and a few of the extras
gem 'jquery-datatables-rails', '~> 3.3.0'
# datatables with ajax
gem 'ajax-datatables-rails'
# Scope & Engine based, clean, powerful, customizable and sophisticated paginator for modern web app frameworks and ORMs. for ajax-datatables
gem "kaminari"


#  takes your flash messages for the backend and automagically passes them to the frontend via HTTP cookies.
# This works with both regular page loads, jQuery AJAX requests, and turbolinks (from v3),
# does not tamper with the page body and requires about 3 extra lines of code in your app
gem 'unobtrusive_flash', '>=3'
#bootstrap 3 Date time picker
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.37'

# required to use Bootstrap tooltips
source 'https://rails-assets.org' do
  gem 'rails-assets-tether', '>= 1.1.0'
end

# Minimal authorization through OO design and pure Ruby classes
gem 'pundit', '~> 1.1'
# Easy multi-tenancy for Rails in a shared database setup
gem 'acts_as_tenant'
# provides a clear syntax for writing and deploying cron jobs.
gem 'whenever', :require => false
# an asset gem containing Adam Shaw's excellent fullcalendar jquery plugin
gem 'fullcalendar-rails'

gem 'chartkick'

group :development, :test do
  # Loads environment variables from `.env`.
  gem 'dotenv-rails'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Preview email in the default browser instead of sending it. This means you do not need to set up email delivery in your development environment,
  # and you no longer need to worry about accidentally sending a test email to someone else's address.
  gem 'letter_opener'
  # testing framework for Rails
  gem 'rspec-rails', '~> 3.0'
  # factory_girl is a fixtures replacement
  gem 'factory_girl_rails', '~> 4.0'
  # Rails: 2 CPUs = 2x Testing Speed for RSpec
  gem 'parallel_tests'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # An IRB alternative and runtime developer console
  gem 'pry-rails'
end

group :test do
  # generates fake data
  gem 'faker', '~> 1.6'
  # Acceptance test framework for web applications
  gem 'capybara', '~> 2.7'
  # Strategies for cleaning databases. Can be used to ensure a clean state for testing.
  gem 'database_cleaner', '~> 1.5'
  # Launchy is helper class for launching cross-platform applications in a fire and forget manner.
  # There are application concepts (browser, email client, etc) that are common across all platforms, and they may be launched differently on each platform
  gem 'launchy', '~> 2.4'
  # WebDriver is a tool for writing automated tests of websites. It aims to mimic the behaviour of a real user, and as such interacts with the HTML of the application
  gem 'selenium-webdriver', '~> 2.53'
  # providing "time travel" and "time freezing" capabilities, making it dead simple to test time-dependent code. It provides a unified method to mock Time.now, Date.today, and DateTime.now in a single call
  gem 'timecop', '~> 0.8.1'
  # A PhantomJS driver for Capybara
  gem 'poltergeist'
  # A Capybara driver for headless WebKit to test JavaScript web apps
  gem "capybara-webkit"
end

group :production do
  # app server
  gem 'puma'
end
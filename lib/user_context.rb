class UserContext
  attr_reader :user, :role_id, :is_super_user, :is_studio_admin, :is_teacher

  def initialize(user, role_id)
    @user = user
    @role_id   = role_id
  end
end
json.array!(@holidays) do |event|
  json.id event.id
  json.title event.title
  json.description ''
  json.start event.start.beginning_of_day
  json.end event.end.end_of_day
  json.allDay true
  if event.end < Date.current
    json.color '#D1DADE' # grey
    json.textColor '#5E5E5E' # dark grey
  else
    json.color '#23c6c8' # light green
  end
end

json.array!(@lessons) do |event|
  json.id event.id
  json.title event.title
  json.description ''
  json.start event.start.in_time_zone
  json.end event.end.in_time_zone
  json.allDay false
  if event.end.in_time_zone < Date.current
    json.color '#D1DADE' # grey
    json.textColor '#5E5E5E' # dark grey
  else
    json.color '#1c84c6' # blue
  end
end
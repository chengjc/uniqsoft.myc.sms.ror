class UserMailer < ApplicationMailer
  def invite_email(user)
    @user = user
    @url  = 'http://www.mycsingapore.com.sg'
    mail(to: @user.email, subject: 'Welcome to MYC')
  end
end

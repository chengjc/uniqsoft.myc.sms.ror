class ApplicationMailer < ActionMailer::Base
  default from: "mycsingapore.app@gmail.com"
  layout 'mailer'
end

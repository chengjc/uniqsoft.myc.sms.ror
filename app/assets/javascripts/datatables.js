
function initTable(tableId, enableFeatures, order_by_column_number, order_by_asc_or_desc) {
    dataTable = $('#' + tableId).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": $('#' + tableId).data('source'),
            "type": "POST"
        },
        pagingType: 'full_numbers',
        order: [[order_by_column_number, order_by_asc_or_desc]],
        ordering:   enableFeatures,
        paging:   enableFeatures,
        info:   enableFeatures,
        bFilter: enableFeatures
    });

    return dataTable;
}

function initTableWithFirstColumnActionMenu(tableId, enableFeatures, order_by_column_number, order_by_asc_or_desc) {
    $('#' + tableId).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": $('#' + tableId).data('source'),
            "type": "POST"
        },
        pagingType: 'full_numbers',
        order: [[order_by_column_number, order_by_asc_or_desc]],
        ordering:   enableFeatures,
        paging:   enableFeatures,
        info:   enableFeatures,
        bFilter: enableFeatures,
        columnDefs: [{targets: 0, width: '3%'}]
    });
}

function initTableWithDeferLoading(tableId, enableFeatures, order_by_column_number, order_by_asc_or_desc) {
    //dataTable = initTable(tableId, enableFeatures);
    //dataTableSettings = dataTable.fnSettings();
    //dataTableSettings.deferLoading = 0;

    dataTable = $('#' + tableId).DataTable({
        processing: true,
        serverSide: true,
        deferLoading: true,
        ajax: {
            "url": $('#' + tableId).data('source'),
            "type": "POST"
        },
        pagingType: 'full_numbers',
        order: [[order_by_column_number, order_by_asc_or_desc]],
        ordering:   enableFeatures,
        paging:   enableFeatures,
        info:   enableFeatures,
        bFilter: enableFeatures
    });

    return dataTable;
}

function initTableWithDeferLoadingAndParameters(tableId, enableFeatures, order_by_column_number, order_by_asc_or_desc, data){
    dataTable = $('#' + tableId).DataTable({
        processing: true,
        serverSide: true,
        deferLoading: true,
        ajax: {
            "url": $('#' + tableId).data('source'),
            "type": "POST",
            "data": data
        },
        pagingType: 'full_numbers',
        order: [[order_by_column_number, order_by_asc_or_desc]],
        ordering:   enableFeatures,
        paging:   enableFeatures,
        info:   enableFeatures,
        bFilter: enableFeatures
    });

    return dataTable;
}

function initCheckboxTableAndCheckbox(tableId, selectAllCheckboxId) {
    dataTable = initCheckboxTable(tableId);
    initCheckbox(tableId, dataTable, selectAllCheckboxId);
}

function initCheckboxTableWithDeferLoading(tableId, selectAllCheckboxId) {
    //dataTable = initCheckboxTable(tableId);
    //dataTableSettings = dataTable.fnSettings();
    //dataTableSettings.deferLoading = 0;
    //dataTable = $('#' + tableId).dataTable();
    dataTable = $('#' + tableId).DataTable({

        processing: true,
        serverSide: true,
        deferLoading: true,
        ajax: {
            "url": $('#' + tableId).data('source'),
            "type": "POST"
        },
        pagingType: 'full_numbers',

        columnDefs: [
            {
                targets: 0,
                width: '3%',
                orderable: true, //gem datatables-ajax-rails => must be sortable for first column
                searchable: false,
                className: 'dt-body-center',
                render: function (data, type, full, meta) {
                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                }
            },
        ],
        select: {
            style: 'multi'
        },
        //order: [[1, 'asc']],
        destroy: true

    });

    initCheckbox(tableId, dataTable, selectAllCheckboxId);

    return dataTable;

}

function initCheckboxTableWithEnableFeatures(tableId, selectAllCheckboxId, enableFeatures) {
    // Initialize the table
    dataTable = $('#' + tableId).DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            "url": $('#' + tableId).data('source'),
            "type": "POST"
        },
        pagingType: 'full_numbers',

        columnDefs: [
            {
                targets: 0,
                width: '3%',
                orderable: true, //gem datatables-ajax-rails => must be sortable for first column
                searchable: false,
                className: 'dt-body-center',
                render: function (data, type, full, meta) {
                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                }
            },
        ],
        select: {
            style: 'multi'
        },
        order: [[1, 'asc']],
        destroy: true,
        ordering:   enableFeatures,
        paging:   enableFeatures,
        info:   enableFeatures,
        bFilter: enableFeatures
    });

    initCheckbox(tableId, dataTable, selectAllCheckboxId);
}

function initCheckboxTable(tableId) {
    // Initialize the table
    dataTable = $('#' + tableId).DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            "url": $('#' + tableId).data('source'),
            "type": "POST"
        },
        pagingType: 'full_numbers',

        columnDefs: [
            {
                targets: 0,
                width: '3%',
                orderable: true, //gem datatables-ajax-rails => must be sortable for first column
                searchable: false,
                className: 'dt-body-center',
                render: function (data, type, full, meta) {
                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                }
            },
        ],
        select: {
            style: 'multi'
        },
        order: [[1, 'asc']],
        destroy: true

    });

    return dataTable;
}

function initSingleSelectCheckboxTable(tableId){
    dataTable = $('#' + tableId).DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            "url": $('#' + tableId).data('source'),
            "type": "POST"
        },
        pagingType: 'full_numbers',

        columnDefs: [
            {
                targets: 0,
                width: '3%',
                orderable: true, //gem datatables-ajax-rails => must be sortable for first column
                searchable: false,
                className: 'dt-body-center',
                render: function (data, type, full, meta) {
                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                }
            },
        ],
        select: {
            style: 'multi'
        },
        order: [[1, 'asc']],
        destroy: true

    });

    $('#' + tableId + ' tbody').on('click', 'input[type="checkbox"]', function (e) {
        var checked = $(this).prop("checked");
        $('#'+ tableId  + ' tbody input[type="checkbox"]:checked').prop('checked', false);
        $(this).prop("checked", checked);
    });

    return dataTable;
}

function initCheckbox(tableId, dataTable, selectAllCheckboxId) {
    // Handle click on checkbox
    $('#' + tableId + ' tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');
        if (this.checked) {
            $row.addClass('active');
        } else {
            $row.removeClass('active');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(dataTable, selectAllCheckboxId);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#' + tableId).on('click', 'tbody td, thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $('thead input[id=' + selectAllCheckboxId + ']', dataTable.table().container()).on('click', function (e) {
        if (this.checked) {
            $('#' + tableId + ' tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#' + tableId + ' tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent sorting
        e.stopPropagation();
    });

    // Handle table draw event
    dataTable.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(dataTable, selectAllCheckboxId);
    });

}

function updateDataTableSelectAllCtrl(dataTable, selectAllCheckboxId) {
    var $table = dataTable.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[id=' + selectAllCheckboxId + ']', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

function sendAjaxData(confirmMessage, url, data, tableId) {
    var selectedIds = getTableSelectedIds(tableId);

    if (selectedIds == '') {
        showMessage('alert', 'Please select a row first')
        return;
    }

    BootstrapDialog.show({
        size: BootstrapDialog.SIZE_SMALL,
        title: 'Confirm',
        message: confirmMessage,
        buttons: [
            {
                label: 'Cancel',
                cssClass: 'btn btn-default',
                action: function (dialogItself) {
                    dialogItself.close();
                }
            },
            {
                label: 'Yes',
                cssClass: 'btn-success',
                action: function (dialogItself) {
                    dialogItself.close();
                    sendAjax('POST', url, data, $('#' + tableId).DataTable());
                }
            }
        ]
    });
}

function sendAjaxDelete(confirmMessage, url, data, tableId) {

    BootstrapDialog.show({
        size: BootstrapDialog.SIZE_SMALL,
        title: 'Confirm',
        message: confirmMessage,
        buttons: [
            {
                label: 'Cancel',
                cssClass: 'btn btn-default',
                action: function (dialogItself) {
                    dialogItself.close();
                }
            },
            {
                label: 'Yes',
                cssClass: 'btn-success',
                action: function (dialogItself) {
                    dialogItself.close();
                    sendAjax('DELETE', url, data, $('#' + tableId).DataTable());
                }
            }
        ]
    });
}

function sendAjax(type, url, data, dataTable) {
    // remove previous message if exists
    $('.unobtrusive-flash-container').empty();

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response) {
            UnobtrusiveFlash.showFlashMessage(response.message, {type: 'success', timeout: 0});
            dataTable.ajax.reload();
        },
        error: function(xhr){
            var jsonResponse = $.parseJSON(xhr.responseText);
            UnobtrusiveFlash.showFlashMessage(jsonResponse.message, {type: 'error', timeout: 0});

            for (var i = 0; i < jsonResponse.errors.length; i++) {
                UnobtrusiveFlash.showFlashMessage(jsonResponse.errors[i], {type: 'error', timeout: 0});
            }
        }
    });
}

function getTableSelectedIds(tableId) {
    var selectedIds = [];
    $('input:checked', $('#' + tableId).dataTable().fnGetNodes()).each(function (i) {
        selectedIds.push(this.value);
    });

    return selectedIds;
}


//set the highlight for the label of the validate field
function setHighlight()
{
    jQuery.validator.messages.required = "";
    
    var errorLabelId = "-error";
    var highlightJson = {
        // set the errorClass as a random string to prevent label disappearing when valid
        errorClass : "errorMessage",
        // use highlight and unhighlight
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("error");
            $(element.form).find("label[for=" + element.id + "]")
                .addClass("error");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass("error");
            $(element.form).find("label[for=" + element.id + "]")
                .removeClass("error");
        }
    };

    return highlightJson;
}

function showMessage(type, message)
{
    UnobtrusiveFlash.showFlashMessage(message, {type: type, timeout: 0});
}


// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//require turbolinks
//= require tether
//= require bootstrap-sprockets
//= require bootstrap3-dialog/bootstrap-dialog.js
//= require dataTables/jquery.dataTables
//= require dataTables/extras/dataTables.responsive
//= require dataTables/extras/dataTables.fixedColumns
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require metisMenu/jquery.metisMenu.js
//= require moment
//= require bootstrap-datetimepicker
//= require slimscroll/jquery.slimscroll.min.js
//= require toastr/toastr.min.js
//= require jquery.validate
// require jquery.validate.additional-methods
//= require unobtrusive_flash
//= require unobtrusive_flash_bootstrap
//= require fullcalendar
//= require_tree .

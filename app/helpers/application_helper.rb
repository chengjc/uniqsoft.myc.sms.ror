module ApplicationHelper

	# Displays object errors of server side validation or CRUD operations
	def error_messages_for(object)
	  render(:partial => 'shared/error_messages', :locals => {:object => object})
	end

	def is_active_material_menu
		case params[:controller]
			when 'hq/product_items'
				return 'active'
			when 'hq/product_packs'
				return 'active'
			when 'hq/product_codes'
				return 'active'
			else
				return nil
		end
	end

	def is_active_people_menu
		case params[:controller]
			when 'myc/students'
				return 'active'
			when 'myc/teachers'
				return 'active'
			when 'myc/families'
				return 'active'
			else
				return nil
		end
	end

	def is_active_money_menu
		case params[:controller]
			when 'myc/teacher_commissions'
				return 'active'
			when 'myc/invoices'
				return 'active'
			when 'myc/payments'
				return 'active'
			else
				return nil
		end
	end

	def is_active_controller(controller_name)
		params[:controller] == controller_name ? "active" : nil
	end

	# Displays messages by programmers
	# def bootstrap_class_for flash_type
	# 	case flash_type
	# 		when "success"
	# 			"alert-success" # green
	# 		when "error"
	# 			"alert-fanger" # red
	# 		when "alert"
	# 			"alert-success" # yellow
	# 		when "notice"
	# 			"alert-info" # blue
	# 		else
	# 			flash_type.to_s
	# 		end
	# end


	def number_to_currency(number)
		ActionController::Base.helpers.number_to_currency(number, unit: '', precision: 2)
	end



end

class Teacher < ActiveRecord::Base
  belongs_to :user, autosave = false
  accepts_nested_attributes_for :user

  has_one :lesson

  validates :gender, presence: true,
            length: {maximum: 1}

  validates :date_of_birth, presence: true
  # validates :phone_number, length: { maximum: 20 }
  validates :number_of_children, allow_blank: true, numericality: {only_integer: true}
  validates :highest_piano_practical_standard_passed, length: {maximum: 50}
  validates :highest_theory_standard_passed, length: {maximum: 50}
  validates :college_others, length: {maximum: 50}
  validates :other_music_qualifications, length: {maximum: 50}
  validates :before_myc_teaching_individual_lesson_started_year, length: {maximum: 4}
  validates :before_myc_teaching_individual_lesson_total_year, length: {maximum: 4}
  validates :before_myc_teaching_group_lesson_started_year, length: {maximum: 4}
  validates :before_myc_teaching_group_lesson_total_year, length: {maximum: 4}
  validates :user_id, presence: true

  # a teacher must exist in teacher table, studio admin can also be a teacher
  def self.teacher_id_and_name
    Teacher
        .joins("LEFT JOIN users ON users.id = teachers.user_id")
        .joins("LEFT JOIN studio_users ON users.id = studio_users.user_id")
        .where("users.status = #{User.statuses[:active]}")
        .select("users.nric_name, teachers.id")
        .order("users.nric_name")
  end

  def self.teacher_id_and_name_by_studio(studio_id)
    Teacher
        .joins("LEFT JOIN users ON users.id = teachers.user_id")
        .joins("LEFT JOIN studio_users ON users.id = studio_users.user_id")
        .where("studio_users.studio_id = #{studio_id}")
        .where("users.status = #{User.statuses[:active]}")
        .select("users.nric_name, teachers.id")
        .order("users.nric_name")
  end

  def self.teacher_id_and_name_can_view_teacher_commission(studio_id, user_id, role_id)

    if role_id == StudioUser.roles[:teacher]
      Teacher
          .joins("LEFT JOIN users ON users.id = teachers.user_id")
          .joins("LEFT JOIN studio_users ON users.id = studio_users.user_id")
          .where("studio_users.studio_id = #{studio_id}")
          .where("users.status = #{User.statuses[:active]}")
          .where("users.id = #{user_id}")
          .select("users.nric_name, teachers.id")
    else
      if role_id == StudioUser.roles[:studio_admin]
        Teacher
            .joins("LEFT JOIN users ON users.id = teachers.user_id")
            .joins("LEFT JOIN studio_users ON users.id = studio_users.user_id")
            .where("studio_users.studio_id = #{studio_id}")
            .where("users.status = #{User.statuses[:active]}")
            .select("users.nric_name, teachers.id")
            .order("users.nric_name")

      end
    end
  end

end

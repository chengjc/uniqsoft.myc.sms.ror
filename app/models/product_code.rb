class ProductCode < ActiveRecord::Base
  has_and_belongs_to_many :product_packs, uniq: true
  has_and_belongs_to_many :product_items, uniq: true
  belongs_to :myc_program

  validates :code, length: {maximum: 100}, presence: true
  validates :price, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true
  validates :remarks, length: {maximum: 200}
  validates :status, presence: true
  validates :code, uniqueness: {scope: [:status]}

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  def self.cost(product_code_id)
    pack_cost = ProductItem.
        joins('LEFT JOIN product_items_packs on product_items_packs.product_item_id = product_items.id').
        joins('LEFT JOIN product_codes_packs on product_codes_packs.product_pack_id = product_items_packs.product_pack_id').
        joins('LEFT JOIN product_packs on product_packs.id = product_items_packs.product_pack_id').
        where("product_packs.status = #{ProductPack.statuses[:active]}").
        where("product_items.status = #{ProductItem.statuses[:active]}").
        where("product_codes_packs.product_code_id = #{product_code_id}").
        sum('product_items.cost')
    item_cost = ProductItem.
        joins('LEFT JOIN product_codes_items on product_codes_items.product_item_id = product_items.id').
        where("product_codes_items.product_code_id = #{product_code_id}").
        where("product_items.status = #{ProductItem.statuses[:active]}").
        sum(:cost)
    return pack_cost + item_cost
  end

end

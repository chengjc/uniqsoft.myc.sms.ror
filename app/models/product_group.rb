class ProductGroup < ActiveRecord::Base
  has_many :product_codes

  validates :description, length: {maximum: 100}, presence: true
  validates :display_order, presence: true
  validates :status, presence: true
  validates :description, uniqueness: {scope: [:status]}

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  def self.select_options_id_description
    ProductGroup.select(:id, :description).where(status: ProductGroup.statuses[:active])
  end
end

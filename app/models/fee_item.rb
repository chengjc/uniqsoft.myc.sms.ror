class FeeItem < ActiveRecord::Base

  enum fee_type: {
      program: 0,
      discount: 1,
  }

  enum lesson_fee_item: {
    term_fee: 0,
    proparated_lesson: 1,
    make_up_lesson: 2
  }

  def self.lesson_fee_item_price(myc_level_id, lesson_fee_item_id)

    myc_level = MycLevel.find(myc_level_id)
    if lesson_fee_item_id == FeeItem.lesson_fee_items[:term_fee]
      return myc_level.lesson_fee_per_term
    else
      return (myc_level.lesson_fee_per_term) / (myc_level.total_lesson)
    end
  end


end

class MycLevel < ActiveRecord::Base
  belongs_to :myc_program
  has_many :myc_classes

  validates :level_name, length: {maximum: 100}
  validates :age, length: {maximum: 10}, presence: true
  validates :total_lesson, numericality: {greater_than: 0}, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :lesson_per_term, numericality: {greater_than: 0}, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :lesson_fee_per_term, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true
  validates :lesson_duration_minute, numericality: true, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :level_order, numericality: true, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :status, presence: true
  validates :myc_program_id, presence: true

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  def self.active_myc_levels_order_by_display_order
    MycLevel.select(:id, :level_name).where(status: MycLevel.statuses[:active]).order(:level_order)
  end
end

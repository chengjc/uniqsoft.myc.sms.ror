class TeacherCommissionItem < ActiveRecord::Base
  belongs_to :teacher_commission

  validates :program, length: {maximum: 50}, presence: true
  validates :day, length: {maximum: 10}, presence: true
  validates :time, length: {maximum: 10}, presence: true
  validates :student_nric_name, length: {maximum: 100}, presence: true
  validates :lesson_date, length: {maximum: 300}, presence: true
  validates :term_fee, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true
  validates :lesson_fee, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true
  validates :no_of_lesson, numericality: true, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :total_fee, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true
  validates :percentage, numericality: true, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :commission, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true
  validates :remarks, length: {maximum: 100}

  def self.current_month_items(studio_id, teacher_id)
    Lesson.
        joins('LEFT JOIN myc_classes ON myc_classes.id = lessons.myc_class_id').
        joins('LEFT JOIN myc_programs ON myc_programs.id = myc_classes.myc_program_id').
        joins('LEFT JOIN attendances ON attendances.lesson_id = lessons.id').
        joins('LEFT JOIN students ON students.id = attendances.student_id').
        # joins('LEFT JOIN myc_classes_students ON myc_classes_students.myc_class_id = myc_classes.id').
        # joins('LEFT JOIN students ON students.id = myc_classes_students.student_id').
        select(
            'program_name',
            'myc_classes.start_at as myc_class_start_at',
            'students.nric_name as student_nric_name',
            "string_agg(to_char(lessons.start_at, 'DD/MM/YY') || '(L' || lesson_no || ')', ', ' order by lesson_no) as lesson_dates",
            'lesson_fee_per_term as term_fee',
            'lesson_fee_per_term/lesson_per_term as lesson_fee',
            'count(lessons.start_at) as no_of_lesson'
        ).
        where("date_trunc('month', lessons.start_at) = date_trunc('month', current_date)").
        where("lessons.status = #{Lesson.statuses[:marked]}").
        where("myc_classes.studio_id = #{studio_id}").
        where("teacher_id = #{teacher_id}").
        group(
            'program_name',
            'myc_classes.start_at',
            'students.nric_name',
            'term_fee',
            'lesson_fee'
        )
  end

  def self.validate_percentage(params)
    instance = self.new

    if ! is_positive_number_0_to_999(params[:percentage])
      instance.errors.add(:percentage, I18n.t('regex_positive_number_0_to_999_message'))
    end

    return instance
  end

  def self.is_positive_number_0_to_999(string)
    string.match(I18n.t('regex_positive_number_0_to_999'))? true : false
  end

end

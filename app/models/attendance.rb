class Attendance < ActiveRecord::Base
  belongs_to :lesson
  belongs_to :student

  enum status: {
      null: 0,
      present: 1,
      absence: 2,
      sick: 3
  }

  validates :lesson_id, presence: true
  validates :student_id, presence: true
  validates :status, presence: true

  def self.is_attendance_marked_for_student(student_id, myc_class_id)
    result = false
    @attendances = Attendance
                       .joins("INNER JOIN lessons ON attendances.lesson_id = lessons.id")
                       .joins("INNER JOIN students ON attendances.student_id = students.id")
                       .where("lessons.myc_class_id = #{myc_class_id}")
                       .where("attendances.student_id = #{student_id}")
                       .where("attendances.status <> #{Attendance.statuses[:null]}")

    result = true if @attendances.length > 0

    return result
  end

  def self.is_attendance_marked_for_lesson(lesson_id)
    result = false
    @attendances = Attendance
                       .joins("INNER JOIN lessons ON attendances.lesson_id = lessons.id")
                       .where("lessons.id = #{lesson_id}")
                       .where("attendances.status <> #{Attendance.statuses[:null]}")

    result = true if @attendances.length > 0

    return result
  end

  def self.is_attendance_marked_for_class(myc_class_id)
    result = false
    @attendances = Attendance
                        .joins("INNER JOIN lessons ON attendances.lesson_id = lessons.id")
                        .where("lessons.myc_class_id = #{myc_class_id}")
                        .where("attendances.status <> #{Attendance.statuses[:null]}")
    result = true if @attendances.length > 0
    return result
  end

  def self.unmarked_attendances_for_student(student_id, myc_class_id)
    @attendances = Attendance
                       .joins("INNER JOIN lessons ON attendances.lesson_id = lessons.id")
                       .joins("INNER JOIN students ON attendances.student_id = students.id")
                       .where("lessons.myc_class_id = #{myc_class_id}")
                       .where("attendances.student_id = #{student_id}")
                       .where("attendances.status = #{Attendance.statuses[:null]}")
  end

  def self.attendances_of_active_students_by_lesson(lesson_id)
    Attendance
        .joins("INNER JOIN lessons ON lessons.id = attendances.lesson_id")
        .joins("INNER JOIN students ON attendances.student_id = students.id")
        .where("lessons.id = #{lesson_id}")
        .where("students.id IN (
           SELECT myc_classes_students.student_id FROM myc_classes_students
           WHERE myc_classes_students.myc_class_id = lessons.myc_class_id AND myc_classes_students.status = #{MycClassStudent.statuses[:in_class]} )")
  end
end

class LatestInvoiceNumber < ActiveRecord::Base
  has_one :studio

  def self.latest_invoice_number_with_increment(studio_id)

    if LatestInvoiceNumber.where(studio_id: studio_id).count == 0
      LatestInvoiceNumber.create([studio_id: studio_id, latest_invoice_number: I18n.t('started_invoice_number')])
    end

    latest_invoice_number = LatestInvoiceNumber.where(studio_id: studio_id).pluck(:latest_invoice_number)
    existing_invoice_number = latest_invoice_number[0].to_s
    number = ''
    last_character_count = 0
    number_zero_count = 0
    existing_invoice_number_length = existing_invoice_number.length
    i = existing_invoice_number_length

    loop do
      i -= 1
      break if i < 0
      if is_number(existing_invoice_number[i])
        number = number + existing_invoice_number[i] # put increment number to variable
      else
        last_character_count = i
        j = i + 1
        loop do
          break if j > existing_invoice_number_length

          if existing_invoice_number[j] == '0' # check number start with zero
            number_zero_count += 1
            last_character_count = j
          else
            break
          end

          j += 1
        end
        last_character_count += 1
        break
      end

    end

    number = number.reverse
    if number_zero_count != 0
      number = number[(number_zero_count),(number.length - 1)]
    end
    increment_number = number.to_i + 1
    invoice_number_character = existing_invoice_number[0, last_character_count]
    increment_invoice_number = invoice_number_character + increment_number.to_s

    return increment_invoice_number

  end

  def self.is_number(object)
    return true if Float(object) rescue false
  end

end


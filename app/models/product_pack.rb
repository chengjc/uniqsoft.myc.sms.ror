class ProductPack < ActiveRecord::Base
  has_and_belongs_to_many :product_items, uniq: true
  has_and_belongs_to_many :product_code, uniq: true

  validates :description, length: {maximum: 100}, presence: true
  validates :status, presence: true
  validates :description, uniqueness: {scope: [:status]}

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  def self.select_options_id_description
    ProductPack.select(:id, :description).where(status: ProductPack.statuses[:active])
  end

  def self.cost(product_pack_id)
    ProductItem.
        joins('LEFT JOIN product_items_packs on product_items_packs.product_item_id = product_items.id').
        where("product_items_packs.product_pack_id = #{product_pack_id}").
        where("product_items.status = #{ProductItem.statuses[:active]}").
        sum(:cost)
  end
end

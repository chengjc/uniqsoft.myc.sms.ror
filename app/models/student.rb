class Student < ActiveRecord::Base

  has_many :lessons, through: :attendances
  has_many :attendances
  has_many :invoices

  has_many :myc_classes, through: :myc_class_students
  has_many :myc_class_students

  belongs_to :family , autosave = false
  belongs_to :myc_level
  accepts_nested_attributes_for :family

  enum status: {
      new_class_registered: 0,
      current: 1,
      suspended: 2,
      terminated: 3,
      deleted: 4
  }

  def self.humanize_statuses
    statuses = Student.statuses.to_a
    humanized_statuses = []
    statuses.each do |status|
      humanized_statuses << [ status[0].humanize, status[0]]
    end

    return humanized_statuses
  end

  def self.active_student_id_and_name_by_studio(studio_id)
    Student
        .where("studio_id = #{studio_id}")
        .where("(status = #{Student.statuses[:new_class_registered]} OR status = #{Student.statuses[:current]})")
        .select(
            "id",
            "nric_name"
        )
  end

  validates :myc_level_id, presence: true
  validates :family_id, presence: true
  validates :studio_id, presence: true
  validates :enrolment_piano, length: {maximum: 10}
  validates :enrolment_theory, length: {maximum: 10}
  validates :enrolment_other, length: {maximum: 10}
  validates :preferred_day_one, length: {maximum: 10}
  validates :preferred_day_two, length: {maximum: 10}
  validates :preferred_day_three, length: {maximum: 10}
  validates :preferred_time_one, length: {maximum: 10}
  validates :preferred_time_two, length: {maximum: 10}
  validates :preferred_time_three, length: {maximum: 10}
  validates :nric_name, length: {maximum: 50}, presence: true
  validates :first_name, length: {maximum: 50}, presence: true
  validates :sur_name, length: {maximum: 50}, presence: true
  validates :gender, length: {maximum: 10}, presence: true
  validates :date_of_birth, presence: true
  validates :piano_standard, length: {maximum: 50}
  validates :piano_college, length: {maximum: 50}
  validates :piano_school_attended, length: {maximum: 50}
  validates :theory_standard, length: {maximum: 50}
  validates :theory_college, length: {maximum: 50}
  validates :theory_school_attended, length: {maximum: 50}
  validates :violin_standard, length: {maximum: 50}
  validates :violin_college, length: {maximum: 50}
  validates :violin_school_attended, length: {maximum: 50}
  validates :other_standard, length: {maximum: 50}
  validates :other_college, length: {maximum: 50}
  validates :other_school_attended, length: {maximum: 50}
  validates :referred_by, length: {maximum: 50}, presence: true
  validates :enquiry_date, presence: true
  validates :status, presence: true

  validates_uniqueness_of :nric_name, :scope => :date_of_birth

end


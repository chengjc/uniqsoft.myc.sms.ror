class StudioAdmin < ActiveRecord::Base
  belongs_to :user, autosave = false
  accepts_nested_attributes_for :user

  has_many :studios
  accepts_nested_attributes_for :studios

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  validates :user_id, presence: true

  validates :nric_name, presence: true
  validates :nric_name, length: { maximum: 100 }

  validates :phone_number, presence: true
  validates :phone_number, length: { maximum: 20 }
end

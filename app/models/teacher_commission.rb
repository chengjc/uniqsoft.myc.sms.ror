class TeacherCommission < ActiveRecord::Base

  has_many :teacher_commission_items, dependent: :destroy
  has_many :teacher_commission_item_additionals, dependent: :destroy
  has_many :teacher_commission_others, dependent: :destroy

  validates :studio_id, presence: true
  validates :teacher_id, presence: true
  validates :teacher_nric_name, presence: true
  validates :statement_date, presence: true
  validates :net_pay, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true

  validates :studio_id, uniqueness: {scope: [:teacher_id, :statement_date]}

  def self.current_month_teachers(studio_id)
    Lesson.
        joins('LEFT JOIN teachers ON lessons.teacher_id = teachers.id').
        joins('LEFT JOIN users ON users.id = teachers.user_id').
        joins('LEFT JOIN myc_classes ON myc_classes.id = lessons.myc_class_id').
        select(
            'DISTINCT ON (teacher_id) teacher_id',
            'users.nric_name as teacher_nric_name'
        ).
        where("date_trunc('month', lessons.start_at) = date_trunc('month', current_date)").
        where("myc_classes.studio_id = #{studio_id}")
  end

  def self.net_pay(teacher_commission_id)
    commission = TeacherCommissionItem.where(teacher_commission_id: teacher_commission_id).sum(:commission)
    commission_additional = TeacherCommissionItemAdditional.where(teacher_commission_id: teacher_commission_id).sum(:commission)
    others = TeacherCommissionOther.where(teacher_commission_id: teacher_commission_id).sum(:amount)
    net_pay = ActionController::Base.helpers.number_with_precision(commission + commission_additional + others, precision: 4)

    if (TeacherCommission.where(id: teacher_commission_id).update_all(net_pay: net_pay) == 0)
      raise 'Failed to update net pay'
    end

    TeacherCommission.where(id: teacher_commission_id).pluck(:net_pay).first
  end

  def self.generate(studio_id)
    teacher_commission = TeacherCommission.new
    teacher_commission_item = TeacherCommissionItem.new

    begin
      ActiveRecord::Base.transaction do
        TeacherCommission.where("statement_date >= '#{Date.current.beginning_of_month.strftime('%m/%d/%Y')}' AND statement_date <= '#{Date.current.end_of_month.strftime('%m/%d/%Y')}'").destroy_all
        current_month_teachers = TeacherCommission.current_month_teachers(studio_id)

        current_month_teachers.each do |current_month_teacher|
          teacher_commission = TeacherCommission.new(
              studio_id: studio_id,
              teacher_id: current_month_teacher.teacher_id,
              teacher_nric_name: current_month_teacher.teacher_nric_name,
              statement_date: Date.current.to_s('%m/%d/%Y'),
              net_pay: 0 # update later
          )
          teacher_commission.save!


          TeacherCommissionItem.current_month_items(studio_id, current_month_teacher.teacher_id).each do |item|
            total_fee = item.no_of_lesson * item.lesson_fee
            percentage = Rails.configuration.teacher_commission_percentage

            teacher_commission_item = TeacherCommissionItem.new(
                teacher_commission_id: teacher_commission.id,
                program: item.program_name,
                day: item.myc_class_start_at.strftime('%a'),
                time: I18n.l(item.myc_class_start_at.localtime, format: :short),
                student_nric_name: item.student_nric_name,
                lesson_date: item.lesson_dates,
                term_fee: ActionController::Base.helpers.number_with_precision(item.term_fee, precision: 4),
                lesson_fee: ActionController::Base.helpers.number_with_precision(item.lesson_fee, precision: 4),
                no_of_lesson: item.no_of_lesson,
                total_fee: ActionController::Base.helpers.number_with_precision(total_fee, precision: 4),
                percentage: percentage,
                commission: ActionController::Base.helpers.number_with_precision(total_fee * percentage / 100, precision: 4)
            )
            teacher_commission_item.save!
          end

          # update net_pay
          TeacherCommission.net_pay(teacher_commission.id)
        end
      end
    rescue Exception => exception
      logger.error(exception.to_s) # output errors and continue the program
      teacher_commission_item.errors.each do |key, value|
        teacher_commission.errors.add(key, value)
      end
      if teacher_commission.errors.count == 0
        teacher_commission.errors.add('error', I18n.t('generate_fail'))
      end
    end

    return teacher_commission
  end

end

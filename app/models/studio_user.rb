class StudioUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :studio

  enum role: {
      super_user: 0,
      studio_admin: 1,
      teacher: 2
  }

  validates :studio_id, presence: true
  validates :user_id, presence: true
  validates :role_id, presence: true

  validates :studio_id, uniqueness: {scope: [:user_id]}

  def self.role_name(role_id)
    StudioUser.roles.each do |role|
      if role[1] == role_id.to_i
        return role[0].humanize
      end
    end
  end

  def self.role_select_options_without_hq
    array = []
    StudioUser.roles.each do |role|
      array.push({id: role[1] , name: role[0].humanize}) unless role[1] == StudioUser.roles[:super_user]
    end
    return array
  end
end

class Studio < ActiveRecord::Base

  has_many :studio_user
  has_many :users, through: :studio_user
  has_many :holiday

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  validates :studio_name, :presence => true,
            :length => {:maximum => 100}
  validates :studio_code, :presence => true,
            :length => {:maximum => 100}
  # :uniqueness => true :conditions =>{ where.not(status: 'deleted') }

  validates_uniqueness_of :studio_code, scope: [:studio_name, :status]

  validates :address, length: {:maximum => 100}
  validates :post_code, length: {:maximum => 10}
  validates :company_name, length: {:maximum => 100}
  validates :company_registration_no, length: {:maximum => 50}
  validates :phone_no, length: {:maximum => 50}
  validates :email, :presence => false,
            :length => {:maximum => 100},
            :format => {:with => /.+@.+\..+/i},
            :allow_blank => true

  validates :facebook_address, length: {maximum: 100}
  validates :website_address, length: {maximum: 200}
  validates :status, :presence => true


  def self.studio_select_options_without_hq
    Studio.select('id, studio_name').where(status: Studio.statuses[:active]).where("studio_name <> '#{Rails.configuration.hq_studio_name}'")
  end
end

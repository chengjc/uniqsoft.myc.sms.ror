class InvoiceItem < ActiveRecord::Base
  belongs_to :invoice

  validates :invoice_id, presence: true
  validates :item_name, length: {maximum: 50}, presence: true
  validates :quantity, numericality: true, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :price, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true

  def self.create(invoice_item, invoice)

    begin
      ActiveRecord::Base.transaction do
        invoice_item.save!
        invoice.save!
      end
    rescue Exception => exception
      logger.error(exception.to_s) # output errors and continue the program
      invoice.errors.each do |key, value|  # put all errors inside 1 model variable
        invoice_item.errors.add(key, value)
      end

      if invoice_item.errors.count == 0 # if no validation errors
        invoice_item.errors[:base] << I18n.t('save_fail') # if server side error, show a generic error message
      end
    end

  invoice_item
  end

  def self.destroy(invoice_item, invoice)

    begin
      ActiveRecord::Base.transaction do
        invoice_item.destroy!
        invoice.save!
      end
    rescue Exception => exception
      logger.error(exception.to_s) # output errors and continue the program
      invoice.errors.each do |key, value|  # put all errors inside 1 model variable
        invoice_item.errors.add(key, value)
      end

      if invoice_item.errors.count == 0 # if no validation errors
        invoice_item.errors[:base] << I18n.t('delete_fail') # if server side error, show a generic error message
      end
    end

    invoice_item
  end
end





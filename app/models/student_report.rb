class StudentReport < ActiveRecord::Base
  validates :studio_id, presence: true
  validates :teacher_id, presence: true
  validates :report_date, presence: true
  validates :student_count_sr1, presence: true
  validates :student_count_sr2, presence: true
  validates :student_count_sr3, presence: true
  validates :student_count_ss1, presence: true
  validates :student_count_ss2, presence: true
  validates :student_count_sb1, presence: true
  validates :student_count_sb2, presence: true
  validates :student_count_sb3, presence: true
  validates :student_count_mb1, presence: true
  validates :student_count_mb2, presence: true
  validates :student_count_mb3, presence: true
  validates :total, presence: true
  validates :report_date, uniqueness: {scope: [:studio_id, :teacher_id]}

  def self.generate
    student_report = StudentReport.new

    begin
      ActiveRecord::Base.transaction do
        StudentReport.destroy_all

        StudentReport.reports.each do |report|
          student_count_sr1 = 0
          student_count_sr2 = 0
          student_count_sr3 = 0
          student_count_ss1 = 0
          student_count_ss2 = 0
          student_count_sb1 = 0
          student_count_sb2 = 0
          student_count_sb3 = 0
          student_count_mb1 = 0
          student_count_mb2 = 0
          student_count_mb3 = 0

          period_date = Date.strptime("01/#{report.month_year}", '%d/%m/%y')

          programs = MycProgram.select(:id).select(:program_code).where(status: MycProgram.statuses[:active])
          programs.each do |program|
            count = StudentReport.student_count(report.studio_id, report.teacher_id, program.id, period_date)

            case program.program_code
              when 'SR1'
                student_count_sr1 = count
              when 'SR2'
                student_count_sr2 = count
              when 'SR3'
                student_count_sr3 = count
              when 'SS1'
                student_count_ss1 = count
              when 'SS2'
                student_count_ss2 = count
              when 'SB1'
                student_count_sb1 = count
              when 'SB2'
                student_count_sb2 = count
              when 'SB3'
                student_count_sb3 = count
              when 'MB1'
                student_count_mb1 = count
              when 'MB2'
                student_count_mb2 = count
              when 'MB3'
                student_count_mb3 = count
            end
          end

          total =
              student_count_sr1 +
                  student_count_sr2 +
                  student_count_sr3 +
                  student_count_ss1 +
                  student_count_ss2 +
                  student_count_sb1 +
                  student_count_sb2 +
                  student_count_sb3 +
                  student_count_mb1 +
                  student_count_mb2 +
                  student_count_mb3

          student_report = StudentReport.new(
              studio_id: report.studio_id,
              teacher_id: report.teacher_id,
              report_date: period_date,
              student_count_sr1: student_count_sr1,
              student_count_sr2: student_count_sr2,
              student_count_sr3: student_count_sr3,
              student_count_ss1: student_count_ss1,
              student_count_ss2: student_count_ss2,
              student_count_sb1: student_count_sb1,
              student_count_sb2: student_count_sb2,
              student_count_sb3: student_count_sb3,
              student_count_mb1: student_count_mb1,
              student_count_mb2: student_count_mb2,
              student_count_mb3: student_count_mb3,
              total: total
          )
          student_report.save!

        end
      end
    rescue Exception => exception
      logger.error(exception.to_s) # output errors and continue the program
      if student_report.errors.size == 0
        student_report.errors.add(:error, I18n.t('generate_fail'))
      end
    end

    return student_report
  end

  def self.reports
    Lesson
        .joins('LEFT JOIN myc_classes ON myc_classes.id = lessons.myc_class_id')
        .select("to_char(lessons.start_at, 'MM/YY') as month_year")
        .select("teacher_id")
        .select("myc_classes.studio_id")
        .group(:month_year)
        .group(:teacher_id)
        .group(:studio_id)
        .order('month_year')
  end

  def self.student_count(studio_id, teacher_id, myc_program_id, period_date)

    start_date = period_date.beginning_of_month.strftime('%m/%d/%Y') # date for sql
    end_date = period_date.end_of_month.strftime('%m/%d/%Y') # date for sql
    date_of_3rd_week = (period_date.end_of_month - 7.days).strftime('%m/%d/%Y') # date for sql
    MycClassStudent
        .joins('LEFT JOIN myc_classes ON myc_classes.id = myc_classes_students.myc_class_id')
        .joins('LEFT JOIN lessons ON lessons.myc_class_id = myc_classes.id')
        .select("myc_classes_students.student_id").distinct
        .where("myc_classes.studio_id = #{studio_id}")
        .where("myc_classes.myc_program_id = #{myc_program_id}")
        .where("myc_classes.status <> #{MycClass.statuses[:dissolved]}")
        .where("lessons.teacher_id = #{teacher_id}")
        .where("lessons.start_at >= '#{start_date}' AND lessons.start_at <= '#{end_date}'")
        .where("NOT EXISTS (
          SELECT status from myc_classes_students
          where
          status <> #{MycClassStudent.statuses[:in_class]}
          AND myc_classes_students.updated_at >= '#{date_of_3rd_week}'
          AND myc_classes_students.updated_at <= '#{end_date}')")
        .where("NOT EXISTS (
          SELECT status from myc_classes
          where
          status = #{MycClass.statuses[:dissolved]}
          AND myc_classes.status_updated_at >= '#{date_of_3rd_week}'
          AND myc_classes.status_updated_at <= '#{end_date}')")
        .count

  end

end

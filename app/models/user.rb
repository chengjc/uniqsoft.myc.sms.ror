class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :timeoutable and :omniauthable, :async, :confirmable, :registerable
  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

  has_one :teacher
  has_many :studio_user
  has_many :studios, through: :studio_user

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2,
      on_leave: 3,
      terminated: 4,
  }

  validates :nric_name, presence: true
  validates :nric_name, length: {maximum: 100}
  validates :email, presence: true
  validates :email, length: {maximum: 100}
  validates :email, format: {with: /.+@.+\..+/i}
  validates :email, uniqueness: true
  validates :status, presence: true

  #this method is called by devise to check for "active" state of the model
  def active_for_authentication?
    #remember to call the super
    #then put our own check to determine "active" state using
    #our own "is_active" column
    super and self.active? and self.studios.count > 0
  end

  def self.status_name(status_id)
    User.statuses.each do |status|
      if status[1] == status_id.to_i
        return status[0].humanize
      end
    end
  end
end

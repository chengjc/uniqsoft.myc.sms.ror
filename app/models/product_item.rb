class ProductItem < ActiveRecord::Base
  has_and_belongs_to_many :product_packs, uniq: true
  # before_save :update_total_cost, if: :cost_changed?


  validates :description, length: {maximum: 100}, presence: true
  validates :cost, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true
  validates :quantity_on_hand, numericality: true, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :quantity_trigger_alert, numericality: true, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :remarks, length: {maximum: 200}
  validates :status, presence: true
  validates :description, uniqueness: {scope: [:status]}

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  def self.select_options_id_description
    ProductItem.select(:id, :description).where(status: ProductItem.statuses[:active])
  end

end

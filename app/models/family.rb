class Family < ActiveRecord::Base
  has_many :students
  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  validates :address1, length: {maximum: 100}, presence: true
  validates :address2, length: {maximum: 100}
  validates :postal_code, length: {maximum: 50}
  validates :city_or_state, length: {maximum: 50}, presence: true
  validates :country, length: {maximum: 50}, presence: true
  validates :primary_contact_name, length: {maximum: 50}, presence: true
  validates :primary_contact_relationship, length: {maximum: 50}, presence: true
  validates :primary_contact_phone_no, length: {maximum: 50}, presence: true
  validates :primary_contact_email, length: {maximum: 50}, presence: true
  validates :primary_contact_email, format: {with: /.+@.+\..+/i}
  validates :primary_contact_occupation, length: {maximum: 100}
  validates :other_contact_name, length: {maximum: 50}
  validates :other_contact_relationship, length: {maximum: 50}
  validates :other_contact_phone_no, length: {maximum: 50}
  validates :other_contact_email, length: {maximum: 50}
  validates :other_contact_email, format: {with: /.+@.+\..+/i}, allow_blank: true
  validates :other_contact_occupation, length: {maximum: 100}
  validates :status, presence: true

  validate :is_checked_accompany_for_student

  def is_checked_accompany_for_student
    errors.add(:base, I18n.t('check_accompany_for_student')) unless primary_contact_accompany_for_student || other_contact_accompany_for_student
  end


end


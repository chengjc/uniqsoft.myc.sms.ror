class Invoice < ActiveRecord::Base
  has_many :latest_invoice_numbers
  has_many :invoice_items
  belongs_to :student
  belongs_to :myc_level
  enum status: {
      open: 0,
      partially_paid: 1,
      paid: 2,
      overdue: 3,
      void: 4,
      deleted: 5
  }

  validates :studio_id, presence: true
  validates_presence_of :student_id, :message => I18n.t("add_student_to_invoice")
  validates :invoice_number, length: {maximum: 20}, presence: true
  validates :invoice_number, uniqueness: {scope: [:studio_id]}
  validates :invoice_on, presence: true
  validates :due_on, presence: true
  validates :total_price, presence: true, numericality: true
  validates :bal_due, presence: true, numericality: true
  validates :status, presence: true

  def self.create (invoice, latest_invoice_number)

    begin
      ActiveRecord::Base.transaction do
        invoice.save!
        latest_invoice_number.save!
      end
    rescue Exception => exception
      logger.error(exception.to_s) # output errors and continue the program
      latest_invoice_number.errors.each do |key, value|  # put all errors inside 1 model variable
        invoice.errors.add(key, value)
      end

      if invoice.errors.count == 0 # if no validation errors
        invoice.errors[:base] << I18n.t('save_fail') # if server side error, show a generic error message
      end
    end

    invoice
  end

  def self.update (invoice, invoice_params, latest_invoice_number)
    begin
      ActiveRecord::Base.transaction do
        invoice.update!(invoice_params)
        latest_invoice_number.save!
      end
    rescue Exception => exception
      logger.error(exception.to_s) # output errors and continue the program
      latest_invoice_number.errors.each do |key, value|  # put all errors inside 1 model variable
        invoice.errors.add(key, value)
      end

      if invoice.errors.count == 0 # if no validation errors
        invoice.errors[:base] << I18n.t('save_fail') # if server side error, show a generic error message
      end
    end

    invoice
  end

end

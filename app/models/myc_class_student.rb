class MycClassStudent < ActiveRecord::Base
  self.table_name = "myc_classes_students"

  belongs_to :myc_class
  belongs_to :student

  enum status: {
      in_class: 0,
      dropped_out: 1,
      graduated: 2,
      take_a_break: 3,
      suspended: 4,
      terminated: 5
  }

  validates :myc_class_id, presence: true
  validates :student_id, presence: true
  validates :status, presence: true

  def self.status_for_remove_student_from_class
    statuses_for_remove_student = []
    statuses = MycClassStudent.statuses.to_a
    statuses.collect {|status| statuses_for_remove_student.push(status) if status[0] == "dropped_out" || status[0] == "take_a_break" }
    return statuses_for_remove_student
  end

  def self.myc_class_student_by_class_and_student(myc_class_id, student_id)
    @myc_class_student = MycClassStudent.where(:myc_class_id => myc_class_id).where(:student_id => student_id).first!
  end

  def self.by_class_id_and_status(myc_class_id, status)
    MycClassStudent
        .where(myc_class_id: myc_class_id)
        .where(status: status)
  end

end

class MycClass < ActiveRecord::Base
  has_many :students, through: :myc_class_students
  has_many :myc_class_students

  belongs_to :studio
  belongs_to :myc_level
  has_one :myc_program, through: :myc_level

  has_many :lessons

  attr_accessor :start_date, :start_time, :teacher_id

  enum status:{
      on_going: 0,
      dissolved: 1,
      finished: 2
  }

  validates :start_at, presence: true
  validates :studio_id, presence: true
  validates :myc_level_id, presence: true
  validates :status, presence: true
  validates :status_updated_at, presence: true

  def self.students_by_class_id(myc_class_id)
    @myc_class = MycClass.find(myc_class_id)
    @students = @myc_class.students
                    .order("students.nric_name")
  end

end

class TeacherCommissionItemAdditional < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper

  belongs_to :teacher_commission
  before_validation :assign_total_fee_and_commission

  validates :program, length: {maximum: 50}, presence: true
  validates :day, length: {maximum: 10}, presence: true
  validates :time, length: {maximum: 10}, presence: true
  validates :student_nric_name, length: {maximum: 100}, presence: true
  validates :lesson_date, length: {maximum: 300}, presence: true
  validates :term_fee, numericality: true, format: {with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message')}, presence: true
  validates :lesson_fee, numericality: true, format: {with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message')}, presence: true
  validates :no_of_lesson, numericality: true, format: {with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message')}, presence: true
  validates :total_fee, numericality: true, format: {with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message')}, presence: true
  validates :percentage, numericality: true, format: {with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message')}, presence: true
  validates :commission, numericality: true, format: {with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message')}, presence: true
  validates :remarks, length: {maximum: 100}

  def self.validate_lesson_fee_and_no_of_lesson_and_percentage(params)
    instance = self.new
    if ! is_currency(params[:lesson_fee])
      instance.errors.add(:lesson_fee, I18n.t('regex_currency_message'))
    end

    if ! is_positive_number_0_to_999(params[:no_of_lesson])
      instance.errors.add(:no_of_lesson, I18n.t('regex_positive_number_0_to_999_message'))
    end

    if ! is_positive_number_0_to_999(params[:percentage])
      instance.errors.add(:percentage, I18n.t('regex_positive_number_0_to_999_message'))
    end

    return instance
  end

  def self.is_currency(string)
    string.match(I18n.t('regex_currency'))? true : false
  end

  def self.is_positive_number_0_to_999(string)
    string.match(I18n.t('regex_positive_number_0_to_999'))? true : false
  end

  protected
  def assign_total_fee_and_commission
    no_of_lesson = self.no_of_lesson.to_i
    lesson_fee = self.lesson_fee.to_f
    percentage = self.percentage.to_f
    total_fee = no_of_lesson * lesson_fee
    self.total_fee = number_with_precision(total_fee, precision: 4)
    self.commission = number_with_precision(total_fee * percentage / 100, precision: 4)
  end
end

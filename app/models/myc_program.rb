class MycProgram < ActiveRecord::Base

  has_many :product_codes
  has_many :myc_levels
  has_many :myc_classes, through: :myc_level

  validates :program_name, length: {maximum: 100}, presence: true
  validates :program_code, length: {maximum: 100}, presence: true
  validates :program_order, numericality: true, format: { with: I18n.t('regex_positive_number_0_to_999'), message: I18n.t('regex_positive_number_0_to_999_message') }, presence: true
  validates :status, presence: true
  validates :program_code, uniqueness: {scope: [:status]}

  enum status: {
      active: 0,
      inactive: 1,
      deleted: 2
  }

  def self.active_myc_programs_order_by_display_order
    # MycProgram.where("status = #{MycProgram.statuses[:active]}").order(:program_order)
    MycProgram.select(:id, :program_name).where(status: MycProgram.statuses[:active]).order(:program_order)
  end

  def self.active_program_and_level_order_by_display_order
    MycProgram.joins("INNER JOIN myc_levels ON myc_programs.id = myc_levels.myc_program_id")
        .where("myc_programs.status = #{MycProgram.statuses[:active]}")
        .where("myc_levels.status = #{MycLevel.statuses[:active]}")
        .order("myc_programs.program_order", "myc_levels.level_order")
        .select(
            "myc_levels.id",
            "concat_ws(' ', myc_programs.program_name, myc_levels.level_name) as program_and_level_name"
        )
  end

  def self.active_program_and_level_order_by_level_id(level_id)
    myc_program = MycProgram.joins("INNER JOIN myc_levels ON myc_programs.id = myc_levels.myc_program_id")
        .where("myc_levels.id = #{level_id}")
        .select(
            "concat_ws(' ', myc_programs.program_name, myc_levels.level_name) as program_and_level_name"
        )
    return myc_program[0].program_and_level_name
  end
end

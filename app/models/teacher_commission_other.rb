class TeacherCommissionOther < ActiveRecord::Base
  belongs_to :teacher_commission

  validates :item, length: {maximum: 100}, presence: true
  validates :amount, numericality: true, format: { with: I18n.t('regex_currency'), message: I18n.t('regex_currency_message') }, presence: true
  validates :remarks, length: {maximum: 100}
end

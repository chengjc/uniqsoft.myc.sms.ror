class Holiday < ActiveRecord::Base
  belongs_to :studio

  validates :studio_id, presence: true
  validates :holiday_date, presence: true
  validates :holiday_name, presence: true, length: {maximum: 100}

  validates :holiday_date, uniqueness: {scope: [:studio_id]}

  def self.year_for_filter
    maximum_year = Holiday.maximum(:holiday_date)
    minimum_year = Holiday.minimum(:holiday_date)
    year_for_filter = []

    if maximum_year != nil || minimum_year != nil
    maximum_year = maximum_year.strftime("%Y").to_i
    minimum_year = minimum_year.strftime("%Y").to_i

    until minimum_year > maximum_year
      year_for_filter << minimum_year
      minimum_year += 1
    end
    return year_for_filter
    end

    return year_for_filter
  end

  def self.events_for_calendar(studio_id, start_date, end_date)
    Holiday.
        where(studio_id: studio_id).
        where("holiday_date >= '#{start_date}' AND holiday_date <= '#{end_date}'").
        select(
            'id',
            "holiday_name as title",
            "holiday_date as start",
            "holiday_date as end"
        )

  end

  def self.by_studio_and_holiday_date(studio_id, holiday_date)
    Holiday
        .where(studio_id: studio_id)
        .where(holiday_date: holiday_date)
  end
end

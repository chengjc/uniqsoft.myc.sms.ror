class Lesson < ActiveRecord::Base

  belongs_to :myc_class

  has_many :students, through: :attendances
  has_many :attendances
  belongs_to :teacher

  enum status: {
      not_marked: 0,
      marked: 1,
      cancel: 2
  }

  validates :lesson_no, presence: true
  validates :start_at, presence: true
  validates :status, presence: true
  validates :myc_class_id, presence: true
  validates :teacher_id, presence: true

  def self.lessons_and_attendances_by_class(myc_class_id)
    Lesson.joins('LEFT JOIN teachers ON lessons.teacher_id = teachers.id')
        .joins('LEFT JOIN users ON teachers.user_id = users.id')
        .joins('LEFT JOIN attendances ON attendances.lesson_id = lessons.id')
        .joins('LEFT JOIN students ON attendances.student_id = students.id')
        .where("lessons.myc_class_id = #{myc_class_id}")
        .select(
            "lessons.id",
            "lessons.lesson_no",
            "lessons.start_at",
            "attendances.status as attendance_status",
            "users.nric_name as user_name",
            "lessons.reason_for_cancel"
        )
        .order("lessons.lesson_no", "students.nric_name")
  end

#   get all lessons below the specified lesson no which are not cancel or dissolve
  def self.all_lessons_below_specify_lesson_no(lesson_no, myc_class_id)
    Lesson.where("myc_class_id = #{myc_class_id}")
        .where("lesson_no >= #{lesson_no}")
        .where("status <> #{ Lesson.statuses[:cancel] }")
  end

  def self.is_lesson_marked(lesson_id)
    result = false
    @lesson = Lesson.where("id = #{lesson_id}").where("status <> #{Lesson.statuses[:not_marked]}").take
    result = true if @lesson.present?
    return result
  end

  def self.lessons_which_are_not_cancelled_by_class(myc_class_id)
    @lessons = Lesson
                   .where("myc_class_id = #{myc_class_id}")
                   .where("status <> #{Lesson.statuses[:cancel]}")
  end

  def self.events_for_calendar(studio_id, start_date, end_date)
    Lesson
        .joins('LEFT JOIN teachers ON lessons.teacher_id = teachers.id')
        .joins('LEFT JOIN users ON users.id = teachers.user_id')
        .joins('LEFT JOIN myc_classes ON myc_classes.id = lessons.myc_class_id')
        .joins('LEFT JOIN myc_programs ON myc_programs.id = myc_classes.myc_program_id')
        .select('lessons.id as id')
        .select("program_code || ',' || users.nric_name as title")
        .select('lessons.start_at as start')
        .select("lessons.start_at + (lesson_duration_minute * interval '1 minute') as end")
        .where("date_trunc('month', lessons.start_at) = date_trunc('month', current_date)")
        .where("myc_classes.studio_id = #{studio_id}")
        .where("lessons.start_at >= '#{start_date}' AND lessons.start_at <= '#{end_date}'")
  end

  def self.all_lesson_teacher_name(myc_class_id)
    lesson_teacher_name = Lesson
                            .where("myc_class_id = #{myc_class_id}")
                            .joins('JOIN teachers ON lessons.teacher_id = teachers.id')
                            .joins('JOIN users ON users.id = teachers.user_id')
                            .select(
                                "array_to_string(array_agg(distinct(users.nric_name)) , ' , ') as teacher_name"
                            )
    return lesson_teacher_name[0]['teacher_name']

  end

end

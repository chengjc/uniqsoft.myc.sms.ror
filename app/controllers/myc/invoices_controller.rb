class Myc::InvoicesController < ApplicationController
  include ApplicationHelper
  def index_data
    respond_to do |format|
      format.json { render json: InvoiceDatatable.new(view_context, { studio_id: session[:current_studio_id] }) }
    end
  end

  def index
  end

  def new
    @invoice = Invoice.new
    @invoice[:invoice_number] = LatestInvoiceNumber.latest_invoice_number_with_increment(session[:current_studio_id])
    @invoice[:status] = Invoice.statuses[:open]
  end

  def create
    invoice_params_with_merge = invoice_params.merge(:total_price=> 0.00, :bal_due=> 0.00)
    invoice = Invoice.new(invoice_params_with_merge)
    latest_invoice_number = LatestInvoiceNumber.find_by_studio_id(session[:current_studio_id])
    latest_invoice_number.latest_invoice_number = invoice.invoice_number
    @invoice = Invoice.create(invoice, latest_invoice_number)
    if @invoice .errors.size == 0
      flash[:success] = t('save_success')
      redirect_to(edit_myc_invoice_path(@invoice.id))
    else
      render('new')
    end

  end

  def edit
    @invoice =Invoice.find(params[:id])
    @student_name = Student.where(id: @invoice.student_id).pluck(:nric_name)
    @program_name = MycProgram.active_program_and_level_order_by_level_id(@invoice.myc_level_id)
    @total_price = number_to_currency(@invoice.total_price)
    @invoice[:invoice_on] = @invoice.invoice_on.to_s
    @invoice[:due_on] = @invoice.due_on.to_s
  end

  def update
    invoice = Invoice.find(params[:id])
    latest_invoice_number = LatestInvoiceNumber.find_by_studio_id(session[:current_studio_id])
    latest_invoice_number.latest_invoice_number = invoice_params[:invoice_number]
    @invoice = Invoice.update(invoice, invoice_params, latest_invoice_number)
    if @invoice .errors.size == 0
      flash[:success] = t('save_success')
      render('index')
    else
      render('edit')
    end
  end

  def choose_student
  end

  def add_existing_student
    @invoice = Invoice.new
    myc_class_student = MycClassStudent.find(params[:myc_class_student_id][0].to_i)
    @invoice[:student_id] = myc_class_student.student_id
    @student_name = myc_class_student.student.nric_name
    @invoice[:myc_level_id] = myc_class_student.myc_class.myc_level_id
    @program_name = MycProgram.active_program_and_level_order_by_level_id(myc_class_student.myc_class.myc_level_id)
    @invoice[:day] = myc_class_student.myc_class.start_at.strftime("%I:%M %p")
    @invoice[:time] = myc_class_student.myc_class.start_at.strftime("%a")
    @invoice[:teacher_name] = Lesson.all_lesson_teacher_name(myc_class_student.myc_class_id)

    respond_to do |format|
      format.js
    end
  end

  def add_new_student
    @invoice = Invoice.new
    student = Student.find(params[:id][0].to_i)
    @invoice[:student_id] = student.id
    @invoice[:myc_level_id] = student.myc_level_id
    @student_name = student.nric_name
    @program_name = MycProgram.active_program_and_level_order_by_level_id(student.myc_level_id)

    respond_to do |format|
      format.js
    end
  end

  def invoice_new_student_data
    respond_to do |format|
      format.json { render json: InvoiceNewStudentDatatable.new(view_context, { studio_id: session[:current_studio_id] }) }
    end
  end

  def invoice_existing_student_data
    respond_to do |format|
      format.json { render json: InvoiceExistingStudentDatatable.new(view_context, { studio_id: session[:current_studio_id] }) }
    end
  end

  def program
    @invoice = Invoice.find(params[:id])
    @fee_item_data_source = "/myc/invoices/#{@invoice.id}/program_fee_item_data";
    @submit_link_to = "/myc/invoices/#{@invoice.id}/add_program_fee_item";
    @modal_title = 'Program'
  end

  def discount
    @invoice = Invoice.find(params[:id])
    @fee_item_data_source = "/myc/invoices/#{@invoice.id}/discount_fee_item_data";
    @submit_link_to = "/myc/invoices/#{@invoice.id}/add_discount_fee_item";
    @modal_title = 'Discount'
  end

  def lesson
    @invoice = Invoice.find(params[:id])
    @fee_item_data_source = "/myc/invoices/#{@invoice.id}/lesson_fee_item_data";
    @submit_link_to = "/myc/invoices/#{@invoice.id}/add_lesson_fee_item";
    @modal_title = 'Lesson'
  end

  def material
    invoice = Invoice.find(params[:id])
    @myc_level_id = invoice.myc_level_id
  end

  def other
    @invoice = Invoice.find(params[:id])
  end

  def material_data
    myc_level_id = params['columns']['3']['search']['value']
    respond_to do |format|
      format.json { render json: MaterialForInvoiceDatatable.new(view_context, { myc_level_id: myc_level_id }) }
    end
  end

  def program_fee_item_data
    fee_type = FeeItem.fee_types['program']
    respond_to do |format|
      format.json { render json: FeeItemDatatable.new(view_context, { fee_type: fee_type }) }
    end
  end

  def discount_fee_item_data
    fee_type = FeeItem.fee_types['discount']
    respond_to do |format|
      format.json { render json: FeeItemDatatable.new(view_context, { fee_type: fee_type }) }
    end
  end

  def lesson_fee_item_data
    myc_level_id = Invoice.find(params[:id]).myc_level_id
    respond_to do |format|
      format.json { render json: LessonFeeItemDatatable.new(view_context, { myc_level_id: myc_level_id }) }
    end
  end

  def add_program_fee_item
    @invoice_item = InvoiceItem.new
    if params[:fee_item_id] == nil
      return @invoice_item.errors.add(:base, t('fee_item_select'))
    else
      fee_item = FeeItem.find(params[:fee_item_id][0].to_i)
      @invoice_item.invoice_id = params[:id]
      @invoice_item.item_name = fee_item.fee_item_name
      @invoice_item.quantity = params[:quantity]
      @invoice_item.price = fee_item.price

      invoice = Invoice.find(params[:id])
      invoice.total_price = invoice.total_price + (@invoice_item.quantity * @invoice_item.price)
      invoice.bal_due = invoice.total_price

      @invoice_item = InvoiceItem.create(@invoice_item, invoice)
      @total_price = number_to_currency(Invoice.where(id: params[:id]).pluck(:total_price)[0])
      flash[:success] = I18n.t('create_success') if @invoice_item .errors.size == 0
    end
  end

  def add_discount_fee_item
    @invoice_item = InvoiceItem.new
    if params[:fee_item_id] == nil
      return @invoice_item.errors.add(:base, t('fee_item_select'))
    else
      fee_item = FeeItem.find(params[:fee_item_id][0].to_i)
      @invoice_item.invoice_id = params[:id]
      @invoice_item.item_name = fee_item.fee_item_name
      @invoice_item.quantity = params[:quantity]
      @invoice_item.price = fee_item.price

      invoice = Invoice.find(params[:id])
      invoice.total_price = invoice.total_price + (@invoice_item.quantity * @invoice_item.price)
      invoice.bal_due = invoice.total_price

      @invoice_item = InvoiceItem.create(@invoice_item, invoice)
      @total_price = number_to_currency(Invoice.where(id: params[:id]).pluck(:total_price)[0])
      flash[:success] = I18n.t('create_success') if @invoice_item .errors.size == 0
    end
  end

  def add_lesson_fee_item
    @invoice_item = InvoiceItem.new
    if params[:fee_item_id] == nil
      return @invoice_item.errors.add(:base, t('fee_item_select'))
    else
      invoice = Invoice.find(params[:id])
      @invoice_item.invoice_id = params[:id]
      @invoice_item.item_name = FeeItem.lesson_fee_items.keys[params[:fee_item_id][0].to_i].humanize
      @invoice_item.quantity = params[:quantity]
      @invoice_item.price = number_to_currency(FeeItem.lesson_fee_item_price(invoice.myc_level_id, params[:fee_item_id][0].to_i))

      invoice.total_price = invoice.total_price + (@invoice_item.quantity * @invoice_item.price)
      invoice.bal_due = invoice.total_price

      @invoice_item = InvoiceItem.create(@invoice_item, invoice)
      @total_price = number_to_currency(Invoice.where(id: params[:id]).pluck(:total_price)[0])
      flash[:success] = I18n.t('create_success') if @invoice_item .errors.size == 0
    end

  end

  def add_material_fee_item
    @invoice_item = InvoiceItem.new
    if params[:product_code_id] == nil
      return @invoice_item.errors.add(:base, t('fee_item_select'))
    else
      invoice = Invoice.find(params[:id])
      product_code = ProductCode.find(params[:product_code_id][0].to_i)
      @invoice_item.invoice_id = params[:id]
      @invoice_item.item_name = product_code.code
      @invoice_item.quantity = params[:quantity]
      @invoice_item.price = product_code.price

      invoice.total_price = invoice.total_price + (@invoice_item.quantity * @invoice_item.price)
      invoice.bal_due = invoice.total_price

      @invoice_item = InvoiceItem.create(@invoice_item, invoice)
      @total_price = number_to_currency(Invoice.where(id: params[:id]).pluck(:total_price)[0])
      flash[:success] = I18n.t('create_success') if @invoice_item .errors.size == 0
    end

  end

  def add_other_fee_item
    @invoice_item = InvoiceItem.new
    invoice = Invoice.find(params[:id])
    @invoice_item.invoice_id = params[:id]
    @invoice_item.item_name = params[:fee_item_name]
    @invoice_item.quantity = params[:quantity]
    @invoice_item.price = params[:price]

    invoice.total_price = invoice.total_price + (@invoice_item.quantity * @invoice_item.price)
    invoice.bal_due = invoice.total_price

    @invoice_item = InvoiceItem.create(@invoice_item, invoice)
    @total_price = number_to_currency(Invoice.where(id: params[:id]).pluck(:total_price)[0])
    flash[:success] = I18n.t('create_success') if @invoice_item .errors.size == 0

  end

  private
  def invoice_params
    invoice_params = params.require(:invoice).permit(
        :studio_id,
        :student_id,
        :invoice_number,
        :teacher_name,
        :day,
        :time,
        :myc_level_id,
        :total_price,
        :bal_due,
        :invoice_on,
        :due_on,
        :status
    ).merge(:studio_id=> session[:current_studio_id])
    invoice_params[:invoice_on] = to_date(invoice_params[:invoice_on])
    invoice_params[:due_on] = to_date(invoice_params[:due_on])

    return invoice_params
  end
end

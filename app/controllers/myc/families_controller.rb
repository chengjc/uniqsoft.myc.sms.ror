class Myc::FamiliesController < ApplicationController
  def index_data
    respond_to do |format|
      format.html
      format.json { render json: FamilyDatatable.new(view_context, { studio_id: session[:current_studio_id] }) }
    end
  end

  def new
    session[:referrer] = request.referrer # set the previous url
    @family = Family.new
  end

  def create
    @family = Family.new(family_params.merge(:studio_id => session[:current_studio_id]))
    if @family.save
      flash[:success] = t('save_success')
      if session[:referrer] == nil
        redirect_to(action: 'index')
      else
        redirect_to(session[:referrer])
      end
      session.delete(:referrer)
    else
      flash[:error] = t('save_fail')
      render('new')
    end
  end

  def edit
    session[:referrer] = request.referrer
    @family = Family.find(params[:id])
  end

  def update
    @family = Family.find(params[:id])
    if @family.update_attributes(family_params)
      flash[:success] = t('save_success')
      if session[:referrer] == nil
        redirect_to(action: 'index')
      else
        redirect_to(session[:referrer])
      end
      session.delete(:referrer)
    else
      render('edit')
    end
  end

  def show
      @family = Family.find(params[:family_id])
      respond_to do |format|
      format.json { render :json => {:data => @family}, status: :ok }
      end
  end

  private
  def family_params
    params.require(:family).permit(
        :address1,
        :address2,
        :postal_code,
        :city_or_state,
        :country,
        :primary_contact_name,
        :primary_contact_relationship,
        :primary_contact_phone_no,
        :primary_contact_email,
        :primary_contact_occupation,
        :primary_contact_music_background,
        :primary_contact_accompany_for_student,
        :other_contact_name,
        :other_contact_relationship,
        :other_contact_phone_no,
        :other_contact_email,
        :other_contact_occupation,
        :other_contact_music_background,
        :other_contact_accompany_for_student,
        :status
    )
  end
end

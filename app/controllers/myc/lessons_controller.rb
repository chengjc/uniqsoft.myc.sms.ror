class Myc::LessonsController < ApplicationController
  layout "application"

  def choose_teacher_to_change
    lesson_id = params[:id]
    @lesson = Lesson.find(lesson_id)
    marked_attendance = Attendance.is_attendance_marked_for_lesson(lesson_id)
    if marked_attendance
      @lesson.errors[:base] = t('cannot_change_teacher')
    end
  end

  def change_teacher
    success = false

    begin
      ActiveRecord::Base.transaction do
        lesson_id = params[:id]
        @lesson = Lesson.find(lesson_id)
        teacher_id = params[:teacher_id].to_i
        change_effect = params[:change]
        marked_attendance = Attendance.is_attendance_marked_for_lesson(lesson_id)

        if marked_attendance
          success = false
        elsif (change_effect == 'OneLesson')
          success = @lesson.update!(teacher_id: teacher_id)
        elsif(change_effect == 'ManyLesson')
          @lessons = Lesson.all_lessons_below_specify_lesson_no(@lesson.lesson_no, @lesson.myc_class_id)
          @lessons.each do |lesson|
            lesson.update!({ 'teacher_id': teacher_id })
          end

          success = true
        end
      end

    rescue Exception => exception
      logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = t('change_success')
    else
      flash[:error] = t('change_fail')
    end
    redirect_to("/myc/myc_classes/#{@lesson.myc_class_id}")
  end

  def set_data_for_lesson_make_up
    lesson_id = params[:id]
    @make_up_type = params[:make_up_type]
    @lesson = Lesson.find(lesson_id)
    is_marked = Lesson.is_lesson_marked(lesson_id)

    if is_marked
      @lesson.errors[:base] = t('cant_make_up_lesson')
    end
  end

  def lesson_make_up
    success = false
    error_message = t('make_up_fail')

    begin
      ActiveRecord::Base.transaction do
        lesson_id = params[:id]
        is_marked = Lesson.is_lesson_marked(lesson_id)
        if is_marked
          success = false
          error_message = t('cant_make_up_lesson')
        else
          @lesson = Lesson.find(lesson_id)
          make_up_type = params[:make_up_type]

          reason = params[:reason]
          @myc_class = @lesson.myc_class

          current_lesson_date_and_time = @lesson.start_at

          if (make_up_type == 'afterLastLesson')
            @last_lesson = @myc_class.lessons.where("lesson_no <> 0").order("start_at DESC").take!
            make_up_at = @last_lesson.start_at + 7.days
            make_up_date_valid = is_make_up_date_valid(make_up_at.to_date)

            until make_up_date_valid == true
              make_up_at = make_up_at + 7.days
              make_up_date_valid = is_make_up_date_valid(make_up_at.to_date)
            end

            @lesson.update!(start_at: make_up_at)
          elsif (make_up_type == 'makeUpToSpecificDate')
            make_up_at = get_date_time_from_string(params[:make_up_date] + ' ' + params[:make_up_time])

            make_up_date_valid = is_make_up_date_valid(make_up_at.to_date)
            if make_up_date_valid == false
              flash[:error] = t('cant_make_up_lesson_because_of_holiday')
              redirect_to("/myc/myc_classes/#{@myc_class.id}")
              return
            else
              @lesson.update!(start_at: make_up_at)
            end
          end

          make_up_lesson = {
              'lesson_no' => 0,
              'start_at' => current_lesson_date_and_time,
              'myc_class_id' => @lesson.myc_class_id,
              'teacher_id' => @lesson.teacher_id
          }

          make_up_lesson['reason_for_cancel'] = reason
          make_up_lesson['status'] = Lesson.statuses[:cancel]
          Lesson.create!(make_up_lesson)

          @lessons = @myc_class.lessons.where("lesson_no <> 0").order("start_at")
          lesson_no = 1
          @lessons.each do |lesson|
            lesson.update!({ 'lesson_no': lesson_no })
            lesson_no += 1
          end

          success = true
        end
      end

    rescue Exception => exception
      logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = t('make_up_success')
    else
      flash[:error] = error_message
    end
    redirect_to("/myc/myc_classes/#{@myc_class.id}")

  end

  def choose_students_to_mark_attendance
    @lesson = Lesson.find(params[:id])
  end

  def mark_attendance
    success = false
    begin
      lesson_id = params[:id]
      lesson = Lesson.find(lesson_id)
      ActiveRecord::Base.transaction do

        lesson.update!({'status': Lesson.statuses[:marked]})

        active_lessons = Lesson.lessons_which_are_not_cancelled_by_class(lesson.myc_class_id)
        if (active_lessons.length == lesson.lesson_no)
          myc_class = lesson.myc_class
          myc_class.update!({'status': MycClass.statuses[:finished]})

          myc_class_students = MycClassStudent.by_class_id_and_status(myc_class.id, MycClassStudent.statuses[:in_class])
          myc_class_students.each do |myc_class_student|
            myc_class_student.update!({'status': MycClassStudent.statuses[:graduated]})
          end
        end
        attendances_data = JSON.parse(params[:attendances_data])
        attendances_data.each do |attendance_data|
          attendance_id = attendance_data["id"]
          attendance_status = attendance_data["status"].underscore
          @attendance = Attendance.find(attendance_id)
          @attendance.update!({ "status": Attendance.statuses[attendance_status]})
        end

        success = true
      end

    rescue Exception => exception
      logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = t('mark_success')
    else
      flash[:error] = t('mark_fail')
    end

    redirect_to("/myc/myc_classes/#{lesson.myc_class_id}")
  end

  def unmark_attendance_confirm
    @lesson = Lesson.find(params[:id])
  end

  def unmark_attendance
    success = false
    lesson_id = params[:id]
    lesson = Lesson.find(lesson_id)
    begin
      ActiveRecord::Base.transaction do
        lesson.update!({'status': Lesson.statuses[:not_marked]})

        active_lessons = Lesson.lessons_which_are_not_cancelled_by_class(lesson.myc_class_id)
        if (active_lessons.length == lesson.lesson_no)
          myc_class = lesson.myc_class
          myc_class.update!({'status': MycClass.statuses[:on_going]})
          myc_class_students = MycClassStudent.by_class_id_and_status(myc_class.id, MycClassStudent.statuses[:graduated])
          myc_class_students.each do |myc_class_student|
            myc_class_student.update!({'status': MycClassStudent.statuses[:in_class]})
          end
        end

        attendances = Attendance.attendances_of_active_students_by_lesson(lesson_id)
        attendances.each do |attendance|
          attendance.update!({ 'status': Attendance.statuses[:null]})
        end

        success = true
      end

    rescue Exception => exception
      logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = t('unmark_success')
    else
      flash[:error] = t('unmark_fail')
    end
    redirect_to("/myc/myc_classes/#{lesson.myc_class_id}")
  end

  private
  def get_date_time_from_string(date_time)
    if date_time.present?
      converted_date_time = Time.strptime(date_time, "%d/%m/%y %I:%M %p")
    end

    return converted_date_time
  end

  def is_make_up_date_valid(make_up_date)
    result = true
    holiday = Holiday.by_studio_and_holiday_date(session[:current_studio_id], make_up_date)
    if holiday.length > 0
      result = false
    end

    return result
  end

end

class Myc::HolidaysController < ApplicationController
  def index_data
    respond_to do |format|
      format.html
      format.json { render json: HolidayDatatable.new(view_context, { studio_id: session[:current_studio_id] }) }
    end
  end

  def index
  end

  def new
    @holiday = Holiday.new
  end

  def create
    @holiday = Holiday.new(holiday_params.merge(:studio_id => session[:current_studio_id]))
    if @holiday.save
      flash[:success] = t('save_success')
      redirect_to(:action => 'index')
    else
      flash[:error] = t('save_fail')
      render('new')
    end
  end

  def edit
    session[:referrer] = request.referrer
    @holiday = Holiday.find(params[:id])
    @holiday[:holiday_date] = @holiday.holiday_date.to_s
  end

  def update
    @holiday = Holiday.find(params[:id])
    if @holiday.update_attributes(holiday_params)
      flash[:success] = t('save_success')
      redirect_to(:action => 'index')
    else
      render('edit')
    end
  end

  private
  def holiday_params
    holiday_params = params.require(:holiday).permit(
        :holiday_date,
        :holiday_name,
    )
    holiday_params[:holiday_date] = to_date(holiday_params[:holiday_date])
    return holiday_params
  end
end

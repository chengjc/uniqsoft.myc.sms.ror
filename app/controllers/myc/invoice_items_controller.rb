class Myc::InvoiceItemsController < ApplicationController
  include ApplicationHelper
  def index_data
    respond_to do |format|
      format.html
      format.json { render json: InvoiceItemDatatable.new(view_context, { invoice_id: params[:invoice_id] }) }
    end
  end

  def delete
    @invoice = Invoice.find(params[:invoice_id])
    @invoice_item = InvoiceItem.find(params[:id])
  end

  def destroy
    @invoice_item = InvoiceItem.find(params[:id])
    invoice = Invoice.find(params[:invoice_id])
    invoice.total_price = invoice.total_price - (@invoice_item.quantity * @invoice_item.price)
    invoice.bal_due = invoice.total_price
    @invoice_item = InvoiceItem.destroy(@invoice_item, invoice)
    @total_price = number_to_currency(Invoice.where(id: params[:invoice_id]).pluck(:total_price)[0])
    flash[:success] = I18n.t('delete_success') if @invoice_item .errors.size == 0

  end

  private
  def invoice_item_params
    params.require(:invoice_item).permit(
        :invoice_id,
        :item_name,
        :quantity,
        :price
    )
  end


end


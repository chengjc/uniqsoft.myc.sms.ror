class Myc::TeacherCommissionOthersController < ApplicationController
  include ApplicationHelper

  def index
  end

  def index_data
    respond_to do |format|
      format.json { render json: TeacherCommissionOtherDatatable.new(view_context, {teacher_commission_id: params[:teacher_commission_id]}) }
    end
  end

  def new
    @teacher_commission_id = params[:teacher_commission_id]
    @teacher_commission_other = TeacherCommissionOther.new
  end

  def create
    @teacher_commission_other = TeacherCommissionOther.new(teacher_commission_other_params.merge(teacher_commission_id: params[:teacher_commission_id]))

    if @teacher_commission_other.save
      @net_pay = number_to_currency(TeacherCommission.net_pay(params[:teacher_commission_id]))
      flash[:success] = t('save_success')
    end
  end

  def edit
    @teacher_commission_other = TeacherCommissionOther.find(params[:id])
  end

  def update
    @teacher_commission_other = TeacherCommissionOther.find(params[:id])

    if @teacher_commission_other.update(teacher_commission_other_params)
      teacher_commission_id = TeacherCommissionOther.where(id: params[:id]).pluck(:teacher_commission_id).first
      @net_pay = number_to_currency(TeacherCommission.net_pay(teacher_commission_id))
      flash[:success] = t('save_success')
    end
  end

  def delete
    @teacher_commission_other = TeacherCommissionOther.find(params[:id])
  end

  def destroy
    # authorize User
    teacher_commission_id = TeacherCommissionOther.where(id: params[:id]).pluck(:teacher_commission_id).first
    @teacher_commission_other = TeacherCommissionOther.find(params[:id])
    @teacher_commission_other.destroy
    if @teacher_commission_other.destroyed?
      @net_pay = number_to_currency(TeacherCommission.net_pay(teacher_commission_id))
      flash[:success] = t('delete_success')
    end
  end

  private
  def teacher_commission_other_params
    params.require(:teacher_commission_other).permit(
        :item,
        :amount,
        :remarks
    )
  end
end

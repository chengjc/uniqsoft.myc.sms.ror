class Myc::TeachersController < ApplicationController
  layout "application"

  def index
  end

  def index_data
    respond_to do |format|
      format.json { render json: MycTeacherDatatable.new(view_context, {studio_id: session[:current_studio_id]}) }
    end
  end

end

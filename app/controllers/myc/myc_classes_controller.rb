class Myc::MycClassesController < ApplicationController
  layout "application"

  def index

  end

  def index_data
    teacher_id = params[:teacher_id]
    myc_level_id = params[:myc_level_id]
    student_id = params[:student_id]
    status = params[:status]

    respond_to do |format|
      format.html
      format.json { render json: MycClassDatatable.new(view_context, { studio_id: session[:current_studio_id], teacher_id: teacher_id, myc_level_id: myc_level_id, student_id: student_id, status: status }) }
    end
  end

  def new
    @myc_class = MycClass.new
  end

  def create
    success = false

    begin
      ActiveRecord::Base.transaction do
        @myc_class_params = myc_class_params
        @myc_class_params[:start_at] = get_date_time_from_string(@myc_class_params[:start_date] + ' ' + @myc_class_params[:start_time])
        @myc_class = MycClass.new(@myc_class_params)
        @myc_class[:studio_id] = session[:current_studio_id]
        @myc_class[:status_updated_at] = Time.now
        @myc_class.save!

        myc_level = MycLevel.find(@myc_class[:myc_level_id])

        x = 0
        lesson_date_and_time = @myc_class[:start_at].to_datetime
        loop do
          holiday = Holiday.by_studio_and_holiday_date(session[:current_studio_id], lesson_date_and_time.to_date)
          if holiday.length == 0
            x += 1
            break if x > myc_level[:total_lesson]

            lesson = get_lesson_object(x, lesson_date_and_time, @myc_class[:id], @myc_class_params[:teacher_id])

            Lesson.create!(lesson)
          end
          lesson_date_and_time = lesson_date_and_time + 7.days
        end

        success = true
      end
    rescue Exception => exception
      logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = t('create_class_success')
      redirect_to("/myc/myc_classes/#{@myc_class.id}")
    else
      @myc_class = MycClass.new(myc_class_params) # to return form with all the input params
      flash[:error] = t('create_fail')
      render('new')
    end
  end

  def show
    @myc_class = MycClass.find(params[:id])
    @students = MycClass.students_by_class_id(params[:id])
    @lessons = Lesson.lessons_and_attendances_by_class(@myc_class.id)

  end

  def new_students_data
    respond_to do |format|
      format.html
      format.json { render json: NewStudentDatatable.new(view_context, { myc_class_id: params[:myc_class_id], studio_id: session[:current_studio_id] }) }
    end
  end

  def old_students_data
    respond_to do |format|
      format.html
      format.json { render json: OldStudentDatatable.new(view_context, { myc_class_id: params[:myc_class_id], studio_id: session[:current_studio_id] }) }
    end
  end

  def in_class_students_data
    respond_to do |format|
      format.html
      format.json { render json: InClassStudentDatatable.new(view_context, { myc_class_id: params[:myc_class_id] }) }
    end
  end

  def choose_students_to_add
    @myc_class = MycClass.find(params[:id])
  end

  def add_students
    success = false
    begin
      ActiveRecord::Base.transaction do
        myc_class_id = params[:id]
        @myc_class = MycClass.find(myc_class_id)

        student_ids = params[:student_id]
        student_ids.each do |student_id|
          success = false
          myc_class_student = get_myc_class_student_object(myc_class_id, student_id)
          MycClassStudent.create!(myc_class_student)

          @lessons = Lesson.lessons_which_are_not_cancelled_by_class(myc_class_id)
          @lessons.each do |lesson|
            attendance = get_attendance_object(lesson.id, student_id)
            Attendance.create!(attendance)
          end

          @myc_class_student = Student.find(student_id)
          @myc_class_student.update!({'status': Student.statuses['current'] })

          success = true
        end
      end

    rescue Exception => exception
      logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = t('add_success')
    else
      @myc_class = MycClass.new(myc_class_params) # to return form with all the input params
      flash[:error] = t('add_fail')
    end

    redirect_to("/myc/myc_classes/#{@myc_class.id}")
  end

  def choose_student_to_remove
    @myc_class = MycClass.find(params[:id])
  end

  def check_student_attendance
    myc_class_id = params[:id]
    @student_id = params[:student_id][0]
    @marked_attendance = Attendance.is_attendance_marked_for_student(@student_id, myc_class_id)

    respond_to do |format|
      format.js
    end
  end

  def remove_student
    success = false
    myc_class_id = params[:id]
    student_id = params[:student_id]
    status = params[:class_student_status]
    begin
      ActiveRecord::Base.transaction do

        marked_attendance = Attendance.is_attendance_marked_for_student(student_id, myc_class_id)

        if (marked_attendance && status.blank?)
          success = false
        else
          @myc_class_student = MycClassStudent.myc_class_student_by_class_and_student(myc_class_id, student_id)
          if marked_attendance
            @myc_class_student.update!({'status': status })
          else
            @myc_class_student.destroy!
            @attendances = Attendance.unmarked_attendances_for_student(student_id, myc_class_id)
            @attendances.each do |attendance|
              attendance.destroy!
            end
          end

          success = true
        end
      end

      rescue Exception => exception
        logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = t('remove_success');
    else
      flash[:error] = t('remove_fail');
    end
    redirect_to("/myc/myc_classes/#{myc_class_id}")
  end

  def student_attendance_data
    respond_to do |format|
      format.html
      format.json { render json: StudentAttendanceDatatable.new(view_context, { lesson_id: params[:lesson_id] }) }
    end
  end

  def dissolve_confirm
    @myc_class = MycClass.find(params[:id])
  end

  def dissolve
    myc_class_id = params[:id]
    success = false
    success_message = ''
    error_message = t('dissolve_fail')
    url = '/myc/myc_classes/' + myc_class_id

    begin
      ActiveRecord::Base.transaction do

        @myc_class = MycClass.find(myc_class_id)
        if (MycClass.statuses[@myc_class.status] != MycClass.statuses[:on_going])
          error_message = t('cant_dissolve_class_because_status_is_not_on_going')

        else
          mark_attendance = Attendance.is_attendance_marked_for_class(myc_class_id)
          if mark_attendance
            @myc_class.update!({
                                   'status': MycClass.statuses[:dissolved],
                                   'status_updated_at': Time.now
                               })
            success_message = t('dissolve_success')
          else
            @myc_class.myc_class_students.each do |myc_class_student|
              myc_class_student.destroy!()
            end

            @myc_class.lessons.each do |lesson|
              lesson.attendances.each do |attendance|
                attendance.destroy!()
              end

              lesson.destroy!()
            end

            @myc_class.destroy!()
            url = '/myc/myc_classes'
            success_message = t('delete_success')
          end

          success = true
        end
      end

    rescue Exception => exception
      logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = success_message
    else
      flash[:error] = error_message
    end
    redirect_to(url)

  end

  def undo_dissolve_confirm
    @myc_class = MycClass.find(params[:id])
  end

  def undo_dissolve
    success = false
    error_message = t('undo_dissolve_fail')

    begin
      ActiveRecord::Base.transaction do
        myc_class_id = params[:id]
        @myc_class = MycClass.find(myc_class_id)
        if (MycClass.statuses[@myc_class.status] != MycClass.statuses[:dissolved])
          error_message = t('cant_undo_dissolve')
          success = false
        else
          @myc_class.update!({
                                 'status': MycClass.statuses[:on_going],
                                 'status_updated_at': Time.now
                             })
          success = true
        end
      end
    rescue Exception => exception
      logger.error(exception.to_s) #handle exception
    end

    if success
      flash[:success] = t('undo_dissolve_success')
    else
      flash[:error] = error_message
    end
    redirect_to("/myc/myc_classes/#{@myc_class.id}")
  end
  
  private

  def myc_class_params
    params.require(:myc_class).permit(
        :start_date,
        :start_time,
        :myc_level_id,
        :teacher_id
    )
  end

  def get_date_time_from_string(date_time)
    if date_time.present?
      converted_date_time = Time.strptime(date_time, "%d/%m/%y %I:%M %p")
    end

    return converted_date_time
  end

  def get_lesson_object(lesson_no, lesson_date_and_time, myc_class_id, teacher_id)
    @lesson = {
        'lesson_no' => lesson_no,
        'start_at' => lesson_date_and_time,
        'myc_class_id' => myc_class_id,
        'teacher_id' => teacher_id
    }
  end

  def get_myc_class_student_object(myc_class_id, student_id)
    @myc_class_student = {
        'myc_class_id' => myc_class_id,
        'student_id' => student_id
    }
  end

  def get_attendance_object(lesson_id, student_id)
    @attendance = {
        'lesson_id' => lesson_id,
        'student_id' => student_id
    }
  end

end

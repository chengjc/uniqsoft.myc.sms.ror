class Myc::CalendarController < ApplicationController
  layout 'application'

  def index
    start_date = params['start']
    end_date = params['end']
    @holidays = Holiday.events_for_calendar(session[:current_studio_id], start_date, end_date)
    @lessons = Lesson.events_for_calendar(session[:current_studio_id], start_date, end_date)
  end
end
class Myc::TeacherCommissionItemsController < ApplicationController
  include ApplicationHelper, ActionView::Helpers::NumberHelper

  def index_data
    respond_to do |format|
      format.json { render json: TeacherCommissionItemDatatable.new(view_context, {teacher_commission_id: params[:teacher_commission_id]}) }
    end
  end

  def edit_percentage
    @teacher_commission_item = TeacherCommissionItem.find(params[:id])
  end

  def edit_remarks
    @teacher_commission_item = TeacherCommissionItem.find(params[:id])
  end

  def update
    @teacher_commission_item = TeacherCommissionItem.find(params[:id])
    if @teacher_commission_item.update(teacher_commission_item_params)
      flash[:success] = t('save_success')
    end
  end

  def update_percentage
    @teacher_commission_item = TeacherCommissionItem.validate_percentage(teacher_commission_item_params)
    if @teacher_commission_item.errors.size == 0
      @teacher_commission_item = TeacherCommissionItem.find(params[:id])
      new_commission = @teacher_commission_item.total_fee * teacher_commission_item_params[:percentage].to_i / 100

      if @teacher_commission_item.update(teacher_commission_item_params.merge(commission: number_with_precision(new_commission, precision: 4)))
        @net_pay = number_to_currency(TeacherCommission.net_pay(params[:teacher_commission_id]))
        flash[:success] = t('save_success')
      end
    end
  end

  private
  def teacher_commission_item_params
    params.require(:teacher_commission_item).permit(
        :percentage,
        :remarks
    )
  end
end

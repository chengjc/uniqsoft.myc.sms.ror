class Myc::StudentsController < ApplicationController
  def index
  end

  def index_data
    respond_to do |format|
      format.html
      format.json { render json: StudentDatatable.new(view_context, { studio_id: session[:current_studio_id] }) }
    end
  end

  def new
    @student = Student.new
    @student.build_family
  end

  def create
    @student = Student.new(student_params.merge(:studio_id => session[:current_studio_id]))
    if @student.save
      flash[:success] = t('save_success')
      redirect_to(:action => 'index')
    else
      flash[:error] = t('save_fail')
      render('new')
    end
  end

  def edit
    @student = Student.find(params[:id])
    @student[:enquiry_date] = @student.enquiry_date.to_s
    @student[:registration_date] = @student.registration_date.to_s
  end

  def update
    puts session[:referrer]
    @student = Student.find(params[:id])

    if @student.update_attributes(student_params)
      flash[:success] = t('save_success')
      redirect_to(:action => 'index')
    else
      flash[:error] = t('save_fail')
      render('edit')
    end
  end

  private
  def student_params
     student_params = params.require(:student).permit(
      :myc_level_id,
      :family_id,
      :enrolment_piano,
      :enrolment_theory,
      :enrolment_other,
      :preferred_day_one,
      :preferred_day_two,
      :preferred_day_three,
      :preferred_time_one,
      :preferred_time_two,
      :preferred_time_three,
      :nric_name,
      :first_name,
      :sur_name,
      :gender,
      :date_of_birth,
      :piano_standard,
      :piano_college,
      :piano_school_attended,
      :theory_standard,
      :theory_college,
      :theory_school_attended,
      :violin_standard,
      :violin_college,
      :violin_school_attended,
      :other_standard,
      :other_college,
      :other_school_attended,
      :instrument_owned_has_piano,
      :instrument_owned_has_organ,
      :instrument_owned_has_keyboard,
      :instrument_owned_has_violin,
      :instrument_owned_other,
      :referred_by,
      :enquiry_date,
      :registration_date,
      :status
    )
     student_params[:enquiry_date] = to_date(student_params[:enquiry_date])
     student_params[:registration_date] = to_date(student_params[:registration_date])

    return student_params

  end

end

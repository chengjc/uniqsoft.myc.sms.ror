class Myc::TeacherCommissionItemAdditionalsController < ApplicationController
  include ApplicationHelper

  def index_data
    respond_to do |format|
      format.json { render json: TeacherCommissionItemAdditionalDatatable.new(view_context, {teacher_commission_id: params[:teacher_commission_id]}) }
    end
  end

  def new
    @teacher_commission_id = params[:teacher_commission_id]
    @teacher_commission_item_additional = TeacherCommissionItemAdditional.new
  end

  def create
    @teacher_commission_item_additional = TeacherCommissionItemAdditional.validate_lesson_fee_and_no_of_lesson_and_percentage(teacher_commission_item_params)
    if @teacher_commission_item_additional.errors.size == 0
      @teacher_commission_item_additional = TeacherCommissionItemAdditional.new(
          teacher_commission_item_params.merge(teacher_commission_id: params[:teacher_commission_id])
      )
      if @teacher_commission_item_additional.save
        @net_pay = number_to_currency(TeacherCommission.net_pay(params[:teacher_commission_id]))
        flash[:success] = t('save_success')
      end
    end
  end

  def edit
    @teacher_commission_item_additional = TeacherCommissionItemAdditional.find(params[:id])
  end

  def update
    @teacher_commission_item_additional = TeacherCommissionItemAdditional.validate_lesson_fee_and_no_of_lesson_and_percentage(teacher_commission_item_params)
    if @teacher_commission_item_additional.errors.size == 0
      @teacher_commission_item_additional = TeacherCommissionItemAdditional.find(params[:id])
      if @teacher_commission_item_additional.update(teacher_commission_item_params)
        teacher_commission_id = TeacherCommissionItemAdditional.where(id: params[:id]).pluck(:teacher_commission_id).first
        @net_pay = number_to_currency(TeacherCommission.net_pay(teacher_commission_id))
        flash[:success] = t('save_success')
      end
    end
  end

  def delete
    @teacher_commission_item_additional = TeacherCommissionItemAdditional.find(params[:id])
  end

  def destroy
    teacher_commission_id = TeacherCommissionItemAdditional.where(id: params[:id]).pluck(:teacher_commission_id).first
    @teacher_commission_item_additional = TeacherCommissionItemAdditional.find(params[:id])
    @teacher_commission_item_additional.destroy
    if @teacher_commission_item_additional.destroyed?
      @net_pay = number_to_currency(TeacherCommission.net_pay(teacher_commission_id))
      flash[:success] = t('delete_success')
    end
  end

  private
  def teacher_commission_item_params
    params.require(:teacher_commission_item_additional).permit(
        :program,
        :day,
        :time,
        :student_nric_name,
        :lesson_date,
        :term_fee,
        :lesson_fee,
        :no_of_lesson,
        :percentage,
        :commission,
        :remarks
    )
  end
end

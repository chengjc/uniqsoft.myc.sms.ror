class Myc::TeacherCommissionsController < ApplicationController
  layout 'application'
  include ApplicationHelper

  def index
  end

  def index_data
    respond_to do |format|
      format.json { render json: TeacherCommissionDatatable.new(view_context, {studio_id: session[:current_studio_id]}) }
    end
  end

  def show
    @teacher_commission = TeacherCommission.find(params[:id])
    authorize @teacher_commission

    @net_pay = number_to_currency(@teacher_commission.net_pay)
  end

  def show_data
    @teacher_commission = TeacherCommission.find(params[:id])
    authorize @teacher_commission
    respond_to do |format|
      format.json { render json: TeacherCommissionItemDatatable.new(view_context, {teacher_commission_id: params[:id]}) }
    end
  end

  def generate_confirm
    authorize TeacherCommission
  end

  def generate
    authorize TeacherCommission

    if TeacherCommission.current_month_teachers(session[:current_studio_id]).size == 0
      flash[:warning] = I18n.t('generate_no_data')
    else
      @teacher_commission = TeacherCommission.generate(session[:current_studio_id])
      flash[:success] = I18n.t('generate_success') if @teacher_commission.errors.size == 0
    end
  end
end

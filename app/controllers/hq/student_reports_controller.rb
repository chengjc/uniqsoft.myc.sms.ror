class Hq::StudentReportsController < ApplicationController
  def index
  end

  def index_data
    respond_to do |format|
      format.json { render json: StudentReportDatatable.new(view_context) }
    end
  end

  def index_total_data
    studio_id = params['columns']['0']['search']['value']
    teacher_id = params['columns']['1']['search']['value']
    respond_to do |format|
      format.json { render json: StudentReportTotalDatatable.new(view_context, {studio_id: studio_id, teacher_id: teacher_id}) }
    end
  end

  def generate

    return flash[:warning] = I18n.t('generate_no_data') if StudentReport.reports == 0

    @student_report = StudentReport.generate
    flash[:success] = I18n.t('generate_success') if @student_report.errors.count == 0
  end
end

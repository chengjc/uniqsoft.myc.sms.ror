class Hq::ProductCodesController < ApplicationController
  include ApplicationHelper

  def index
  end

  def index_data
    respond_to do |format|
      format.json { render json: ProductCodeDatatable.new(view_context) }
    end
  end

  def pack_data_by_code
    respond_to do |format|
      format.json { render json: ProductPackByCodeDatatable.new(view_context, {product_code_id: params[:id]}) }
    end
  end

  def item_data_by_code
    respond_to do |format|
      format.json { render json: ProductItemByCodeDatatable.new(view_context, {product_code_id: params[:id]}) }
    end
  end

  def show_pack_items
    @product_pack_id = params[:product_pack_id]
  end

  def new
    @product_code = ProductCode.new
    @product_code.price = 0.0
    # @product_group_select_options = ProductGroup.select_options_id_description
    @myc_program_select_options = MycProgram.active_myc_programs_order_by_display_order

  end

  def create
    @product_code = ProductCode.new(product_code_params)

    if @product_code.save
      flash[:success] = t('save_success')
      redirect_to "/hq/product_codes/#{@product_code.id}/edit"
    else
      flash[:error] = t('save_fail')
      @myc_program_select_options = MycProgram.active_myc_programs_order_by_display_order
      render 'new'
    end
  end

  def edit
    @product_code = ProductCode.find(params[:id])
    @cost = ProductCode.cost(params[:id])
    @myc_program_select_options = MycProgram.active_myc_programs_order_by_display_order
  end

  def update
    @product_code = ProductCode.find(params[:id])

    if @product_code.update(product_code_params)
      flash[:success] = t('save_success')
      redirect_to action: 'index'
    else
      flash[:error] = t('save_fail')
      @myc_program_select_options = MycProgram.active_myc_programs_order_by_display_order
      render 'edit'
    end
  end

  def select_pack
    @product_code_id = params[:id]
  end

  def add_pack
    if params[:product_pack_id].blank?
      @product_pack = ProductPack.new
      @product_pack.errors[:base] << t('product_pack_cannot_be_empty')
    else
      @product_code = ProductCode.find(params[:id])
      @product_pack = ProductPack.find(params[:product_pack_id])
      if @product_code.product_packs.include?(@product_pack)
        @product_pack.errors[:base] << t('product_pack_already_exists')
      else
        @product_code.product_packs << @product_pack
        @cost = number_to_currency(ProductCode.cost(params[:id]))
        flash[:success] = t('save_success')
      end
    end
  end

  def remove_pack_confirm
    @product_code_id = params[:id]
    @product_pack_id = params[:product_pack_id]
  end

  def remove_pack
    @product_code = ProductCode.find(params[:id])
    @product_pack = ProductPack.find(params[:product_pack_id])
    @product_code.product_packs.delete(@product_pack)
    @cost = number_to_currency(ProductCode.cost(params[:id]))
    flash[:success] = t('save_success') unless @product_code.product_packs.include?(@product_pack)
  end

  def select_item
    @product_code_id = params[:id]
  end

  def add_item
    if params[:product_item_id].blank?
      @product_item = ProductItem.new
      @product_item.errors[:base] << t('product_item_cannot_be_empty')
    else
      @product_code = ProductCode.find(params[:id])
      @product_item = ProductItem.find(params[:product_item_id])
      if @product_code.product_items.include?(@product_item)
        @product_item.errors[:base] << t('product_item_already_exists')
      else
        @product_code.product_items << @product_item
        @cost = number_to_currency(ProductCode.cost(params[:id]))
        flash[:success] = t('save_success')
      end
    end
  end

  def remove_item_confirm
    @product_code_id = params[:id]
    @product_item_id = params[:product_item_id]
  end

  def remove_item
    @product_code = ProductCode.find(params[:id])
    @product_item = ProductItem.find(params[:product_item_id])
    @product_code.product_items.delete(@product_item)
    @cost = number_to_currency(ProductCode.cost(params[:id]))
    flash[:success] = t('save_success') unless @product_code.product_items.include?(@product_item)
  end


  private
  def product_code_params
    params.require(:product_code).permit(
        :code,
        :price,
        :cost,
        :status,
        :product_pack_id,
        :remarks,
        :myc_program_id
    )
  end
end

class Hq::MycProgramsController < ApplicationController

  def index
    authorize User
  end

  def index_data
    respond_to do |format|
      format.json { render json: MycProgramDatatable.new(view_context) }
    end
  end
end

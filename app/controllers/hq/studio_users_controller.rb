class Hq::StudioUsersController < ApplicationController
  layout "application"

  after_action :verify_authorized

  def index
    authorize StudioUser

    user = User.find(params[:user_id])
    @nric_name = user.nric_name
    @user_id = user.id
  end

  def index_data
    authorize StudioUser

    respond_to do |format|
      format.json { render json: StudioUserDatatable.new(view_context, {user_id: params[:user_id]}) }
    end
  end

  def new
    authorize StudioUser

    @studio_user = StudioUser.new
    @studio_select_options = Studio.studio_select_options_without_hq
    @role_select_options_without_hq = StudioUser.role_select_options_without_hq
  end

  def create
    authorize StudioUser

    @studio_user = StudioUser.new(studio_user_params.merge(user_id: params[:user_id]))
    if @studio_user.save
      flash[:success] = t('save_success')
    end
  end

  def delete
    authorize StudioUser

    @studio_user = StudioUser.find(params[:id])
  end

  def destroy
    authorize StudioUser

    @studio_user = StudioUser.find(params[:id])
    @studio_user.destroy

    flash[:success] = t('delete_success') if @studio_user.destroyed?
  end

  private
  def studio_user_params
    params.require(:studio_user).permit(
        :studio_id,
        :user_id,
        :role_id
    )
  end

end

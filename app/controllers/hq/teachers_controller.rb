class Hq::TeachersController < ApplicationController
  layout "application"
  after_action :verify_authorized

  def new
    authorize Teacher

    @teacher = Teacher.new
    user = User.find(params[:user_id])
    @nric_name = user.nric_name
    @email = user.email
    @phone_number = user.phone_number
  end

  def create
    authorize Teacher

    @teacher = Teacher.new(teacher_params.merge(user_id: params[:user_id]))

    if @teacher.save
      flash[:success] = t('save_success')
      redirect_to "/hq/users/"
    else
      @teacher = Teacher.new(teacher_params) # to return form with all the input params
      flash[:error] = t('save_fail')
      render 'new'
    end

  end

  def edit
    authorize Teacher

    @teacher = Teacher.find(params[:id])

    user = User.find(@teacher.user_id)
    @nric_name = user.nric_name
    @email = user.email
    @phone_number = user.phone_number
    @user_edit_link = "/hq/users/#{user.id}/edit"


    @teacher[:date_of_myc_teacher_agreement] = @teacher.date_of_myc_teacher_agreement.to_s
    @teacher[:date_of_myc_studio_agreement] = @teacher.date_of_myc_studio_agreement.to_s
    @teacher[:date_of_myc_l1] = @teacher.date_of_myc_l1.to_s
    @teacher[:date_of_myc_l2ss2] = @teacher.date_of_myc_l2ss2.to_s
    @teacher[:date_of_myc_l2sb2] = @teacher.date_of_myc_l2sb2.to_s
    @teacher[:date_of_myc_l2mb2] = @teacher.date_of_myc_l2mb2.to_s
    @teacher[:date_of_myc_l3sb3] = @teacher.date_of_myc_l3sb3.to_s
    @teacher[:date_of_myc_l3mb3] = @teacher.date_of_myc_l3mb3.to_s

    @teacher[:date_of_1st_teacher_visit] = @teacher.date_of_1st_teacher_visit.to_s
    @teacher[:date_of_2nd_teacher_visit] = @teacher.date_of_2nd_teacher_visit.to_s
    @teacher[:date_of_3rd_teacher_visit] = @teacher.date_of_3rd_teacher_visit.to_s
  end

  def update
    authorize Teacher

    @teacher = Teacher.find(params[:id])

    if @teacher.update(teacher_params)
      flash[:success] = t('save_success')
      redirect_to '/hq/users/'
    else
      render 'edit'
    end
  end

  private
  def teacher_params
    teacher_params = params.require(:teacher).permit(
        # :nric_name,
        :gender,
        :date_of_birth,
        # :phone_number,
        :number_of_children,
        :full_time_occupation,

        :highest_piano_practical_standard_passed,
        :highest_theory_standard_passed,
        :has_college_abrsm,
        :has_college_trinity,
        :has_college_lcm,
        :college_others,
        :other_music_qualifications,

        :before_myc_teaching_individual_lesson_started_year,
        :before_myc_teaching_individual_lesson_total_year,
        :before_myc_teaching_group_lesson_started_year,
        :before_myc_teaching_group_lesson_total_year,

        :date_of_myc_teacher_agreement,
        :date_of_myc_studio_agreement,
        :date_of_myc_l1,
        :date_of_myc_l2ss2,
        :date_of_myc_l2sb2,
        :date_of_myc_l2mb2,
        :date_of_myc_l3sb3,
        :date_of_myc_l3mb3,

        :date_of_1st_teacher_visit,
        :date_of_2nd_teacher_visit,
        :date_of_3rd_teacher_visit
        # user_attributes: [:id, :nric_name, :phone_number, :email, :status]
    )

    teacher_params[:date_of_myc_teacher_agreement] = to_date(teacher_params[:date_of_myc_teacher_agreement])
    teacher_params[:date_of_myc_studio_agreement] = to_date(teacher_params[:date_of_myc_studio_agreement])
    teacher_params[:date_of_myc_l1] = to_date(teacher_params[:date_of_myc_l1])
    teacher_params[:date_of_myc_l2ss2] = to_date(teacher_params[:date_of_myc_l2ss2])
    teacher_params[:date_of_myc_l2sb2] = to_date(teacher_params[:date_of_myc_l2sb2])
    teacher_params[:date_of_myc_l2mb2] = to_date(teacher_params[:date_of_myc_l2mb2])
    teacher_params[:date_of_myc_l3sb3] = to_date(teacher_params[:date_of_myc_l3sb3])
    teacher_params[:date_of_myc_l3mb3] = to_date(teacher_params[:date_of_myc_l3mb3])
    teacher_params[:date_of_1st_teacher_visit] = to_date(teacher_params[:date_of_1st_teacher_visit])
    teacher_params[:date_of_2nd_teacher_visit] = to_date(teacher_params[:date_of_2nd_teacher_visit])
    teacher_params[:date_of_3rd_teacher_visit] = to_date(teacher_params[:date_of_3rd_teacher_visit])

    return teacher_params
  end
end



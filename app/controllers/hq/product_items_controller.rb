class Hq::ProductItemsController < ApplicationController
  def index
  end

  def index_data
    respond_to do |format|
      format.json { render json: ProductItemDatatable.new(view_context) }
    end
  end


  def new
    @product_item = ProductItem.new
    @product_item.quantity_trigger_alert = 0
  end

  def create
    @product_item = ProductItem.new(product_item_params)

    if @product_item.save
      flash[:success] = t('save_success')
      redirect_to action: 'index'
    else
      flash[:error] = t('save_fail')
      render 'new'
    end
  end

  def edit
    @product_item = ProductItem.find(params[:id])
  end

  def update
    @product_item = ProductItem.find(params[:id])

    if @product_item.update(product_item_params)
      flash[:success] = t('save_success')
      redirect_to action: 'index'
    else
      flash[:error] = t('save_fail')
      render 'edit'
    end
  end

  private
  def product_item_params
    params.require(:product_item).permit(
        :description,
        :cost,
        :quantity_on_hand,
        :quantity_trigger_alert,
        :remarks,
        :status)
  end
end

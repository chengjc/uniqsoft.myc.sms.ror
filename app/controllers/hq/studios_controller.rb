class Hq::StudiosController < ApplicationController
	layout "application"

  def index

  end

  def data
    respond_to do |format|
      format.html
      format.json { render json: StudioDatatable.new(view_context) }
    end  
  end

  def new
    @studio = Studio.new
  end

  def create
      @studio = Studio.new(studio_params)

    if @studio.save
      flash[:success] = t('save_success')
      redirect_to(:action => 'index')
    else
      flash[:error] = t('save_fail')
      render('new')
    end

  end


  def edit
    @studio = Studio.find(params[:id])
  end

  def update
    @studio = Studio.find(params[:id])

    if @studio.update(studio_params)
      flash[:success] = t('save_success')
      redirect_to(:action => 'index')
    else
      flash[:error] = t('save_fail')
      render('edit')
    end
  end

  private
  def studio_params
    params.require(:studio).permit(:studio_name, :studio_code, :address, :post_code, :company_name, :company_registration_no, :phone_no, :email, :facebook_address, :website_address, :status)
  end


end

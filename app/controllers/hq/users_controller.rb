class Hq::UsersController < ApplicationController
  layout "application"

  after_action :verify_authorized

  def index
    authorize User
  end

  def index_data
    authorize User

    respond_to do |format|
      format.html
      format.json { render json: UserDatatable.new(view_context) }
    end
  end

  def new
    authorize User
    @user = User.new
  end

  def create
    authorize User
    @user = User.new(
        nric_name: user_params[:nric_name],
        phone_number: user_params[:phone_number],
        email: user_params[:email],
        status: user_params[:status]
    )

    @user.valid? # check validation first or else, User.invite will cause @user to be <persisted> and the <render new> action will create a <patch> form instead of <new> form!!

    success = false
    if @user.errors.size == 0 || (@user.errors.size == 1 && @user.errors.full_messages[0] == "Password can't be blank") # ignore password now, will send invitation link to reset password
      @user = User.invite!(
          nric_name: user_params[:nric_name],
          phone_number: user_params[:phone_number],
          email: user_params[:email],
          status: user_params[:status],
          skip_invitation: true
      )

      if @user.errors.size == 0
        success = true
      end
    end

    if success
      flash[:success] = t('save_success_and_click_teacher_profile_or_assign_studios')
      redirect_to '/hq/users/'
    else
      flash[:error] = t('save_fail')
      render 'new'
    end
  end

  def edit
    authorize User
    @user = User.find(params[:id])

    teacher_id = Teacher.where(user_id: params[:id]).pluck(:id).first
    if teacher_id.nil?
      @teacher_link = "/hq/users/#{params[:id]}/teachers/new"
    else
      @teacher_link = "/hq/users/#{params[:id]}/teachers/#{teacher_id}/edit"
    end
  end

  def edit_status
    authorize User
    @user = User.find(params[:id])
  end

  def update
    authorize User
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update(user_params)
        flash[:success] = t('save_success')
        format.js
        format.html { redirect_to action: :index }
      else
        format.html { render :edit }
      end
    end
  end

  # def update_status
  #   authorize User
  #
  #   success = false
  #
  #   begin
  #     ActiveRecord::Base.transaction do
  #       params[:ids].each do |id|
  #         success = false
  #         @user = User.find(id)
  #
  #         success = @user.update_attributes!(status: params[:status_id].to_i)
  #       end
  #     end
  #   rescue Exception => exception
  #     logger.error(exception.to_s) # handle exception
  #   end
  #
  #   respond_to do |format|
  #     if success
  #       format.json { render :json => {:message => t('save_success')}, status: :ok }
  #     else
  #       format.json { render :json => {:message => t('save_fail'), :errors => @user.errors.full_messages}, status: :unprocessable_entity }
  #     end
  #   end
  # end

  def invite_confirm
    authorize User
    @user = User.find(params[:id])
  end

  def invite
    authorize User

    @user = User.find(params[:id])
    @user.deliver_invitation

    flash[:success] = t('invite_success') if @user.errors.size == 0

    respond_to do |format|
      format.js
    end
  end

  private
  def user_params
    params.fetch(:user, {}).permit(
        :nric_name,
        :phone_number,
        :email,
        :status
    )
  end
end

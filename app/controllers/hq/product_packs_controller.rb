class Hq::ProductPacksController < ApplicationController
  def index
  end

  def index_data
    respond_to do |format|
      format.json { render json: ProductPackDatatable.new(view_context) }
    end
  end

  def item_data_by_pack
    respond_to do |format|
      format.json { render json: ProductItemByPackDatatable.new(view_context, {product_pack_id: params[:id]}) }
    end
  end

  def new
    @product_pack = ProductPack.new
  end

  def create
    @product_pack = ProductPack.new(product_pack_params)

    if @product_pack.save
      flash[:success] = t('save_success')
      redirect_to "/hq/product_packs/#{@product_pack.id}/edit"
    else
      flash[:error] = t('save_fail')
      render 'new'
    end
  end

  def edit
    @product_pack = ProductPack.find(params[:id])
    @cost = ProductPack.cost(params[:id])
  end

  def update
    @product_pack = ProductPack.find(params[:id])
    @cost = ProductPack.cost(params[:id])

    if @product_pack.update(product_pack_params)
      flash[:success] = t('save_success')
      redirect_to action: 'index'
    else
      flash[:error] = t('save_fail')
      render 'edit'
    end
  end

  def select_item
    @product_pack_id = params[:id]
  end

  def add_item
    if params[:product_item_id].blank?
      @product_item = ProductItem.new
      @product_item.errors[:base] << t('product_item_cannot_be_empty')
    else
      @product_pack = ProductPack.find(params[:id])
      @product_item = ProductItem.find(params[:product_item_id])
      if @product_pack.product_items.include?(@product_item)
        @product_item.errors[:base] << t('product_item_already_exists')
      else
        @product_pack.product_items << @product_item
        @cost = ProductPack.cost(params[:id])
        flash[:success] = t('save_success')
      end
    end
  end

  def remove_item_confirm
    @product_pack_id = params[:id]
    @product_item_id = params[:product_item_id]
  end

  def remove_item
    @product_pack = ProductPack.find(params[:id])
    @product_item = ProductItem.find(params[:product_item_id])
    @product_pack.product_items.delete(@product_item)
    @cost = ProductPack.cost(params[:id])
    flash[:success] = t('save_success') unless @product_pack.product_items.include?(@product_item)
  end


  private
  def product_pack_params
    params.require(:product_pack).permit(
        :description,
        :status,
        :product_item_id
    )
  end
end

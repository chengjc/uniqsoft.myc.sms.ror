class HomeController < ApplicationController

  def index
  end

  def show
    studio_id = params[:id]
    if StudioUser.where(user_id: current_user.id, studio_id: studio_id).count == 1
      session[:current_studio_id] = studio_id
      session[:current_studio_name] = Studio.find(studio_id).studio_name
      role_id = StudioUser.where(user_id: current_user.id, studio_id: studio_id).pluck(:role_id).first
      session[:current_user_role_id] = role_id
      session[:current_user_role_name] = StudioUser.role_name(role_id)

      render :index
    else
      sign_out current_user
      redirect_to '/logout/'
    end
  end
end

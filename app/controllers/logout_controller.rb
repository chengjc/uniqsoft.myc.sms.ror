class LogoutController < ApplicationController
  layout "login"

  skip_before_filter :authenticate_user!

  def index
  end
end

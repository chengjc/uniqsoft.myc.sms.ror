class ApplicationController < ActionController::Base

  include Pundit
  set_current_tenant_through_filter

  before_filter :set_current_studio
  before_filter :authenticate_user!

  after_filter :prepare_unobtrusive_flash


  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def pundit_user
    UserContext.new(current_user, session[:current_user_role_id])
  end

  private
  def after_sign_out_path_for(resource_or_scope)
    "/logout/"
  end

  def after_sign_in_path_for(resource_or_scope)
    studio_id = current_user.studios.order(studio_name: :asc).pluck(:id).first
    "/home/#{studio_id}"
  end

  def user_not_authorized
    # redirect_to :back, :alert => t('permission_do_not_have')
    redirect_to '/users/sign_in', :alert => t('permission_do_not_have')
  end

  def set_current_studio
    set_current_tenant(session[:current_studio_id])
  end

  def to_date(date_string)
    if date_string.present?
      Date.strptime(date_string, Date::DATE_FORMATS[:default])
    end
  end

  def to_time(time_string)
    if time_string.present?
      Time.strptime(time_string, I18n.t('time.formats.short'))
    end
  end

end

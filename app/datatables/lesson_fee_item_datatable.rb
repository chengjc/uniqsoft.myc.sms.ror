class LessonFeeItemDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari
  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(

    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(

)
  end

  private

  def data
    records.map do |record|
      lesson_term_fee_data(record)
    end
  end

  def get_raw_records
    FeeItem.lesson_fee_items
  end

  def lesson_term_fee_data(lesson_fee_item)
    myc_level = MycLevel.find(options[:myc_level_id])
    if lesson_fee_item[1] == FeeItem.lesson_fee_items['term_fee']
      return [lesson_fee_item[1], lesson_fee_item[0].humanize, number_to_currency(myc_level.lesson_fee_per_term)]

    else
      return [lesson_fee_item[1], lesson_fee_item[0].humanize, number_to_currency((myc_level.lesson_fee_per_term) / (myc_level.total_lesson))]

    end
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

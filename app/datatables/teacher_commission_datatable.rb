class TeacherCommissionDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      TeacherCommission.teacher_nric_name
      TeacherCommission.statement_date
      TeacherCommission.net_pay
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      TeacherCommission.teacher_nric_name
      TeacherCommission.statement_date
      TeacherCommission.net_pay
    )
  end

  private

  def data
    records.map do |record|
      [
          # "<a href='/myc/teacher_commissions/#{record.id}/teacher_commission_items'>#{record.teacher_nric_name}</a>",
          "<a href='/myc/teacher_commissions/#{record.id}'>#{record.teacher_nric_name}</a>",
          record.statement_date.strftime('%m/%y'),
          number_to_currency(record.net_pay)
      ]
    end
  end

  def get_raw_records
    # insert query here
    TeacherCommission.
        select('
          teacher_commissions.id,
          teacher_commissions.teacher_nric_name,
          teacher_commissions.statement_date,
          teacher_commissions.net_pay
        ').
        where(studio_id: options[:studio_id])
  end

  def create_link(record)
    # link_to(record.teacher_nric_name, myc_teacher_commission_path(record))
    link_to(record.teacher_nric_name, myc_teacher_commission_path(record))
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

# class TeacherDatatable < AjaxDatatablesRails::Base
#
#   include AjaxDatatablesRails::Extensions::Kaminari
#
#   def_delegator :@view, :link_to
#   def_delegator :@view, :edit_hq_teacher_path
#
#   def sortable_columns
#     # Declare strings in this format: ModelName.column_name
#     @sortable_columns ||= ['teachers.id', 'teachers.nric_name']
#   end
#
#   def searchable_columns
#     # Declare strings in this format: ModelName.column_name
#     @searchable_columns ||= [
#         'teachers.nric_name',
#         'teachers.phone_number',
#         'teachers.status'
#     ]
#   end
#
#   private
#
#   def data
#     records.map do |record|
#       [
#         record.id, #checkbox column
#         link_to(record.nric_name, edit_hq_teacher_path(record)),
#         record.phone_number,
#         record.status.humanize,
#         record.date_of_myc_l1,
#         record.date_of_myc_l2ss2,
#         record.date_of_myc_l2sb2,
#         record.date_of_myc_l2mb2,
#         record.date_of_myc_l3sb3,
#         record.date_of_myc_l3mb3,
#         record.date_of_1st_teacher_visit,
#         record.date_of_2nd_teacher_visit,
#         record.date_of_3rd_teacher_visit
#       ]
#     end
#   end
#
#   def get_raw_records
#     Teacher.all
#   end
#
#   # ==== Insert 'presenter'-like methods below if necessary
# end

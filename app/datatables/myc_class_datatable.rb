class MycClassDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :myc_myc_class_path

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
        MycProgram.program_name
        MycClass.start_at
        MycClass.status
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
        MycProgram.program_name
        MycClass.start_at
        MycClass.status
    )
  end

  private

  def data
    records.map do |record|
      [
        # comma separated list of the values for each cell of a table row
        # example: record.attribute,

          link_to(record.program_and_level_name, myc_myc_class_path(record)),
          record.start_at.strftime("%I:%M"),
          record.start_at.strftime("%a"),
          record.teacher_name,
          record.status.humanize,
          record.lesson_no
      ]
    end
  end

  def get_raw_records
    myc_classes = MycClass
        .joins("INNER JOIN myc_levels ON myc_levels.id = myc_classes.myc_level_id")
        .joins("INNER JOIN myc_programs ON myc_levels.myc_program_id = myc_programs.id")
        .where("studio_id = #{options[:studio_id]}")

    myc_classes = myc_classes.where("myc_classes.id IN (SELECT lessons.myc_class_id FROM lessons WHERE lessons.teacher_id = #{options[:teacher_id]})") if options[:teacher_id].present?
    myc_classes = myc_classes.where("myc_classes.myc_level_id = #{options[:myc_level_id]}") if options[:myc_level_id].present?
    myc_classes = myc_classes.where("myc_classes.id IN (SELECT mcs.myc_class_id FROM myc_classes_students mcs WHERE mcs.myc_class_id = myc_classes.id AND mcs.student_id = #{options[:student_id]} AND mcs.status = #{MycClassStudent.statuses[:in_class]})") if options[:student_id].present?
    myc_classes = myc_classes.where("myc_classes.status = #{options[:status]}") if options[:status].present?

    myc_classes
        .select(
            "myc_classes.id",
            "concat_ws(' ', myc_programs.program_name, myc_levels.level_name) as program_and_level_name",
            "myc_classes.start_at",
            "array_to_string(array(
              SELECT distinct(users.nric_name) FROM teachers
              INNER JOIN lessons ON teachers.id = lessons.teacher_id
              INNER JOIN users ON users.id = teachers.user_id
              WHERE lessons.myc_class_id = myc_classes.id), ', ') as teacher_name",
            "myc_classes.status",
            "(SELECT lessons.lesson_no FROM lessons WHERE myc_class_id = myc_classes.id AND lessons.status= #{Lesson.statuses[:marked]} ORDER BY lessons.lesson_no DESC limit 1) as lesson_no"
        )
        .distinct()

  end

  # ==== Insert 'presenter'-like methods below if necessary
end

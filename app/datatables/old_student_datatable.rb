class OldStudentDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
        Student.nric_name
        Student.status
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
        Student.nric_name
        Student.status
    )
  end

  private

  def data
    records.map do |record|
      [
        # comma separated list of the values for each cell of a table row
        # example: record.attribute,
          record.id,
          record.nric_name,
          record.program_and_level_name,
          record.status.humanize
      ]
    end
  end

  def get_raw_records
    Student
        .joins("LEFT JOIN myc_classes_students ON students.id = myc_classes_students.student_id")
        .joins("LEFT JOIN myc_classes ON myc_classes_students.myc_class_id = myc_classes.id")
        .joins("LEFT JOIN myc_levels ON myc_classes.myc_level_id = myc_levels.id")
        .joins("LEFT JOIN myc_programs ON myc_levels.myc_program_id = myc_programs.id")
        .where("students.studio_id=#{options[:studio_id]}")
        .where("students.status NOT IN ( #{Student.statuses['new_class_registered']}, #{Student.statuses['deleted']} )")
        .where("students.id NOT IN (SELECT myc_classes_students.student_id FROM myc_classes_students WHERE myc_classes_students.myc_class_id = #{options[:myc_class_id]})")
        .where("myc_classes_students.updated_at = (SELECT MAX(cls.updated_at) FROM myc_classes_students cls WHERE cls.student_id = myc_classes_students.student_id) OR myc_classes_students.updated_at IS NULL")
        .select(
            "students.id",
            "students.nric_name",
            "concat_ws(' ', myc_programs.program_name, myc_levels.level_name) as program_and_level_name",
            "students.status"
        )
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

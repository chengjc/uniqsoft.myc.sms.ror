class UserDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  # def_delegator :@view, :edit_hq_studio_admin_path
  # def_delegator :@view, :edit_hq_teacher_path
  def_delegator :@view, :edit_hq_user_path
  def_delegator :@view, :edit_status_hq_users_path

  def sortable_columns
    @sortable_columns ||= %w(
        User.id
        User.nric_name
        User.phone_number
        User.email
        StudioUser.role_id
        User.status
        User.studio_code
        User.invitation_sent_at
        User.failed_attempts
        User.locked_at
        User.sign_in_count
        User.current_sign_in_at
        User.current_sign_in_ip
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      User.nric_name
      User.phone_number
      User.email
      StudioUser.role_id
      User.status
      User.studio_code
      User.invitation_sent_at
      User.failed_attempts
      User.locked_at
      User.sign_in_count
      User.current_sign_in_at
      User.current_sign_in_ip
    )
  end

  private

  def data
    records.map do |record|
      [
          # record.id, # checkbox column
          create_action_menu(record.id),
          create_link(record),
          record.phone_number,
          record.email,
          to_name(record.role_id),
          record.status.humanize,
          record.studio_code,
          record.invitation_sent_at.to_s,
          record.failed_attempts,
          record.locked_at,
          record.sign_in_count,
          record.current_sign_in_at.to_s,
          record.current_sign_in_ip,
      ]
    end
  end


  def get_raw_records
    User.
        joins('LEFT JOIN studio_users ON studio_users.user_id = users.id').
        joins('LEFT JOIN studios ON studios.id = studio_users.studio_id').
        select(
        "users.id",
        "users.nric_name",
        "users.email",
        "string_agg(studios.studio_code, ',') as studio_code",
        "users.phone_number",
        "string_agg(studio_users.role_id::character varying, ',') as role_id",
        "users.status",
        "users.invitation_sent_at",
        "users.failed_attempts",
        "users.locked_at",
        "users.sign_in_count",
        "users.current_sign_in_at",
        "users.current_sign_in_ip"
    ).group(
        "users.id",
        "users.nric_name",
        "users.email",
        "users.phone_number",
        "users.status",
        "users.invitation_sent_at",
        "users.failed_attempts",
        "users.locked_at",
        "users.sign_in_count",
        "users.current_sign_in_at",
        "users.current_sign_in_ip"
    )
  end

  def create_link(record)
    link_to(record.nric_name, edit_hq_user_path(record))
  end

  def create_action_menu(user_id)
    teacher_id = Teacher.where(user_id: user_id).pluck(:id).first
    if (teacher_id == nil)
      teacher_action_name = 'New teacher profile'
      teacher_action_link = "/hq/users/#{user_id}/teachers/new"
    else
      teacher_action_name = 'Edit teacher profile'
      teacher_action_link = "/hq/teachers/#{teacher_id}/edit"
    end

    "<div class='dropdown'>
        <button id='btn-action' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          <span class='caret'></span>
        </button>
        <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
          <li><a href='#{teacher_action_link}'>#{teacher_action_name}</a></li>
          <li role='separator' class='divider'></li>
          <li><a href='/hq/users/#{user_id}/studio_users'>Assign studios</a></li>
          <li role='separator' class='divider'></li>
          <li><a href='/hq/users/#{user_id}/edit_status' data-remote='true'>Edit status</a></li>
          <li role='separator' class='divider'></li>
          <li><a href='/hq/users/#{user_id}/invite_confirm' data-remote='true'>Invite</a></li>
      </ul>
    </div>"
  end

  # def format_datetime(datetime)
  #   I18n.l(datetime) if datetime.present?
  # end

  def to_name(role_ids)
    return '' if role_ids.nil?

    role_name_array = Array.new
    role_id_array = role_ids.split(',')

    role_id_array.each do |role_id|
      StudioUser.roles.each do |role|
        if role[1] == role_id.to_i
          role_name = role[0].humanize
          unless  role_name_array.include? role_name
            role_name_array << role_name
            break
          end
        end
      end
    end

    return role_name_array.join(',')
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

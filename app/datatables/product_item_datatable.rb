class ProductItemDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :edit_hq_product_item_path
  def_delegator :@view, :number_to_currency
  def_delegator :@view, :link_to

  def sortable_columns
    @sortable_columns ||= %w(
      ProductItem.description
      ProductItem.cost
      ProductItem.quantity_on_hand
      ProductItem.quantity_trigger_alert
      ProductItem.remarks
      ProductItem.status
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      ProductItem.description
      ProductItem.cost
      ProductItem.quantity_on_hand
      ProductItem.quantity_trigger_alert
      ProductItem.remarks
      ProductItem.status
    )
  end

  private

  def data
    records.map do |record|
      [
          link_to(record.description, edit_hq_product_item_path(record)),
          number_to_currency(record.cost),
          record.quantity_on_hand,
          record.quantity_trigger_alert,
          record.remarks,
          record.status.humanize
      ]
    end
  end


  def get_raw_records
    ProductItem.
        select(
            "id",
            "description",
            "cost",
            "quantity_on_hand",
            "quantity_trigger_alert",
            "remarks",
            "status"
        )
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

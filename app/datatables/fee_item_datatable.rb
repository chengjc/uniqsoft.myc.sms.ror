class FeeItemDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      FeeItem.id
      FeeItem.fee_item_name
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      FeeItem.id
      FeeItem.fee_item_name
)
  end

  private

  def data
    records.map do |record|
      [
          record.id,
          record.fee_item_name,
          number_to_currency(record.price)
      ]
    end
  end

  def get_raw_records
    FeeItem.where(fee_type: options[:fee_type])
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

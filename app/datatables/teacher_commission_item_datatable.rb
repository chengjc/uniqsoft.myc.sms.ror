class TeacherCommissionItemDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :number_to_percentage
  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
    )
  end

  private

  def data
    records.map do |record|
      [
          # "<a href='/myc/teacher_commissions/#{options[:teacher_commission_id]}/teacher_commission_items/#{record.id}/edit'>Edit</a>",
          create_action(record.id),
          record.program,
          record.day,
          record.time,
          record.student_nric_name,
          record.lesson_date,
          number_to_currency(record.term_fee),
          number_to_currency(record.lesson_fee),
          record.no_of_lesson,
          number_to_currency(record.total_fee),
          number_to_percentage(record.percentage, precision: 0),
          number_to_currency(record.commission),
          record.remarks
      ]
    end
  end

  def get_raw_records
    TeacherCommissionItem.
        select('
          teacher_commission_items.id,
          teacher_commission_items.program,
          teacher_commission_items.day,
          teacher_commission_items.time,
          teacher_commission_items.student_nric_name,
          teacher_commission_items.lesson_date,
          teacher_commission_items.term_fee,
          teacher_commission_items.lesson_fee,
          teacher_commission_items.no_of_lesson,
          teacher_commission_items.total_fee,
          teacher_commission_items.percentage,
          teacher_commission_items.commission,
          teacher_commission_items.remarks
        ').
        where(teacher_commission_id: options[:teacher_commission_id]).
        order(:student_nric_name)
  end

  def create_action(id)
    "<div class='dropdown'>
        <button id='btn-action' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          <span class='caret'></span>
        </button>
        <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
          <li><a href='/myc/teacher_commissions/#{options[:teacher_commission_id]}/teacher_commission_items/#{id}/edit_percentage' data-remote='true'>Edit percentage</a></li>
          <li><a href='/myc/teacher_commissions/#{options[:teacher_commission_id]}/teacher_commission_items/#{id}/edit_remarks' data-remote='true'>Edit remarks</a></li>
      </ul>
    </div>"
  end

  # def commission(record)
  #   display_currency((record.total_fee * record.percentage / 100))
  # end
  # ==== Insert 'presenter'-like methods below if necessary
end

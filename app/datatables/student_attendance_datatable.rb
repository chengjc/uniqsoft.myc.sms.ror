class StudentAttendanceDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
        Student.nric_name
        Attendance.status
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
        Student.nric_name
        Attendance.status
    )
  end

  private

  def data
    records.map do |record|
      [
        # comma separated list of the values for each cell of a table row
        # example: record.attribute,
          record.id,
          record.nric_name,
          record.status.humanize
      ]
    end
  end

  def get_raw_records
    # insert query here
    Attendance
        .joins("INNER JOIN lessons ON lessons.id = attendances.lesson_id")
        .joins("INNER JOIN students ON attendances.student_id = students.id")
        .where("lessons.id = #{options[:lesson_id]}")
        .where("students.id IN (
           SELECT myc_classes_students.student_id FROM myc_classes_students
           WHERE myc_classes_students.myc_class_id = lessons.myc_class_id AND myc_classes_students.status = #{MycClassStudent.statuses[:in_class]} )")
        .select(
            "attendances.id",
            "students.nric_name",
            "attendances.status"
        )
        .order(
            "students.nric_name"
        )

  end

  # ==== Insert 'presenter'-like methods below if necessary
end

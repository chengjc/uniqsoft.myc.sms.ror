class TeacherCommissionOtherDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
    )
  end

  private

  def data
    records.map do |record|
      [
          create_action_menu(record.id),
          record.item,
          number_to_currency(record.amount),
          record.remarks
      ]
    end
  end

  def get_raw_records
    TeacherCommissionOther.
        select('
          teacher_commission_others.id,
          teacher_commission_others.item,
          teacher_commission_others.amount,
          teacher_commission_others.remarks
        ').
        where(teacher_commission_id: options[:teacher_commission_id]).
        order(:item)
  end

  def create_action_menu(id)
    "<div class='dropdown'>
        <button id='btn-action' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          <span class='caret'></span>
        </button>
        <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
          <li><a href='/myc/teacher_commission_others/#{id}/edit' data-remote='true'>Edit</a></li>
          <li><a href='/myc/teacher_commission_others/#{id}/delete' data-remote='true'>Delete</a></li>
      </ul>
    </div>"
  end
  # ==== Insert 'presenter'-like methods below if necessary
end

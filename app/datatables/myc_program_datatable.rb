class MycProgramDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def sortable_columns
    @sortable_columns ||= %w(
        MycProgram.program_name
        MycProgram.program_code
        MycProgram.status
        MycProgram.program_order
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      MycProgram.program_name
      MycProgram.program_code
      MycProgram.status
      MycProgram.program_order
    )
  end

  private

  def data
    records.map do |record|
      [
          record.program_name,
          record.program_code,
          record.status.humanize,
          record.program_order
      ]
    end
  end


  def get_raw_records
    MycProgram.
        select(
            'id',
            'program_name',
            'program_code',
            'status',
            'program_order'
        )
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

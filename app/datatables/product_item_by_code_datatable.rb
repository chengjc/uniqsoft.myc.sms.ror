class ProductItemByCodeDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :number_to_currency

  def sortable_columns
    @sortable_columns ||= %w(
      ProductItem.description
      ProductItem.cost
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      ProductItem.description
      ProductItem.cost
    )
  end

  private

  def data
    records.map do |record|
      [
          create_action_menu(record.id),
          record.description,
          number_to_currency(record.cost)
      ]
    end
  end


  def get_raw_records
    ProductItem.
        joins('LEFT JOIN product_codes_items ON product_codes_items.product_item_id = product_items.id').
        select(
            "id",
            "description",
            "cost"
        ).
        where("product_codes_items.product_code_id = #{options[:product_code_id]}").
        where("product_items.status = #{ProductItem.statuses[:active]}")

  end

  def create_action_menu(id)
    "<div class='dropdown'>
        <button id='btn-action' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          <span class='caret'></span>
        </button>
        <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
          <li><a href='/hq/product_codes/#{options[:product_code_id]}/remove_item_confirm/#{id}' data-remote='true'>Remove</a></li>
      </ul>
    </div>"
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

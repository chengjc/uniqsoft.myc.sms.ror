class InClassStudentDatatable < AjaxDatatablesRails::Base

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
        Student.nric_name
        Student.status
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
        Student.nric_name
        Student.status
    )
  end

  private

  def data
    records.map do |record|
      [
        # comma separated list of the values for each cell of a table row
        # example: record.attribute,
          record.id,
          record.nric_name,
          record.status.humanize
      ]
    end
  end

  def get_raw_records
    Student
        .joins("LEFT JOIN myc_classes_students ON students.id = myc_classes_students.student_id")
        .where("myc_classes_students.myc_class_id = #{options[:myc_class_id]}")
        .where("myc_classes_students.status = #{MycClassStudent.statuses[:in_class]}")

  end

  # ==== Insert 'presenter'-like methods below if necessary
end

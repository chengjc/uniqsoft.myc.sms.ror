class FamilyDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :edit_myc_family_path

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      Family.id
      Family.primary_contact_name
      Family.status
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      Family.id
      Family.primary_contact_name
      Family.status
)
  end

  private

  def data
    records.map do |record|
      [
          link_to(record.primary_contact_name, edit_myc_family_path(record)),
          record.primary_contact_accompany_for_student ?  'Yes' :  '',
          record.primary_contact_relationship,
          record.primary_contact_phone_no,
          record.primary_contact_email,
          record.status.humanize,
      ]
    end
  end

  def get_raw_records
    Family.where(studio_id: options[:studio_id])
  end



  # ==== Insert 'presenter'-like methods below if necessary
end

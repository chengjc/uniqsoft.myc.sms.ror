class StudentReportDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :strftime

  def sortable_columns
    @sortable_columns ||= %w(
      Studio.id
      Teacher.id
      Studio.studio_name
      User.nric_name
      StudentReport.report_date
      StudentReport.student_count_sr1
      StudentReport.student_count_sr2
      StudentReport.student_count_sr3
      StudentReport.student_count_ss1
      StudentReport.student_count_ss2
      StudentReport.student_count_sb1
      StudentReport.student_count_sb2
      StudentReport.student_count_sb3
      StudentReport.student_count_mb1
      StudentReport.student_count_mb2
      StudentReport.student_count_mb3
      StudentReport.total
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      Studio.id
      Teacher.id
      Studio.studio_name
      User.nric_name
      StudentReport.report_date
      StudentReport.student_count_sr1
      StudentReport.student_count_sr2
      StudentReport.student_count_sr3
      StudentReport.student_count_ss1
      StudentReport.student_count_ss2
      StudentReport.student_count_sb1
      StudentReport.student_count_sb2
      StudentReport.student_count_sb3
      StudentReport.student_count_mb1
      StudentReport.student_count_mb2
      StudentReport.student_count_mb3
      StudentReport.total
    )
  end

  private

  def data
    records.map do |record|
      [
          record.studio_id,
          record.teacher_id,
          record.studio_name,
          record.nric_name,
          record.report_date.strftime('%m/%y'),
          record.student_count_sr1,
          record.student_count_sr2,
          record.student_count_sr3,
          record.student_count_ss1,
          record.student_count_ss2,
          record.student_count_sb1,
          record.student_count_sb2,
          record.student_count_sb3,
          record.student_count_mb1,
          record.student_count_mb2,
          record.student_count_mb3,
          record.total
      ]
    end
  end


  def get_raw_records
    StudentReport
        .joins('LEFT JOIN teachers on teachers.id = student_reports.teacher_id')
        .joins('LEFT JOIN users on users.id = teachers.user_id')
        .joins('LEFT JOIN studios on studios.id = student_reports.studio_id')
        .select('student_reports.studio_id')
        .select('student_reports.teacher_id')
        .select('studios.studio_name')
        .select('users.nric_name')
        .select('student_reports.report_date')
        .select('student_reports.student_count_sr1')
        .select('student_reports.student_count_sr2')
        .select('student_reports.student_count_sr3')
        .select('student_reports.student_count_ss1')
        .select('student_reports.student_count_ss2')
        .select('student_reports.student_count_sb1')
        .select('student_reports.student_count_sb2')
        .select('student_reports.student_count_sb3')
        .select('student_reports.student_count_mb1')
        .select('student_reports.student_count_mb2')
        .select('student_reports.student_count_mb3')
        .select('student_reports.total')

  end

  # ==== Insert 'presenter'-like methods below if necessary
end

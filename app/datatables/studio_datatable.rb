class StudioDatatable < AjaxDatatablesRails::Base

include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :edit_hq_studio_path

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      Studio.id
      Studio.studio_name
      Studio.studio_code
      Studio.address
      Studio.post_code
      Studio.company_name
      Studio.company_registration_no
      Studio.phone_no
      Studio.email
      Studio.facebook_address
      Studio.website_address
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      Studios.studio_name
      Studio.studio_code
      Studio.phone_no
      Studio.email
      Studio.status
      )
  end

  private

  def data
    records.map do |record|
      [
        # comma separated list of the values for each cell of a table row
        # example: record.attribute,

          (if record.studio_name == Rails.configuration.hq_studio_name
            record.studio_name
          else
            link_to(record.studio_name, edit_hq_studio_path(record))
          end),
          record.studio_code,
          record.address,
          record.post_code,
          record.company_name,
          record.company_registration_no,
          record.phone_no,
          record.email,
          record.facebook_address,
          record.website_address,
          record.status.humanize
      ]
    end
  end

  def get_raw_records
    # insert query here
    Studio.all
  end

  # ==== Insert 'presenter'-like methods below if necessary
end


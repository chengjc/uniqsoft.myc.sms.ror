class TeacherCommissionItemAdditionalDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :number_to_percentage
  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
    )
  end

  private

  def data
    records.map do |record|
      [
          # "<a href='/myc/teacher_commissions/#{options[:teacher_commission_id]}/teacher_commission_items/#{record.id}/edit'>Edit</a>",
          create_action(record.id),
          record.program,
          record.day,
          record.time,
          record.student_nric_name,
          record.lesson_date,
          number_to_currency(record.term_fee),
          number_to_currency(record.lesson_fee),
          record.no_of_lesson,
          number_to_currency(record.total_fee),
          number_to_percentage(record.percentage, precision: 0),
          number_to_currency(record.commission),
          record.remarks
      ]
    end
  end

  def get_raw_records
    TeacherCommissionItemAdditional.
        select('
          teacher_commission_item_additionals.id,
          teacher_commission_item_additionals.program,
          teacher_commission_item_additionals.day,
          teacher_commission_item_additionals.time,
          teacher_commission_item_additionals.student_nric_name,
          teacher_commission_item_additionals.lesson_date,
          teacher_commission_item_additionals.term_fee,
          teacher_commission_item_additionals.lesson_fee,
          teacher_commission_item_additionals.no_of_lesson,
          teacher_commission_item_additionals.total_fee,
          teacher_commission_item_additionals.percentage,
          teacher_commission_item_additionals.commission,
          teacher_commission_item_additionals.remarks
        ').
        where(teacher_commission_id: options[:teacher_commission_id]).
        order(:student_nric_name).
        order(:lesson_date)
  end

  def create_action(id)
    "<div class='dropdown'>
        <button id='btn-action' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          <span class='caret'></span>
        </button>
        <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
          <li><a href='/myc/teacher_commission_item_additionals/#{id}/edit' data-remote='true'>Edit</a></li>
          <li><a href='/myc/teacher_commission_item_additionals/#{id}/delete' data-remote='true'>Delete</a></li>
      </ul>
    </div>"
  end

  # def commission(record)
  #   display_currency((record.total_fee * record.percentage / 100), precision: 2)
  # end
  # ==== Insert 'presenter'-like methods below if necessary
end

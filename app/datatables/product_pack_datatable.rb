class ProductPackDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :edit_hq_product_pack_path
  def_delegator :@view, :link_to

  def sortable_columns
    @sortable_columns ||= %w(
      ProductPack.description
      ProductPack.status
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      ProductPack.description
      ProductPack.status
    )
  end

  private

  def data
    records.map do |record|
      [
          link_to(record.description, edit_hq_product_pack_path(record)),
          record.status.humanize
      ]
    end
  end


  def get_raw_records
    ProductPack.
        select(
            "id",
            "description",
            "status"
        )
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

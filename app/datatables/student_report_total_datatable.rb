class StudentReportTotalDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :strftime

  def sortable_columns
    @sortable_columns ||= %w(
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
    )
  end

  private

  def data
    records.map do |record|
      [
          record.student_count_sr1,
          record.student_count_sr2,
          record.student_count_sr3,
          record.student_count_ss1,
          record.student_count_ss2,
          record.student_count_sb1,
          record.student_count_sb2,
          record.student_count_sb3,
          record.student_count_mb1,
          record.student_count_mb2,
          record.student_count_mb3,
          record.total
      ]
    end
  end

  def get_raw_records
    query =
        StudentReport
            .select('sum(student_count_sr1) as student_count_sr1')
            .select('sum(student_count_sr2) as student_count_sr2')
            .select('sum(student_count_sr3) as student_count_sr3')
            .select('sum(student_count_ss1) as student_count_ss1')
            .select('sum(student_count_ss2) as student_count_ss2')
            .select('sum(student_count_sb1) as student_count_sb1')
            .select('sum(student_count_sb2) as student_count_sb2')
            .select('sum(student_count_sb3) as student_count_sb3')
            .select('sum(student_count_mb1) as student_count_mb1')
            .select('sum(student_count_mb2) as student_count_mb2')
            .select('sum(student_count_mb3) as student_count_mb3')
            .select('sum(total) as total')


    query = query.where(studio_id: options[:studio_id]) unless options[:studio_id].blank?

    query = query.where(teacher_id: options[:teacher_id]) unless options[:teacher_id].blank?

    return query
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

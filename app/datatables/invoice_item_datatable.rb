class InvoiceItemDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      InvoiceItem.id
      InvoiceItem.item_name
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      InvoiceItem.id
      InvoiceItem.item_name
)
  end

  private

  def data
    records.map do |record|
      [
          create_delete_link(record),
          record.item_name,
          record.quantity,
          number_to_currency(record.price),
          number_to_currency(record.quantity * record.price)
      ]
    end
  end

  def get_raw_records
    InvoiceItem.where(invoice_id:  options[:invoice_id])
  end

  def create_delete_link(record)
    link_to "Delete", "/myc/invoices/#{options[:invoice_id]}/invoice_items/#{record.id}/delete", remote: true
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

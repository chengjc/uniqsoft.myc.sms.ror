class InvoiceDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :edit_myc_invoice_path
  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      Invoice.invoice_number
      Invoice.invoice_on
      Invoice.due_on
      Invoice.student_id
      Invoice.total_price
      Invoice.bal_due
      Invoice.status
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      Invoice.status
    )
  end

  private

  def data
    records.map do |record|
      [
          # comma separated list of the values for each cell of a table row
          # example: record.attribute,
          link_to(record.invoice_number, edit_myc_invoice_path(record)),
          record.invoice_on.to_s,
          record.due_on.to_s,
          record.nric_name,
          number_to_currency(record.total_price),
          number_to_currency(record.bal_due),
          record.status,
      ]
    end
  end

  def get_raw_records
    Invoice.
        joins('LEFT JOIN students ON invoices.student_id = students.id').
        select(
            "invoices.id",
            "invoices.invoice_number",
            "invoices.invoice_on",
            "invoices.due_on",
            "students.nric_name",
            "invoices.total_price",
            "invoices.bal_due",
            "invoices.status",

        ).where("invoices.studio_id = #{options[:studio_id]}")
  end

  # ==== Insert 'presenter'-like methods below if necessary
end


class StudentDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :edit_myc_student_path

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      Student.id
      Student.nric_name
      Student.status')
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      Student.nric_name
      Student.status
    )
  end

  private

  def data
    records.map do |record|
      [
          link_to(record.nric_name, edit_myc_student_path(record)),
          record.status.humanize,
      ]
    end
  end

  def get_raw_records
    # insert query here
    Student.where(studio_id: options[:studio_id])
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

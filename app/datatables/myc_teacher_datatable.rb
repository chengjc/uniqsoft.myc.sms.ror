class MycTeacherDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def sortable_columns
    @sortable_columns ||= %w(
      User.id
      User.nric_name
      User.phone_number
      User.email
      User.status
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      User.nric_name
      User.phone_number
      User.email
      User.status
    )
  end

  private

  def data
    records.map do |record|
      [
        record.nric_name,
        record.phone_number,
        record.email,
        # record.status.humanize,
        User.status_name(record.status),
        record.date_of_myc_l1.to_s,
        record.date_of_myc_l2ss2.to_s,
        record.date_of_myc_l2sb2.to_s,
        record.date_of_myc_l2mb2.to_s,
        record.date_of_myc_l3sb3.to_s,
        record.date_of_myc_l3mb3.to_s,
        record.date_of_1st_teacher_visit.to_s,
        record.date_of_2nd_teacher_visit.to_s,
        record.date_of_3rd_teacher_visit.to_s
      ]
    end
  end


  def get_raw_records
    Teacher.
        joins('LEFT JOIN users ON teachers.user_id = users.id').
        joins('LEFT JOIN studio_users ON studio_users.user_id = users.id').
        select(
            "users.id",
            "users.nric_name",
            "users.email",
            "users.phone_number",
            "users.status",
            "teachers.date_of_myc_l1",
            "teachers.date_of_myc_l2ss2",
            "teachers.date_of_myc_l2sb2",
            "teachers.date_of_myc_l2mb2",
            "teachers.date_of_myc_l3sb3",
            "teachers.date_of_myc_l3mb3",
            "teachers.date_of_1st_teacher_visit",
            "teachers.date_of_2nd_teacher_visit",
            "teachers.date_of_3rd_teacher_visit"
        ).where("studio_users.studio_id = #{options[:studio_id]}")
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

class ProductItemByPackDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :edit_hq_product_item_path
  def_delegator :@view, :number_to_currency
  def_delegator :@view, :link_to

  def sortable_columns
    @sortable_columns ||= %w(
      ProductItem.description
      ProductItem.cost
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      ProductItem.description
      ProductItem.cost
    )
  end

  private

  def data
    records.map do |record|
      [
          create_action_menu(record.id),
          record.description,
          number_to_currency(record.cost)
      ]
    end
  end


  def get_raw_records
    ProductItem.
        joins('LEFT JOIN product_items_packs ON product_items_packs.product_item_id = product_items.id').
        select(
            "id",
            "description",
            "cost"
        ).
        where("product_items_packs.product_pack_id = #{options[:product_pack_id]}").
        where("product_items.status = #{ProductItem.statuses[:active]}")
  end

  def create_action_menu(id)
    "<div class='dropdown'>
        <button id='btn-action' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          <span class='caret'></span>
        </button>
        <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
          <li><a href='/hq/product_packs/#{options[:product_pack_id]}/remove_item_confirm/#{id}' data-remote='true'>Remove</a></li>
      </ul>
    </div>"
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

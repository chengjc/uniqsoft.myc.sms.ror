class HolidayDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :edit_myc_holiday_path

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      Holiday.holiday_date
      Holiday.holiday_name
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      Holiday.holiday_date
      Holiday.holiday_name
    )
  end

  private

  def data
    records.map do |record|
      [
          # comma separated list of the values for each cell of a table row
          # example: record.attribute,
          link_to(record.holiday_name, edit_myc_holiday_path(record)),
          record.holiday_date.to_s,
      ]
    end
  end

  def get_raw_records
    # insert query here
    Holiday.where(studio_id: options[:studio_id])
  end

  # ==== Insert 'presenter'-like methods below if necessary
end


class MaterialForInvoiceDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :number_to_currency

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      product_codes.id
      product_codes.code
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      product_codes.code
      product_codes.myc_level_id
)
  end

  private

  def data
    records.map do |record|
      [
          record.id,
          record.code,
          number_to_currency(record.price),
          record.myc_level_id
      ]
    end
  end

  def get_raw_records
    query = ProductCode.all

    query = query.where(myc_level_id: options[:myc_level_id]) unless options[:myc_level_id].blank?

    return query
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

class InvoiceExistingStudentDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari


  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      Student.id
      Student.nric_name
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      Student.nric_name
      Lesson.teacher_id
    )
  end

  private

  def data
    records.map do |record|
      [
          record.id,
          record.nric_name,
          record.program_and_level_name,
          record.status.humanize,
          record.teacher_id,
      ]
    end
  end

  def get_raw_records
    MycClassStudent
        .joins("LEFT JOIN students ON students.id = myc_classes_students.student_id")
        .joins("INNER JOIN myc_classes ON myc_classes.id = myc_classes_students.myc_class_id")
        .joins("LEFT JOIN myc_levels ON myc_classes.myc_level_id = myc_levels.id")
        .joins("LEFT JOIN myc_programs ON myc_levels.myc_program_id = myc_programs.id")
        .joins("INNER JOIN lessons ON lessons.myc_class_id = myc_classes.id")
        .where("myc_classes.studio_id = #{options[:studio_id]}")
        .select(
            "myc_classes_students.id",
            "students.nric_name",
            "concat_ws(' ', myc_programs.program_name, myc_levels.level_name) as program_and_level_name",
            "myc_classes_students.status",
            "lessons.teacher_id"
        )
        .order(
            "students.nric_name"
        )
        .distinct("myc_classes_students.id")

  end



  # ==== Insert 'presenter'-like methods below if necessary
end


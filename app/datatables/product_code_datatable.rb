class ProductCodeDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :edit_hq_product_code_path
  def_delegator :@view, :number_to_currency
  def_delegator :@view, :link_to

  def sortable_columns
    @sortable_columns ||= %w(
      MycProgram.program_name
      ProductCode.code
      ProductCode.cost
      ProductCode.price
      ProductCode.remarks
      ProductCode.status
      MycProgram.program_order
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      MycProgram.program_name
      ProductCode.code
      ProductCode.cost
      ProductCode.price
      ProductCode.remarks
      ProductCode.status
      MycProgram.program_order
    )
  end

  private

  def data
    records.map do |record|
      [
          record.program_name,
          link_to(record.code, edit_hq_product_code_path(record)),
          number_to_currency(ProductCode.cost(record.id)),
          number_to_currency(record.price),
          record.remarks,
          record.status.humanize,
          record.program_order
      ]
    end
  end


  def get_raw_records
    ProductCode.
        joins('LEFT JOIN myc_programs ON myc_programs.id = product_codes.myc_program_id').
        select(
            "product_codes.id",
            "product_codes.code",
            "product_codes.price",
            "myc_programs.program_name",
            "product_codes.remarks",
            "product_codes.status",
            "myc_programs.program_order"
        )
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

class InvoiceNewStudentDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari


  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
      Student.id
      Student.nric_name
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
      Student.id
      Student.nric_name
    )
  end

  private

  def data
    records.map do |record|
      [
          record.id,
          record.nric_name,
          record.program_and_level_name,
      ]
    end
  end

  def get_raw_records
    # Student.where(studio_id: options[:studio_id])

    Student
        .where("students.studio_id = #{options[:studio_id]}")
        .where("students.id NOT IN (SELECT myc_classes_students.student_id FROM myc_classes_students)")
        .joins("LEFT JOIN myc_levels ON students.myc_level_id = myc_levels.id")
        .joins("LEFT JOIN myc_programs ON myc_levels.myc_program_id = myc_programs.id")
        .select(
            "students.id",
            "students.nric_name",
            "concat_ws(' ', myc_programs.program_name, myc_levels.level_name) as program_and_level_name"
        )
        .order(
            "students.nric_name"
        )

  end

  # ==== Insert 'presenter'-like methods below if necessary
end


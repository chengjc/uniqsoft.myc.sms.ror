class StudioUserDatatable < AjaxDatatablesRails::Base
  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :hq_studio_user_path

  def sortable_columns
    # Declare strings in this format: ModelName.column_name
    @sortable_columns ||= %w(
        StudioUser.id
        Studio.studio_name
        StudioUser.role_id
    )
  end

  def searchable_columns
    # Declare strings in this format: ModelName.column_name
    @searchable_columns ||= %w(
        StudioUser.id
        Studio.studio_name
        StudioUser.role_id
    )
  end

  private

  def data
    records.map do |record|
      [
          create_delete_link(record.id),
          record.studio_name,
          to_name(record.role_id)
      ]
    end
  end

  def get_raw_records
    # insert query here
    StudioUser.
        joins('LEFT JOIN studios ON studios.id = studio_users.studio_id').
        select('
          studio_users.id,
          studios.studio_name,
          studio_users.role_id
        ').
        where(user_id: options[:user_id])
  end

  def to_name(role_id)
    StudioUser.roles.each do |role|
      if role[1] == role_id
        return role[0].humanize
      end
    end
  end

  def create_delete_link(id)
    # link_to('Delete', hq_studio_user_path(record.id), method: :delete)
    # "<a href=javascript:deleteStudio(#{record.id})>Delete</a>" if record.role_id != User.roles[:super_user]
    # "<a href='#' data-delete-id='#{record.id}'>Delete</a>"
    link_to "Delete", "/hq/studio_users/#{id}/delete", remote: true
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

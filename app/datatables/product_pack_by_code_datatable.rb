class ProductPackByCodeDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :number_to_currency
  def_delegator :@view, :link_to

  def sortable_columns
    @sortable_columns ||= %w(
      ProductPack.description
      ProductPack.cost
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      ProductPack.description
      ProductPack.cost
    )
  end

  private

  def data
    records.map do |record|
      [
          create_action_menu(record.id),
          link_to(record.description, "/hq/product_packs/#{record.id}/edit"),
          number_to_currency(record.cost)
      ]
    end
  end


  def get_raw_records
    # ProductPack.
    #     joins('LEFT JOIN product_codes_packs ON product_codes_packs.product_pack_id = product_packs.id').
    #     select(
    #         "id",
    #         "description",
    #         "cost"
    #     ).
    #     where("product_codes_packs.product_code_id = #{options[:product_code_id]}").
    #     where("product_packs.status = #{ProductPack.statuses[:active]}")

    ProductItem.
        joins('LEFT JOIN product_items_packs on product_items_packs.product_item_id = product_items.id').
        joins('LEFT JOIN product_codes_packs on product_codes_packs.product_pack_id = product_items_packs.product_pack_id').
        joins('LEFT JOIN product_packs on product_packs.id = product_items_packs.product_pack_id').
        where("product_packs.status = #{ProductPack.statuses[:active]}").
        where("product_items.status = #{ProductItem.statuses[:active]}").
        where("product_codes_packs.product_code_id = #{options[:product_code_id]}").
        select(
            "product_packs.id",
            "product_packs.description",
            "sum(product_items.cost) as cost"
        ).
        group('product_packs.id, product_packs.description')

  end

  def create_action_menu(id)
    "<div class='dropdown'>
        <button id='btn-action' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
          <span class='caret'></span>
        </button>
        <ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'>
          <li><a href='/hq/product_codes/#{options[:product_code_id]}/show_pack_items/#{id}' data-remote='true'>View</a></li>
          <li><a href='/hq/product_codes/#{options[:product_code_id]}/remove_pack_confirm/#{id}' data-remote='true'>Remove</a></li>
      </ul>
    </div>"
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

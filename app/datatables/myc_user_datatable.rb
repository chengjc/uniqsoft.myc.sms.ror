class MycUserDatatable < AjaxDatatablesRails::Base

  include AjaxDatatablesRails::Extensions::Kaminari

  def_delegator :@view, :link_to
  def_delegator :@view, :edit_myc_user_path

  def sortable_columns
    @sortable_columns ||= %w(
        User.id
        User.nric_name
        User.phone_number
        User.email
        User.role
        User.status
        User.failed_attempts
        User.locked_at
        User.sign_in_count
        User.current_sign_in_at
        User.current_sign_in_ip
    )
  end

  def searchable_columns
    @searchable_columns ||= %w(
      User.nric_name
      User.phone_number
      User.email
      User.role
      User.status
      User.failed_attempts
      User.locked_at
      User.sign_in_count
      User.current_sign_in_at
      User.current_sign_in_ip
    )
  end

  private

  def data
    records.map do |record|
      [
          record.id, # checkbox column
          create_link(record),
          record.phone_number,
          record.email,
          record.role.humanize,
          record.status.humanize,
          record.failed_attempts,
          record.locked_at,
          record.sign_in_count,
          record.current_sign_in_at,
          record.current_sign_in_ip,
      ]
    end
  end


  def get_raw_records
    # insert query here
    User.select('
      id,
      nric_name,
      phone_number,
      email,
      status,
      role,
      failed_attempts,
      locked_at,
      sign_in_count,
      current_sign_in_at,
      current_sign_in_ip
    ')
  end

  private

  def create_link(record)
    case record.role
      when 'teacher'
        link_to(record.nric_name, "/myc/teachers/#{record.id}/edit", method: :get)
      else
        record.nric_name
    end
  end

  # ==== Insert 'presenter'-like methods below if necessary
end

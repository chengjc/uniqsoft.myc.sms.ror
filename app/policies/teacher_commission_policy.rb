class TeacherCommissionPolicy < ApplicationPolicy

  attr_reader :current_user, :user

  def initialize(context, record)
    @user = context.user
    @role_id = context.role_id
    @record = record
  end

  def show?
    can_view
  end

  def show_data?
    can_view
  end

  def generate_confirm?
    role_id == StudioUser.roles[:studio_admin]
  end

  def generate?
    role_id == StudioUser.roles[:studio_admin]
  end

  private

  def can_view
    return true if role_id == StudioUser.roles[:studio_admin]
    return true if Teacher.where(user_id: user.id, id: record.teacher_id).count == 1
    false
  end

end

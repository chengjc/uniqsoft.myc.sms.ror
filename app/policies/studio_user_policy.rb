class StudioUserPolicy < ApplicationPolicy

  attr_reader :current_user, :user

  def initialize(context, record)
    @user = context.user
    @role_id = context.role_id
    @record = record
  end

  def index?
    role_id == StudioUser.roles[:super_user]
  end

  def index_data?
    role_id == StudioUser.roles[:super_user]
  end

  def new?
    role_id == StudioUser.roles[:super_user]
  end

  def create?
    role_id == StudioUser.roles[:super_user]
  end

  def delete?
    role_id == StudioUser.roles[:super_user]
  end

  def destroy?
    role_id == StudioUser.roles[:super_user]
  end

end

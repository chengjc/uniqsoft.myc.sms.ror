class TeacherPolicy < ApplicationPolicy

  attr_reader :current_user, :user

  def initialize(context, record)
    @user = context.user
    @role_id = context.role_id
    @record = record
  end

  def new?
    role_id == StudioUser.roles[:super_user]
  end

  def create?
    role_id == StudioUser.roles[:super_user]
  end

  def edit?
    role_id == StudioUser.roles[:super_user]
  end

  def update?
    role_id == StudioUser.roles[:super_user]
  end

end

class UserPolicy < ApplicationPolicy

  attr_reader :current_user, :user

  # def initialize(current_user, user)
  #   @current_user = current_user
  #   @user = user
  # end

  def initialize(context, record)
    @user = context.user
    @role_id = context.role_id
    @record = record
  end

  def index?
    role_id == StudioUser.roles[:super_user]
  end

  def index_data?
    role_id == StudioUser.roles[:super_user]
  end

  def new?
    role_id == StudioUser.roles[:super_user]
  end

  def create?
    role_id == StudioUser.roles[:super_user]
  end

  def edit?
    role_id == StudioUser.roles[:super_user]
  end

  def update?
    role_id == StudioUser.roles[:super_user]
  end

  def edit_status?
    role_id == StudioUser.roles[:super_user]
  end

  def invite_confirm?
    role_id == StudioUser.roles[:super_user]
  end

  def invite?
    role_id == StudioUser.roles[:super_user]
  end

end

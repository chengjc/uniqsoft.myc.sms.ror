# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160614082331) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attendances", force: :cascade do |t|
    t.integer "lesson_id"
    t.integer "student_id"
    t.integer "status",     limit: 2, default: 0, null: false
  end

  add_index "attendances", ["lesson_id"], name: "index_attendances_on_lesson_id", using: :btree
  add_index "attendances", ["student_id"], name: "index_attendances_on_student_id", using: :btree

  create_table "families", force: :cascade do |t|
    t.integer  "studio_id"
    t.string   "address1",                              limit: 100,                 null: false
    t.string   "address2",                              limit: 100
    t.string   "postal_code",                           limit: 10
    t.string   "city_or_state",                         limit: 50,                  null: false
    t.string   "country",                               limit: 50,                  null: false
    t.string   "primary_contact_name",                  limit: 50,                  null: false
    t.string   "primary_contact_relationship",          limit: 50,                  null: false
    t.string   "primary_contact_phone_no",              limit: 50,                  null: false
    t.string   "primary_contact_email",                 limit: 50,                  null: false
    t.string   "primary_contact_occupation",            limit: 100
    t.string   "primary_contact_music_background",      limit: 50
    t.boolean  "primary_contact_accompany_for_student",             default: false
    t.string   "other_contact_name",                    limit: 50
    t.string   "other_contact_relationship",            limit: 50
    t.string   "other_contact_phone_no",                limit: 50
    t.string   "other_contact_email",                   limit: 50
    t.string   "other_contact_occupation",              limit: 100
    t.string   "other_contact_music_background",        limit: 50
    t.boolean  "other_contact_accompany_for_student",               default: false
    t.integer  "status",                                limit: 2,                   null: false
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
  end

  add_index "families", ["studio_id"], name: "index_families_on_studio_id", using: :btree

  create_table "fee_items", force: :cascade do |t|
    t.string   "fee_item_name", limit: 50,                          null: false
    t.decimal  "price",                    precision: 19, scale: 4, null: false
    t.integer  "fee_type",      limit: 2,                           null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
  end

  create_table "holidays", force: :cascade do |t|
    t.integer  "studio_id",                null: false
    t.date     "holiday_date",             null: false
    t.string   "holiday_name", limit: 100, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "holidays", ["studio_id", "holiday_date"], name: "index_holidays_on_studio_id_and_holiday_date", unique: true, using: :btree

  create_table "invoice_items", force: :cascade do |t|
    t.integer  "invoice_id"
    t.string   "item_name",  limit: 50,                          null: false
    t.integer  "quantity",                                       null: false
    t.decimal  "price",                 precision: 19, scale: 4, null: false
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  add_index "invoice_items", ["invoice_id"], name: "index_invoice_items_on_invoice_id", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.integer  "studio_id"
    t.integer  "student_id"
    t.integer  "myc_level_id"
    t.string   "invoice_number", limit: 20,                          null: false
    t.date     "invoice_on",                                         null: false
    t.date     "due_on",                                             null: false
    t.string   "teacher_name",   limit: 50
    t.string   "day",            limit: 25
    t.string   "time",           limit: 25
    t.decimal  "total_price",               precision: 19, scale: 4, null: false
    t.decimal  "bal_due",                   precision: 19, scale: 4, null: false
    t.integer  "status",                                             null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  add_index "invoices", ["myc_level_id"], name: "index_invoices_on_myc_level_id", using: :btree
  add_index "invoices", ["student_id"], name: "index_invoices_on_student_id", using: :btree
  add_index "invoices", ["studio_id"], name: "index_invoices_on_studio_id", using: :btree

  create_table "latest_invoice_numbers", force: :cascade do |t|
    t.integer  "studio_id"
    t.string   "latest_invoice_number", limit: 100, null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "latest_invoice_numbers", ["studio_id"], name: "index_latest_invoice_numbers_on_studio_id", using: :btree

  create_table "lessons", force: :cascade do |t|
    t.integer  "lesson_no",                               null: false
    t.datetime "start_at",                                null: false
    t.string   "reason_for_cancel"
    t.integer  "status",            limit: 2, default: 0, null: false
    t.integer  "myc_class_id"
    t.integer  "teacher_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "lessons", ["myc_class_id"], name: "index_lessons_on_myc_class_id", using: :btree
  add_index "lessons", ["teacher_id"], name: "index_lessons_on_teacher_id", using: :btree

  create_table "myc_classes", force: :cascade do |t|
    t.integer  "studio_id"
    t.integer  "myc_level_id"
    t.datetime "start_at",                                null: false
    t.integer  "status",            limit: 2, default: 0, null: false
    t.datetime "status_updated_at",                       null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "myc_classes", ["myc_level_id"], name: "index_myc_classes_on_myc_level_id", using: :btree
  add_index "myc_classes", ["studio_id"], name: "index_myc_classes_on_studio_id", using: :btree

  create_table "myc_classes_students", force: :cascade do |t|
    t.integer  "myc_class_id"
    t.integer  "student_id"
    t.integer  "status",       limit: 2, default: 0, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "myc_classes_students", ["myc_class_id"], name: "index_myc_classes_students_on_myc_class_id", using: :btree
  add_index "myc_classes_students", ["student_id"], name: "index_myc_classes_students_on_student_id", using: :btree

  create_table "myc_levels", force: :cascade do |t|
    t.string   "level_name",             limit: 100
    t.string   "age",                    limit: 10,                           null: false
    t.integer  "total_lesson",                                                null: false
    t.integer  "lesson_per_term",                                             null: false
    t.decimal  "lesson_fee_per_term",                precision: 19, scale: 4, null: false
    t.integer  "lesson_duration_minute",                                      null: false
    t.integer  "level_order",                                                 null: false
    t.integer  "status",                 limit: 2,                            null: false
    t.integer  "myc_program_id"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
  end

  add_index "myc_levels", ["myc_program_id"], name: "index_myc_levels_on_myc_program_id", using: :btree

  create_table "myc_programs", force: :cascade do |t|
    t.string   "program_code",  limit: 100, null: false
    t.string   "program_name",  limit: 100, null: false
    t.integer  "program_order",             null: false
    t.integer  "status",        limit: 2,   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "myc_programs", ["program_name", "status"], name: "index_myc_programs_on_program_name_and_status", unique: true, using: :btree

  create_table "product_codes", force: :cascade do |t|
    t.integer  "myc_level_id"
    t.string   "code",         limit: 100,                          null: false
    t.decimal  "price",                    precision: 19, scale: 4, null: false
    t.string   "remarks",      limit: 200
    t.integer  "status",       limit: 2,                            null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
  end

  add_index "product_codes", ["code", "status"], name: "index_product_codes_on_code_and_status", unique: true, using: :btree
  add_index "product_codes", ["myc_level_id"], name: "index_product_codes_on_myc_level_id", using: :btree

  create_table "product_codes_items", id: false, force: :cascade do |t|
    t.integer "product_code_id", null: false
    t.integer "product_item_id", null: false
  end

  add_index "product_codes_items", ["product_code_id", "product_item_id"], name: "product_code_id_and_product_item_id", using: :btree
  add_index "product_codes_items", ["product_code_id", "product_item_id"], name: "unique_product_code_id_and_product_item_id", unique: true, using: :btree

  create_table "product_codes_packs", id: false, force: :cascade do |t|
    t.integer "product_code_id", null: false
    t.integer "product_pack_id", null: false
  end

  add_index "product_codes_packs", ["product_code_id", "product_pack_id"], name: "product_code_id_and_product_pack_id", using: :btree
  add_index "product_codes_packs", ["product_code_id", "product_pack_id"], name: "unique_product_code_id_and_product_pack_id", unique: true, using: :btree

  create_table "product_groups", force: :cascade do |t|
    t.string   "description",   limit: 100, null: false
    t.integer  "display_order", limit: 2,   null: false
    t.integer  "status",        limit: 2,   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "product_groups", ["description", "status"], name: "index_product_groups_on_description_and_status", unique: true, using: :btree

  create_table "product_items", force: :cascade do |t|
    t.string   "description",            limit: 100,                          null: false
    t.decimal  "cost",                               precision: 19, scale: 4, null: false
    t.integer  "quantity_on_hand",                                            null: false
    t.integer  "quantity_trigger_alert",                                      null: false
    t.string   "remarks",                limit: 200
    t.integer  "status",                 limit: 2,                            null: false
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
  end

  add_index "product_items", ["description", "status"], name: "index_product_items_on_description_and_status", unique: true, using: :btree

  create_table "product_items_packs", id: false, force: :cascade do |t|
    t.integer "product_item_id", null: false
    t.integer "product_pack_id", null: false
  end

  add_index "product_items_packs", ["product_item_id", "product_pack_id"], name: "product_item_id_and_product_pack_id", using: :btree
  add_index "product_items_packs", ["product_item_id", "product_pack_id"], name: "unique_product_item_id_and_product_pack_id", unique: true, using: :btree

  create_table "product_packs", force: :cascade do |t|
    t.string   "description", limit: 100, null: false
    t.integer  "status",      limit: 2,   null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "product_packs", ["description", "status"], name: "index_product_packs_on_description_and_status", unique: true, using: :btree

  create_table "student_reports", force: :cascade do |t|
    t.integer  "studio_id",                   null: false
    t.integer  "teacher_id",                  null: false
    t.date     "report_date",                 null: false
    t.integer  "student_count_sr1",           null: false
    t.integer  "student_count_sr2",           null: false
    t.integer  "student_count_sr3",           null: false
    t.integer  "student_count_ss1",           null: false
    t.integer  "student_count_ss2",           null: false
    t.integer  "student_count_sb1",           null: false
    t.integer  "student_count_sb2",           null: false
    t.integer  "student_count_sb3",           null: false
    t.integer  "student_count_mb1",           null: false
    t.integer  "student_count_mb2",           null: false
    t.integer  "student_count_mb3",           null: false
    t.integer  "total",             limit: 8, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "student_reports", ["studio_id", "teacher_id", "report_date"], name: "index_student_reports_studio_id_teacher_id_report_date", unique: true, using: :btree

  create_table "students", force: :cascade do |t|
    t.integer  "myc_level_id"
    t.integer  "studio_id"
    t.integer  "family_id"
    t.string   "enrolment_piano",               limit: 10
    t.string   "enrolment_theory",              limit: 10
    t.string   "enrolment_other",               limit: 10
    t.string   "preferred_day_one",             limit: 10
    t.string   "preferred_day_two",             limit: 10
    t.string   "preferred_day_three",           limit: 10
    t.string   "preferred_time_one",            limit: 10
    t.string   "preferred_time_two",            limit: 10
    t.string   "preferred_time_three",          limit: 10
    t.string   "nric_name",                     limit: 50, null: false
    t.string   "first_name",                    limit: 50, null: false
    t.string   "sur_name",                      limit: 50, null: false
    t.string   "gender",                        limit: 10, null: false
    t.date     "date_of_birth",                            null: false
    t.string   "piano_standard",                limit: 50
    t.string   "piano_college",                 limit: 50
    t.string   "piano_school_attended",         limit: 50
    t.string   "theory_standard",               limit: 50
    t.string   "theory_college",                limit: 50
    t.string   "theory_school_attended",        limit: 50
    t.string   "violin_standard",               limit: 50
    t.string   "violin_college",                limit: 50
    t.string   "violin_school_attended",        limit: 50
    t.string   "other_standard",                limit: 50
    t.string   "other_college",                 limit: 50
    t.string   "other_school_attended",         limit: 50
    t.boolean  "instrument_owned_has_piano"
    t.boolean  "instrument_owned_has_organ"
    t.boolean  "instrument_owned_has_keyboard"
    t.boolean  "instrument_owned_has_violin"
    t.string   "instrument_owned_other",        limit: 50
    t.string   "referred_by",                   limit: 50, null: false
    t.date     "enquiry_date",                             null: false
    t.date     "registration_date"
    t.integer  "status"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "students", ["family_id"], name: "index_students_on_family_id", using: :btree
  add_index "students", ["myc_level_id"], name: "index_students_on_myc_level_id", using: :btree
  add_index "students", ["nric_name", "date_of_birth"], name: "index_students_on_nric_name_and_date_of_birth", unique: true, using: :btree
  add_index "students", ["studio_id"], name: "index_students_on_studio_id", using: :btree

  create_table "studio_users", force: :cascade do |t|
    t.integer  "studio_id",            null: false
    t.integer  "user_id",              null: false
    t.integer  "role_id",    limit: 2, null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "studio_users", ["role_id"], name: "index_studio_users_on_role_id", using: :btree
  add_index "studio_users", ["studio_id"], name: "index_studio_users_on_studio_id", using: :btree
  add_index "studio_users", ["user_id"], name: "index_studio_users_on_user_id", using: :btree

  create_table "studios", force: :cascade do |t|
    t.string  "studio_code",             limit: 10,  null: false
    t.string  "studio_name",             limit: 100, null: false
    t.string  "address",                 limit: 100
    t.string  "post_code",               limit: 10
    t.string  "company_name",            limit: 100
    t.string  "company_registration_no", limit: 50
    t.string  "phone_no",                limit: 50
    t.string  "email",                   limit: 100
    t.string  "facebook_address",        limit: 100
    t.string  "website_address",         limit: 200
    t.integer "status",                  limit: 2,   null: false
  end

  add_index "studios", ["studio_code", "status"], name: "index_studios_on_studio_code_and_status", unique: true, using: :btree

  create_table "studios_teachers", id: false, force: :cascade do |t|
    t.integer "studio_id",  null: false
    t.integer "teacher_id", null: false
  end

  add_index "studios_teachers", ["studio_id", "teacher_id"], name: "index_studios_teachers_on_studio_id_and_teacher_id", using: :btree

  create_table "teacher_commission_item_additionals", force: :cascade do |t|
    t.integer  "teacher_commission_id",                                      null: false
    t.string   "program",               limit: 50,                           null: false
    t.string   "day",                   limit: 10,                           null: false
    t.string   "time",                  limit: 10,                           null: false
    t.string   "student_nric_name",     limit: 100,                          null: false
    t.string   "lesson_date",           limit: 300,                          null: false
    t.decimal  "term_fee",                          precision: 19, scale: 4, null: false
    t.decimal  "lesson_fee",                        precision: 19, scale: 4, null: false
    t.integer  "no_of_lesson",                                               null: false
    t.decimal  "total_fee",                         precision: 19, scale: 4, null: false
    t.integer  "percentage",                                                 null: false
    t.decimal  "commission",                        precision: 19, scale: 4, null: false
    t.string   "remarks",               limit: 100
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
  end

  create_table "teacher_commission_items", force: :cascade do |t|
    t.integer  "teacher_commission_id",                                      null: false
    t.string   "program",               limit: 50,                           null: false
    t.string   "day",                   limit: 10,                           null: false
    t.string   "time",                  limit: 10,                           null: false
    t.string   "student_nric_name",     limit: 100,                          null: false
    t.string   "lesson_date",           limit: 300,                          null: false
    t.decimal  "term_fee",                          precision: 19, scale: 4, null: false
    t.decimal  "lesson_fee",                        precision: 19, scale: 4, null: false
    t.integer  "no_of_lesson",                                               null: false
    t.decimal  "total_fee",                         precision: 19, scale: 4, null: false
    t.integer  "percentage",                                                 null: false
    t.decimal  "commission",                        precision: 19, scale: 4, null: false
    t.string   "remarks",               limit: 100
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
  end

  create_table "teacher_commission_others", force: :cascade do |t|
    t.integer  "teacher_commission_id",                                      null: false
    t.string   "item",                  limit: 100,                          null: false
    t.decimal  "amount",                            precision: 19, scale: 4, null: false
    t.string   "remarks",               limit: 100,                          null: false
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
  end

  create_table "teacher_commissions", force: :cascade do |t|
    t.integer  "studio_id",                                  null: false
    t.integer  "teacher_id",                                 null: false
    t.string   "teacher_nric_name",                          null: false
    t.date     "statement_date",                             null: false
    t.decimal  "net_pay",           precision: 19, scale: 4, null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "teacher_commissions", ["studio_id", "teacher_id", "statement_date"], name: "studio_id_and_teacher_id_and_statement_date", unique: true, using: :btree

  create_table "teachers", force: :cascade do |t|
    t.string   "gender",                                             limit: 1,                  null: false
    t.date     "date_of_birth",                                                                 null: false
    t.integer  "number_of_children"
    t.string   "full_time_occupation",                               limit: 50
    t.string   "highest_piano_practical_standard_passed",            limit: 50
    t.string   "highest_theory_standard_passed",                     limit: 50
    t.boolean  "has_college_abrsm",                                             default: false
    t.boolean  "has_college_trinity",                                           default: false
    t.boolean  "has_college_lcm",                                               default: false
    t.string   "college_others",                                     limit: 50
    t.string   "other_music_qualifications",                         limit: 50
    t.string   "before_myc_teaching_individual_lesson_started_year", limit: 4
    t.string   "before_myc_teaching_individual_lesson_total_year",   limit: 4
    t.string   "before_myc_teaching_group_lesson_started_year",      limit: 4
    t.string   "before_myc_teaching_group_lesson_total_year",        limit: 4
    t.date     "date_of_myc_teacher_agreement"
    t.date     "date_of_myc_studio_agreement"
    t.date     "date_of_myc_l1"
    t.date     "date_of_myc_l2ss2"
    t.date     "date_of_myc_l2sb2"
    t.date     "date_of_myc_l2mb2"
    t.date     "date_of_myc_l3sb3"
    t.date     "date_of_myc_l3mb3"
    t.date     "date_of_1st_teacher_visit"
    t.date     "date_of_2nd_teacher_visit"
    t.date     "date_of_3rd_teacher_visit"
    t.integer  "user_id",                                                                       null: false
    t.datetime "created_at",                                                                    null: false
    t.datetime "updated_at",                                                                    null: false
  end

  add_index "teachers", ["user_id"], name: "index_teachers_on_user_id", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "nric_name",              limit: 100,              null: false
    t.string   "phone_number",           limit: 20
    t.integer  "status",                 limit: 2,                null: false
    t.string   "email",                              default: "", null: false
    t.string   "encrypted_password",                 default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",                    default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",                  default: 0
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end

class CreateMycLevels < ActiveRecord::Migration
  def change
    create_table :myc_levels do |t|
      t.string :level_name, limit: 100
      t.string :age, null: false, limit: 10
      t.integer :total_lesson, null: false, limit: 3
      t.integer :lesson_per_term, null: false, limit: 3
      t.decimal :lesson_fee_per_term, null: false, precision: 19, scale: 4
      t.integer :lesson_duration_minute, null: false, limit: 3
      t.integer :level_order, null: false, limit: 3
      t.integer :status, null: false, limit: 2
      t.belongs_to :myc_program, index: true

      t.timestamps null: false
    end
  end
end

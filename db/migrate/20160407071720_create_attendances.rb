class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.belongs_to :lesson, index: true
      t.belongs_to :student, index: true
      t.integer :status, default: Attendance.statuses[:null], null: false, limit: 2
    end
  end
end

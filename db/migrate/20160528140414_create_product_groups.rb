class CreateProductGroups < ActiveRecord::Migration
  def change
    create_table :product_groups do |t|
      t.string :description, null: false, limit: 100
      t.integer :display_order, null: false, limit: 2
      t.integer :status, null: false, limit: 2

      t.timestamps null: false
    end
    add_index :product_groups, [:description, :status],  unique: true
  end
end

class CreateTeacherCommissionOthers < ActiveRecord::Migration
  def change
    create_table :teacher_commission_others do |t|
      t.integer :teacher_commission_id, null: false
      t.string :item, null: false, limit: 100
      t.decimal :amount, precision: 19, scale: 4, null: false
      t.string :remarks, null: false, limit: 100

      t.timestamps null: false
    end
  end
end

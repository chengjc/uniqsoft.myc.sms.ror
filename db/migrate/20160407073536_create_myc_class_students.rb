class CreateMycClassStudents < ActiveRecord::Migration
  def change
    create_table :myc_classes_students do |t|
      t.belongs_to :myc_class, index: true
      t.belongs_to :student, index: true
      t.integer :status, default: MycClassStudent.statuses[:in_class], null: false, limit: 2
      t.timestamps null: false
    end
  end
end

class CreateProductCodes < ActiveRecord::Migration
  def change
    create_table :product_codes do |t|
      t.belongs_to :myc_level, index: true
      t.string :code, null: false, limit: 100
      t.decimal :price, precision: 19, scale: 4, null: false
      t.string :remarks, limit: 200
      t.integer :status, null: false, limit: 2

      t.timestamps null: false
    end
    add_index :product_codes, [:code, :status],  unique: true

  end
end

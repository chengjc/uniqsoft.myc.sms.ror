class CreateStudios < ActiveRecord::Migration
  def change
    create_table (:studios) do |t|
    	t.string :studio_code, :limit=>10, :null=>false
    	t.string :studio_name, :limit=>100, :null=>false
    	t.string :address, :limit=>100
    	t.string :post_code, :limit=>10
    	t.string :company_name, :limit=>100
    	t.string :company_registration_no, :limit=>50
    	t.string :phone_no, :limit=>50
    	t.string :email, :limit=>100
    	t.string :facebook_address, :limit=>100
    	t.string :website_address, :limit=>200
    	t.integer :status, null: false, :limit => 2
    end
    add_index :studios,[:studio_code, :status],  unique: true
  end
end

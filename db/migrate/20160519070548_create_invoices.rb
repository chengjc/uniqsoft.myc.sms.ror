class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|

      t.belongs_to :studio, index: true
      t.belongs_to :student, index: true
      t.belongs_to :myc_level, index: true
      t.string :invoice_number, :limit => 20, :null => false
      t.date :invoice_on, :null => false
      t.date :due_on, :null => false
      t.string :teacher_name, :limit => 50
      t.string :day, :limit => 25
      t.string :time, :limit => 25
      t.decimal :total_price, :precision => 19, :scale => 4, :null => false
      t.decimal :bal_due, :precision => 19, :scale => 4, :null => false
      t.integer :status, :null => false
      t.timestamps null: false

    end
  end
end

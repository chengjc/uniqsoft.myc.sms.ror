class CreateTeacherCommissionItems < ActiveRecord::Migration
  def change
    create_table :teacher_commission_items do |t|
      t.integer :teacher_commission_id, null: false
      t.string :program, null: false, limit: 50
      t.string :day, null: false, limit: 10
      t.string :time, null: false, limit: 10
      t.string :student_nric_name, null: false, limit: 100
      t.string :lesson_date, null: false, limit: 300
      t.decimal :term_fee, precision: 19, scale: 4, null: false
      t.decimal :lesson_fee, precision: 19, scale: 4, null: false
      t.integer :no_of_lesson, null: false, limit: 3
      t.decimal :total_fee, precision: 19, scale: 4, null: false
      t.integer :percentage, null: false, limit: 3
      t.decimal :commission, precision: 19, scale: 4, null: false
      t.string :remarks, limit: 100

      t.timestamps null: false
    end
  end
end

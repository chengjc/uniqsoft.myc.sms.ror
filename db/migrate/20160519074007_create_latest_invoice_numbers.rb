class CreateLatestInvoiceNumbers < ActiveRecord::Migration
  def change
    create_table :latest_invoice_numbers do |t|

      t.belongs_to :studio, index: true
      t.string :latest_invoice_number , :limit => 100, :null => false
      t.timestamps null: false

    end
  end
end

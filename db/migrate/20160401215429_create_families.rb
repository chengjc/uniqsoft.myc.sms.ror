class CreateFamilies < ActiveRecord::Migration
  def change
    create_table :families do |t|
      t.belongs_to :studio, index: true
      t.string :address1, :limit => 100, :null => false
      t.string :address2, :limit => 100
      t.string :postal_code, :limit => 10
      t.string :city_or_state, :limit => 50, :null => false
      t.string :country, :limit => 50, :null => false
      t.string :primary_contact_name, :limit => 50, :null => false
      t.string :primary_contact_relationship, :limit => 50, :null => false
      t.string :primary_contact_phone_no, :limit => 50, :null => false
      t.string :primary_contact_email, :limit => 50, :null => false
      t.string :primary_contact_occupation, :limit => 100
      t.string :primary_contact_music_background, :limit => 50
      t.boolean :primary_contact_accompany_for_student, :default => false
      t.string :other_contact_name, :limit => 50
      t.string :other_contact_relationship, :limit => 50
      t.string :other_contact_phone_no, :limit => 50
      t.string :other_contact_email, :limit => 50
      t.string :other_contact_occupation, :limit => 100
      t.string :other_contact_music_background, :limit => 50
      t.boolean :other_contact_accompany_for_student, :default => false
      t.integer :status, null: false, :limit => 2
      t.timestamps null: false
    end
  end
end





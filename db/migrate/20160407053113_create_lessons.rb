class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.integer :lesson_no, null: false
      t.datetime :start_at, null: false
      t.string :reason_for_cancel
      t.integer :status, default: Lesson.statuses[:not_marked], null: false, limit: 2
      t.belongs_to :myc_class, index: true
      t.belongs_to :teacher, index: true

      t.timestamps null: false
    end
  end
end

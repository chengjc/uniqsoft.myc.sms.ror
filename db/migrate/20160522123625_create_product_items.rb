class CreateProductItems < ActiveRecord::Migration
  def change
    create_table :product_items do |t|
      t.string :description, null: false, limit: 100
      t.decimal :cost, precision: 19, scale: 4, null: false
      t.integer :quantity_on_hand, limit: 3, null: false
      t.integer :quantity_trigger_alert, limit: 3, null: false
      t.string :remarks, limit: 200
      t.integer :status, null: false, limit: 2

      t.timestamps null: false
    end

    add_index :product_items, [:description, :status],  unique: true
  end
end

class CreateHolidays < ActiveRecord::Migration
  def change
    create_table :holidays do |t|
      t.integer :studio_id, :null=>false
      t.date :holiday_date, :null=>false
      t.string :holiday_name, :limit=>100, :null=>false
      t.timestamps null: false
    end
    add_index :holidays,[:studio_id, :holiday_date],  unique: true
  end
end

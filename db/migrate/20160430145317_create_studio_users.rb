class CreateStudioUsers < ActiveRecord::Migration
  def change
    create_table :studio_users do |t|
      t.belongs_to :studio, null: false, index: true
      t.belongs_to :user, null: false, index: true
      t.integer :role_id, null: false, index: true, :limit => 2

      t.timestamps null: false
    end
  end
end

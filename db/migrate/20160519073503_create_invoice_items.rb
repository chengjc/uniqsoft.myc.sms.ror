class CreateInvoiceItems < ActiveRecord::Migration
  def change
    create_table :invoice_items do |t|

      t.belongs_to :invoice, index: true
      t.string :item_name, :limit => 50, :null => false
      t.integer :quantity, :null => false, :limit => 3
      t.decimal :price, :precision => 19, :scale => 4, :null => false

      t.timestamps null: false
    end
  end
end

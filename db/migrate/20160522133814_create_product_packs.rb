class CreateProductPacks < ActiveRecord::Migration
  def change
    create_table :product_packs do |t|
      t.string :description, null: false, limit: 100
      t.integer :status, null: false, limit: 2

      t.timestamps null: false
    end
    add_index :product_packs, [:description, :status],  unique: true
  end
end

class CreateStudentReports < ActiveRecord::Migration
  def change
    create_table :student_reports do |t|
      t.integer :studio_id, null: false
      t.integer :teacher_id, null: false
      t.date :report_date, null: false
      t.integer :student_count_sr1, null: false, limit: 3
      t.integer :student_count_sr2, null: false, limit: 3
      t.integer :student_count_sr3, null: false, limit: 3
      t.integer :student_count_ss1, null: false, limit: 3
      t.integer :student_count_ss2, null: false, limit: 3
      t.integer :student_count_sb1, null: false, limit: 3
      t.integer :student_count_sb2, null: false, limit: 3
      t.integer :student_count_sb3, null: false, limit: 3
      t.integer :student_count_mb1, null: false, limit: 3
      t.integer :student_count_mb2, null: false, limit: 3
      t.integer :student_count_mb3, null: false, limit: 3
      t.integer :total, null: false, limit: 5

      t.timestamps null: false
    end
    add_index :student_reports, [:studio_id, :teacher_id, :report_date],  unique: true, name: 'index_student_reports_studio_id_teacher_id_report_date'
  end
end

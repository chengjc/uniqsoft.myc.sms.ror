class CreateMycPrograms < ActiveRecord::Migration
  def change
    create_table :myc_programs do |t|
      t.string :program_code, null: false, limit: 100
      t.string :program_name, null: false, limit: 100
      t.integer :program_order, null: false, limit: 3
      t.integer :status, null: false, limit: 2

      t.timestamps null: false
    end

    add_index :myc_programs, [:program_name, :status],  unique: true
  end
end

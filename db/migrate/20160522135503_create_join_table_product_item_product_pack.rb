class CreateJoinTableProductItemProductPack < ActiveRecord::Migration
  def change
    create_join_table :product_items, :product_packs do |t|
      t.index [:product_item_id, :product_pack_id], name: 'product_item_id_and_product_pack_id'
    end
    add_index :product_items_packs, [:product_item_id, :product_pack_id], unique: true, name: 'unique_product_item_id_and_product_pack_id'
  end
end

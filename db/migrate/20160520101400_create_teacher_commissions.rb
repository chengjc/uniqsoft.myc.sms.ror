class CreateTeacherCommissions < ActiveRecord::Migration
  def change
    create_table :teacher_commissions do |t|
      t.integer :studio_id, null: false
      t.integer :teacher_id, null: false
      t.string :teacher_nric_name, null: false
      t.date :statement_date, null: false
      t.decimal :net_pay, precision: 19, scale: 4, null: false

      t.timestamps null: false
    end

    add_index :teacher_commissions, [:studio_id, :teacher_id, :statement_date],  unique: true, name: 'studio_id_and_teacher_id_and_statement_date'
  end
end

class CreateMycClasses < ActiveRecord::Migration
  def change
    create_table :myc_classes do |t|
      t.belongs_to :studio, index: true
      t.belongs_to :myc_level, index: true
      t.datetime :start_at, null: false
      t.integer :status, default: MycClass.statuses[:on_going], null: false, :limit => 2
      t.datetime :status_updated_at, null: false

      t.timestamps null: false
    end
  end
end

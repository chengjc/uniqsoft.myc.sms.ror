class CreateFeeItems < ActiveRecord::Migration
  def change
    create_table :fee_items do |t|
      t.string :fee_item_name, null: false, limit: 50
      t.decimal :price, precision: 19, scale: 4, null: false
      t.integer :fee_type, null: false, :limit => 2
      t.timestamps null: false
    end
  end
end

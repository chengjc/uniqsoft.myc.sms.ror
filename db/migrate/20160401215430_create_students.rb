class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|

      t.belongs_to :myc_level, index: true
      t.belongs_to :studio, index: true
      t.belongs_to :family, index: true
      t.string  :enrolment_piano, :limit => 10
      t.string  :enrolment_theory, :limit => 10
      t.string  :enrolment_other, :limit => 10

      t.string  :preferred_day_one, :limit => 10
      t.string  :preferred_day_two, :limit => 10
      t.string  :preferred_day_three, :limit => 10
      t.string  :preferred_time_one, :limit => 10
      t.string  :preferred_time_two, :limit => 10
      t.string  :preferred_time_three, :limit => 10

      t.string  :nric_name, :limit => 50, :null => false
      t.string  :first_name, :limit => 50, :null => false
      t.string  :sur_name, :limit => 50, :null => false
      t.string  :gender, :limit => 10, :null => false
      t.date    :date_of_birth, :null => false

      t.string  :piano_standard, :limit => 50
      t.string  :piano_college, :limit => 50
      t.string  :piano_school_attended, :limit => 50
      t.string  :theory_standard, :limit => 50
      t.string  :theory_college, :limit => 50
      t.string  :theory_school_attended, :limit => 50
      t.string  :violin_standard, :limit => 50
      t.string  :violin_college, :limit => 50
      t.string  :violin_school_attended, :limit => 50
      t.string  :other_standard, :limit => 50
      t.string  :other_college, :limit => 50
      t.string  :other_school_attended, :limit => 50

      t.boolean :instrument_owned_has_piano
      t.boolean :instrument_owned_has_organ
      t.boolean :instrument_owned_has_keyboard
      t.boolean :instrument_owned_has_violin
      t.string  :instrument_owned_other, :limit => 50

      t.string  :referred_by, :limit => 50, :null => false

      t.date :enquiry_date, :null => false
      t.date :registration_date

      t.integer :status

      t.timestamps null: false
    end
    add_index :students,[:nric_name, :date_of_birth],  unique: true
  end
end

class CreateTeachers < ActiveRecord::Migration
  def change
    create_table :teachers do |t|

        # t.string :nric_name, null: false, :limit => 100
    	t.string :gender, null: false, :limit => 1
    	t.date :date_of_birth, null: false
        # t.string :phone_number
    	t.integer :number_of_children
    	t.string :full_time_occupation, :limit => 50

    	t.string :highest_piano_practical_standard_passed, :limit => 50
    	t.string :highest_theory_standard_passed, :limit => 50
    	t.boolean :has_college_abrsm, :default => false
    	t.boolean :has_college_trinity, :default => false
    	t.boolean :has_college_lcm, :default => false

    	t.string :college_others, :limit => 50
    	t.string :other_music_qualifications, :limit => 50

    	t.string :before_myc_teaching_individual_lesson_started_year, :limit => 4
    	t.string :before_myc_teaching_individual_lesson_total_year, :limit => 4
    	t.string :before_myc_teaching_group_lesson_started_year, :limit => 4
    	t.string :before_myc_teaching_group_lesson_total_year, :limit => 4

    	t.date :date_of_myc_teacher_agreement
    	t.date :date_of_myc_studio_agreement
    	t.date :date_of_myc_l1
    	t.date :date_of_myc_l2ss2
    	t.date :date_of_myc_l2sb2
    	t.date :date_of_myc_l2mb2
    	t.date :date_of_myc_l3sb3
    	t.date :date_of_myc_l3mb3

    	t.date :date_of_1st_teacher_visit
    	t.date :date_of_2nd_teacher_visit
    	t.date :date_of_3rd_teacher_visit

			t.integer :user_id, null: false


      	t.timestamps null: false

    end

    add_index :teachers, :user_id, unique: true
  end
end

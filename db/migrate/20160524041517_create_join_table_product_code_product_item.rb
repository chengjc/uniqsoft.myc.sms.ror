class CreateJoinTableProductCodeProductItem < ActiveRecord::Migration
  def change
    create_join_table :product_codes, :product_items do |t|
      t.index [:product_code_id, :product_item_id], name: 'product_code_id_and_product_item_id'
    end
    add_index :product_codes_items, [:product_code_id, :product_item_id], unique: true, name: 'unique_product_code_id_and_product_item_id'
  end
end

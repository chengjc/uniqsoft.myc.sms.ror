class CreateJoinTableProductCodeProductPack < ActiveRecord::Migration
  def change
    create_join_table :product_codes, :product_packs do |t|
      t.index [:product_code_id, :product_pack_id], name: 'product_code_id_and_product_pack_id'
    end
    add_index :product_codes_packs, [:product_code_id, :product_pack_id], unique: true, name: 'unique_product_code_id_and_product_pack_id'
  end
end

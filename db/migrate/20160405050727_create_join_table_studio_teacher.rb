class CreateJoinTableStudioTeacher < ActiveRecord::Migration
  def change
    create_join_table :studios, :teachers do |t|
      t.index [:studio_id, :teacher_id]
    end
  end
end

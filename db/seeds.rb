# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if User.count == 0
  user_customer = User.create(
      nric_name: 'sannlymyc',
      phone_number: 123,
      email: 'sannlymyc@gmail.com',
      password: 'muditar@sannly',
      status: User.statuses[:active]
  )

  user_cheng = User.create(
      nric_name: 'cheng',
      phone_number: 123,
      email: 'chengjc.uns@gmail.com',
      password: 'muditar@chengjc',
      status: User.statuses[:active]
  )

  user_mk = User.create(
      nric_name: 'myat kaung',
      phone_number: 123,
      email: 'myatkaung.uns@gmail.com',
      password: 'muditar@myatkaung',
      status: User.statuses[:active]
  )

  user_tz = User.create(
      nric_name: 'thant zaw',
      phone_number: 123,
      email: 'thantzaw.uns@gmail.com',
      password: 'muditar@thantzaw',
      status: User.statuses[:active]
  )

  studio_hq = Studio.create(studio_code: Rails.configuration.hq_studio_name, studio_name: Rails.configuration.hq_studio_name, status: Studio.statuses[:active])

  Studio.create(
      studio_code: 'AMK',
      studio_name:  'MYC Ang Mo Kio',
      address:  'Block 608, Ang Mo Kio Avenue 5, #01-2787, Singapore',
      post_code: '560608',
      phone_no: '+65 6100 1930',
      email: 'angmokio@mycsingapore.com.sg',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'BT',
      studio_name:  'MYC Bukit Timah',
      address:  '557, Bukit Timah Road, #02-07 Crown Centre, Singapore',
      post_code: '269694',
      phone_no: '+65 6100 1803',
      email: 'bukittimah@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/mycbktimah',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'EC',
      studio_name:  'MYC East Coast',
      address:  '45 Amber Road, Singapore',
      post_code: '439886',
      phone_no: '+65 6100 2355',
      email: 'eastcoast@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/MycEastCoast',
      website_address: 'http://www.myctaly.com',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'HG',
      studio_name:  'MYC Hougang',
      address:  'Block 806, Hougang Central, #04-148, Singapore',
      post_code: '530806',
      phone_no: '+65 6100 4465',
      email: 'hougang@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/We.Love.Legato.mychougang',
      website_address: 'http://www.legato-musicschool.com',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'JE',
      studio_name:  'MYC Jurong East',
      address:  'Block 131, Jurong East Street 13, #04-245, Singapore',
      post_code: '600131',
      phone_no: '+65 6100 1316',
      email: 'jurongeast@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/mycjurongeast',
      website_address: 'http://www.icampus.com.sg',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'NT',
      studio_name:  'MYC Newton',
      address:  '1 Goldhill Plaza, #03-11 Podium Block, Singapore',
      post_code: '308899',
      phone_no: '+65 6100 6181',
      email: 'newton@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/mycnewton',
      website_address: '',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'ORC',
      studio_name:  'MYC Orchard',
      address:  '19 Tanglin Road, #05-22 Tanglin Shopping Centre, Singapore',
      post_code: '247909',
      phone_no: '+65 6100 2587',
      email: 'orchard@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/mycthomson',
      website_address: 'http://www.mycthomson.com',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'SB',
      studio_name:  'MYC Sembawang',
      address:  '2, Sembawang Crescent #04-02, Singapore',
      post_code: '757632',
      phone_no: '+65 6100 6316',
      email: 'sembawang@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/mycsembawang.sg',
      website_address: '',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'TS',
      studio_name:  'MYC Thomson',
      address:  'Midview City #07-97 Sin Ming Lane Singapore',
      post_code: '573960',
      phone_no: '+65 6100 2587',
      email: 'thomson@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/mycthomson',
      website_address: 'http://www.mycthomson.com',
      status: Studio.statuses[:active]
  )
  Studio.create(
      studio_code: 'TE',
      studio_name:  'MYC Tampines East',
      address:  'Block 201E Tampines Street 23 #03-100 Singapore',
      post_code: '527201',
      phone_no: '+65 6100 1129',
      email: 'tampineseast@mycsingapore.com.sg',
      facebook_address: 'https://www.facebook.com/myctampines',
      website_address: '',
      status: Studio.statuses[:active]
  )
  
  StudioUser.create(studio_id: studio_hq.id, user_id: user_customer.id, role_id: StudioUser.roles[:super_user])
  StudioUser.create(studio_id: studio_hq.id, user_id: user_cheng.id, role_id: StudioUser.roles[:super_user])
  StudioUser.create(studio_id: studio_hq.id, user_id: user_mk.id, role_id: StudioUser.roles[:super_user])
  StudioUser.create(studio_id: studio_hq.id, user_id: user_tz.id, role_id: StudioUser.roles[:super_user])

  myc_program_sr1 = MycProgram.create(program_code: 'SR', program_name: 'Sunrise', program_order: 1, status: MycProgram.statuses[:active])
  myc_program_ss1 = MycProgram.create(program_code: 'SS', program_name: 'Sunshine', program_order: 2, status: MycProgram.statuses[:active])
  myc_program_sb1 = MycProgram.create(program_code: 'SB', program_name: 'Sunbeams', program_order: 3, status: MycProgram.statuses[:active])
  myc_program_mb1 = MycProgram.create(program_code: 'MB', program_name: 'Moonbeams', program_order: 4, status: MycProgram.statuses[:active])
  myc_program_mmbc = MycProgram.create(program_code: 'MMBC', program_name: 'Music My Best Choice', program_order: 5, status: MycProgram.statuses[:active])
  myc_program_others = MycProgram.create(program_code: 'Others', program_name: 'Others', program_order: 6, status: MycProgram.statuses[:active])

  myc_level_sr1 = MycLevel.create(level_name: '1', age: '2,3', total_lesson: 10, lesson_per_term: 10, lesson_fee_per_term: 290, lesson_duration_minute: 45, level_order: 1, status: MycLevel.statuses[:active], myc_program_id: myc_program_sr1.id)
  myc_level_sr2 = MycLevel.create(level_name: '2', age: '2,3', total_lesson: 10, lesson_per_term: 10, lesson_fee_per_term: 290, lesson_duration_minute: 45, level_order: 2, status: MycLevel.statuses[:active], myc_program_id: myc_program_sr1.id)
  myc_level_sr3 = MycLevel.create(level_name: '3', age: '2,3', total_lesson: 10, lesson_per_term: 10, lesson_fee_per_term: 290, lesson_duration_minute: 45, level_order: 3, status: MycLevel.statuses[:active], myc_program_id: myc_program_sr1.id)
  myc_level_ss1 = MycLevel.create(level_name: '1', age: '3,4', total_lesson: 36, lesson_per_term: 9, lesson_fee_per_term: 330, lesson_duration_minute: 60, level_order: 4, status: MycLevel.statuses[:active], myc_program_id: myc_program_ss1.id)
  myc_level_ss2 = MycLevel.create(level_name: '2', age: '3,4', total_lesson: 36, lesson_per_term: 9, lesson_fee_per_term: 360, lesson_duration_minute: 60, level_order: 5, status: MycLevel.statuses[:active], myc_program_id: myc_program_ss1.id)
  myc_level_sb1 = MycLevel.create(level_name: '1', age: '5,6', total_lesson: 36, lesson_per_term: 9, lesson_fee_per_term: 330, lesson_duration_minute: 60, level_order: 6, status: MycLevel.statuses[:active], myc_program_id: myc_program_sb1.id)
  myc_level_sb2 = MycLevel.create(level_name: '2', age: '5,6', total_lesson: 36, lesson_per_term: 9, lesson_fee_per_term: 360, lesson_duration_minute: 60, level_order: 7, status: MycLevel.statuses[:active], myc_program_id: myc_program_sb1.id)
  myc_level_sb3 = MycLevel.create(level_name: '3', age: '5,6', total_lesson: 36, lesson_per_term: 9, lesson_fee_per_term: 390, lesson_duration_minute: 60, level_order: 8, status: MycLevel.statuses[:active], myc_program_id: myc_program_sb1.id)
  myc_level_mb1 = MycLevel.create(level_name: '1', age: '7,8,9', total_lesson: 36, lesson_per_term: 9, lesson_fee_per_term: 330, lesson_duration_minute: 60, level_order: 9, status: MycLevel.statuses[:active], myc_program_id: myc_program_mb1.id)
  myc_level_mb2 = MycLevel.create(level_name: '2', age: '7,8,9', total_lesson: 36, lesson_per_term: 9, lesson_fee_per_term: 360, lesson_duration_minute: 60, level_order: 10, status: MycLevel.statuses[:active], myc_program_id: myc_program_mb1.id)
  myc_level_mb3 = MycLevel.create(level_name: '3', age: '7,8,9', total_lesson: 36, lesson_per_term: 9, lesson_fee_per_term: 480, lesson_duration_minute: 60, level_order: 11, status: MycLevel.statuses[:active], myc_program_id: myc_program_mb1.id)
  myc_level_mmbc = MycLevel.create(level_name: nil, age: '-', total_lesson: 1, lesson_per_term: 1, lesson_fee_per_term: 0, lesson_duration_minute: 0, level_order: 12, status: MycLevel.statuses[:active], myc_program_id: myc_program_mmbc.id)
  myc_level_others = MycLevel.create(level_name: nil, age: '-', total_lesson: 1, lesson_per_term: 1, lesson_fee_per_term: 0, lesson_duration_minute: 0, level_order: 13, status: MycLevel.statuses[:active], myc_program_id: myc_program_others.id)

  studio_ids = Studio.all().pluck(:id)
  studio_ids.each do |studio_id|
    Holiday.create(studio_id: studio_id, holiday_date: '2017-01-01', holiday_name: 'New Year\'s Day')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-02-28', holiday_name: 'Chinese New Year')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-02-29', holiday_name: 'Chinese New Year')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-04-14', holiday_name: 'Good Friday')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-05-01', holiday_name: 'Labour Day')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-05-10', holiday_name: 'Vesak Day')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-06-25', holiday_name: 'Hari Raya Puasa')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-08-09', holiday_name: 'National Day')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-09-01', holiday_name: 'Hari Raya Haji')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-10-18', holiday_name: 'Deepavali')
    Holiday.create(studio_id: studio_id, holiday_date: '2017-12-25', holiday_name: 'Christmas Day')

    Holiday.create(studio_id: studio_id, holiday_date: '2016-01-01', holiday_name: 'New Year\'s Day')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-02-08', holiday_name: 'Chinese New Year')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-02-09', holiday_name: 'Chinese New Year')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-03-25', holiday_name: 'Good Friday')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-05-01', holiday_name: 'Labour Day')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-05-21', holiday_name: 'Vesak Day')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-07-06', holiday_name: 'Hari Raya Puasa')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-08-09', holiday_name: 'National Day')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-09-12', holiday_name: 'Hari Raya Haji')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-10-29', holiday_name: 'Deepavali')
    Holiday.create(studio_id: studio_id, holiday_date: '2016-12-25', holiday_name: 'Christmas Day')
  end

  FeeItem.create(fee_item_name:'Registration', price: 30.00 , fee_type: FeeItem.fee_types[:program])
  FeeItem.create(fee_item_name:'Deposit', price: 120.00, fee_type: FeeItem.fee_types[:program])
  FeeItem.create(fee_item_name:'Sibling', price: -25.00, fee_type: FeeItem.fee_types[:discount])
  FeeItem.create(fee_item_name:'Recommendation', price: 30.00, fee_type: FeeItem.fee_types[:discount])
  FeeItem.create(fee_item_name:'Trial Class', price: 0.00, fee_type: FeeItem.fee_types[:discount])

end

if ProductItem.count == 0
  # product_group_sr = ProductGroup.create(description: 'Sunrise', display_order: 1, status: ProductGroup.statuses[:active])
  # product_group_ss = ProductGroup.create(description: 'Sunshine', display_order: 2, status: ProductGroup.statuses[:active])
  # product_group_sb = ProductGroup.create(description: 'Sunbeams', display_order: 3, status: ProductGroup.statuses[:active])
  # product_group_mb = ProductGroup.create(description: 'Moonbeams', display_order: 4, status: ProductGroup.statuses[:active])
  # product_group_others = ProductGroup.create(description: 'Others', display_order: 5, status: ProductGroup.statuses[:active])

  # SR
  item_sr1_student_manual = ProductItem.create(description: 'Sunrise 1 Student Manual', cost: '35', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_sr1_cd = ProductItem.create(description: 'Sunrise 1 CD', cost: '20',              quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_claves = ProductItem.create(description: 'Claves', cost: '7.5',                   quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_sr_bag = ProductItem.create(description: 'Sunrise Bag', cost: '3',                quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_sr1_product_development_fee = ProductItem.create(description: 'SR1 Product Development Fee', cost: '45', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  item_sr2_student_manual = ProductItem.create(description: 'Sunrise 2 Student Manual', cost: '35', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_sr2_cd = ProductItem.create(description: 'Sunrise 2 CD', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_tamporine =ProductItem.create(description: 'Tambourine', cost: '10', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_sr2_product_development_fee = ProductItem.create(description: 'SR2 Product Development Fee', cost: '45', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  item_sr3_student_manual = ProductItem.create(description: 'Sunrise 3 Student Manual', cost: '35', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_sr3_cd = ProductItem.create(description: 'Sunrise 3 CD', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_shaker = ProductItem.create(description: 'Shaker(a pair)', cost: '10', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_sr3_product_development_fee = ProductItem.create(description: 'SR3 Product Development Fee', cost: '45', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_sr1_std_pdf_sr_bag = ProductPack.create(description: 'Sunrise 1 Standard PDF & Sunrise Bag', status: ProductPack.statuses[:active])
  pack_sr1_std_pdf_sr_bag.product_items << ProductItem.find(item_sr1_student_manual.id)
  pack_sr1_std_pdf_sr_bag.product_items << ProductItem.find(item_sr1_cd.id)
  pack_sr1_std_pdf_sr_bag.product_items << ProductItem.find(item_claves.id)
  pack_sr1_std_pdf_sr_bag.product_items << ProductItem.find(item_sr_bag.id)
  pack_sr1_std_pdf_sr_bag.product_items << ProductItem.find(item_sr1_product_development_fee.id)

  pack_sr1_fam_pdf_sr_bag = ProductPack.create(description: 'Sunrise 1 Family PDF & Sunrise Bag', status: ProductPack.statuses[:active])
  pack_sr1_fam_pdf_sr_bag.product_items << ProductItem.find(item_sr1_student_manual.id)
  pack_sr1_fam_pdf_sr_bag.product_items << ProductItem.find(item_claves.id)
  pack_sr1_fam_pdf_sr_bag.product_items << ProductItem.find(item_sr_bag.id)
  pack_sr1_fam_pdf_sr_bag.product_items << ProductItem.find(item_sr1_product_development_fee.id)

  pack_sr2_std_pdf = ProductPack.create(description: 'Sunrise 2 Standard PDF', status: ProductPack.statuses[:active])
  pack_sr2_std_pdf.product_items << ProductItem.find(item_sr2_student_manual.id)
  pack_sr2_std_pdf.product_items << ProductItem.find(item_sr2_cd.id)
  pack_sr2_std_pdf.product_items << ProductItem.find(item_tamporine.id)
  pack_sr2_std_pdf.product_items << ProductItem.find(item_sr2_product_development_fee.id)

  pack_sr2_fam_pdf = ProductPack.create(description: 'Sunrise 2 Family PDF', status: ProductPack.statuses[:active])
  pack_sr2_fam_pdf.product_items << ProductItem.find(item_sr2_student_manual.id)
  pack_sr2_fam_pdf.product_items << ProductItem.find(item_tamporine.id)
  pack_sr2_fam_pdf.product_items << ProductItem.find(item_sr2_product_development_fee.id)

  pack_sr3_std_pdf = ProductPack.create(description: 'Sunrise 3 Standard PDF', status: ProductPack.statuses[:active])
  pack_sr3_std_pdf.product_items << ProductItem.find(item_sr3_student_manual.id)
  pack_sr3_std_pdf.product_items << ProductItem.find(item_sr3_cd.id)
  pack_sr3_std_pdf.product_items << ProductItem.find(item_shaker.id)
  pack_sr3_std_pdf.product_items << ProductItem.find(item_sr3_product_development_fee.id)

  pack_sr3_fam_pdf = ProductPack.create(description: 'Sunrise 3 Family PDF', status: ProductPack.statuses[:active])
  pack_sr3_fam_pdf.product_items << ProductItem.find(item_sr3_student_manual.id)
  pack_sr3_fam_pdf.product_items << ProductItem.find(item_shaker.id)
  pack_sr3_fam_pdf.product_items << ProductItem.find(item_sr3_product_development_fee.id)

  code_sr1_std_pdf_sr_bag = ProductCode.create(code: 'SR 1 Std PDF & SR Bag', price: '110', myc_level_id: myc_level_sr1.id, status: ProductCode.statuses[:active])
  code_sr1_std_pdf_sr_bag.product_packs << ProductPack.find(pack_sr1_std_pdf_sr_bag.id)

  code_sr1_fam_pdf_sr_bag = ProductCode.create(code: 'SR 1 Fam PDF & SR Bag', price: '90', myc_level_id: myc_level_sr1.id, remarks: 'For student whose elder sibling has purchased SR1 Std before.', status: ProductCode.statuses[:active])
  code_sr1_fam_pdf_sr_bag.product_packs << ProductPack.find(pack_sr1_fam_pdf_sr_bag.id)

  code_sr2_std_pdf = ProductCode.create(code: 'SR 2 Std PDF', price: '110', myc_level_id: myc_level_sr2.id, status: ProductCode.statuses[:active])
  code_sr2_std_pdf.product_packs << ProductPack.find(pack_sr2_std_pdf.id)

  code_sr2_fam_pdf = ProductCode.create(code: 'SR 2 Fam PDF', price: '90', myc_level_id: myc_level_sr2.id, remarks: 'For student whose elder sibling has purchased SR2 Std before.', status: ProductCode.statuses[:active])
  code_sr2_fam_pdf.product_packs << ProductPack.find(pack_sr2_fam_pdf.id)

  code_sr3_std_pdf = ProductCode.create(code: 'SR 3 Std PDF', price: '110', myc_level_id: myc_level_sr3.id, status: ProductCode.statuses[:active])
  code_sr3_std_pdf.product_packs << ProductPack.find(pack_sr3_std_pdf.id)

  code_sr3_fam_pdf = ProductCode.create(code: 'SR 3 Fam PDF', price: '90', myc_level_id: myc_level_sr3.id, remarks: 'For student whose elder sibling has purchased SR3 Std before.', status: ProductCode.statuses[:active])
  code_sr3_fam_pdf.product_packs << ProductPack.find(pack_sr3_fam_pdf.id)

  item_SR123PDF = ProductItem.create(description: 'SR 1/2/3 Product Development Fee', cost: '45', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  code_SR123PDF = ProductCode.create(code: 'SR 1/2/3 PDF', price: '45', myc_level_id: myc_level_others.id, status: ProductCode.statuses[:active])
  code_SR123PDF.product_items << ProductItem.find(item_SR123PDF.id)

  code_sr2_sr3_bag = ProductCode.create(code: 'SR 2/3 Bag', price: '3', myc_level_id: myc_level_others.id, status: ProductCode.statuses[:active])
  code_sr2_sr3_bag.product_items << ProductItem.find(item_sr_bag.id)

  # registration pack
  item_TinSheet = ProductItem.create(description: 'Tin Sheet', cost: '23', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MagnetStrip = ProductItem.create(description: 'Magnet Strip', cost: '3', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MagnetsSet = ProductItem.create(description: 'Magnets (Set of 12)', cost: '4', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Castanet = ProductItem.create(description: 'Castanet', cost: '3', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MYCBag = ProductItem.create(description: 'MYC Bag', cost: '12', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_registration = ProductPack.create(description: 'Registration Pack', status: ProductPack.statuses[:active])
  pack_registration.product_items << ProductItem.find(item_TinSheet.id)
  pack_registration.product_items << ProductItem.find(item_MagnetStrip.id)
  pack_registration.product_items << ProductItem.find(item_MagnetsSet.id)
  pack_registration.product_items << ProductItem.find(item_Castanet.id)
  pack_registration.product_items << ProductItem.find(item_MYCBag.id)

  # SS1
  item_Sunshine1AStudentManual = ProductItem.create(description: 'Sunshine 1A Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MYCL1ActivitiesBook = ProductItem.create(description: 'MYC L1 Activities Book', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_crittersmeetthestaffbook = ProductItem.create(description: 'Critters Meet The Staff Book', cost: '10', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Sunshine1CDSet1Sunshine1VocalCD = ProductItem.create(description: 'Sunshine 1 CD Set 1 Sunshine 1 Vocal CD', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Sunshine1CDSet2CrittersMeetTheStaffCD = ProductItem.create(description: 'Sunshine 1 CD Set 2 Critters Meet The Staff CD', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_CrittersSticker2pcs = ProductItem.create(description: 'Critters Sticker (2 pcs of A5 size)', cost: '3', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SS1ProductDevelopmentFee = ProductItem.create(description: 'SS1 Product Development Fee', cost: '150', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Sunbeams1AStudentManual = ProductItem.create(description: 'Sunbeams 1A Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SS1BStudentManual = ProductItem.create(description: 'SS1B Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SS1CStudentManual = ProductItem.create(description: 'SS1C Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_ss1a_std_pdf = ProductPack.create(description: 'Sunshine 1A Standard PDF', status: ProductPack.statuses[:active])
  pack_ss1a_std_pdf.product_items << ProductItem.find(item_Sunshine1AStudentManual.id)
  pack_ss1a_std_pdf.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_ss1a_std_pdf.product_items << ProductItem.find(item_crittersmeetthestaffbook.id)
  pack_ss1a_std_pdf.product_items << ProductItem.find(item_Sunshine1CDSet1Sunshine1VocalCD.id)
  pack_ss1a_std_pdf.product_items << ProductItem.find(item_Sunshine1CDSet2CrittersMeetTheStaffCD.id)
  pack_ss1a_std_pdf.product_items << ProductItem.find(item_CrittersSticker2pcs.id)
  pack_ss1a_std_pdf.product_items << ProductItem.find(item_SS1ProductDevelopmentFee.id)

  code_ss1a_std_pdf_reg = ProductCode.create(code: 'SS1A Std PDF & Reg', price: '290', myc_level_id: myc_level_ss1.id, status: ProductCode.statuses[:active])
  code_ss1a_std_pdf_reg.product_packs << ProductPack.find(pack_ss1a_std_pdf.id)
  code_ss1a_std_pdf_reg.product_packs << ProductPack.find(pack_registration.id)

  pack_ss1a_fam_pdf = ProductPack.create(description: 'Sunshine 1A Family PDF', status: ProductPack.statuses[:active])
  pack_ss1a_fam_pdf.product_items << ProductItem.find(item_Sunshine1AStudentManual.id)
  pack_ss1a_fam_pdf.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_ss1a_fam_pdf.product_items << ProductItem.find(item_CrittersSticker2pcs.id)
  pack_ss1a_fam_pdf.product_items << ProductItem.find(item_SS1ProductDevelopmentFee.id)

  code_ss1a_fam_pdf_reg = ProductCode.create(code: 'SS1A Fam PDF & Reg', price: '255', myc_level_id: myc_level_ss1.id, remarks: 'For student whose elder sibling has purchased SS1A Std before.', status: ProductCode.statuses[:active])
  code_ss1a_fam_pdf_reg.product_packs << ProductPack.find(pack_ss1a_fam_pdf.id)
  code_ss1a_fam_pdf_reg.product_packs << ProductPack.find(pack_registration.id)

  pack_Sunshine1AStandardPackSunbeams1A = ProductPack.create(description: 'Sunshine 1A Standard Pack & Sunbeams 1A', status: ProductPack.statuses[:active])
  pack_Sunshine1AStandardPackSunbeams1A.product_items << ProductItem.find(item_Sunshine1AStudentManual.id)
  pack_Sunshine1AStandardPackSunbeams1A.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Sunshine1AStandardPackSunbeams1A.product_items << ProductItem.find(item_crittersmeetthestaffbook.id)
  pack_Sunshine1AStandardPackSunbeams1A.product_items << ProductItem.find(item_Sunshine1CDSet1Sunshine1VocalCD.id)
  pack_Sunshine1AStandardPackSunbeams1A.product_items << ProductItem.find(item_Sunshine1CDSet2CrittersMeetTheStaffCD.id)
  pack_Sunshine1AStandardPackSunbeams1A.product_items << ProductItem.find(item_CrittersSticker2pcs.id)
  pack_Sunshine1AStandardPackSunbeams1A.product_items << ProductItem.find(item_SS1ProductDevelopmentFee.id)
  pack_Sunshine1AStandardPackSunbeams1A.product_items << ProductItem.find(item_Sunbeams1AStudentManual.id)

  code_SS1CStdPDFRegSB1A = ProductCode.create(code: 'SS1C Std PDF & Reg & SB 1A', price: '340', myc_level_id: myc_level_ss1.id, remarks: 'For new student who is minimum 5 years old.', status: ProductCode.statuses[:active])
  code_SS1CStdPDFRegSB1A.product_packs << ProductPack.find(pack_Sunshine1AStandardPackSunbeams1A.id)
  code_SS1CStdPDFRegSB1A.product_packs << ProductPack.find(pack_registration.id)

  pack_Sunshine1AfamilyPackSunbeams1A = ProductPack.create(description: 'Sunshine 1A Family Pack & Sunbeams 1A', status: ProductPack.statuses[:active])
  pack_Sunshine1AfamilyPackSunbeams1A.product_items << ProductItem.find(item_Sunshine1AStudentManual.id)
  pack_Sunshine1AfamilyPackSunbeams1A.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Sunshine1AfamilyPackSunbeams1A.product_items << ProductItem.find(item_CrittersSticker2pcs.id)
  pack_Sunshine1AfamilyPackSunbeams1A.product_items << ProductItem.find(item_SS1ProductDevelopmentFee.id)
  pack_Sunshine1AfamilyPackSunbeams1A.product_items << ProductItem.find(item_Sunbeams1AStudentManual.id)

  code_SS1CFamPDFRegSB1A = ProductCode.create(code: 'SS1C Fam PDF & Reg & SB 1A', price: '305', myc_level_id: myc_level_ss1.id, remarks: 'For student whose elder sibling has purchased SS1A Std before.', status: ProductCode.statuses[:active])
  code_SS1CFamPDFRegSB1A.product_packs << ProductPack.find(pack_Sunshine1AfamilyPackSunbeams1A.id)
  code_SS1CFamPDFRegSB1A.product_packs << ProductPack.find(pack_registration.id)

  code_SS1B = ProductCode.create(code: 'SS1B', price: '50', myc_level_id: myc_level_ss1.id, remarks: 'SS1B for lesson 11-21.', status: ProductCode.statuses[:active])
  code_SS1B.product_items << ProductItem.find(item_SS1BStudentManual.id)

  code_SS1C = ProductCode.create(code: 'SS1C', price: '50', myc_level_id: myc_level_ss1.id, remarks: 'SS1B for lesson 22-36.', status: ProductCode.statuses[:active])
  code_SS1C.product_items << ProductItem.find(item_SS1CStudentManual.id)

  code_SS1PDF = ProductCode.create(code: 'SS1 PDF', price: '150', myc_level_id: myc_level_ss1.id, status: ProductCode.statuses[:active])
  code_SS1PDF.product_items << ProductItem.find(item_SS1ProductDevelopmentFee.id)

  # SS2
  item_Sunshine2StudentManual = ProductItem.create(description: 'Sunshine 2 Student Manual', cost: '105', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MYCL2ActivitiesBook = ProductItem.create(description: 'MYC L2 Activities Book', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Sunshine1CDSet1KeyboardCD12 = ProductItem.create(description: 'Sunshine 2 CD Set 1 Keyboard CD 1/2', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Sunshine1CDSet2SongsEnsembleCD22 = ProductItem.create(description: 'Sunshine 2 CD Set 2 Songs, Ensemble, Warmups CD 2/2', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_TechniqueToolboxBasic = ProductItem.create(description: 'Technique Toolbox Basic', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SS2ProductDevelopmentFee = ProductItem.create(description: 'SS2 Product Development Fee', cost: '150', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_Sunshine2StandardPDFPack = ProductPack.create(description: 'Sunshine 2 Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_Sunshine2StandardPDFPack.product_items << ProductItem.find(item_Sunshine2StudentManual.id)
  pack_Sunshine2StandardPDFPack.product_items << ProductItem.find(item_MYCL2ActivitiesBook.id)
  pack_Sunshine2StandardPDFPack.product_items << ProductItem.find(item_Sunshine1CDSet1KeyboardCD12.id)
  pack_Sunshine2StandardPDFPack.product_items << ProductItem.find(item_Sunshine1CDSet2SongsEnsembleCD22.id)
  pack_Sunshine2StandardPDFPack.product_items << ProductItem.find(item_TechniqueToolboxBasic.id)
  pack_Sunshine2StandardPDFPack.product_items << ProductItem.find(item_SS2ProductDevelopmentFee.id)

  code_SS2StdPDF = ProductCode.create(code: 'SS2 Std PDF', price: '315', myc_level_id: myc_level_ss2.id, status: ProductCode.statuses[:active])
  code_SS2StdPDF.product_packs << ProductPack.find(pack_Sunshine2StandardPDFPack.id)

  pack_Sunshine2FamilyPDFPack = ProductPack.create(description: 'Sunshine 2 Family PDF Pack', status: ProductPack.statuses[:active])
  pack_Sunshine2FamilyPDFPack.product_items << ProductItem.find(item_Sunshine2StudentManual.id)
  pack_Sunshine2FamilyPDFPack.product_items << ProductItem.find(item_MYCL2ActivitiesBook.id)
  pack_Sunshine2FamilyPDFPack.product_items << ProductItem.find(item_TechniqueToolboxBasic.id)
  pack_Sunshine2FamilyPDFPack.product_items << ProductItem.find(item_SS2ProductDevelopmentFee.id)

  code_SS2FamPDF = ProductCode.create(code: 'SS2 Fam PDF', price: '285', myc_level_id: myc_level_ss2.id, remarks: 'For student whose elder sibling has purchased SS2 Std before.', status: ProductCode.statuses[:active])
  code_SS2FamPDF.product_packs << ProductPack.find(pack_Sunshine2FamilyPDFPack.id)

  code_SS2PDF = ProductCode.create(code: 'SS2 PDF', price: '285', myc_level_id: myc_level_ss2.id, status: ProductCode.statuses[:active])
  code_SS2PDF.product_items << ProductItem.find(item_SS2ProductDevelopmentFee.id)

  # SB1
  # item_Sunbeams1AStudentManual = ProductItem.create(description: 'Sunbeams 1A Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  # MYC L1 Activities Book
  item_Sunbeams1CDSetKeyboardCD13 = ProductItem.create(description: 'Sunbeams 1 CD Set Keyboard CD 1/3', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Sunbeams1CDSetCD23 = ProductItem.create(description: 'Sunbeams 1 CD Set Ensembles & Keyboard CD 2/3', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Sunbeams1CDSetCD33 = ProductItem.create(description: 'Sunbeams 1 CD Set Singing, Singing Supplementary & Warmups CD 3/3', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  #item_TechniqueToolboxBasic = ProductItem.create(description: 'Technique Toolbox Basic', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SB1ProductDevelopmentFee = ProductItem.create(description: 'SB1 Product Development Fee', cost: '150', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Moonbeams1AStudentManual = ProductItem.create(description: 'Moonbeams 1A Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SB1BStudentManual = ProductItem.create(description: 'SB1B Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SB1CStudentManual = ProductItem.create(description: 'SB1C Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_Sunbeams1AStandardPDFPack = ProductPack.create(description: 'Sunbeams 1A Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_Sunbeams1AStandardPDFPack.product_items << ProductItem.find(item_Sunbeams1AStudentManual.id)
  pack_Sunbeams1AStandardPDFPack.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Sunbeams1AStandardPDFPack.product_items << ProductItem.find(item_Sunbeams1CDSetKeyboardCD13.id)
  pack_Sunbeams1AStandardPDFPack.product_items << ProductItem.find(item_Sunbeams1CDSetCD23.id)
  pack_Sunbeams1AStandardPDFPack.product_items << ProductItem.find(item_Sunbeams1CDSetCD33.id)
  pack_Sunbeams1AStandardPDFPack.product_items << ProductItem.find(item_TechniqueToolboxBasic.id)
  pack_Sunbeams1AStandardPDFPack.product_items << ProductItem.find(item_SB1ProductDevelopmentFee.id)

  code_SB1AStdPDFReg = ProductCode.create(code: 'SB1A Std PDF & Reg', price: '290', myc_level_id: myc_level_sb1.id, status: ProductCode.statuses[:active])
  code_SB1AStdPDFReg.product_packs << ProductPack.find(pack_Sunbeams1AStandardPDFPack.id)
  code_SB1AStdPDFReg.product_packs << ProductPack.find(pack_registration.id)

  pack_Sunbeams1AFamilyPDFPack = ProductPack.create(description: 'Sunbeams 1A Family PDF Pack', status: ProductPack.statuses[:active])
  pack_Sunbeams1AFamilyPDFPack.product_items << ProductItem.find(item_Sunbeams1AStudentManual.id)
  pack_Sunbeams1AFamilyPDFPack.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Sunbeams1AFamilyPDFPack.product_items << ProductItem.find(item_TechniqueToolboxBasic.id)
  pack_Sunbeams1AFamilyPDFPack.product_items << ProductItem.find(item_SB1ProductDevelopmentFee.id)

  code_SB1AFamPDFReg = ProductCode.create(code: 'SB1A Fam PDF & Reg', price: '290', myc_level_id: myc_level_sb1.id, remarks: 'For student whose elder sibling has purchased SB1A Std before.', status: ProductCode.statuses[:active])
  code_SB1AFamPDFReg.product_packs << ProductPack.find(pack_Sunbeams1AFamilyPDFPack.id)
  code_SB1AFamPDFReg.product_packs << ProductPack.find(pack_registration.id)

  code_SB1AStdPDF = ProductCode.create(code: 'SB1A Std PDF', price: '270', myc_level_id: myc_level_sb1.id, remarks: 'For current student progresses from SS1 to SB1A without registration pack.', status: ProductCode.statuses[:active])
  code_SB1AStdPDF.product_packs << ProductPack.find(pack_Sunbeams1AStandardPDFPack.id)

  code_SB1AFamPDF = ProductCode.create(code: 'SB1A Fam PDF', price: '230', myc_level_id: myc_level_sb1.id, remarks: 'For current student progresses from SS1 to SB1A without registration pack and whose elder sibling has purhased SB1A std before.', status: ProductCode.statuses[:active])
  code_SB1AFamPDF.product_packs << ProductPack.find(pack_Sunbeams1AFamilyPDFPack.id)

  pack_Sunbeams1AStandardPDFPackMoonbeams1A = ProductPack.create(description: 'Sunbeams 1A Standard PDF Pack & Moonbeams 1A', status: ProductPack.statuses[:active])
  pack_Sunbeams1AStandardPDFPackMoonbeams1A.product_items << ProductItem.find(item_Sunbeams1AStudentManual.id)
  pack_Sunbeams1AStandardPDFPackMoonbeams1A.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Sunbeams1AStandardPDFPackMoonbeams1A.product_items << ProductItem.find(item_Sunbeams1CDSetKeyboardCD13.id)
  pack_Sunbeams1AStandardPDFPackMoonbeams1A.product_items << ProductItem.find(item_Sunbeams1CDSetCD23.id)
  pack_Sunbeams1AStandardPDFPackMoonbeams1A.product_items << ProductItem.find(item_Sunbeams1CDSetCD33.id)
  pack_Sunbeams1AStandardPDFPackMoonbeams1A.product_items << ProductItem.find(item_TechniqueToolboxBasic.id)
  pack_Sunbeams1AStandardPDFPackMoonbeams1A.product_items << ProductItem.find(item_SB1ProductDevelopmentFee.id)
  pack_Sunbeams1AStandardPDFPackMoonbeams1A.product_items << ProductItem.find(item_Moonbeams1AStudentManual.id)

  code_SB1AStdPDFRegMB1A = ProductCode.create(code: 'SB1C Std PDF & Reg & MB1A', price: '340', myc_level_id: myc_level_sb1.id, remarks: 'For new student who is minimum 7 years old.', status: ProductCode.statuses[:active])
  code_SB1AStdPDFRegMB1A.product_packs << ProductPack.find(pack_Sunbeams1AStandardPDFPackMoonbeams1A.id)
  code_SB1AStdPDFRegMB1A.product_packs << ProductPack.find(pack_registration.id)

  pack_Sunbeams1AFamilyPDFPackMoonbeams1A = ProductPack.create(description: 'Sunbeams 1A Family PDF Pack & Moonbeams 1A', status: ProductPack.statuses[:active])
  pack_Sunbeams1AFamilyPDFPackMoonbeams1A.product_items << ProductItem.find(item_Sunbeams1AStudentManual.id)
  pack_Sunbeams1AFamilyPDFPackMoonbeams1A.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Sunbeams1AFamilyPDFPackMoonbeams1A.product_items << ProductItem.find(item_TechniqueToolboxBasic.id)
  pack_Sunbeams1AFamilyPDFPackMoonbeams1A.product_items << ProductItem.find(item_SB1ProductDevelopmentFee.id)
  pack_Sunbeams1AFamilyPDFPackMoonbeams1A.product_items << ProductItem.find(item_Moonbeams1AStudentManual.id)

  code_SB1AFamPDFRegMB1A = ProductCode.create(code: 'SB1C Fam PDF & Reg & MB1A', price: '305', myc_level_id: myc_level_sb1.id, remarks: 'For new student who is 7 years old and whose elder sibling has purchased SB1A Std before.', status: ProductCode.statuses[:active])
  code_SB1AFamPDFRegMB1A.product_packs << ProductPack.find(pack_Sunbeams1AFamilyPDFPackMoonbeams1A.id)
  code_SB1AFamPDFRegMB1A.product_packs << ProductPack.find(pack_registration.id)

  code_SB1B = ProductCode.create(code: 'SB1B', price: '270', myc_level_id: myc_level_sb1.id, remarks: 'SB1B for lesson 11-21.', status: ProductCode.statuses[:active])
  code_SB1B.product_items << ProductItem.find(item_SB1BStudentManual.id)

  code_SB1C = ProductCode.create(code: 'SB1C', price: '270', myc_level_id: myc_level_sb1.id, remarks: 'SB1B for lesson 22-36.', status: ProductCode.statuses[:active])
  code_SB1C.product_items << ProductItem.find(item_SB1CStudentManual.id)

  code_SB1PDF = ProductCode.create(code: 'SB1 PDF', price: '270', myc_level_id: myc_level_sb1.id, status: ProductCode.statuses[:active])
  code_SB1PDF.product_items << ProductItem.find(item_SB1ProductDevelopmentFee.id)

  # SB2
  item_Sunbeams2StudentManual = ProductItem.create(description: 'Sunbeams 2 Student Manual', cost: '105', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  # MYC L2 Activities Book
  item_Sunbeams1CDVocalCD = ProductItem.create(description: 'Sunbeams 2 CD (1 CD) Vocal CD', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_TechniqueToolboxConstructor = ProductItem.create(description: 'Technique Toolbox Constructors', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SB2ProductDevelopmentFee = ProductItem.create(description: 'SB2 Product Development Fee', cost: '150', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_Sunbeams2StandardPDFPack = ProductPack.create(description: 'Sunbeams 2 Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_Sunbeams2StandardPDFPack.product_items << ProductItem.find(item_Sunbeams2StudentManual.id)
  pack_Sunbeams2StandardPDFPack.product_items << ProductItem.find(item_MYCL2ActivitiesBook.id)
  pack_Sunbeams2StandardPDFPack.product_items << ProductItem.find(item_Sunbeams1CDVocalCD.id)
  pack_Sunbeams2StandardPDFPack.product_items << ProductItem.find(item_TechniqueToolboxConstructor.id)
  pack_Sunbeams2StandardPDFPack.product_items << ProductItem.find(item_SB2ProductDevelopmentFee.id)

  code_SB2StdPDF = ProductCode.create(code: 'SB2 Std PDF', price: '315', myc_level_id: myc_level_sb2.id, status: ProductCode.statuses[:active])
  code_SB2StdPDF.product_packs << ProductPack.find(pack_Sunbeams2StandardPDFPack.id)

  pack_Sunbeams2FamilyPDFPack = ProductPack.create(description: 'Sunbeams 2 Family PDF Pack', status: ProductPack.statuses[:active])
  pack_Sunbeams2FamilyPDFPack.product_items << ProductItem.find(item_Sunbeams2StudentManual.id)
  pack_Sunbeams2FamilyPDFPack.product_items << ProductItem.find(item_MYCL2ActivitiesBook.id)
  pack_Sunbeams2FamilyPDFPack.product_items << ProductItem.find(item_TechniqueToolboxConstructor.id)
  pack_Sunbeams2FamilyPDFPack.product_items << ProductItem.find(item_SB2ProductDevelopmentFee.id)

  code_SB2FamPDF = ProductCode.create(code: 'SB2 Fam PDF', price: '285', myc_level_id: myc_level_sb2.id, remarks: 'For existing student whose elder sibling has purchased SB2 Std before.', status: ProductCode.statuses[:active])
  code_SB2FamPDF.product_packs << ProductPack.find(pack_Sunbeams2FamilyPDFPack.id)

  code_SB2PDF = ProductCode.create(code: 'SB2 PDF', price: '150', myc_level_id: myc_level_sb2.id, status: ProductCode.statuses[:active])
  code_SB2PDF.product_items << ProductItem.find(item_SB2ProductDevelopmentFee.id)

  # SB3
  item_Sunbeams3StudentManual = ProductItem.create(description: 'Sunbeams 3 Student Manual', cost: '105', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  # MYC L2 Activities Book
  item_BrightIdea1 = ProductItem.create(description: 'Bright Idea 1', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_RCMPrepBRepertoire = ProductItem.create(description: 'RCM Prep B Repertoire', cost: '22', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Sunbeams3CDVocalCD = ProductItem.create(description: 'Sunbeams 3 CD (1 CD) Vocal CD', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_TechniqueToolboxJourneyman = ProductItem.create(description: 'Technique Toolbox Journeyman', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_SB3ProductDevelopmentFee = ProductItem.create(description: 'SB3 Product Development Fee', cost: '150', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_Sunbeams3StandardPDFPack = ProductPack.create(description: 'Sunbeams 3 Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_Sunbeams3StandardPDFPack.product_items << ProductItem.find(item_Sunbeams3StudentManual.id)
  pack_Sunbeams3StandardPDFPack.product_items << ProductItem.find(item_MYCL2ActivitiesBook.id)
  pack_Sunbeams3StandardPDFPack.product_items << ProductItem.find(item_BrightIdea1.id)
  pack_Sunbeams3StandardPDFPack.product_items << ProductItem.find(item_RCMPrepBRepertoire.id)
  pack_Sunbeams3StandardPDFPack.product_items << ProductItem.find(item_Sunbeams3CDVocalCD.id)
  pack_Sunbeams3StandardPDFPack.product_items << ProductItem.find(item_TechniqueToolboxJourneyman.id)
  pack_Sunbeams3StandardPDFPack.product_items << ProductItem.find(item_SB3ProductDevelopmentFee.id)

  code_SB3StdPDF = ProductCode.create(code: 'SB3 Std PDF', price: '345', myc_level_id: myc_level_sb3.id, status: ProductCode.statuses[:active])
  code_SB3StdPDF.product_packs << ProductPack.find(pack_Sunbeams3StandardPDFPack.id)

  pack_Sunbeams3FamilyPDFPack = ProductPack.create(description: 'Sunbeams 3 Family PDF Pack', status: ProductPack.statuses[:active])
  pack_Sunbeams3FamilyPDFPack.product_items << ProductItem.find(item_Sunbeams3StudentManual.id)
  pack_Sunbeams3FamilyPDFPack.product_items << ProductItem.find(item_MYCL2ActivitiesBook.id)
  pack_Sunbeams3FamilyPDFPack.product_items << ProductItem.find(item_BrightIdea1.id)
  pack_Sunbeams3FamilyPDFPack.product_items << ProductItem.find(item_RCMPrepBRepertoire.id)
  pack_Sunbeams3FamilyPDFPack.product_items << ProductItem.find(item_TechniqueToolboxJourneyman.id)
  pack_Sunbeams3FamilyPDFPack.product_items << ProductItem.find(item_SB3ProductDevelopmentFee.id)

  code_SB3FamPDF = ProductCode.create(code: 'SB3 Fam PDF', price: '335', myc_level_id: myc_level_sb3.id, remarks: 'For existing student whose elder sibling has purchased SB3 Std before.', status: ProductCode.statuses[:active])
  code_SB3FamPDF.product_packs << ProductPack.find(pack_Sunbeams3FamilyPDFPack.id)

  code_SB3PDF = ProductCode.create(code: 'SB3 PDF', price: '150', myc_level_id: myc_level_sb3.id, status: ProductCode.statuses[:active])
  code_SB3PDF.product_items << ProductItem.find(item_SB3ProductDevelopmentFee.id)

  # MB1
  #item_Moonbeams1AStudentManual = ProductItem.create(description: 'Moonbeams 1A Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Moonbeams1HomeworkCard = ProductItem.create(description: 'Moonbeams 1 Homework Card', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  # MYC L1 Activities Book
  item_Moonbeams1CDSetCD13 = ProductItem.create(description: 'Moonbeams 1 CD Set (i) Ensembles & Keyboard CD 1/3', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Moonbeams1CDSetCD23 = ProductItem.create(description: 'Moonbeams 1 CD Set (ii) Vocal & Keyboard CD 2/3', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_Moonbeams1CDSetCD33 = ProductItem.create(description: 'Moonbeams 1 CD Set  (iii) Vocal, Songs & Warmups CD 3/3', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  #item_TechniqueToolboxBasic
  #item_TechniqueToolboxJourneyman = ProductItem.create(description: 'Technique Toolbox Journeyman', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MB1ProductDevelopmentFee = ProductItem.create(description: 'MB1 Product Development Fee', cost: '150', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_Moonbeams1AStandardPDFPack = ProductPack.create(description: 'Moonbeams 1A Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_Moonbeams1AStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1AStudentManual.id)
  pack_Moonbeams1AStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1HomeworkCard.id)
  pack_Moonbeams1AStandardPDFPack.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Moonbeams1AStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1CDSetCD13.id)
  pack_Moonbeams1AStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1CDSetCD23.id)
  pack_Moonbeams1AStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1CDSetCD33.id)
  pack_Moonbeams1AStandardPDFPack.product_items << ProductItem.find(item_TechniqueToolboxJourneyman.id)
  pack_Moonbeams1AStandardPDFPack.product_items << ProductItem.find(item_MB1ProductDevelopmentFee.id)

  code_MB1AStdPDFReg = ProductCode.create(code: 'MB1A Std PDF & Reg', price: '290', myc_level_id: myc_level_mb1.id, status: ProductCode.statuses[:active])
  code_MB1AStdPDFReg.product_packs << ProductPack.find(pack_Moonbeams1AStandardPDFPack.id)
  code_MB1AStdPDFReg.product_packs << ProductPack.find(pack_registration.id)

  pack_Moonbeams1AFamilyPDFPack = ProductPack.create(description: 'Moonbeams 1A Family PDF Pack', status: ProductPack.statuses[:active])
  pack_Moonbeams1AFamilyPDFPack.product_items << ProductItem.find(item_Moonbeams1AStudentManual.id)
  pack_Moonbeams1AFamilyPDFPack.product_items << ProductItem.find(item_Moonbeams1HomeworkCard.id)
  pack_Moonbeams1AFamilyPDFPack.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Moonbeams1AFamilyPDFPack.product_items << ProductItem.find(item_TechniqueToolboxJourneyman.id)
  pack_Moonbeams1AFamilyPDFPack.product_items << ProductItem.find(item_MB1ProductDevelopmentFee.id)

  code_MB1AFamPDFReg = ProductCode.create(code: 'MB1A Fam PDF & Reg', price: '255', myc_level_id: myc_level_mb1.id, status: ProductCode.statuses[:active])
  code_MB1AFamPDFReg.product_packs << ProductPack.find(pack_Moonbeams1AFamilyPDFPack.id)
  code_MB1AFamPDFReg.product_packs << ProductPack.find(pack_registration.id)

  code_MB1APDF = ProductCode.create(code: 'MB1A PDF', price: '270', myc_level_id: myc_level_mb1.id, remarks: 'For current stutdent progresses from SB1 to MB1A without registration pack.', status: ProductCode.statuses[:active])
  code_MB1APDF.product_packs << ProductPack.find(pack_Moonbeams1AStandardPDFPack.id)

  code_MB1AFamPDF = ProductCode.create(code: 'MB1A Fam PDF', price: '230', myc_level_id: myc_level_mb1.id, remarks: 'For current student progresses from SB1 to MB1A without registration pack, and whose elder sibling has purchased MB1A Std before.', status: ProductCode.statuses[:active])
  code_MB1AFamPDF.product_packs << ProductPack.find(pack_Moonbeams1AFamilyPDFPack.id)

  item_Moonbeams1BStudentManual = ProductItem.create(description: 'Moonbeams 1B Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_Moonbeams1BStandardPDFPack = ProductPack.create(description: 'Moonbeams 1B Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_Moonbeams1BStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1BStudentManual.id)
  pack_Moonbeams1BStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1HomeworkCard.id)
  pack_Moonbeams1BStandardPDFPack.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Moonbeams1BStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1CDSetCD13.id)
  pack_Moonbeams1BStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1CDSetCD23.id)
  pack_Moonbeams1BStandardPDFPack.product_items << ProductItem.find(item_Moonbeams1CDSetCD33.id)
  pack_Moonbeams1BStandardPDFPack.product_items << ProductItem.find(item_TechniqueToolboxJourneyman.id)
  pack_Moonbeams1BStandardPDFPack.product_items << ProductItem.find(item_MB1ProductDevelopmentFee.id)

  code_MB1BPDF = ProductCode.create(code: 'MB1B PDF', price: '270', myc_level_id: myc_level_mb1.id, remarks: 'For current stutdent progresses from SB1 to MB1B without registration pack.', status: ProductCode.statuses[:active])
  code_MB1BPDF.product_packs << ProductPack.find(pack_Moonbeams1BStandardPDFPack.id)

  pack_Moonbeams1BFamilyPDFPack = ProductPack.create(description: 'Moonbeams 1B Family PDF Pack', status: ProductPack.statuses[:active])
  pack_Moonbeams1BFamilyPDFPack.product_items << ProductItem.find(item_Moonbeams1BStudentManual.id)
  pack_Moonbeams1BFamilyPDFPack.product_items << ProductItem.find(item_Moonbeams1HomeworkCard.id)
  pack_Moonbeams1BFamilyPDFPack.product_items << ProductItem.find(item_MYCL1ActivitiesBook.id)
  pack_Moonbeams1BFamilyPDFPack.product_items << ProductItem.find(item_TechniqueToolboxJourneyman.id)
  pack_Moonbeams1BFamilyPDFPack.product_items << ProductItem.find(item_MB1ProductDevelopmentFee.id)

  code_MB1BFamPDF = ProductCode.create(code: 'MB1B Fam PDF', price: '230', myc_level_id: myc_level_mb1.id, remarks: 'For current student progresses from SB1 to MB1B without registration pack, and whose elder sibling has purchased MB1A Std before.', status: ProductCode.statuses[:active])
  code_MB1BFamPDF.product_packs << ProductPack.find(pack_Moonbeams1BFamilyPDFPack.id)

  code_MB1B = ProductCode.create(code: 'MB1B', price: '50', myc_level_id: myc_level_mb1.id, remarks: 'MB1B for lesson 12-21', status: ProductCode.statuses[:active])
  code_MB1B.product_items << ProductItem.find(item_Moonbeams1BStudentManual.id)

  item_Moonbeams1CStudentManual = ProductItem.create(description: 'Moonbeams 1C Student Manual', cost: '50', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  code_MB1C = ProductCode.create(code: 'MB1C', price: '50', myc_level_id: myc_level_mb1.id, remarks: 'MB1B for lesson 22-36', status: ProductCode.statuses[:active])
  code_MB1C.product_items << ProductItem.find(item_Moonbeams1CStudentManual.id)

  code_MB1 = ProductCode.create(code: 'MB1', price: '150', myc_level_id: myc_level_mb1.id, status: ProductCode.statuses[:active])
  code_MB1.product_items << ProductItem.find(item_MB1ProductDevelopmentFee.id)

  # MB2
  item_Moonbeams2StudentManual = ProductItem.create(description: 'Moonbeams 2 Student Manual', cost: '105', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  # MYC L2 Activities Book
  item_MB2ProductDevelopmentFee = ProductItem.create(description: 'MB2 Product Development Fee', cost: '150', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_Moonbeams2StandardPDFPack = ProductPack.create(description: 'Moonbeams 2 Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_Moonbeams2StandardPDFPack.product_items << ProductItem.find(item_Moonbeams2StudentManual.id)
  pack_Moonbeams2StandardPDFPack.product_items << ProductItem.find(item_MYCL2ActivitiesBook.id)
  pack_Moonbeams2StandardPDFPack.product_items << ProductItem.find(item_BrightIdea1.id)
  pack_Moonbeams2StandardPDFPack.product_items << ProductItem.find(item_RCMPrepBRepertoire.id)
  pack_Moonbeams2StandardPDFPack.product_items << ProductItem.find(item_TechniqueToolboxJourneyman.id)
  pack_Moonbeams2StandardPDFPack.product_items << ProductItem.find(item_MB2ProductDevelopmentFee.id)

  code_MB1StdPDF = ProductCode.create(code: 'MB2 Std PDF', price: '345', myc_level_id: myc_level_mb2.id, status: ProductCode.statuses[:active])
  code_MB1StdPDF.product_packs << ProductPack.find(pack_Moonbeams2StandardPDFPack.id)

  code_MB2PDF = ProductCode.create(code: 'MB2 PDF', price: '150', myc_level_id: myc_level_mb2.id, status: ProductCode.statuses[:active])
  code_MB2PDF.product_items << ProductItem.find(item_MB2ProductDevelopmentFee.id)

  # MB3
  item_Breese = ProductItem.create(description: 'Breese', cost: '30', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_HootHanon = ProductItem.create(description: 'Hoot & Hanon', cost: '25', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MYCChecklist = ProductItem.create(description: 'MYC Checklist', cost: '16', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_BrightIdeas2 = ProductItem.create(description: 'Bright Ideas 2', cost: '60', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_IamAStar = ProductItem.create(description: 'I am A Star', cost: '20', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_ABRSMExamPcsGr2 = ProductItem.create(description: 'ABRSM Exam Pcs (Gr2)', cost: '10', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_ABRSMABRSMScalesArpeggiosGr2 = ProductItem.create(description: 'ABRSM Scales & Arpeggios (Gr 2)', cost: '6', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MB3ProductDevelopmentFee = ProductItem.create(description: 'MB3 Product Development Fee', cost: '150', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_Moonbeams3StandardPDFPack = ProductPack.create(description: 'Moonbeams 3 Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_Moonbeams3StandardPDFPack.product_items << ProductItem.find(item_Breese.id)
  pack_Moonbeams3StandardPDFPack.product_items << ProductItem.find(item_HootHanon.id)
  pack_Moonbeams3StandardPDFPack.product_items << ProductItem.find(item_MYCChecklist.id)
  pack_Moonbeams3StandardPDFPack.product_items << ProductItem.find(item_BrightIdeas2.id)
  pack_Moonbeams3StandardPDFPack.product_items << ProductItem.find(item_IamAStar.id)
  pack_Moonbeams3StandardPDFPack.product_items << ProductItem.find(item_ABRSMExamPcsGr2.id)
  pack_Moonbeams3StandardPDFPack.product_items << ProductItem.find(item_ABRSMABRSMScalesArpeggiosGr2.id)
  pack_Moonbeams3StandardPDFPack.product_items << ProductItem.find(item_MB3ProductDevelopmentFee.id)

  code_MB3StdPDF = ProductCode.create(code: 'MB3 Std PDF', price: '315', myc_level_id: myc_level_mb3.id, status: ProductCode.statuses[:active])
  code_MB3StdPDF.product_packs << ProductPack.find(pack_Moonbeams3StandardPDFPack.id)

  pack_Moonbeams3SpecialPDFPack1 = ProductPack.create(description: 'Moonbeams 3 Special Pack 1', status: ProductPack.statuses[:active])
  pack_Moonbeams3SpecialPDFPack1.product_items << ProductItem.find(item_Breese.id)
  pack_Moonbeams3SpecialPDFPack1.product_items << ProductItem.find(item_HootHanon.id)
  pack_Moonbeams3SpecialPDFPack1.product_items << ProductItem.find(item_MYCChecklist.id)
  pack_Moonbeams3SpecialPDFPack1.product_items << ProductItem.find(item_BrightIdeas2.id)
  pack_Moonbeams3SpecialPDFPack1.product_items << ProductItem.find(item_IamAStar.id)
  pack_Moonbeams3SpecialPDFPack1.product_items << ProductItem.find(item_MB3ProductDevelopmentFee.id)

  code_MB3SP1PDF = ProductCode.create(code: 'MB3 SP1 PDF', price: '300', myc_level_id: myc_level_mb3.id, status: ProductCode.statuses[:active])
  code_MB3SP1PDF.product_packs << ProductPack.find(pack_Moonbeams3SpecialPDFPack1.id)

  pack_Moonbeams3SpecialPDFPack2 = ProductPack.create(description: 'Moonbeams 3 Special Pack 2', status: ProductPack.statuses[:active])
  pack_Moonbeams3SpecialPDFPack2.product_items << ProductItem.find(item_Breese.id)
  pack_Moonbeams3SpecialPDFPack2.product_items << ProductItem.find(item_MYCChecklist.id)
  pack_Moonbeams3SpecialPDFPack2.product_items << ProductItem.find(item_BrightIdeas2.id)
  pack_Moonbeams3SpecialPDFPack2.product_items << ProductItem.find(item_MB3ProductDevelopmentFee.id)

  code_MB3SP2PDF = ProductCode.create(code: 'MB3 SP2 PDF', price: '255', myc_level_id: myc_level_mb3.id, remarks: 'For existing student whose elder sibling who has purchased MB3 Std before.', status: ProductCode.statuses[:active])
  code_MB3SP2PDF.product_packs << ProductPack.find(pack_Moonbeams3SpecialPDFPack2.id)

  code_HootHanon = ProductCode.create(code: 'Hoot & Hanon', price: '25', myc_level_id: myc_level_mb3.id, status: ProductCode.statuses[:active])
  code_HootHanon.product_items << ProductItem.find(item_HootHanon.id)

  code_IamAStar = ProductCode.create(code: 'I am a Star', price: '20', myc_level_id: myc_level_mb3.id, status: ProductCode.statuses[:active])
  code_IamAStar.product_items << ProductItem.find(item_IamAStar.id)

  code_ABRSMABRSMScalesArpeggiosGr2 = ProductCode.create(code: 'ABRSM Scales & Arpeggios (Gr 2)', price: '6', myc_level_id: myc_level_mb3.id, status: ProductCode.statuses[:active])
  code_ABRSMABRSMScalesArpeggiosGr2.product_items << ProductItem.find(item_ABRSMABRSMScalesArpeggiosGr2.id)

  code_MB3ProductDevelopmentFee = ProductCode.create(code: 'ABRSM Exam Pcs (Gr2)', price: '10', myc_level_id: myc_level_mb3.id, status: ProductCode.statuses[:active])
  code_MB3ProductDevelopmentFee.product_items << ProductItem.find(item_ABRSMExamPcsGr2.id)

  # MMBC
  item_MusicMyBestChoiceStudentManual = ProductItem.create(description: 'Music My Best Choice Student Manual', cost: '65', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  item_MMBCProductDevelopmentFee = ProductItem.create(description: 'MMBC Product Development Fee', cost: '45', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])

  pack_MusicMyBestChoiceStandardPDFPack = ProductPack.create(description: 'Music My Best Choice Standard PDF Pack', status: ProductPack.statuses[:active])
  pack_MusicMyBestChoiceStandardPDFPack.product_items << ProductItem.find(item_MusicMyBestChoiceStudentManual.id)
  pack_MusicMyBestChoiceStandardPDFPack.product_items << ProductItem.find(item_sr_bag.id)
  pack_MusicMyBestChoiceStandardPDFPack.product_items << ProductItem.find(item_MMBCProductDevelopmentFee.id)

  code_MMBCPDFStd = ProductCode.create(code: 'MMBC PDF Std', price: '110', myc_level_id: myc_level_mmbc.id, status: ProductCode.statuses[:active])
  code_MMBCPDFStd.product_packs << ProductPack.find(pack_MusicMyBestChoiceStandardPDFPack.id)

  #others
  item_MyYoungClassic = ProductItem.create(description: 'My Young Classic', cost: '10', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  code_MyYoungClassic = ProductCode.create(code: 'My Young Classic', price: '10', myc_level_id: myc_level_others.id, status: ProductCode.statuses[:active])
  code_MyYoungClassic.product_items << ProductItem.find(item_MyYoungClassic.id)

  code_SRBag = ProductCode.create(code: 'SR Bag', price: '3', myc_level_id: myc_level_others.id, status: ProductCode.statuses[:active])
  code_SRBag.product_items << ProductItem.find(item_sr_bag.id)

  item_ReceiptBook = ProductItem.create(description: 'Receipt Book', cost: '3', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  code_ReceiptBook = ProductCode.create(code: 'Receipt Book', price: '3', myc_level_id: myc_level_others.id, status: ProductCode.statuses[:active])
  code_ReceiptBook.product_items << ProductItem.find(item_ReceiptBook.id)

  item_CritterSticker1pcofA5size = ProductItem.create(description: 'Critters Sticker (1 pcs of A5 size)', cost: '1.5', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  code_CritterSticker1pcofA5size = ProductCode.create(code: 'Critter Sticker (1 pc of A5 size)', price: '1.5', myc_level_id: myc_level_others.id, status: ProductCode.statuses[:active])
  code_CritterSticker1pcofA5size.product_items << ProductItem.find(item_CritterSticker1pcofA5size.id)

  item_MYCRedBag = ProductItem.create(description: 'MYC Red Bag', cost: '12', quantity_on_hand: 0, quantity_trigger_alert: 0, status: ProductItem.statuses[:active])
  code_MYCRedBag = ProductCode.create(code: 'MYC Red Bag', price: '12', myc_level_id: myc_level_others.id, status: ProductCode.statuses[:active])
  code_MYCRedBag.product_items << ProductItem.find(item_MYCRedBag.id)
end



